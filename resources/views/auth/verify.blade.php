@extends('layouts.internal-layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Pengesahan alamat e-mel anda') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Pautan pengesahan telah dihantar ke alamat e-mel anda.') }}
                        </div>
                    @endif

                    {{ __('Sebelum meneruskan, sila semak e-mel anda untuk mendapatkan pautan pengesahan.') }}
                    {{ __('Jika anda tidak menerima e-mel') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('klik disini untuk pengesahan semula') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
