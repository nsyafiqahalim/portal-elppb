@extends('layouts.website-layout')

@section('content')
<div class="view full-page-intro" style="background-image: url('img/banner/paddy-field.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container" style="margin-top:2rem; margin-bottom:2rem;">
            <div class="row wow fadeIn">
                <div class="col-md-6 col-xl-5 mb-4" style="margin:auto;">
                    <div class="card">
                        <div class="card-header" style="font-weight:500;background-color:#28a745;color:white;">{{ __('Log Masuk') }}</div>
                        <div class="card-body">
                            <form class="register-form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="login_id" class="text-uppercase" style="font-weight:bold;">ID Pengguna
                                      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                          <i class="fas fa-info-circle"></i>
                                      </button>
                                    </label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-user"></i>
                                            </span>
                                        </div>
                                        <input id="login_id" type="login_id" class="form-control @error('login_id') is-invalid @enderror" name="login_id" placeholder=''
                                        value="{{ old('login_id') }}" required
                                        autocomplete="login_id" maxlength="12" autofocus>
                                        </input>
                                        @error('login_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="text-uppercase" style="font-weight:bold;">Kata Laluan <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal1">
                                        <i class="fas fa-info-circle"></i>
                                    </button></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-lock"></i>
                                            </span>
                                        </div>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember">
                                            {{ __('Ingat saya?') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12" width="auto">
                                        <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                                            {{ __('Log Masuk') }}
                                        </button>
                                    </div>

                                    <div class="col-md-8 col-offset-4">
                                      <a href="{{ route('forgot.userid') }}" class="btn btn-link"><small id="idForgot" class="form-text text-muted">Lupa ID Pengguna</small></a>

                                      <a href="{{ route('password.request') }}" class="btn btn-link"><small id="passwordForgot" class="form-text text-muted">Lupa Kata Laluan</small></a>
                                    </div>

                                </div>
                                <div class="form-group row mb-0">
                                  @if (Route::has('register'))
                                  <a class="btn btn-link" href="{{ route('register') }}">
                                      {{ __('Daftar Pengguna') }}
                                  </a>
                                  @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Container -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="col-12 modal-title text-center" id="exampleModalLabel">Pengumuman!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ID Pengguna adalah <strong>nombor Kad Pengenalan</strong>. Sila masukkan nombor Kad Pengenalan anda tanpa <strong>(-)</strong>. <br><i>Contoh: 560710025567.</i>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div><!-- End Modal 1-->
        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModal1Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="col-12 modal-title text-center" id="exampleModal1Label">Pengumuman!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Bagi pengguna awam, kata laluan mestilah mempunyai sekurang-kurangnya <strong>8 aksara</strong>. Manakala, bagi pegawai KPB, kata laluan mestilah <strong>12 aksara</strong>. Format kata laluan terdiri daripada <b>1 simbol, 1 huruf besar, 1 digit</b>.<br>
                        <i>Contoh: kataLaluan!2</i>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endpush
