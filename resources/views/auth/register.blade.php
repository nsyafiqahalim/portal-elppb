@extends('layouts.register-layout')

@section('content')
<div class="view full-page-intro" style="background-image: url('img/banner/paddy-field.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container" style="margin-top:2rem; margin-bottom:2rem;">
            <div class="row wow fadeIn">
                <div class="col-md-6 col-xl-5 mb-4" style="margin:auto;">
                    <div class="card">
                        <div class="card-header" style="font-weight:500;background-color:#28a745;color:white;">{{ __('Daftar Pengguna') }}</div>
                        <div class="card-body">
                            <form class="register-form" method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="name" class="text-uppercase" style="font-weight:bold;">Nama Penuh</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-user"></i>
                                            </span>
                                        </div>
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="kad_pengenalan" class="text-uppercase" style="font-weight:bold;">Nombor Kad Pengenalan
                                        <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                    </label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-credit-card"></i>
                                            </span>
                                        </div>
                                        <input id="kad_pengenalan" type="text" class="form-control @error('kad_pengenalan') is-invalid @enderror" name="kad_pengenalan" value="{{ old('kad_pengenalan') }}"
                                        required autocomplete="kad_pengenalan" autofocus maxlength="12">
                                        @error('kad_pengenalan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="text-uppercase" style="font-weight:bold;">E-mel</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-email"></i>
                                            </span>
                                        </div>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="text-uppercase" style="font-weight:bold;">Kata Laluan <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal1">
                                            <i class="fas fa-info-circle"></i>
                                        </button></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-lock"></i>
                                            </span>
                                        </div>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="text-uppercase" style="font-weight:bold;">Pengesahan Kata Laluan</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-lock"></i>
                                            </span>
                                        </div>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-3 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Daftar Masuk') }}
                                        </button>
                                    </div>
                                    <div class="col-md-3 offset-md-4">
                                        <a class="btn btn-link" href="{{ route('login') }}">
                                            {{ __('kembali') }}
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Container -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="col-12 modal-title text-center" id="exampleModalLabel">Pengumuman!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Sila masukkan <strong>nombor Kad Pengenalan</strong> anda tanpa <strong>(-)</strong>.<br><i>Contoh: 560710025567.</i>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div><!-- End Modal 1-->
        <!-- Modal 2 -->
        <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModal1Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="col-12 modal-title text-center" id="exampleModal1Label">Pengumuman!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Bagi pengguna awam, kata laluan mestilah mempunyai sekurang-kurangnya <strong>8 aksara</strong>. Manakala, bagi pegawai KPB, kata laluan mestilah <strong>12 aksara</strong>. Format kata laluan terdiri daripada <b>1 simbol, 1
                            huruf besar, 1 digit</b>.<br>
                        <i>Contoh: kataLaluan!2</i>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endpush
