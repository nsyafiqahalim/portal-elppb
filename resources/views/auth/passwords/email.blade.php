@extends('layouts.website-layout')

@section('content')
<div class="view full-page-intro" style="background-image:url('/img/banner/paddy-field.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container" style="margin-top:2rem; margin-bottom:2rem;">
            <div class="row wow fadeIn">
                <div class="col-md-6 col-xl-5 mb-4" style="margin:auto;">
                    <div class="card">
                        <div class="card-header" style="font-weight:500;background-color:#28a745;color:white;">{{ __('Tukar Kata laluan') }}</div>
                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="kad_pengenalan" class="text-uppercase" style="font-weight:bold;">Nombor Kad Pengenalan <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="right"
                                          title="Contoh: 560710025567. Sila masukkan tanpa (-)"></i></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-credit-card"></i>
                                            </span>
                                        </div>
                                        <input id="kad_pengenalan" type="kad_pengenalan" class="form-control @error('kad_pengenalan') is-invalid @enderror" name="kad_pengenalan" value="{{ old('kad_pengenalan') }}" required
                                        autocomplete="kad_pengenalan" autofocus maxlength="12">
                                        @error('kad_pengenalan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> <!-- End Kad Pengenalan -->
                                <div class="form-group">
                                    <label for="email" class="text-uppercase" style="font-weight:bold;">Alamat E-mel</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-email"></i>
                                            </span>
                                        </div>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Hantar') }}
                                        </button>
                                    </div>
                                    <div class="col-md-6 offset-md-4">
                                        <a class="btn btn-link" href="{{ route('login') }}">
                                            {{ __('kembali') }}
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('/path/js/date_time.js') }}"></script>
@endpush
