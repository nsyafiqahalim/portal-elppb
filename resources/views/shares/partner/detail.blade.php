<!---
<div class="row">
    <div class="col-md-10 ">
    <h3>Butiran Rakan Kongsi</h3>
    </div>
        <div class="col-md-2">
        <button class="btn btn-success" style="margin-right:4.2%;"><i class="fa fa-plus"></i> Rakan Kongsi</button>
    </div>
</div>
<div class="form-body">
    <hr>
    <div class="row">
        <div class="col-md-12 ">
            <table class="table table-bordered table-striped no-wrap">
                <thead>
                    <tr style='text-align:center'>
                        <th>Bil.</th>
                        <th>Nama</th>
                        <th>No<br/> K/P</th>
                        <th>No<br/> Telefon</th>
                        <th>Bangsa</th>
                        <th>Status<br/> Pemilikan</th>
                        <th>Jawatan</th>
                        <th>Jumlah<br/> Syer(%)</th>
                        <th width="200">Tindakan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style='text-align:center'>
                        <td>1</td>
                        <td>
                            <input type="text" class="form-control" name="name" value="Ali"/>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="name" value="9010101-23-2330"/>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="name" value="+6310221"/>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="name" value="Melayu"/>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="name" value="Pemilik"/>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="name" value="Pengarah"/>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="name" value="100"/>
                        </td>
                        <td>
                            <button class="btn btn-md btn-danger"><i class="fa fa-trash"></i> Padam </button>
                        </td>  
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br/>
</div>
---->

<!---
<div class="form-body">
    <h3 class="card-title">Maklumat Syarikat</h3>
    <hr>
    <div class="row pt-3">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Nama Syarikat</label>
                <input type="text" id="firstName" class="form-control" placeholder="" >
                <small class="form-control-feedback"> </small> </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">No Pendaftaran/ MYCOID</label>
                <input type="text" class="form-control" placeholder="" >
                <small class="form-control-feedback"></small> </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Jenis Syarikat</label>
                <input type="text" class="form-control" placeholder=" " >
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Tarikh Mansuh (Cth: 23/12/2016)</label>
                <input type="text" class="form-control" placeholder="" >
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Modal Berbayar (RM)</label>
                <input type="text" class="form-control" placeholder="" >
            </div>
        </div>
    </div>


    <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
    <hr>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label>Alamat 1</label>
                <input type="text" value="" class="form-control" >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label>Alamat 2</label>
                <input type="text" value="" class="form-control" >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label>Bandar</label>
                <input type="text" value="" class="form-control" >
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Poskod</label>
                <input type="text" VALUE="" class="form-control" >
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Negeri</label>
                <input type="text" value="" class="form-control" >
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Nombor Telefon</label>
                <input type="text" value="" class="form-control" >
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Emel</label>
                <input type="text" VALUE="" class="form-control" >
            </div>
        </div>
    </div>
</div>
---->

 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <button alt="default" data-toggle="modal" data-target=".rakan-kongsi" class="btn btn-success float-right" >
                <i class="fa fa-plus"></i> Tambah Rakan Kongsi
            </button>
            <br/><br/>
        </div>
    </div>
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6">Maklumat Rakan Kongsi Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <thead>
                    <tr style='text-align:center'>
                        <td>Bil.</td>
                        <td style='text-align:center'>Nama</td>
                        <td style='text-align:center'>No<br/> K/P</td>
                        <td style='text-align:center'>No<br/> Telefon</td>
                        <td style='text-align:center'>Bangsa</td>
                        <td style='text-align:center'>Status<br/> Pemilikan</td>
                        <td style='text-align:center'>Jawatan</td>
                        <td style='text-align:center'>Jumlah<br/> Syer(%)</td>
                        <td style='text-align:center'>Tindakan</td>
                    </tr>
                </thead>
                <tr>
                    <td style='text-align:left'>1.</td>
                    <td style='text-align:center'>ALI</td>
                    <td style='text-align:center'>901010-10-4040</td>
                    <td style='text-align:center'>+60124040122</td>
                    <td style='text-align:center'>MELAYU</td>
                    <td style='text-align:center'>BUMIPUTERA</td>
                    <td style='text-align:center'>PENGARAH</td>
                    <td style='text-align:center'>100</td>
                    <td style='text-align:center'>
                        <button class="btn btn-danger">
                            Hapus
                    </button>
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
<div class="modal fade-scale rakan-kongsi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Tambah Rakan Kongsi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h3 class="card-title">Maklumat Rakan Kongsi</h3>
                <hr>
                <div class="row pt-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Nama </label>
                            <input type="text" class="form-control" name="name" value="Ali"/>
                            <small class="form-control-feedback"> </small> </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Kad Pengenalan</label>
                            <input type="text" class="form-control" name="name" value="Ali"/>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Nombor Telefon</label>
                            <input type="text" class="form-control" placeholder=" " >
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Emel</label>
                            <input type="text" VALUE="test@test.com" class="form-control"  >
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Bangsa</label>
                            <select class="form-control">
                                <option>Melayu</option>
                                <option>Cina</option>
                                <option>India</option>
                                <option>Bumiputera Sabah</option>
                                <option>Bumiputera Sarawak</option>
                                <option>Lain - lain</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Status Pemilikan</label>
                            <input type="text" class="form-control" value="Bumiputera" readonly >
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Jawatan</label>
                            <input type="text" class="form-control" placeholder="" >
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Jumlah Syer (RM)</label>
                            <input type="text" class="form-control" placeholder="" >
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Peratus Syer(%)</label>
                            <input type="text" class="form-control" placeholder="" >
                        </div>
                    </div>
                </div>
                <h3 class="box-title mt-5">Alamat</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Alamat 1</label>
                            <input type="text" value="NO 10" class="form-control"  >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Alamat 2</label>
                            <input type="text" value="KAMPONG" class="form-control"  >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Bandar</label>
                            <input type="text" value="BATU CAVES" class="form-control"  >
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Poskod</label>
                            <input type="text" VALUE="52100" class="form-control"  >
                        </div>
                    </div>

                    <!--/span-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Negeri</label>
                            <input type="text" value="KEDAH" class="form-control"  >
                        </div>
                    </div>
                    <!--/span-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success waves-effect text-left" data-dismiss="modal">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@push('js')
    <script src="{{ asset('assets/plugins/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/gmaps/jquery.gmaps.js') }}"></script>
@endpush