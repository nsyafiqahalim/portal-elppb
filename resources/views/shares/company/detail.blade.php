<div class="form-body">
    <h3 class="card-title">Maklumat Syarikat</h3>
    <hr>
    <div class="row pt-3">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Nama Syarikat</label>
                <input type="text" id="firstName" class="form-control" disabled placeholder="Ali Baba" >
                <small class="form-control-feedback"> </small> </div>
        </div>
    </div>
    <!--/row-->
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">No Pendaftaran/ MYCOID</label>
                <input type="text" class="form-control col-md-8" disabled placeholder="1232422" >
                -
                <input type="text" class="form-control col-md-3" disabled placeholder="D" >
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Jenis Syarikat</label>
                <input type="text" class="form-control" disabled placeholder=" PERSENDIRIAN" >
            </div>
        </div>

        <!--/span-->
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Tarikh Mansuh (Cth: 23/12/2016)</label>
                <input type="text" class="form-control" disabled placeholder="10/10/2019" >
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Modal Berbayar (RM)</label>
                <input type="text" class="form-control" disabled placeholder="2,100.00" >
            </div>
        </div>
        <!--/span-->
    </div>


    <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
    <hr>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label>Alamat 1</label>
                <input type="text" value="NO 10" class="form-control" disabled >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label>Alamat 2</label>
                <input type="text" value="KAMPONG" class="form-control" disabled >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label>Bandar</label>
                <input type="text" value="BATU CAVES" class="form-control" disabled >
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Poskod</label>
                <input type="text" VALUE="52100" class="form-control" disabled >
            </div>
        </div>

        <!--/span-->
        <div class="col-md-4">
            <div class="form-group">
                <label>Negeri</label>
                <input type="text" value="KEDAH" class="form-control" disabled >
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Nombor Telefon</label>
                <input type="text" value="+601-232323" class="form-control" disabled >
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Emel</label>
                <input type="text" VALUE="test@test.com" class="form-control" disabled >
            </div>
        </div>
    </div>
</div>