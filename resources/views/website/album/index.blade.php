@extends('layouts.portal-layout')

@section('content')
  
@if (count($albums) > 0)
<div class="row">
    @foreach ($albums as $album)
    <div class="col-md-4">
        <div class="card mb-4 shadow-sm">
            <img src="{{ asset('file/'.$album->cover_image) }}" alt="{{ $album->cover_image }}" height="200px">
            <div class="card-body">
                <p class="card-text">{{ $album->name }}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <a href="{{ route('album-show', $album->id) }}" class="btn btn-sm btn-primary" style="margin-right:15px;">Papar</a>
                        <a href="{{ route('album-destroy', $album->id) }}" class="btn btn-sm btn-outline-danger" onclick="return myFunction();"><i class="fa fa-trash"></i></a>
                    </div>
                    <small class="text-muted">{{ $album->description }}</small>
                </div>
            </div>
        </div>
    </div> <!-- End col-md-4 -->
    @endforeach
</div> <!-- End Row -->
@else
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:center;">Tiada album</h3>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@push('js')
<script>
    function myFunction() {
        if (!confirm("Anda Pasti? Album ini akan dihapuskan secara kekal, ia tidak lagi boleh dilihat dalam mana-mana folder anda dan anda tidak akan dapat memulihkannya."))
            event.preventDefault();
    }
</script>
@endpush
