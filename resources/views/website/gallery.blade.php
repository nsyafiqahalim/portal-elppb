@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Galeri</li>
            </ol>
        </nav>
    </div>
</section>

<!-- Gallery -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-4">
                <button class="btn btn-default filter-button" data-filter="all">Semua</button>
                <button class="btn btn-default filter-button" data-filter="hdpe">Padi</button>
                <button class="btn btn-default filter-button" data-filter="sprinkle">Kawalselia Padi Beras</button>
                <button class="btn btn-default filter-button" data-filter="spray">Aktiviti</button>
            </div>
        </div>
        <div class="row">
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                <a href="img/gallery/paddy/1.jpg" data-fancybox="images" data-caption="Gambar 1">
                    <img src="img/gallery/paddy/1.jpg" class="img-responsive"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                <a href="img/gallery/2/KT_1.jpg" data-fancybox="images" data-caption="Gambar 2">
                    <img src="img/gallery/2/KT_1.jpg" class="img-responsive"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                <a href="img/gallery/2/KT_5.jpg" data-fancybox="images" data-caption="Gambar 3">
                    <img src="img/gallery/2/KT_5.jpg" class="img-responsive" style="height:260"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                <a href="img/banner/paddy-field.jpg" data-fancybox="images" data-caption="Gambar 4">
                    <img src="img/banner/paddy-field.jpg" class="img-responsive" style="height:260;"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                <a href="img/gallery/1/KN_2.jpg" data-fancybox="images" data-caption="Gambar 5">
                    <img src="img/gallery/1/KN_2.jpg" class="img-responsive"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                <a href="img/banner/insect_paddy.jpg" data-fancybox="images" data-caption="Gambar 6">
                    <img src="img/banner/insect_paddy.jpg" class="img-responsive"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                <a href="img/gallery/2/KT_2.jpg" data-fancybox="images" data-caption="Gambar 7">
                    <img src="img/gallery/2/KT_2.jpg" class="img-responsive"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                <a href="img/banner/padi-beras-cover.png" data-fancybox="images" data-caption="Gambar 8">
                    <img src="img/banner/padi-beras-cover.png" class="img-responsive" style="height:260"/>
              </div>
              <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                <a href="img/about/about.png" data-fancybox="images" data-caption="Gambar 9">
                    <img src="img/about/about.png" class="img-responsive" style="height:260;width:400"/>
              </div>
        </div> <!-- End Row -->
    </div>
</section>
@endsection

@push('css')
<style>
    .gallery-title {
        font-size: 36px;
        color: #42B32F;
        text-align: center;
        font-weight: 500;
        margin-bottom: 70px;
    }

    .gallery-title:after {
        content: "";
        position: absolute;
        width: 7.5%;
        left: 46.5%;
        height: 45px;
        border-bottom: 1px solid #5e5e5e;
    }

    .filter-button {
        font-size: 18px;
        border: 1px solid #42B32F;
        border-radius: 5px;
        text-align: center;
        color: #42B32F;
        margin-bottom: 30px;
    }

    .filter-button:hover {
        font-size: 18px;
        border: 1px solid #42B32F;
        border-radius: 5px;
        text-align: center;
        color: #ffffff;
        background-color: #42B32F;
    }

    .btn-default:active .filter-button:active {
        background-color: #42B32F;
        color: white;
    }

    .port-image {
        width: 100%;
    }

    .gallery_product {
        margin-bottom: 30px;
    }
</style>
@endpush

@push('js')
<script>
    $(document).ready(function() {

        $(".filter-button").click(function() {
            var value = $(this).attr('data-filter');

            if (value == "all") {
                //$('.filter').removeClass('hidden');
                $('.filter').show('1000');
            } else {
                //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
                //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                $(".filter").not('.' + value).hide('3000');
                $('.filter').filter('.' + value).show('3000');

            }
        });

        if ($(".filter-button").removeClass("active")) {
            $(this).removeClass("active");
        }
        $(this).addClass("active");

    });
</script>

<script>
    $('[data-fancybox="images"]').fancybox({
        buttons: [
            'slideShow',
            'share',
            'zoom',
            'fullScreen',
            'close'
        ],
        thumbs: {
            autoStart: true
        }
    });
</script>
@endpush
