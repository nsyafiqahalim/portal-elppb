@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Permit</li>
            </ol>
        </nav>
    </div>
</section>

<section class="container" style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Permit</h2>
                <hr>
            </div>
        </div>
    </div>
    <div class="row" style="font-family:Poppins, sans-serif; font-size:15pt;">
        <div class="col-lg-12 col-md-4 accordion_area container">
            <div class="faq_ask">
                <div id="accordion">
                    @foreach($permitDAOs as $index => $permitDAO)
                    <div class="card">
                        <div class="card-header" id="heading{{ $index }}">
                            <h5 class="mb-0">
                                <button aria-controls="collapse{{ $index }}" aria-expanded="false" class="btn btn-link collapsed" data-target="#collapse{{ $index }}" data-toggle="collapse">
                                    {{ $permitDAO->title }}
                                </button>
                            </h5>
                        </div>
                        <div aria-labelledby="heading{{ $index }}" class="collapse" data-parent="#accordion" id="collapse{{ $index }}" style="">
                            <div class="card-body">
                                <article class="col-12 item-page">
                                    {!! $permitDAO->description !!}
                                </article>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
