@extends('layouts.website-layout')

@section('content')
<!-- Slide Imej Utama -->
<section class="slider_area space-md bg-primary blend bg-image py-5" style='background: url("img/banner/cover-2.png") center center / cover no-repeat;'>
    <div class="container">
        <div class="row d-flex align-items-center">
            <!--Carousel Wrapper-->
            <div id="carouselAnnouncement" class="carousel slide carousel-fade col-xl-12" data-ride="carousel" data-interval="10000">
              <!--Indicators-->
              <ol class="carousel-indicators">
                <li data-target="#carouselAnnouncement" data-slide-to="0" class="active"></li>
                <li data-target="#carouselAnnouncement" data-slide-to="1"></li>
                <li data-target="#carouselAnnouncement" data-slide-to="2"></li>
              </ol>
              <!--/.Indicators-->
              <!--Slides-->
              <div class="carousel-inner" role="listbox">
                @foreach($galleryDAOs as $index => $galleryDAO)
                <div class="carousel-item @if($index == 0) active @endif">
                  <div class="view">
                    <a href="{{ asset('file/'.$galleryDAO->file_path) }}" class="img-pop-up">
                      <div class="single-gallery-image" style="background: url({{ asset('file/'.$galleryDAO->file_path) }});"></div>
                    </a>
                    <div class="mask rgba-black-light"></div>
                  </div>
                </div>
                @endforeach
              </div>
              <!--/.Slides-->
              <!--Controls-->
              <a class="carousel-control-prev" href="#carouselAnnouncement" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselAnnouncement" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->
        </div>
        <!-- /row-->
    </div>
</section>

<!-- Ikon Panduan Permohonan -->
<section class="py-2" style="margin-top:25pt;">
    <div class="container">
        <div class="row text-center py-1">
            <div class="col-md-6 col-lg-3">
                <div class="single_service">
                    <div class="thumb my-1">
                        <a href="{{ route('website.condition-application') }}"><img src="img/icon/permohonan.png" alt="syarat-permohonan" style="max-height: 90px;"></a>
                    </div>
                    <div class="service_info">
                        <h5 class="text-uppercase font-weight-bold"><a href="{{ route('website.condition-application') }}">Syarat Permohonan</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="single_service">
                    <div class="thumb my-1">
                        <a href="{{ route('website.license-responsibility') }}"><img src="img/icon/tanggungjawab-pelesen.png" alt="tanggungjawab-pelesen" style="max-height: 90px;"></a>
                    </div>
                    <div class="service_info">
                        <h5 class="text-uppercase font-weight-bold"><a href="{{ route('website.license-responsibility') }}">Tanggungjawab Pelesen</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="single_service">
                    <div class="thumb my-1">
                        <a href="{{ route('website.license-fee-rate') }}"><img src="img/icon/license-fee.png" alt="kadar-fee-lesen" style="max-height: 90px;"></a>
                    </div>
                    <div class="service_info">
                        <h5 class="text-uppercase font-weight-bold"><a href="{{ route('website.license-fee-rate') }}">Kadar Fee Lesen</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="single_service">
                    <div class="thumb my-1">
                        <a href="{{ route('website.mobile-app') }}"><img src="img/icon/mobile-app.png" alt="aplikasi-mudah-alih" style="max-height: 90px;"></a>
                    </div>
                    <div class="service_info">
                        <h5 class="text-uppercase font-weight-bold"><a href="{{ route('website.mobile-app') }}">Aplikasi Mudah Alih</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Gallery -->
<section style="margin-top:5rem;" class="pb-0">
    <div class="container">
        <h3>Galeri <i class="fas fa-images"></i></h3>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <a href="img/gallery/1/KN_2.jpg" data-fancybox="images" data-caption="Gambar 1">
                    <img src="img/gallery/1/KN_2.jpg" />
                </a>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="img/gallery/2/KT_7.jpg" data-fancybox="images" data-caption="Membuat pemeriksaan di kilang">
                    <img src="img/gallery/2/KT_7.jpg" />
                </a>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="img/banner/cover-1.png" data-fancybox="images" data-caption="Jelapang padi">
                    <img src="img/banner/cover-1.png" style="height:260" />
                </a>
            </div>
        </div><!-- End Row -->

        <div class="row" style="margin-top:25pt;">
            <div class="col-lg-4 col-md-4">
                <a href="img/gallery/5/PB_2.jpg" data-fancybox="images" data-caption="Pemeriksaan di kedai-kedai runcit">
                    <img src="img/gallery/5/PB_2.jpg" style="height:260" />
                </a>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="img/gallery/3/PK_3.jpg" data-fancybox="images" data-caption="Pemeriksaan beras import dan export di supermarket">
                    <img src="img/gallery/3/PK_3.jpg" style="height:260" />
                </a>
            </div>
            <div class="col-lg-4 col-md-4">
                <a href="img/gallery/2/KT_1.jpg" data-fancybox="images" data-caption="Perbincangan pegawai-pegawai KPB">
                    <img src="img/gallery/2/KT_1.jpg" />
                </a>
            </div>
        </div>

        <a class="boxed-btn3 btn my-4" href="{{ route('website.gallery') }}" role="button">Lebih banyak gambar <i class="ml-2 fas fa-arrow-right"></i></a>
    </div>
</section>
@endsection

@push('js')
<script>
    $('[data-fancybox="images"]').fancybox({
        buttons: [
            'slideShow',
            'share',
            'zoom',
            'fullScreen',
            'close'
        ],
        thumbs: {
            autoStart: true
        }
    });
</script>
@endpush

@push('css')
<style>
    .carousel-item p {
        color: white;
    }
</style>
@endpush
