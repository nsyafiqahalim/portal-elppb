@extends('layouts.website-layout')

@section('content')

<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb" style="margin-top:-30px;">
            <ol class="breadcrumb bg-transparent px-0 pt-5">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tanggungjawab Pelesen</li>
            </ol>
        </nav>
    </div>
</section>

<section class="container" style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Tanggungjawab Pelesen</h2>
                <hr>
            </div>
        </div>
    </div>
</section>
<div class="service_details_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!--Accordion wrapper-->
                <div class="accordion md-accordion accordion-4" id="accordionEx2" role="tablist" aria-multiselectable="true">
                    <!-- Accordion card -->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header z-depth-1 teal lighten-4" role="tab" id="heading10">
                            <a data-toggle="collapse" data-parent="#accordionEx2" href="#collapse10" aria-expanded="true" aria-controls="collapse10">
                                <h4 class="mb-0 black-text text-center font-weight-bold text-uppercase">
                                    Lesen Runcit
                                </h4>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse10" class="collapse show" role="tabpanel" aria-labelledby="heading10" data-parent="#accordionEx2">
                            <div class="card-body rgba-teal-strong white-text">
                                <p style="font-size:12pt;">Berikut ini adalah syarat-syarat yang dikenakan ke atas pemegang-pemegang Lesen Runcit Beras. Sila baca dengan teliti dan fahamkan ke atas setiap kenyataan yang disenaraikan. Mana-mana
                                    pelesen yang melanggar atau
                                    gagal mematuhi syarat-syarat ini adalah melakukan kesalahan.</p>

                                <p>
                                    <dl style="font-size:12pt;">
                                        <dd style="padding-bottom:5pt;">1.&ensp; Lesen hendaklah lengkap ditandatangani oleh pelesen atau penama di dalam lesen tersebut beserta cop rasmi syarikat.</dd>
                                        <dd style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan di dalam lesen.</dd>
                                        <dd style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Pelesen dilarang <strong>MEMINDA, MEMADAM</strong> dan <strong>MENAMBAH</strong> sebarang angka atau perkataan di dalam lesen.</dd>
                                        <dd style="padding-bottom:5pt;">4.&ensp; Lesen ini tidak boleh dipindahmilik atau ditukar nama atau diserah kepada orang lain. Jika berlaku sesuatu perkara yang</dd>
                                            <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;memerlukan perubahan maklumat di dalam lesen,
                                                pelesen perlu membuat permohonan perubahan secepat mungkin.</dd>
                                        <dd style="padding-bottom:5pt;">5.&ensp; Pelesen tidak boleh mempunyai dalam stok berasnya melebihi daripada had muatan yang ditetapkan dalam lesennya pada</dd>
                                        <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;satu-satu masa.</dd>
                                        <dd style="padding-bottom:5pt;">6.&ensp; Pelesen tidak boleh menyetor atau menyimpan beras di tempat selain daripada yang dinyatakan di dalan lesen</dd>
                                        <dd style="padding-bottom:5pt;">7.&ensp; Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan beras.</dd>
                                        <dd style="padding-bottom:5pt;">8.&ensp; Pelesen tidak boleh memperolehi atau membeli beras daripada mana-mana sumber selain daripada pemegang Lesen Borong</dd>
                                        <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;Beras.</dd>
                                        <dd style="padding-bottom:5pt;">9.&ensp; Pelesen tidak boleh menjual beras melebihi 100 kg kepada mana - mana orang dalam suatu masa.</dd>
                                        <dd style="padding-bottom:5pt;">10.&ensp; Pelesen hendaklah meletakkan label atau tanda dalam Bahasa Melayu yang menunjukkan gred dan harga beras pada kampit</dd>
                                            <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;atau bekas beras untuk jualan yang dipamerkan di
                                                bahagian jualan runcit yang boleh dimasuki oleh orang awam.</dd>
                                        <dd style="padding-bottom:5pt;">11.&ensp; Pelesen hendaklah menyenggara (menyediakan) suatu akaun harian bagi:-</dd>
                                        <dd><span style="padding-right: 1.8em"></span> a)&nbsp;Beras yang diperolehi atau dibeli dan disokong dengan invois – invois.</dd>
                                        <dd><span style="padding-right: 1.8em"></span>b)&nbsp;Jumlah jualan</dd>
                                        <dd><span style="padding-right: 1.8em"></span>c)&nbsp;Baki stok dalam tangan</dd>
                                        <dd style="padding-bottom:5pt;">12.&ensp; Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen berkenaan.</dd>
                                        <dd style="padding-bottom:5pt;">13.&ensp; Lesen yang sudah tamat tempoh akan dibatalkan secara automatik sekiranya tiada permohonan pembaharuan lesen dilakukan</dd>
                                    </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header z-depth-1 teal lighten-3" role="tab" id="heading11">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx2" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                <h4 class="mb-0 black-text text-center font-weight-bold text-uppercase">
                                    Lesen Borong
                                </h4>
                            </a>
                        </div>
                        <!-- Card body -->
                        <div id="collapse11" class="collapse" role="tabpanel" aria-labelledby="heading11" data-parent="#accordionEx2">
                            <div class="card-body rgba-teal-strong white-text">
                                <p style="font-size:12pt;">Berikut ini adalah syarat-syarat yang dikenakan ke atas pemegang-pemegang Lesen Borong Beras. Sila baca dengan teliti dan fahamkan ke atas setiap kenyataan yang disenaraikan. Mana-mana
                                    pelesen yang melanggar atau gagal mematuhi syarat-syarat ini adalah melakukan kesalahan.</p>

                                <p>
                                    <dl style="font-size:12pt;">
                                        <dd style="padding-bottom:5pt;">1.&ensp; Lesen hendaklah lengkap ditandatangani oleh pelesen atau penama di dalam lesen tersebut beserta cop rasmi syarikat.</dd>
                                        <dd style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan di dalam lesen.</dd>
                                        <dd style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Pelesen dilarang <strong>MEMINDA, MEMADAM</strong> dan <strong>MENAMBAH</strong> sebarang angka atau perkataan di dalam lesen.</dd>
                                        <dd style="padding-bottom:5pt;">4.&ensp; Lesen ini tidak boleh dipindahmilik atau ditukar nama atau diserah kepada orang lain. Jika berlaku sesuatu perkara yang</dd>
                                            <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;memerlukan perubahan maklumat di dalam lesen,
                                                pelesen perlu membuat permohonan perubahan secepat mungkin.</dd>
                                        <dd style="padding-bottom:5pt;">5.&ensp; Pelesen tidak boleh mempunyai dalam stok berasnya melebihi daripada had muatan yang ditetapkan dalam lesennya pada</dd>
                                        <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;satu-satu masa.</dd>
                                        <dd style="padding-bottom:5pt;">6.&ensp; Pelesen tidak boleh menyetor atau menyimpan beras di tempat selain daripada yang dinyatakan di dalan lesen</dd>
                                        <dd style="padding-bottom:5pt;">7.&ensp; Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan beras.</dd>
                                        <dd style="padding-bottom:5pt;">8.&ensp; Pelesen tidak boleh memperolehi atau membeli beras daripada mana-mana sumber selain daripada pemegang Lesen Borong</dd>
                                        <dd style="padding-bottom:5pt;">&nbsp;<span style="padding-right:1.5em;"></span>Beras atau Lesen Kilang Padi (Komersial).</dd>
                                        <dd style="padding-bottom:5pt;">9.&ensp; Pelesen tidak boleh menjual beras kepada mana – mana orang atau syarikat yang tidak mempunyai lesen runcit atau lesen</dd>
                                        <dd style="padding-bottom:5pt;">&nbsp;<span style="padding-right:1.5em;"></span>borong  yang sah.</dd>
                                        <dd style="padding-bottom:5pt;">10.&ensp; Pelesen hendaklah meletakkan label atau tanda dalam Bahasa Melayu yang menunjukkan gred, harga dan kandungan hancur</dd>
                                        <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;beras dikampit atau guni beras.</dd>
                                        <dd style="padding-bottom:5pt;">11.&ensp; Pelesen hendaklah menyenggara (menyediakan) suatu akaun harian bagi:-</dd>
                                        <dd><span style="padding-right: 1.8em"></span>a)&nbsp;Beras yang diperolehi atau dibeli dan disokong dengan invois – invois.</dd>
                                        <dd><span style="padding-right: 1.8em"></span>b)&nbsp;Jumlah jualan</dd>
                                        <dd><span style="padding-right: 1.8em"></span>c)&nbsp;Baki stok dalam tangan</dd>
                                        <dd style="padding-bottom:5pt;">12.&ensp; Pelesen dikehendaki mengemaskini laporan 2 mingguan melalui sistem eLPPB 2.0 seperti yang diarahkan. Sekiranya laporan</dd>
                                        <dd style="padding-bottom:5pt;">&nbsp;
                                        <span style="padding-right:1.5em;"></span>tersebut gagal dikemaskini maka permohonan pembaharuan lesen tidak boleh dilakukan di dalam sistem.</dd>
                                        <dd style="padding-bottom:5pt;">13.&ensp; Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen berkenaan.</dd>
                                        <dd>14.&ensp; Lesen yang sudah tamat tempoh akan dibatalkan secara automatik sekiranya tiada permohonan pembaharuan lesen dilakukan.</dd>
                                    </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header z-depth-1 teal lighten-2" role="tab" id="heading12">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx2" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                <h4 class="mb-0 black-text text-center font-weight-bold text-uppercase">
                                    Lesen Import
                                </h4>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse12" class="collapse" role="tabpanel" aria-labelledby="heading12" data-parent="#accordionEx2">
                            <div class="card-body rgba-teal-strong white-text">
                                <p style="font-size:12pt;">Berikut ini adalah syarat-syarat yang dikenakan ke atas pemegang-pemegang Lesen Import. Sila baca dengan teliti dan fahamkan ke atas setiap kenyataan yang disenaraikan. Mana-mana pelesen yang melanggar atau gagal mematuhi syarat-syarat ini adalah melakukan kesalahan.</p>

                                <p><dl style="font-size:12pt;">
                                    <dd style="padding-bottom:5pt;">1.&ensp; Lesen hendaklah lengkap ditandatangani oleh pelesen atau penama di dalam lesen tersebut beserta cop rasmi syarikat.</dd>
                                    <dd style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan di dalam lesen.</dd>
                                    <dd style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Pelesen dilarang <strong>MEMINDA, MEMADAM</strong> dan <strong>MENAMBAH</strong> sebarang angka atau perkataan di dalam lesen.</dd>
                                    <dd style="padding-bottom:5pt;">4.&ensp; Lesen ini tidak boleh dipindahmilik atau ditukar nama atau diserah kepada orang lain. Jika berlaku sesuatu perkara yang</dd>
                                        <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;memerlukan perubahan maklumat di dalam lesen,
                                            pelesen perlu membuat permohonan perubahan secepat mungkin.</dd>
                                    <dd style="padding-bottom:5pt;">5.&ensp; Pelesen tidak boleh menyetor atau menyimpan beras di tempat selain daripada yang dinyatakan di dalan lesen</dd>
                                    <dd style="padding-bottom:5pt;">6.&ensp; Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan beras.</dd>
                                    <dd style="padding-bottom:5pt;">7.&ensp;Pelesen hanya dibenarkan mengimport produk seperti yang dinyatakan di dalam lesen sahaja.</dd>
                                    <dd style="padding-bottom:5pt;">8.&ensp; Pelesen hendaklah menyenggara (menyediakan) suatu akaun harian bagi:-</dd>
                                    <dd><span style="padding-right: 1.8em"></span>a)&nbsp;Beras yang diperolehi atau dibeli dan disokong dengan invois – invois.</dd>
                                    <dd><span style="padding-right: 1.8em"></span>b)&nbsp;Jumlah jualan</dd>
                                    <dd><span style="padding-right: 1.8em"></span>c)&nbsp;Baki stok dalam tangan</dd>
                                    <dd style="padding-bottom:5pt;">9.&ensp; Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen berkenaan.</dd>
                                    <dd style="padding-bottom:5pt;">10.&ensp; Lesen yang sudah tamat tempoh akan dibatalkan secara automatik sekiranya tiada permohonan pembaharuan lesen dilakukan</dd>
                                </dl></p>
                            </div>
                        </div>
                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header z-depth-1 teal lighten-1" role="tab" id="heading13">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx2" href="#collapse13" aria-expanded="true" aria-controls="collapse13">
                                <h4 class="mb-0 black-text text-center font-weight-bold text-uppercase">
                                    Lesen Eksport
                                </h4>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse13" class="collapse" role="tabpanel" aria-labelledby="heading13" data-parent="#accordionEx2">
                            <div class="card-body rgba-teal-strong white-text">
                                <p style="font-size:12pt;">Berikut ini adalah syarat-syarat yang dikenakan ke atas pemegang-pemegang Lesen Eksport. Sila baca dengan teliti dan fahamkan ke atas setiap kenyataan yang disenaraikan. Mana-mana pelesen yang melanggar atau gagal mematuhi syarat-syarat ini adalah melakukan kesalahan.</p>

                                <p>
                                  <dl style="font-size:12pt;">
                                      <dd style="padding-bottom:5pt;">1.&ensp; Lesen hendaklah lengkap ditandatangani oleh pelesen atau penama di dalam lesen tersebut beserta cop rasmi syarikat.</dd>
                                      <dd style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan di dalam lesen.</dd>
                                      <dd style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Pelesen dilarang <strong>MEMINDA, MEMADAM</strong> dan <strong>MENAMBAH</strong> sebarang angka atau perkataan di dalam lesen.</dd>
                                      <dd style="padding-bottom:5pt;">4.&ensp; Lesen ini tidak boleh dipindahmilik atau ditukar nama atau diserah kepada orang lain. Jika berlaku sesuatu perkara yang</dd>
                                          <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;memerlukan perubahan maklumat di dalam lesen,
                                              pelesen perlu membuat permohonan perubahan secepat mungkin.</dd>
                                      <dd style="padding-bottom:5pt;">5.&ensp; 5.	Pelesen tidak boleh menyetor atau menyimpan beras / hasil sampingan di tempat selain daripada yang dinyatakan di dalam lesen</dd>
                                      <dd style="padding-bottom:5pt;">6.&ensp;Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan beras / hasil sampingan.</dd>
                                      <dd style="padding-bottom:5pt;">7.&ensp;Pelesen hanya dibenarkan mengeksport produk seperti yang dinyatakan di dalam lesen sahaja.</dd>
                                      <dd style="padding-bottom:5pt;">8.&ensp; Pelesen hendaklah menyenggara (menyediakan) suatu akaun harian bagi:-</dd>
                                      <dd><span style="padding-right: 1.8em"></span>a)&nbsp;Beras yang diperolehi atau dibeli dan disokong dengan invois – invois.</dd>
                                      <dd><span style="padding-right: 1.8em"></span>b)&nbsp;Jumlah jualan</dd>
                                      <dd><span style="padding-right: 1.8em"></span>c)&nbsp;Baki stok dalam tangan</dd>
                                      <dd style="padding-bottom:5pt;">9.&ensp; Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen berkenaan.</dd>
                                      <dd>10.&ensp; Lesen yang sudah tamat tempoh akan dibatalkan secara automatik sekiranya tiada permohonan pembaharuan lesen dilakukan.</dd>
                                  </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Accordion card -->

                    <!-- Accordion card -->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header z-depth-1 teal" role="tab" id="heading14">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx2" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                <h4 class="mb-0 black-text text-center font-weight-bold text-uppercase">
                                    Lesen Membeli Padi
                                </h4>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse14" class="collapse" role="tabpanel" aria-labelledby="heading14" data-parent="#accordionEx2">
                            <div class="card-body rgba-teal-strong white-text">
                                <p style="font-size:12pt;">Berikut ini adalah syarat-syarat yang dikenakan ke atas pemegang-pemegang Lesen Membeli Padi. Sila baca dengan teliti dan fahamkan ke atas setiap kenyataan yang disenaraikan. Mana-mana pelesen yang melanggar atau gagal mematuhi syarat-syarat ini adalah melakukan kesalahan.</p>

                                <p>
                                  <dl style="font-size:12pt;">
                                      <dd style="padding-bottom:5pt;">1.&ensp; Lesen hendaklah lengkap ditandatangani oleh pelesen atau penama di dalam lesen tersebut beserta cop rasmi syarikat.</dd>
                                      <dd style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan di dalam lesen.</dd>
                                      <dd style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Pelesen dilarang <strong>MEMINDA, MEMADAM</strong> dan <strong>MENAMBAH</strong> sebarang angka atau perkataan di dalam lesen.</dd>
                                      <dd style="padding-bottom:5pt;">4.&ensp; Lesen ini tidak boleh dipindahmilik atau ditukar nama atau diserah kepada orang lain. Jika berlaku sesuatu perkara yang</dd>
                                          <dd><span style="padding-right:1.5em;"></span>&nbsp;memerlukan perubahan maklumat di dalam lesen,
                                              pelesen perlu membuat permohonan perubahan secepat mungkin.</dd>
                                      <dd style="padding-bottom:5pt;">5.&ensp;Pelesen tidak boleh menyetor atau menyimpan beras / hasil sampingan di tempat selain daripada yang dinyatakan di dalam</dd>
                                      <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&nbsp;lesen.</dd>
                                      <dd style="padding-bottom:5pt;">6.&ensp;Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan padi.</dd>
                                      <dd style="padding-bottom:5pt;">7.&ensp;Pelesen tidak boleh memperolehi atau membeli padi daripada mana – mana sumber selain pesawah.</dd>
                                      <dd style="padding-bottom:5pt;">8.&ensp;Pelesen tidak boleh menjual padi kepada mana – mana orang atau syarikat yang tidak mempunyai lesen kilang padi (komersial)</dd>
                                      <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&ensp;yang sah.</dd>
                                      <dd style="padding-bottom:5pt;">9.&ensp;Pelesen hendaklah membuat permohonan membeli padi setiap kali musim bermula dengan menyatakan tarikh mula belian padi.</dd>
                                      <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&ensp;Permohonan boleh dihantar kepada pejabat KPB negeri atau cawangan.</dd>
                                      <dd style="padding-bottom:5pt;">10.&ensp;Pelesen hendaklah membuat sampel potongan sebenar padi bagi setiap padi yang dijual daripada pesawah untuk mengetahui</dd>
                                      <dd style="padding-bottom:5pt;"><span style="padding-right:1.5em;"></span>&ensp;kandungan wap basah, hampa, kotoran dan hampa berat bagi setiap padi yang dibeli.</dd>
                                      <dd style="padding-bottom:5pt;">11.&ensp; Pelesen hendaklah menghantar laporan kepada pejabat KPB negeri jumlah belian padi musim tersebut bagi:-</dd>
                                      <dd><span style="padding-right: 1.8em"></span>a)&nbsp;Jenis padi.</dd>
                                      <dd><span style="padding-right: 1.8em"></span>b)&nbsp;Berat kasar</dd>
                                      <dd><span style="padding-right: 1.8em"></span>c)&nbsp;Berat bersih</dd>
                                      <dd style="padding-bottom:5pt;">9.&ensp; Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen berkenaan.</dd>
                                      <dd>10.&ensp; Lesen yang sudah tamat tempoh akan dibatalkan secara automatik sekiranya tiada permohonan pembaharuan lesen dilakukan.</dd>
                                  </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- Accordion card -->

                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header z-depth-1 teal" role="tab" id="heading15">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx2" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                <h4 class="mb-0 black-text text-center font-weight-bold text-uppercase">
                                    Lesen Kilang Padi (Komersial)
                                </h4>
                            </a>
                        </div>

                        <!-- Card body -->
                        <div id="collapse15" class="collapse" role="tabpanel" aria-labelledby="heading15" data-parent="#accordionEx2">
                            <div class="card-body rgba-teal-strong white-text">
                                <p style="font-size:12pt;">Berikut ini adalah syarat-syarat yang dikenakan ke atas pemegang-pemegang Lesen Kilang Padi (Komersial). Sila baca dengan teliti dan fahamkan ke atas setiap kenyataan yang disenaraikan. Mana-mana pelesen yang melanggar atau gagal mematuhi syarat-syarat ini adalah melakukan kesalahan.</p>

                                <p>
                                  <dl style="font-size:12pt;">
                                      <dd style="padding-bottom:5pt;">1.&ensp; Lesen hendaklah lengkap ditandatangani oleh pelesen atau penama di dalam lesen tersebut beserta cop rasmi syarikat.</dd>
                                      <dd style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan di dalam lesen.</dd>
                                      <dd style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Pelesen dilarang <strong>MEMINDA, MEMADAM</strong> dan <strong>MENAMBAH</strong> sebarang angka atau perkataan di dalam lesen.</dd>
                                      <dd style="padding-bottom:5pt;">4.&ensp; Lesen ini tidak boleh dipindahmilik atau ditukar nama atau diserah kepada orang lain. Jika berlaku sesuatu perkara yang</dd>
                                          <dd><span style="padding-right:1.5em;"></span>&nbsp;memerlukan perubahan maklumat di dalam lesen,
                                              pelesen perlu membuat permohonan perubahan secepat mungkin.</dd>
                                              <dd style="padding-bottom:5pt;">5.&ensp;Pelesen tidak boleh mempunyai stok padi / beras melebihi had muatan yang ditetapkan dalam lesennya pada satu-satu masa.</dd>
                                      <dd style="padding-bottom:5pt;">6.&ensp;Pelesen tidak boleh menyetor atau menyimpan beras / hasil sampingan di tempat selain daripada yang dinyatakan di dalam</dd>
                                      <dd><span style="padding-right:1.5em;"></span>&nbsp;lesen.</dd>
                                      <dd style="padding-bottom:5pt;">7.&ensp;Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan padi / beras.</dd>
                                      <dd style="padding-bottom:5pt;">8.&ensp;Pelesen tidak boleh memperolehi atau membeli padi daripada mana – mana sumber daripada pemegang Lesen Membeli Padi</dd>
                                      <dd>&ensp;<span style="padding-right:1.5em;"></span>atau pesawah.</dd>
                                      <dd style="padding-bottom:5pt;">9.&ensp;Pelesen tidak boleh menjual beras kepada mana – mana orang atau syarikat yang tidak mempunyai lesen borong  yang sah.</dd>
                                      <dd style="padding-bottom:5pt;">10.&ensp;Pelesen hendaklah meletakkan label atau tanda dalam Bahasa Melayu yang menunjukkan gred, harga dan kandungan hancur</dd>
                                      <dd>&ensp;<span style="padding-right:1.5em;"></span>beras dikampit atau guni beras.</dd>
                                      <dd style="padding-bottom:5pt;">11.&ensp; Pelesen hendaklah menyenggara (menyediakan) suatu akaun harian bagi:-</dd>
                                      <dd><span style="padding-right: 1.8em"></span>a)&nbsp;Padi / beras yang diperolehi atau dibeli disokong dengan invois – invois.</dd>
                                      <dd><span style="padding-right: 1.8em"></span>b)&nbsp;Jumlah jualan</dd>
                                      <dd><span style="padding-right: 1.8em"></span>c)&nbsp;Baki stok dalam tangan</dd>
                                      <dd style="padding-bottom:5pt;">12.&ensp; Pelesen dikehendaki mengemaskini laporan 2 mingguan melalui sistem eLPPB 2.0 seperti yang diarahkan. Sekiranya laporan</dd>
                                      <dd>&ensp;<span style="padding-right:1.5em;"></span>tersebut gagal dikemaskini maka permohonan pembaharuan lesen tidak boleh dilakukan di dalam sistem.</dd>
                                      <dd style="padding-bottom:5pt;">9.&ensp; Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen berkenaan.</dd>
                                      <dd>10.&ensp; Lesen yang sudah tamat tempoh akan dibatalkan secara automatik sekiranya tiada permohonan pembaharuan lesen dilakukan.</dd>
                                  </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.Accordion wrapper-->
            </div>
        </div>
    </div>
</div>
@endsection
