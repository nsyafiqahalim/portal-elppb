@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb" style="margin-top:-30px;">
            <ol class="breadcrumb bg-transparent px-0 pt-5">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Kadar Fee Lesen</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Kadar Fee Lesen</h2>
                <hr>
            </div>
        </div>
    </div>
</section>

<div class="service_details_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <p style="font-size:12pt;margin-top:-35pt;">Catatan: Tempoh pembaharuan lesen yang dibenarkan adalah minimun satu (1) tahun dan maximum tiga (3) tahun</p>
                <p>
                    <div class="accordion md-accordion accordion-blocks" id="accordionEx78" role="tablist" aria-multiselectable="true">
                        <!-- Accordion card Lesen Runcit -->
                        <div class="card">
                            <!-- Card header -->
                            <div class="card-header" role="tab" id="heading79">
                                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse79" aria-expanded="true" aria-controls="collapse79">
                                    <h5 class="mt-1 mb-0">
                                        <span>Lesen Runcit</span>
                                    </h5>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapse79" class="collapse show" role="tabpanel" aria-labelledby="heading79" data-parent="#accordionEx78">
                                <div class="card-body">
                                    <div class="table-responsive mx-3">
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>10</td>
                                                    <td>20</td>
                                                    <td>40</td>
                                                    <td>60</td>
                                                </tr>
                                                <tr>
                                                    <td>20</td>
                                                    <td>30</td>
                                                    <td>60</td>
                                                    <td>90</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Table responsive wrapper -->
                                    <p style="font-size:12pt;"><strong>CATATAN:</strong>&ensp;RM 10 bagi setiap pertambahan 10 m/t atau sebahagian</p>
                                </div>
                            </div>
                        </div>
                        <!-- Accordion card Lesen Borong Beras -->
                        <div class="card">
                            <div class="card-header" role="tab" id="headingUnfiled">
                                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapseUnfiled" aria-expanded="true" aria-controls="collapseUnfiled">
                                    <h5 class="mt-1 mb-0">
                                        <span>Lesen Borong Beras</span>
                                    </h5>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapseUnfiled" class="collapse" role="tabpanel" aria-labelledby="headingUnfiled" data-parent="#accordionEx78">
                                <div class="card-body">
                                    <div class="table-responsive mx-3">
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                            <!--Table head-->
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>100</td>
                                                    <td>200</td>
                                                    <td>400</td>
                                                    <td>600</td>
                                                </tr>
                                                <tr>
                                                    <td>200</td>
                                                    <td>210</td>
                                                    <td>420</td>
                                                    <td>630</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Table responsive wrapper -->
                                    <p style="font-size:12pt;"><strong>CATATAN:</strong>&ensp;RM 10 bagi setiap pertambahan 100 m/t atau sebahagian</p>
                                </div>
                            </div>
                        </div>
                        <!-- Accordion card  Import/Export -->
                        <div class="card">
                            <div class="card-header" role="tab" id="heading80">
                                <!-- Heading -->
                                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse80" aria-expanded="true" aria-controls="collapse80">
                                    <h5 class="mt-1 mb-0">
                                        <span>Lesen Import/Eksport</span>
                                    </h5>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapse80" class="collapse" role="tabpanel" aria-labelledby="heading80" data-parent="#accordionEx78">
                                <div class="card-body">
                                    <div class="table-responsive mx-3">
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th>3 Tahun (RM)</th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>-</td>
                                                    <td>200</td>
                                                    <td>400</td>
                                                    <td>600</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Table responsive wrapper -->
                                    <p style="font-size:12pt;"><strong>CATATAN:</strong>&ensp;Fee lesen import/eksport tiada had muatan kerana syarat untuk lesen ini perlu mempunyai lesen beras borong</p>
                                </div>
                            </div>
                        </div>

                        <!-- Accordion card Paddy Factory -->
                        <div class="card">
                            <div class="card-header" role="tab" id="heading">
                                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse81" aria-expanded="true" aria-controls="collapse81">
                                    <h5 class="mt-1 mb-0">
                                        <span>Lesen Kilang Padi</span>
                                    </h5>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapse81" class="collapse" role="tabpanel" aria-labelledby="heading" data-parent="#accordionEx78">
                                <div class="card-body">
                                    <div class="table-responsive mx-3">
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                          <p style="font-size:12pt;font-weight:600;">i) Lesen Kilang Padi (Semenanjung)</p>
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>1000</td>
                                                    <td>200</td>
                                                    <td>400</td>
                                                    <td>600</td>
                                                </tr>
                                                <tr>
                                                    <td>2000</td>
                                                    <td>210</td>
                                                    <td>420</td>
                                                    <td>630</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                          <p style="font-size:12pt;font-weight:600;">ii) Lesen Kilang Padi (Sabah/Sarawak)</p>
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>1000</td>
                                                    <td>100</td>
                                                    <td>200</td>
                                                    <td>300</td>
                                                </tr>
                                                <tr>
                                                    <td>2000</td>
                                                    <td>110</td>
                                                    <td>220</td>
                                                    <td>330</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Table responsive wrapper -->
                                    <p style="font-size:12pt;"><strong>CATATAN:</strong>&ensp;Fee Lesen Kilang Padi (mengering dan mengilang padi kepunyaan petani untuk kegunaan sendiri) bagi Semenanjung adalah RM 50.00 setahun, Sabah dan Sarawak RM25.00 setahun</p>
                                </div>
                            </div>
                        </div>
                        <!-- Accordion card Borong Beras (Pengilang) -->
                        <div class="card">
                            <div class="card-header" role="tab" id="heading">
                                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse82" aria-expanded="true" aria-controls="collapse82">
                                    <h5 class="mt-1 mb-0">
                                        <span>Lesen Borong Beras (Pengilang)</span>
                                    </h5>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapse82" class="collapse" role="tabpanel" aria-labelledby="heading" data-parent="#accordionEx78">
                                <div class="card-body">
                                    <div class="table-responsive mx-3">
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>100</td>
                                                    <td>200</td>
                                                    <td>400</td>
                                                    <td>600</td>
                                                </tr>
                                                <tr>
                                                    <td>200</td>
                                                    <td>210</td>
                                                    <td>420</td>
                                                    <td>630</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Table responsive wrapper -->
                                    <p style="font-size:12pt;"><strong>CATATAN:</strong>&ensp;Bagi Sabah dan Sarawak, fee untuk Kilang Padi yang dikeluarkan di bawah Peraturan-Peraturan Kawalan Padi dan Beras (Pelesenan Kilang-Kilang Padi) 1996 ditetapkan pada RM50.00 setahun tidak tertakluk pada had muatan</p>
                                </div>
                            </div>
                        </div>
                        <!-- Accordion card Buy Paddy -->
                        <div class="card">
                            <div class="card-header" role="tab" id="heading">
                                <a data-toggle="collapse" data-parent="#accordionEx78" href="#collapse83" aria-expanded="true" aria-controls="collapse83">
                                    <h5 class="mt-1 mb-0">
                                        <span>Lesen Membeli Padi</span>
                                    </h5>
                                </a>
                            </div>

                            <!-- Card body -->
                            <div id="collapse83" class="collapse" role="tabpanel" aria-labelledby="heading" data-parent="#accordionEx78">
                                <div class="card-body">
                                    <div class="table-responsive mx-3">
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                          <p style="font-size:12pt;font-weight:600;">i) Semenanjung</p>
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>-</td>
                                                    <td>50</td>
                                                    <td>100</td>
                                                    <td>150</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-hover mb-0" style="font-size:12pt;">
                                          <p style="font-size:12pt;font-weight:600;">ii) Sabah/Sarawak</p>
                                            <thead>
                                                <tr>
                                                    <th class="th-lg"><a>MUATAN (M/T)</a></th>
                                                    <th class="th-lg"><a>1 Tahun (RM)</a></th>
                                                    <th class="th-lg"><a>2 Tahun (RM)</a></th>
                                                    <th><a>3 Tahun (RM)</a></th>
                                                </tr>
                                            </thead>
                                            <!--Table head-->

                                            <!--Table body-->
                                            <tbody>
                                                <tr>
                                                    <td>-</td>
                                                    <td>10</td>
                                                    <td>20</td>
                                                    <td>30</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- Table responsive wrapper -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.Accordion wrapper-->
                </p>
            </div>
        </div>
    </div>
</div>

@endsection
