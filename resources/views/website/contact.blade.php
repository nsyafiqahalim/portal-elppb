@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Hubungi Kami</li>
                <li class="breadcrumb-item active" aria-current="page">Alamat Cawangan</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Pejabat Cawangan Kawalselia Padi dan Beras</h2>
                <hr>
            </div>
        </div>

        <div id="mapCawangan" class="uk-flex uk-flex-center uk-padding-large">
            <div class="uk-inline uk-dark">
                <img src="img/about/map.png" alt="malaysia-map">
                <div id="caw1">
                    <a class="uk-position-absolute uk-transform-center" style="left: 20%; top: 81.5%" href="#" uk-tooltip="JOHOR" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom uk-column-1-2 uk-column-divider">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>JOHOR BAHRU (JB)</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;34, Jalan Garuda 1, Larkin Jaya, 80350 Johor Bahru, Johor</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;07-228 1911</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;07-228 1912</a>
                                    </li>
                                </ul><br><br>

                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>KLUANG</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Ketua Cawangan</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;No. 8, Jalan Intan 2/1, Taman Intan, 86000 Kluang, Johor</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;07-776 5258 / 5259</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;07-778 1849</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Johor -->

                <div id="caw2">
                    <a class="uk-position-absolute uk-transform-center" style="left: 5.5%; top: 18%" href="#" uk-tooltip="KEDAH" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>ALOR SETAR</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;No. 233 & 234, Jalan Shahab 2, Shahab Perdana, 05150 Alor Setar, Kedah </a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;04-734 9697</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;04-734 9157</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- End Kedah -->

                <div id="caw3">
                    <a class="uk-position-absolute uk-transform-center" style="left: 14%; top: 23%" href="#" uk-tooltip="KELANTAN" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>KOTA BHARU</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;Lot 2052 & 2053, Jalan Dato Lundang, 15200 Kota Bharu, Kelantan</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;09-744 8700</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;09-744 5700</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Kelantan -->

                <div id="caw4">
                    <a class="uk-position-absolute uk-transform-center" style="left: 14.8%; top: 76%" href="#" uk-tooltip="MELAKA" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>MELAKA TENGAH</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;G19, Blok Begonia, Saujana Puri, Bukit Katil, 75450 Melaka</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;06-233 2752</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;06-233 2839</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Melaka -->

                <div id="caw5">
                    <a class="uk-position-absolute uk-transform-center" style="left: 14%; top: 67%" href="#" uk-tooltip="NEGERI SEMBILAN" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>SEREMBAN</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;Tingkat 1, PT 2171, Jalan Seremban, Tampin,<br>
                                           Batu 4 ½, 70450 Senawang, Negeri Sembilan</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;06-677 9228</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;06-677 9213</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End N9 -->

                <div id="caw6">
                    <a class="uk-position-absolute uk-transform-center" style="left: 15%; top: 50%" href="#" uk-tooltip="PAHANG" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom uk-column-1-2 uk-column-divider">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>KUANTAN</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;B24 Jalan IM 3/10, BIM POINT, 25200 Kuantan, Pahang</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;09-573 2536</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;09-573 5535</a>
                                    </li>
                                </ul><br><br>

                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>TEMERLOH</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Ketua Cawangan</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;Lot 1208, Jalan Jerantut, Kawasan Perindustrian Songsang, 28000 Temerloh, Pahang</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;09-278 2307</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;09- 277 5097</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Pahang -->

                <div id="caw7">
                    <a class="uk-position-absolute uk-transform-center" style="left: 8%; top: 35%" href="#" uk-tooltip="PERAK" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>KINTA</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;No. 186, Taman Anda, Jalan Sultan Azlan Shah Utara, <br>31400 Ipoh, Perak</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;05-545 2150</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;05-547 3359</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Perak -->

                <div id="caw8">
                    <a class="uk-position-absolute uk-transform-center" style="left: 4%; top: 10%" href="#" uk-tooltip="PERLIS" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>KANGAR</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;No. 16, Jalan Sena Indah Satu,<br> Taman Sena Indah, 01000 Kangar, Perlis</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;04-976 2105 / 3770</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;04-977 5329</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Perlis -->

                <div id="caw9">
                    <a class="uk-position-absolute uk-transform-center" style="left: 5%; top: 28%" href="#" uk-tooltip="PULAU PINANG" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>SEBERANG JAYA</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;No. 10, Jalan Todak 5, Pusat Bandar Seberang Jaya, <br>13700 Perai, Pulau Pinang</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;04-390 3400</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;04-397 6219</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Penang -->

                <div id="caw10">
                    <a class="uk-position-absolute uk-transform-center" style="left: 88%; top: 27.5%" href="#" uk-tooltip="SABAH" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                        <div class="uk-card">
                            <div class="uk-card-body uk-margin-bottom uk-column-1-3 uk-column-divider">
                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>KOTA KINABALU</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;Lot 10, Blok F, Jalan Lintas Highway, Lintas Jaya New Uptownship Penampang, 88300 Kota Kinabalu, Sabah</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;088-726 978 / 088-726 987</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;088-726 943</a>
                                    </li>
                                </ul>

                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>SANDAKAN</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Ketua Cawangan</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;Lot 6, Blok 16, Aras Bawah, Bandar Indah Shophouse, <br>Batu 4, Jalan Labuk, W.D.T No. 599, 90000 Sandakan, Sabah</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;089-227 209</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;089-229 209</a>
                                    </li>
                                </ul>

                                <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                    <li class="uk-nav-header">
                                        <strong>TAWAU</strong>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-user-tie"></i>&nbsp;Ketua Cawangan</a>
                                    </li>
                                    <li>
                                        <a><i class="far fa-building"></i>&nbsp;Bahagian Kawal Selia Padi Dan Beras, TB 8279 Perdana Square, <br>Batu 3 Jalan Apas, 91000 Tawau, Sabah</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-phone-square-alt"></i>&nbsp;089-914 008</a>
                                    </li>
                                    <li>
                                        <a><i class="fas fa-fax"></i>&nbsp;089-914 009</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Sabah -->

                <div id="caw11">
                    <a class="uk-position-absolute uk-transform-center" style="left: 70%; top: 70%" href="#" uk-tooltip="SARAWAK" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; pos: top-center; uk-animation-slide-top-small; duration: 950">
                      <div class="uk-card">
                          <div class="uk-card-body uk-margin-left uk-column-1-3 uk-column-divider">
                              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                  <li class="uk-nav-header">
                                      <strong>KUCHING</strong>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                  </li>
                                  <li>
                                      <a><i class="far fa-building"></i>&nbsp;AG 101 & 1G 102, Blog G, Plot 14, Jalan Batu Kawah, <br>
                                        Batu Kawah New Township, Kuching, Sarawak 93250</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-phone-square-alt"></i>&nbsp;082-578 941</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-fax"></i>&nbsp;082-451 758</a>
                                  </li>
                              </ul>

                              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                  <li class="uk-nav-header">
                                      <strong>MIRI</strong>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-user-tie"></i>&nbsp;Ketua Cawangan</a>
                                  </li>
                                  <li>
                                      <a><i class="far fa-building"></i>&nbsp;Lot 2303, Taman Bulatan, Commercial Centre, Jalan Dato MUIP, 98000 Miri, Sarawak</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-phone-square-alt"></i>&nbsp;085-665 523</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-fax"></i>&nbsp;085-665 243</a>
                                  </li>
                              </ul><br>

                              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                  <li class="uk-nav-header">
                                      <strong>SIBU</strong>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-user-tie"></i>&nbsp;Ketua Cawangan</a>
                                  </li>
                                  <li>
                                      <a><i class="far fa-building"></i>&nbsp;No. 4C, Tingkat 1, Lot 1157, Blok 6, Sibu Town District, Jalan Hua Kiew, 96000 Sibu, Sarawak</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-phone-square-alt"></i>&nbsp;084-326 494</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-fax"></i>&nbsp;084-324 404</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                    </div>
                </div> <!-- End Srwk -->

                <div id="caw12">
                    <a class="uk-position-absolute uk-transform-center" style="left: 10%; top: 60%" href="#" uk-tooltip="SELANGOR" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                      <div class="uk-card">
                          <div class="uk-card-body uk-margin-bottom">
                              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                  <li class="uk-nav-header">
                                      <strong>KUALA SELANGOR</strong>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                  </li>
                                  <li>
                                      <a><i class="far fa-building"></i>&nbsp;Blok B, IADA Barat Laut Selangor,<br> Kompleks Pejabat IADA Barat Laut Selangor, <br>
                                        45000 Kuala Selangor, Selangor</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-phone-square-alt"></i>&nbsp;03-3289 8419</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-fax"></i>&nbsp;03-3289 8924</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                    </div>
                </div> <!-- End Selangor -->

                <div id="caw12">
                    <a class="uk-position-absolute uk-transform-center" style="left: 11%; top: 67%" href="#" uk-tooltip="PUTRAJAYA" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; uk-animation-slide-top-small; duration: 950">
                      <div class="uk-card">
                          <div class="uk-card-body uk-margin-bottom">
                              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                  <li class="uk-nav-header">
                                      <strong>PUTRAJAYA</strong>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah Pelesenan</a>
                                  </li>
                                  <li>
                                      <a><i class="far fa-building"></i>&nbsp;Aras 2, Wisma Tani,<br> No. 28 Persiaran Perdana Presint 4, <br>
                                        62624, W.P Putrajaya</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-phone-square-alt"></i>&nbsp;03-8870 1000</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-fax"></i>&nbsp;03-8888 6020</a>
                                  </li>
                                  <li>
                                      <a><i class="fa fa-clock-o"></i>&nbsp;Waktu Urusan:<br>
                                      Isnin - Khamis: 8:30 pagi - 4:30 petang<br><span style="padding-right:7.5em;"></span>1:00 petang - 2 petang (rehat)<br>Jumaat<span style="padding-right:3.2em;"></span>: 8:30 pagi - 4:30 petang<br><span style="padding-right:7.5em;"></span>12:15 tengahari - 2:45 petang (rehat)</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                    </div>
                </div> <!-- End Putrajaya -->

                <div id="caw13">
                    <a class="uk-position-absolute uk-transform-center" style="left: 17%; top: 35%" href="#" uk-tooltip="TERENGGANU" uk-marker="icon: location"></a>
                    <div class="uk-padding-remove" uk-dropdown="mode:click; pos: top-left; animation: uk-animation-slide-top-small; duration: 950">
                      <div class="uk-card">
                          <div class="uk-card-body uk-margin-bottom">
                              <ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
                                  <li class="uk-nav-header">
                                      <strong>KUALA TERENGGANU</strong>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-user-tie"></i>&nbsp;Pengarah</a>
                                  </li>
                                  <li>
                                      <a><i class="far fa-building"></i>&nbsp;PT 2347K, Jalan Rajawali 5, Kawasan Perindustrian Chendering,<br>
                                         21080 Kuala Terengganu, Terengganu</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-phone-square-alt"></i>&nbsp;09-617 8816</a>
                                  </li>
                                  <li>
                                      <a><i class="fas fa-fax"></i>&nbsp;09-617 8819</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                    </div>
                </div> <!-- End Terengganu -->
            </div>
        </div>
    </div><!-- End Container -->
</section>
@endsection

@push('script')

@endpush
