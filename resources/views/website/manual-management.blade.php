@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Manual Pengguna</li>
                <li class="breadcrumb-item active" aria-current="page">Pengurusan Maklumat Asas</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Manual Pengurusan Maklumat Asas</h2>
                <hr>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top:-30pt;">
        <div class="row">
            <div class="col-sm-12">
                <div class="row my-4">
                    <div class="col-md-4 mt-3 pt-2">
                        <div class="view z-depth-1">
                            <a href="https://www.malaysia.gov.my/media/uploads/cdef5a3e-11e3-41a2-8382-d2cfa96b1a26.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Maklumat_Asas" title="Maklumat Asas" class="img-fluid"></a>
                        </div>
                        <p style="margin-left:25px;font-size:12pt;">Pengurusan Maklumat Asas</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
