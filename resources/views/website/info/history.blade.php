@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Info</li>
                <li class="breadcrumb-item active" aria-current="page">Sejarah Penubuhan Seksyen Kawalselia Padi dan Beras</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Sejarah Penubuhan Seksyen Kawalselia Padi dan Beras</h2>
                <hr>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="features_main_wrap">
        <div class="row align-items-center">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="features_info">
                    <p style="text-align:justify;">
                        <img src="{{ $imageHistory->value }}" style="border-style:groove;">
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
