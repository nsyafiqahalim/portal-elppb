@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Info</li>
                <li class="breadcrumb-item active" aria-current="page">Perutusan Ketua Pengarah</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Perutusan Ketua Pengarah</h2>
                <hr>
                <div style="text-align: center;">
                <!--
                <img src="img/team/ketua-pengarah-baru.png" class="img" alt="Azman Mahmood">
                -->
                <!--
                <img src="{{ asset('img/team/ketua-pengarah-baru.png') }}" class="img" alt="{{ $nameD->value }}">
                -->

                 <img src="{{ asset($imageD->value) }}" class="img" alt="{{ $nameD->value }}">
                </div>
                <p style="text-align:center;">
                    Assalamualaikum Warahmatullahi Wabarakatuh dan Salam Sejahtera.
                </p>
                <p style="text-align:justify;">
                    {!! $infoD->value !!}
                <p style="text-align:justify; margin-top:35px;">
                    <b style="color:black;">{{ $nameD->value }}</b>&nbsp;
                    <small style="font-style:italic;font-size:9pt;">{{ $medalD->value }}</small>
                </p>
                <div style="text-align: justify;"><span style="font-size: 12pt;">Ketua Pengarah</span><br><span style="font-size: 12pt;">Kawalselia Padi dan Beras (KPB)</span><br><span style="font-size: 12pt;">Kementerian Pertanian dan Industri Makanan</span></div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('css')
<style>
    .img {
        width: 250px;
        height: 280px;
        border: 2px solid #000000;
        padding: 2px;
        background: transparent;
    }
</style>
@endpush
