@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Manual Pengguna</li>
                <li class="breadcrumb-item active" aria-current="page">Permohonan Lesen</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Manual Pengguna Permohonan Lesen</h2>
                <hr>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <!--Accordion wrapper-->
                <div class="accordion md-accordion accordion-1" id="accordionEx23" role="tablist">
                    <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading96" data-toggle="collapse" href="#collapse96" aria-expanded="true" aria-controls="collapse96">
                            <h4 class="text-uppercase mb-0 py-1">
                                <a class="white-text font-weight-bold" data-toggle="collapse" href="#collapse96" aria-expanded="true" aria-controls="collapse96">
                                    LESEN BAHARU
                                </a><img src="img/icon/arrow-down.png" align="right">
                            </h4>
                        </div>
                        <div id="collapse96" class="collapse show" role="tabpanel" aria-labelledby="heading96" data-parent="#accordionEx23">
                            <div class="card-body">
                                <div class="row my-4">
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen Runcit" title="Lesen Runcit" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Runcit</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen_Borong" title="Lesen Borong" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Borong</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen_Import" title="Lesen Import" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Import</p>
                                    </div>
                                </div>
                                <!-- Next Row -->
                                <div class="row my-4">
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf"target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen_Eksport" title="Lesen Eksport" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Eksport</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf"target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen_Beli_Padi" title="Lesen Membeli Padi" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Membeli Padi</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf"target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen_Kilang_Padi_Komersial" title="Lesen Kilang Padi (Komersial)" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Kilang Padi Komersial</p>
                                    </div>
                                </div>
                                <!-- NExt Row -->
                                <div class="row my-4">
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf"target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Lesen_Mengering" title="Lesen Mengering/Mengilang" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Bagi Mengering atau <br>Mengilang Untuk Kegunaan Sendiri</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Daftar Lesen -->
                    <div class="card">
                        <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading97" data-toggle="collapse" href="#collapse97" aria-expanded="true" aria-controls="collapse97">
                            <h4 class="text-uppercase mb-0 py-1">
                                <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse97" aria-expanded="false" aria-controls="collapse97">
                                    PEMBAHARUAN LESEN
                                </a><img src="img/icon/arrow-down.png" align="right">
                            </h4>
                        </div>
                        <div id="collapse97" class="collapse" role="tabpanel" aria-labelledby="heading97" data-parent="#accordionEx23">
                            <div class="card-body">
                                <div class="row my-4">
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan" title="Lesen Runcit" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Runcit</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan" title="Lesen Borong" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Borong</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan" title="Lesen Import" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Import</p>
                                    </div>
                                </div>
                                <!-- Next Row -->
                                <div class="row my-4">
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan" title="Lesen Eksport"
                                                  class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:25px;font-size:12pt;">Lesen Eksport</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan" title="Lesen Membeli Padi"
                                                  class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:10px;font-size:12pt;text-align:left;">Lesen Membeli Padi</p>
                                    </div>
                                    <!-- Next Row -->
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan"
                                                  title="Lesen Kilang Padi (Komersial)" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:10px;font-size:12pt;">Lesen Kilang Padi (Komersial)</p>
                                    </div>
                                    <div class="col-md-4 mt-3 pt-2">
                                        <div class="view z-depth-1">
                                            <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan"
                                                  title="Lesen Mengering/Mengilang" class="img-fluid"></a>
                                        </div>
                                        <p style="margin-left:10px;font-size:12pt;">Lesen Bagi Mengering atau <br>Mengilang Untuk Kegunaan Sendiri</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Perubahan Lesen -->
                <div class="card">
                    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading98" data-toggle="collapse" href="#collapse98" aria-expanded="true" aria-controls="collapse98">
                        <h4 class="text-uppercase mb-0 py-1">
                            <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse98" aria-expanded="false" aria-controls="collapse98">
                                PERMOHONAN PERUBAHAN
                            </a><img src="img/icon/arrow-down.png" align="right">
                        </h4>
                    </div>
                    <div id="collapse98" class="collapse" role="tabpanel" aria-labelledby="heading98" data-parent="#accordionEx23">
                      <div class="card-body">
                          <div class="row my-4">
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Perubahan" title="Maklumat Perniagaan" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Maklumat Perniagaan</p>
                              </div>
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Perubahan" title="Alamat Premis Perniagaan" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Alamat Premis Perniagaan</p>
                              </div>
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Perubahan" title="Alamat Stor" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Alamat Stor</p>
                              </div>
                          </div>
                          <!-- Next Row -->
                          <div class="row my-4">
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Perubahan" title="Muatan Runcit"
                                            class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Muatan Runcit</p>
                              </div>
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Perubahan" title="Muatan Stor"
                                            class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:10px;font-size:12pt;">Muatan Stor</p>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <!-- Perubahan Pembaharuan & Perubahan -->
                <div class="card">
                    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading99" data-toggle="collapse" href="#collapse99" aria-expanded="true" aria-controls="collapse99">
                        <h4 class="text-uppercase mb-0 py-1">
                            <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse99" aria-expanded="false" aria-controls="collapse99">
                                PEMBAHARUAN DAN PERUBAHAN BERSEKALI
                            </a><img src="img/icon/arrow-down.png" align="right">
                        </h4>
                    </div>
                    <div id="collapse99" class="collapse" role="tabpanel" aria-labelledby="heading99" data-parent="#accordionEx23">
                      <div class="card-body">
                          <div class="row my-4">
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan_Perubahan" title="Lesen Runcit" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Pembaharuan dan Perubahan<br> Bersekali Lesen Runcit</p>
                              </div>
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembaharuan_Perubahan" title="Selain Lesen Runcit" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Pembaharuan dan Perubahan <br> Bersekali Selain Lesen Runcit</p>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <!-- GANTIAN -->
                <div class="card">
                    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading100" data-toggle="collapse" href="#collapse100" aria-expanded="true" aria-controls="collapse100">
                        <h4 class="text-uppercase mb-0 py-1">
                            <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse100" aria-expanded="false" aria-controls="collapse100">
                                GANTIAN LESEN
                            </a><img src="img/icon/arrow-down.png" align="right">
                        </h4>
                    </div>
                    <div id="collapse100" class="collapse" role="tabpanel" aria-labelledby="heading100" data-parent="#accordionEx23">
                      <div class="card-body">
                          <div class="row my-4">
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Gantian" title="Semua Lesen" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Semua Jenis Lesen</p>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <!-- PEMBATALAN -->
                <div class="card">
                    <div class="card-header blue lighten-3 z-depth-1" role="tab" id="heading101" data-toggle="collapse" href="#collapse101" aria-expanded="true" aria-controls="collapse101">
                        <h4 class="text-uppercase mb-0 py-1">
                            <a class="collapsed font-weight-bold white-text" data-toggle="collapse" href="#collapse101" aria-expanded="false" aria-controls="collapse101">
                                PEMBATALAN LESEN
                            </a><img src="img/icon/arrow-down.png" align="right">
                        </h4>
                    </div>
                    <div id="collapse101" class="collapse" role="tabpanel" aria-labelledby="heading101" data-parent="#accordionEx23">
                      <div class="card-body">
                          <div class="row my-4">
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembatalan" title="Semua Jenis Lesen" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Semua Jenis Lesen</p>
                              </div>
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembatalan" title="Semua Jenis Lesen Automatik" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Semua Jenis Lesen Automatik</p>
                              </div>
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembatalan" title="Semua Jenis Permohonan Automatik" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Secara Automatik Semua Jenis<br> Permohonan Automatik</p>
                              </div>
                          </div>
                          <!-- Next Row -->
                          <div class="row my-4">
                              <div class="col-md-4 mt-3 pt-2">
                                  <div class="view z-depth-1">
                                      <a href="pdf\new\HELP_INT_LBPERMOHONANBARU_230218.pdf" target="_blank"><img src="img/icon/adobe-pdf.png" style="width:100;height:100;" alt="Pembatalan" title="Pembatalan Lesen oleh Ketua Pengarah" class="img-fluid"></a>
                                  </div>
                                  <p style="margin-left:25px;font-size:12pt;">Pembatalan Lesen oleh<br> Ketua Pengarah</p>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
            <!--Accordion wrapper-->
        </div>
    </div>
    </div>
</section>
@endsection
