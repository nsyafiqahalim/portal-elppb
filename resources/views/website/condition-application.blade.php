@extends('layouts.website-layout')

@section('content')

<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-5">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Syarat Permohonan</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Syarat Permohonan</h2>
                <hr>
            </div>
        </div>
    </div>
</section>

<div class="aboutus-section" style="margin-top:8pt;">
    <div class="container">
        <div class="row" style="margin-bottom:4rem;">
            <div class="col-lg-12">
                <div class="accordion" id="accordionExample">
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingOne">
                            <h3 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Lesen Runcit
                                </button>
                            </h3>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana syarikat yang berkelayakan untuk menjual beras secara runcit dengan had muatan bermula 10 MT.</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang berumur 18 tahun dan ke atas sahaja.</dd>
                                    <dd>2. &ensp;Mempunyai premis yang sesuai dan munasabah untuk perniagaan runcit beras.</dd>
                                    <dd>3. &ensp;Tiada Lesen Runcit Beras lain yang wujud di alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em"></span>&ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan bagi tahun terkini daripada
                                        Suruhanjaya Syarikat Malaysia (SSM).</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk</dd>
                                    <dd><span style="padding-right: 2.8em"></span> &nbsp;memohon Lesen Runcit Beras.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingTwo">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Lesen Borong
                                </button>
                            </h3>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana syarikat yang berkelayakan untuk menjual beras secara runcit dengan had muatan bermula 10 MT.</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang berumur 21 tahun dan ke atas sahaja.</dd>
                                    <dd>2. &ensp;Mempunyai premis yang sesuai dan munasabah untuk perniagaan borong beras.</dd>
                                    <dd>3. &ensp;Tiada Lesen Borong Beras lain yang wujud di alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em"></span>&ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama pemilik syarikat atau
                                        nama perniagaan yang didaftarkan dengan baki</dd>
                                        <dd><span style="padding-right: 2.8em"></span> &nbsp;terkini tidak kurang daripada
                                            RM100,000 bagi syarikat Bumiputera atau RM150,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang terkini daripada Suruhanjaya
                                        Syarikat Malaysia (SSM) yang mana modal</dd>
                                        <dd><span style="padding-right: 2.8em"></span> &nbsp;berbayar sekurang-kurangnya
                                            RM100,000 bagi syarikat Bumiputera dan RM150,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama pemilik syarikat atau
                                        nama perniagaan yang didaftarkan dengan baki</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;terkini tidak kurang daripada
                                            RM100,000 bagi syarikat Bumiputera atau RM150,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk</dd>
                                    <dd><span style="padding-right: 2.8em"></span> &nbsp;memohon Lesen Borong Beras.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama koperasi / pertubuhan
                                        yang didaftarkan dengan baki terkini tidak kurang</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;daripada RM100,000 bagi
                                            koperasi / pertubuhan Bumiputera atau RM150,000 bagi koperasi / pertubuhan
                                            Bukan Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>e) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingThree">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Lesen Import
                                </button>
                            </h3>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana syarikat bagi tujuan pengimportan produk
                                    hiliran beras (Bihun, Laksa, Kue Teow dan lain - lain yang dibenarkan oleh Ketua Pengarah
                                    Kawalselia Padi dan Beras)</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl>
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang berumur 21 tahun dan ke atas sahaja.</dd>
                                    <dd>2. &ensp;Mempunyai premis dan stor yang sesuai dan munasabah untuk perniagaan import.</dd>
                                    <dd>3. &ensp;Tiada Lesen Import lain yang wujud di alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em"></span> &ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang terkini daripada Suruhanjaya
                                        <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk</dd>
                                    <dd><span style="padding-right: 2.8em"></span>&nbsp;memohon Lesen Import Beras.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingFour">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Lesen Eksport
                                </button>
                            </h3>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana syarikat bagi tujuan pengeskportan produk
                                    hiliran beras (Bihun, Laksa, Kue Teow dan lain - lain yang dibenarkan oleh Ketua Pengarah
                                    Kawalselia Padi dan Beras)</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang berumur 21 tahun dan ke atas sahaja.</dd>
                                    <dd>2. &ensp;Mempunyai premis dan stor yang sesuai dan munasabah untuk perniagaan eksport.</dd>
                                    <dd>3. &ensp;Tiada Lesen Eksport lain yang wujud di alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em"></span>&ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang terkini daripada Suruhanjaya
                                        Syarikat Malaysia (SSM).</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah
                                        atau Ahli Jawatankuasa bersetuju untuk</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;memohon Lesen Eksport.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingFive">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Lesen Membeli Padi
                                </button>
                            </h3>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana syarikat yang berkelayakan untuk membeli padi.</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang berumur 21 tahun dan ke atas sahaja.</dd>
                                    <dd>2. &ensp;Pemohon perlu mempunyai kelulusan bersyarat membina pusat belian padi terlebih
                                        dahulu.</dd>
                                    <dd>3. &ensp;Tapak pengumpulan padi hendaklah di kawasan yang bersesuaian dan tidak
                                        mengganggu kawasan awam.</dd>
                                    <dd>4. &ensp;Tiada Lesen Membeli Padi lain yang wujud di alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em"></span>&ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama pemilik syarikat atau
                                        nama perniagaan yang didaftarkan dengan baki</dd>
                                        <dd><span style="padding-right: 2.8em"></span> &nbsp;terkini tidak kurang daripada
                                            RM50,000 bagi syarikat Bumiputera atau RM70,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor atau tanah / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang terkini daripada Suruhanjaya
                                        Syarikat Malaysia (SSM) yang mana modal</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;berbayar sekurang-kurangnya
                                            7
                                            RM50,000 bagi syarikat Bumiputera dan RM70,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama pemilik syarikat atau
                                        perniagaan yang didaftarkan dengan baki terkini</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;tidak kurang daripada
                                            RM50,000 untuk syarikat Bumiputera atau RM70,000 untuk syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor atau tanah / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah
                                        atau Ahli Jawatankuasa bersetuju untuk</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;memohon Lesen Membeli Padi.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama koperasi / pertubuhan
                                        yang didaftarkan dengan baki terkini tidak kurang</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;daripada RM50,000 bagi
                                            koperasi / pertubuhan Bumiputera atau RM70,000 bagi koperasi / pertubuhan
                                            Bukan Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>e) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor atau tanah / geran tanah.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingSix">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    Lesen Kilang Padi (Komersial)
                                </button>
                            </h3>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana syarikat yang berkelayakan untuk
                                    mengendalikan kilang beras bagi maksud komersial.</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang berumur 21 tahun dan ke atas sahaja.</dd>
                                    <dd>2. &ensp;Pemohon perlu mempunyai kelulusan bersyarat membina kilang padi (komersial)
                                        terlebih dahulu.</dd>
                                    <dd>3. &ensp;Tiada Lesen Kilang Padi (Komersial) lain yang wujud di alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em"></span>&ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama pemilik syarikat atau
                                        nama perniagaan yang didaftarkan dengan baki</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;tidak kurang daripada
                                            RM200,000 bagi syarikat Bumiputera atau RM250,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan surat kelulusan Jabatan Alam Sekitar.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan surat kelulusan ubah syarat tapak kilang kepada Industri / kilang padi.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa
                                        Tempatan.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang terkini daripada Suruhanjaya
                                        Syarikat Malaysia (SSM) yang mana modal</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;berbayar sekurang-kurangnya
                                            RM200,000 bagi syarikat Bumiputera dan RM250,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama pemilik syarikat atau
                                        nama perniagaan yang didaftarkan dengan baki</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;terkini tidak kurang daripada
                                            RM200,000 bagi syarikat Bumiputera atau RM250,000 bagi syarikat Bukan
                                            Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan surat kelulusan Jabatan Alam Sekitar.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan surat kelulusan ubah syarat tapak kilang kepada Industri / kilang padi.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>e) &nbsp;Salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa
                                        Tempatan.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>e) &nbsp;Salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah
                                        atau Ahli Jawatankuasa bersetuju untuk</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;memohon Lesen Kilang Padi
                                            (Komersial).</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan penyata akaun bank 3 bulan terkini atas nama koperasi / pertubuhan
                                        yang didaftarkan dengan baki terkini</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;tidak kurang daripada RM200,000 bagi
                                            koperasi / pertubuhan Bumiputera atau RM250,000 untuk Bukan Bumiputera.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>e) &nbsp;Salinan surat kelulusan Jabatan Alam Sekitar.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>f) &nbsp;Salinan surat kelulusan ubah syarat tapak kilang kepada Industri / kilang padi.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>g) &nbsp;Salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa
                                        Tempatan.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>h) &nbsp;Salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingSeven">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    Lesen Bagi Mengering / Mengilang Untuk Kegunaan Sendiri
                                </button>
                            </h3>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-size:12pt;">Lesen yang dikeluarkan kepada mana-mana individu atau syarikat yang berkelayakan bagi
                                    maksud pengeringan atau pengilangan padi kepunyaan petani bagi kegunaan sendiri.</p>
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd>1. &ensp; Pemilik syarikat atau pemegang syer syarikat hendaklah warganegara Malaysia yang
                                        berumur 18 tahun dan ke atas</dd>
                                    <dd>2. &ensp;Premis yang sesuai dan lokasi yang munasabah.</dd>
                                    <dd>3. &ensp;Tiada Lesen Bagi Mengering / Mengilang Untuk Kegunaan Sendiri lain yang wujud di
                                        alamat yang sama.</dd>
                                </dl>
                                </p>

                                <p style="font-weight:600;font-size:12pt;">Dokumen Yang Diperlukan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dt style="padding-bottom:5pt;">1.&ensp;Syarikat Persendirian / Perkongsian</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan pemilik syarikat.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang masih sah tempoh iaitu;</dd>
                                    <dd><span style="padding-right: 4em"></span>i. &nbsp; Salinan Suruhanjaya Syarikat Malaysia (SSM); atau</dd>
                                    <dd><span style="padding-right: 4em; text-align:justify;"></span>ii. &nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration)
                                        dan Extract of Business Names Registration bagi negeri</dd>
                                        <dd><span style="padding-right: 4.6em; text-align:justify;"></span>&ensp;Sarawak; atau</dd>
                                    <dd><span style="padding-right: 4em"></span>iii. &nbsp;Salinan Trading Licence bagi negeri Sabah.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">2.&ensp;Syarikat Sdn Bhd / Berhad</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Maklumat Pendaftaran Perniagaan yang terkini daripada Suruhanjaya
                                        Syarikat Malaysia (SSM).</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Koperasi / Pertubuhan</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Sijil Pendaftaran.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan Buku Undang-Undang Kecil Tubuh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah
                                        atau Ahli Jawatankuasa bersetuju untuk</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;memohon Lesen Bagi Mengering /
                                            Mengilang Untuk Kegunaan Sendiri</dd>
                                    <dd><span style="padding-right: 1.5em"></span>d) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                    <!-- dt -->
                                    <dt style="padding-bottom:5pt;padding-top:5pt;">3.&ensp;Orang Perseorangan (Individu)</dt>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Salinan Kad Pengenalan.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Salinan perjanjian sewa stor / perjanjian jual beli stor / surat kebenaran
                                        menggunakan stor / geran tanah.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingEight">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                    Permit Permindahan Beras / Hasil Sampingan
                                </button>
                            </h3>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd style="font-weight:500;">1. &ensp; Individu</dd>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Mana – mana individu atau orang perseorangan yang ingin membawa beras dari satu
                                        negeri ke negeri yang lain bagi tujuan</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;kegunaan sendiri melebihi 10 kg ke atas didalam
                                            satu – satu kenderaan.</dd>
                                    <dd style="padding-top:5pt;font-weight:500;">2. &ensp;Syarikat</dd>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Lesen Borong Beras, Lesen Kilang Padi (Komersial) yang masih sah tempoh.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>b) &nbsp;Bagi syarikat yang mempunyai lesen di negeri Kelantan, permohonan permit
                                        perlu dibuat satu hari lebih awal kerana permit</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;hanya dikeluarkan pada hari
                                            Ahad, Selasa dan Khamis sahaja.</dd>
                                    <dd><span style="padding-right: 1.5em"></span>c) &nbsp;Bagi syarikat seperti pembekal makanan (katerer) yang membeli beras dari
                                        negeri yang berlainan dan ingin membawa beras</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;perlulah memohon permit
                                            dengan melampirkan Salinan SSM e-Info terkini yang mana jenis perniagaan ada
                                            menyatakan</dd>
                                            <dd><span style="padding-right: 2.8em"></span>&nbsp;pembekal makanan (katerer).</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card z-depth-0 bordered">
                        <div class="card-header" id="headingNine">
                            <h3 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                    Permit Permindahan Padi
                                </button>
                            </h3>
                        </div>
                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                            <div class="card-body">
                                <p style="font-weight:600;font-size:12pt;">Syarat Permohonan</p>
                                <p>
                                <dl style="font-size:12pt;">
                                    <dd style="font-weight:500;">1. &ensp; Individu</dd>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Mana – mana individu atau orang perseorangan (pesawah) yang ingin membawa padi
                                        dari satu negeri ke negeri yang lain</dd>
                                        <dd><span style="padding-right: 2.8em"></span>&nbsp;bagi tujuan menjual ke kilang.</dd>
                                    <dd style="padding-top:5pt;font-weight:500;">2. &ensp;Syarikat</dd>
                                    <dd><span style="padding-right: 1.5em"></span>a) &nbsp;Hanya pemegang lesen membeli padi sahaja dibenarkan dan lesen yang masih sah
                                        tempoh.</dd>
                                </dl>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
