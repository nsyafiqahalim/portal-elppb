@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dasar Privasi</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Dasar Privasi</h2><hr>
                <p style="text-align:justify;">
                  ​​​​​Halaman ini menerangkan dasar privasi yang merangkumi penggunaan dan perlindungan maklumat yang dikemukakan oleh pengunjung.Sekiranya anda ingin mendaftar atau menghantar e-mel yang mengandungi maklumat peribadi, maklumat ini mungkin akan dikongsi bersama dengan jabatan/agensi Kementerian untuk membantu penyediaan perkhidmatan yang lebih berkesan dan efektif.  Contohnya seperti di dalam menyelesaikan aduan yang memerlukan maklum balas dari jabatan/agensi lain.
                </p>
                <p style="text-align:justify; font-weight:500; color:black;">Maklumat Yang Dikumpul</p>
                <p style="text-align:justify;">Tiada maklumat peribadi akan dikumpul semasa anda melayari laman web ini kecuali maklumat yang dikemukakan oleh anda melalui e-mel atau pendaftaran yang merupakan bahagian yang dilindungi dalam laman web ini​.</p>
                <p style="text-align:justify; font-weight:500; color:black;">Maklum Balas</p>
                <p style="text-align:justify;">Kami amat mengalu-alukan maklum balas/pertanyaan anda. Sekiranya maklumat yang terperinci diperlukan tidak terdapat di dalam laman web ini, keperluan anda akan dimajukan kepada jabatan/agensi yang berkaitan.​</p>
                <p style="text-align:justify; font-weight:500; color:black;">Pindaan Dasar</p>
                <p style="text-align:justify;">Sekiranya dasar privasi ini dipinda, pindaan akan dikemas kini di halaman ini. Dengan sering melayari halaman ini, anda akan dikemas kini dengan maklumat yang dikumpul, cara ia digunakan dan dalam keadaan tertentu, bagaimana maklumat dikongsi bersama pihak yang lain.​</p>
            </div>
        </div>
    </div>
</section>
@endsection
