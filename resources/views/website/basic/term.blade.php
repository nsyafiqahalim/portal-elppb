@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Terma & Syarat</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Terma & Syarat</h2>
                <hr>
                <p style="text-align:justify; font-weight:500; color:black;">​Syarat-syarat</p>
                <p style="text-align:justify;">
                    Di bawah adalah syarat-syarat penggunaan laman web ​Portal eLPPB serta hak dan kewajipan anda semasa mengakses atau menggunakan perkhidmatan di laman web ini.</p>
                <p style="text-align:justify;">
                    Sekiranya anda mengakses laman web ini, ia merupakan pengakuan dan persetujuan bahawa anda terikat kepada syarat-syarat ini dan dengan ini, terbentuklah satu persetujuan di antara anda sebagai pelanggan, dan kami, pihak
                    Kementerian bagi akses atau penggunaan laman web ini.Penggunaan atau akses anda kepada perkhidmatan ini seterusnya akan dianggap sebagai penerimaan syarat-syarat ini.
                </p>
                <p style="text-align:justify; font-weight:500; color:black;">Had Tanggungjawab</p>
                <p style="text-align:justify;">
                    Anda dengan jelas memahami dan bersetuju bahawa pihak Kementerian tidak bertanggungjawab ke atas sebarang bentuk kerugian, secara langsung atau tidak langsung, yang berkaitan atau khusus, turutan atau exemplary, termasuk tetapi
                    tidak terhad kepada kerugian berbentuk kehilangan keuntungan, kepercayaan atau kerugian tak ketara yang lain akibat daripada:</p>
                <p style="text-align:justify; margin-top:20px;">1.&nbsp;Penggunaan atau ketidakupayaan untuk menggunakan perkhidmatan ini;</p>
                <p style="text-align:justify; margin-top:10px;">2.&nbsp;Kos perolehan barangan dan perkhidmatan gantian berikutan pembelian sebarang barangan, data, maklumat atau perkhidmatan atau mesej yang diterima atau transaksi yang dibuat
                    menerusi atau daripada laman web ini;</p>
                <p style="text-align:justify; margin-top:10px;">3.&nbsp;Akses tanpa kebenaran atau perubahan dalam penghantaran atau data anda;</p>
                <p style="text-align:justify; margin-top:10px;">4.&nbsp;Kenyataan atau tindakan pihak ketiga di laman web ini; atau</p>
                <p style="text-align:justify; margin-top:10px;">5.&nbsp;Perkara-perkara lain yang berkaitan dengan laman web ini.</p>
                <p style="text-align:justify; font-weight:500; color:black;">​Pautan</p>
                <p style="text-align:justify;">
                    Pihak Kementerian boleh menyediakan pautan kepada laman web yang lain. Laman-laman web tersebut dimiliki dan dikendalikan oleh pihak ketiga dan oleh itu, pihak Kementerian tidak mempunyai kawalan ke atas laman dan sumber tersebut.
                    Anda mengakui dan bersetuju bahawa pihak Kementerian tidak bertanggungjawab terhadap laman dan sumber luar tersebut dan tidak menyokong serta tidak bertanggungjawab ke atas sebarang kandungan, iklan, produk atau bahan-bahan lain
                    yang disediakan di laman dan sumber tersebut. Anda seterusnya mengakui dan bersetuju bahawa pihak Kementerian tidak bertanggungjawab, secara langsung atau tidak langsung, terhadap sebarang kerosakan atau kerugian yang disebabkan
                    oleh atau dipercayai sebagai sebabnya atau dikaitkan dengan penggunaan atau pergantungan ke atas kandungan, barangan atau perkhidmatan yang disediakan di atau menerusi laman atau sumber tersebut.</p>
                <p style="text-align:justify; font-weight:500; color:black;">​Penamatan</p>
                <p style="text-align:justify;">
                    Pihak Kementerian boleh menamatkan akses anda ke mana-mana bahagian perkhidmatan atau seluruh perkhidmatan dan mana-mana perkhidmatan yang berkaitan pada bila-bila masa, dengan sebab atau tanpa sebab, dengan notis atau tanpa
                    notis, dan berkuatkuasa serta-merta. Pihak Kementerian boleh juga menamatkan atau menggantung akaun anda yang tidak aktif, iaitu, didefinisikan sebagai kegagalan untuk menggunakan perkhidmatan laman web menerusi akaun tersebut
                    bagi suatu tempoh tertentu. Anda bersetuju bahawa pihak Kementerian tidak bertanggungjawab kepada anda atau mana-mana pihak ketiga di atas penamatan akses kepada perkhidmatan tersebut.</p>
                <p style="text-align:justify; font-weight:500; color:black;">Perubahan kepada Syarat-syarat Perkhidmatan</p>
                <p style="text-align:justify;">
                    Pihak Kementerian mempunyai hak untuk membuat perubahan, pengubahsuaian, pembatalan atau penambahan kepada syarat-syarat ini pada bila-bila masa dengan memberi notis terlebih dahulu. Walau bagaimanapun, dalam keadaan kecemasan
                    atau demi menjaga keselamatan laman web atau dalam keadaan di luar kawalan, di mana pihak Kementerian mendapati perlunya untuk mengubah, mengubahsuai, membatal atau menambah syarat-syarat ini, ia akan berbuat demikian tanpa notis
                    terdahulu kepada anda. Adalah dipersetujui bahawa anda akan mengakses dan meneliti syarat-syarat ini dari semasa ke semasa untuk mengetahui sebarang perubahan, pengubahsuaian, pembatalan atau penambahan yang terkini. Anda
                    selanjutnya bersetuju dan menerima bahawa akses secara berterusan dan penggunaan syarat-syarat (yang diubah atau diubahsuai dari semasa ke semasa) akan dianggap sebagai penerimaan anda terhadap sebarang perubahan, pengubahsuaian,
                    pembatalan atau penambahan kepada syarat-syarat ini.Pengubahsuaian kepada Perkhidmatan Pihak Kementerian mempunyai hak untuk mengubahsuai atau menamatkan perkhidmatan (atau sebahagian daripada perkhidmatan) sama ada secara
                    sementara atau tetap pada bila-bila masa dengan notis atau tanpa notis. Anda bersetuju bahawa pihak Kementerian tidak bertanggungjawab kepada anda atau mana-mana pihak ketiga di atas sebarang pengubahsuaian, penggantungan atau
                    penamatan perkhidmatannya.</p>
            </div>
        </div>
    </div>
</section>
@endsection
