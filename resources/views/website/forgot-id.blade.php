@extends('layouts.website-layout')

@section('content')
<div class="view full-page-intro" style="background-image:url('/img/banner/paddy-field.jpg'); background-repeat: no-repeat; background-size: cover;">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
        <div class="container" style="margin-top:2rem; margin-bottom:2rem;">
            <div class="row wow fadeIn">
                <div class="col-md-6 col-xl-5 mb-4" style="margin:auto;">
                    <div class="card">
                        <div class="card-header" style="font-weight:500;background-color:#28a745;color:white;">{{ __('Lupa ID Pengguna') }}</div>
                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif
                            <form class="register-form" method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="ssm_number" class="text-uppercase" style="font-weight:bold;">Nombor SSM <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="right"
                                          title="Contoh: AB123456789. Sila masukkan tanpa (-)"></i></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fas fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <input id="ssm_number" type="ssm_number" class="form-control @error('ssm_number') is-invalid @enderror" name="ssm_number" value="{{ old('ssm_number') }}" required
                                        autocomplete="ssm_number" autofocus>
                                        @error('ssm_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="license_number" class="text-uppercase" style="font-weight:bold;">Nombor Lesen</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fas fa-file"></i>
                                            </span>
                                        </div>
                                        <input id="license_number" type="license_number" class="form-control @error('license_number') is-invalid @enderror" name="license_number" value="{{ old('license_number') }}" required
                                        autocomplete="license_number" autofocus>
                                        @error('license_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div><!-- End Row License Number -->

                                <div class="form-group">
                                    <label for="company_name" class="text-uppercase" style="font-weight:bold;">Nama Syarikat</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fas fa-building"></i>
                                            </span>
                                        </div>
                                        <input id="company_name" type="company_name" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}" required
                                        autocomplete="company_name" autofocus>
                                        @error('company_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div><!-- End Row Company Name -->

                                <div class="form-group">
                                    <label for="email" class="text-uppercase" style="font-weight:bold;">Alamat E-mel</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="ti-email"></i>
                                            </span>
                                        </div>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-3 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Hantar') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endpush
