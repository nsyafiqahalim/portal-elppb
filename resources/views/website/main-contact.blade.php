@extends('layouts.website-layout')

@section('content')
<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent px-0 pt-3">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Hubungi Kami</li>
                <li class="breadcrumb-item active" aria-current="page">Alamat Ibu Pejabat</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Ibu Pejabat Kawalselia Padi dan Beras</h2>
                <hr>
            </div>
        </div>
    </div>
</section>

<div class="aboutus-section">
    <div class="container">
        <div class="row" style="margin-bottom:4rem;margin-top:-30pt;">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="aboutus-banner">
                    <img src="{{ asset('img/gallery/moa.jpg') }}" class="img" style="width:1200;height:400;" alt="Kementerian Pertanian Dan Industri Asas Tani">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="aboutus">
                    <p class="aboutus-text">Seksyen Kawal Selia Padi dan Beras<br>
                        Bahagian Industri Padi Dan Beras<br>
                        Kementerian Pertanian dan Industri Makanan<br>
                        Aras 2, Wisma Tani, No.28 Persiaran Perdana, Presint 4<br>
                        62624 W.P. Putrajaya, Malaysia
                    </p>
                    <p class="aboutus-text"><i class="fas fa-phone-alt mr-3"></i>
                        +603-88701000 &ensp;
                        <i class="fas fa-fax mr-3"></i>
                        +603-88886020 &ensp;
                        <a href="https://www.facebook.com/kpbmoa/" target="_blank">
                            <i class="fab fa-facebook fa-lg" class="facebook"></i>
                        </a>
                    </p>
                    <p class="aboutus-text"><i class="far fa-clock mr-3"></i>
                        <strong>Waktu Urusan:</strong> <br><span style="padding-right:2.2em;"></span>Isnin - Khamis : &nbsp; 8:30 pagi - 4:30 petang
                        <br><span style="padding-right:9.8em;"></span>&nbsp; 1:00 petang - 2:00 petang (rehat)
                        <br><br><span style="padding-right:2.2em;"></span>Jumaat<span style="padding-right:3.3em;"></span>:&ensp; 8:30 pagi - 4:30 petang
                        <br><span style="padding-right:9.8em;"></span>&nbsp; 12:15 tengahari - 2:45 petang (rehat)
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="aboutus">
                    <p class="aboutus-text"><iframe
                          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.6576972577946!2d101.68104441475673!3d2.9144657978772366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb775569457d3%3A0xc832f6606c8f0cc3!2sSeksyen%20Kawal%20Selia%20Padi%20dan%20Beras!5e0!3m2!1sen!2smy!4v1583311501175!5m2!1sen!2smy"
                          width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe></p>
                </div>
            </div>
        </div> <!-- End Row Address -->
        <!--<div class="row" style="margin-bottom:4rem;">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="aboutus-banner">
                        <img src="{{ asset('img/gallery/moa.jpg') }}" class="img" style="width:1250;height:550;" alt="Kementerian Pertanian Dan Industri Asas Tani">
                    </div>
                </div>
            </div> End Row Banner Footer -->
    </div>
</div>


@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/contact-us.css') }}">
@endpush
