@extends('layouts.website-layout')

@section('content')

<section class="border-top">
    <div class="container">
        <nav aria-label="breadcrumb" style="margin-top:-30px;">
            <ol class="breadcrumb bg-transparent px-0 pt-5">
                <li class="breadcrumb-item"><a href="{{ route('website.index') }}">Utama</a></li>
                <li class="breadcrumb-item active" aria-current="page">Aplikasi Mudah Alih</li>
            </ol>
        </nav>
    </div>
</section>

<section style="margin-top:30px; margin-bottom:30px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <h2 class="mt-0 mb-3">Aplikasi Mudah Alih</h2>
                <hr>
            </div>
        </div>
    </div>
</section>

<div class="aboutus-section" style="margin-top:-75pt;">
    <div class="container">
        <div class="row" style="margin-bottom:0.5rem;margin-top:-25px;">
            <div class="col-md-8 col-sm-4 col-xs-12">
                <div class="aboutus">
                    <p class="aboutus-text">Aplikasi Mudah Alih eLPPB 2.0 adalah inovasi baharu yang dibangunkan di bawah Projek Sistem eLPPB 2.0 bagi memudahkan pelesen untuk menyemak status lesen dan permit dan status permohonan lesen dan permit
                        menerusi platform mudah alih dan boleh dicapai di mana-mana lokasi.
                      </p>
                      <p class="aboutus-text" style="margin-top:-20px;margin-bottom:-10px;">Khidmat yang terdapat di dalam Aplikasi Mudah Alih eLPPB 2.0 adalah:
                        <dl style="font-size:12pt;">
                        <dd><span style="padding-right:1.5em;"></span>1. Memaparkan maklumat asas Pelesen</dd>
                        <dd><span style="padding-right:1.5em;"></span>2. Memaparkan maklumat syarikat</dd>
                        <dd><span style="padding-right:1.5em;"></span>3. Status Lesen dan Permit</dd>
                        <dd><span style="padding-right:1.5em;"></span>4. Status Permohonan Lesen dan Permit</dd>
                      </dl></p>
                    <p class="aboutus-text">Aplikasi Mudah Alih eLPPB 2.0 ini boleh dimuat turun melalui telefon pintar dan tablet yang menggunakan platform Android dan iOS.<br><br>
                        <a href="https://apps.apple.com/us/genre/ios-games/id6014" target="_blank"><img src="{{ asset('img/icon/app-store.jpg') }}" style="width:auto;height:auto;" alt="App Store" title="App Store"></a>&ensp;
                        <a href="https://play.google.com/store?hl=en" target="_blank"><img src="{{ asset('img/icon/google-play.jpg') }}" style="width:auto;height:auto;" alt="Google Play Store"></a>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="aboutus-banner">
                    <img src="{{ asset('img/banner/mobile-app-2.png') }}" style="width:auto;height:auto;" align="right" alt="Aplikasi Mudah Alih eLPPB">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/contact-us.css') }}">
@endpush
