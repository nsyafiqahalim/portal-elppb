<head>
    <style>
        .hh {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
            min-height: 100vh;
            background-image: url('img/banner/blackboard.jpg');
            background-size: cover;
            background-position: center;
            position: relative;
        }

        .hh:after {
            display: block;
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 0;
            background-color: rgba(34, 34, 34, 0.5);
        }

        .content {
            position: relative;
            z-index: 1;
        }

        .pp {
            color: #fff;
            font-size: 45px;
            font-weight: 900;
            margin: 30px 0px;
        }

        .countdown {
            color: #fff;
            font-size: 36px;
            font-weight: 400;
            display:flex;
        }
        .countdown div {
          position: relative;
          width: 100px;
          height: 100px;
          line-height: 100px;
          text-align: center;
          background: #333;
          color: #fff;
          margin: 0 15px;
          font-style: 3em;
        }
        .countdown div:before {
          position: absolute;
          content: '';
          bottom: -30px;
          left: 0;
          width: 100%;
          height: 35px;
          color: #333;
          font-style: 0.35em;
          line-height: 35px;
          font-weight: 300;
          background: #ff0;
        }

        .countdown #day:before {
          content: 'Hari';
        }
        .countdown #hour:before {
          content: 'Jam';
        }
        .countdown #minute:before {
          content: 'Minit';
        }
        .countdown #second:before {
          content: 'Saat';
        }
    </style>
</head>

<body>
    <div class="hh">
        <div class="content">
            <h1 class="pp">AKAN DATANG</h1>
            <!--<div class="countdown">
              <div id="day">NA</div>
              <div id="hour">NA</div>
              <div id="minute">NA</div>
              <div id="second">NA</div>
            </div>-->
        </div>
    </div>
    <script type="text/javascript">
    var countDate = new Date('Jan 1, 2020 00:00:00'),getTime();

    function newYear() {
      var now = new Date().getTime();
      gap = countDate - now;

      var second = 1000;
      var minute = second * 60;
      var hour = minute * 60;
      var day = hour * 24;

      var d = Math.floor(gap / (day));
      var h = Math.floor((gap % (day)) / (hour));
      var m = Math.floor((gap % (hour)) / (minute));
      var s = Math.floor((gap % (minute)) / (second));

      document.getElementById('day').innerText = d;
      document.getElementById('hour').innerText = d;
      document.getElementById('minute').innerText = d;
      document.getElementById('second').innerText = d;
    }

    setInterval(function(){
      newYear();
    }, 1000)
    </script>
</body>
</html>
