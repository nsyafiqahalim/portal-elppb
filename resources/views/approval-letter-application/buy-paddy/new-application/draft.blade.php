@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Surat Kelulusan Bersyarat Beli Padi</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Baharu</a></li>
            <li class="breadcrumb-item">Surat Kelulusan Bersyarat Beli Padi</li>
            <li class="breadcrumb-item active">Baharu</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="tab-wizard wizard-circle" id='submission-form' text="Anda Pasti?" method="post" >
                    @csrf
                    @method('PATCH')
                    <input type='hidden' name='company_id' id='company_id' value="@if(isset($inputParam['approval_letter_application_temporary']->company_id)){{ encrypt($inputParam['approval_letter_application_temporary']->company_id)}}@endif" />
                    <input type='hidden' name='premise_id' id='premise_id' value="@if(isset($inputParam['approval_letter_application_temporary']->company_premise_id)){{encrypt($inputParam['approval_letter_application_temporary']->company_premise_id)}}@endif"/>
                    <input type='hidden' name='store_id' id='store_id' value="@if(isset($inputParam['approval_letter_application_temporary']->company_store_id)){{encrypt($inputParam['approval_letter_application_temporary']->company_store_id)}}@endif"/>
                    <input type='hidden' name='user_id' id='user_id' value="{{ encrypt(Auth::user()->id) }}"/>
                     <input type='hidden' name='material_domain' id='material_domain' value="{{ encrypt('BUY_PADDY_NEW') }}"/>
                    <!-- Step 1 -->
                    <h6>Syarikat Dan Rakan Kongsi</h6>
                    <section>
                        @include('approval-letter-application.component.company.draft.add')
                        @include('approval-letter-application.component.partner.draft.add')
                    </section>
                    <!-- Step 2 -->
                    <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('approval-letter-application.component.premise.draft.add')
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                     <h6>Stor</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('approval-letter-application.component.store.draft.add')
                            </div>
                        </div>
                    </section>

                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('approval-letter-application.component.material.draft.add')
                            </div>
                        </div>
                    </section>
                    <!-- Step 4 -->
                    <h6>Maklumat Permohonan</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="example-email-input" class="col-2 col-form-label">Syarat Membeli Padi</label>
                                    <div class="col-6">
                                        <select class="form-control" name='license_application_buy_paddy_condition_id'>
                                                <option value="">Sila Pilih</option>
                                                @foreach($inputParam['license_application_buy_pady_conditions'] as $item)
                                                    <option 
                                                        @if( isset($inputParam['approval_letter_application_buy_paddy']) && $item->id == $inputParam['approval_letter_application_buy_paddy']->license_application_buy_paddy_condition_id)
                                                            selected
                                                        @endif
                                                    value="{{ encrypt($item->id) }}">{{ $item->name }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script type='text/javascript'>   
    $(document).ready(function(){
        $(document).on('click','.selectCompany',function(){
            id = $(this).data('id');
            $('#company_id').val(id);
            $('#company_premise_company_id').val(id);
            $('#company_partner_company_id').val(id);
            $('#company_stor_company_id').val(id);

            $('#company-partner-datatable').DataTable().destroy();
            $('#company-partner-datatable').DataTable(loadCompanyPartnerConfig());
            $('#company-premise-datatable').DataTable().destroy();
            $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig());
            $('#company-store-datatable').DataTable().destroy();
            $('#company-store-datatable').DataTable(loadCompanyStoreConfig());
        // $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig(form)).ajax.reload();
        })

        $(document).on('click','.selectPremise',function(){
            id = $(this).data('id');
            $('#premise_id').val(id);
        })

        $(document).on('click','.selectStore',function(){
            id = $(this).data('id');
            $('#store_id').val(id);
        })

        $(document).on('click','.submitButton',function(){
            $("#submission-form").removeClass("form-data-submission-using-message-bag");
            $("#submission-form").addClass("form-data-submission-with-confirmation-using-message-bag");
            $('#submission-form').attr('action',"{{ route('api.approval_letter_application.buy_paddy.draft.submit',[encrypt($inputParam['approval_letter_application']->id)]) }}");
        })

        $(document).on('click','.draftButton',function(){
            $("#submission-form").removeClass("form-data-submission-with-confirmation-using-message-bag");
            $("#submission-form").addClass("form-data-submission-using-message-bag");
            $('#submission-form').attr('action', "{{ route('api.approval_letter_application.buy_paddy.draft.update',[encrypt($inputParam['approval_letter_application']->id)]) }}");
        })
    }); 
</script>
@endpush

@push('wizard')
<script type='text/javascript'>   
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "Simpan",
            previous: "Kembali",
            next: "Seterusnya"
        }
        , onFinished: function (event, currentIndex) {
            alert("Form Submitted!");

        },
        onStepChanged: function (event, currentIndex, priorIndex) { 
            if(currentIndex == 4){
            $('ul[aria-label=Pagination] a[href="#finish"]').remove();

            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-primary draftButton" value="Simpan"  /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));
            
            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-success submitButton" value="Hantar" /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));

            }
            else {
            $('ul[aria-label=Pagination] input[value="Hantar"]').remove();
            $('ul[aria-label=Pagination] input[value="Simpan"]').remove();
            }
        }
    });
    
</script>
@endpush