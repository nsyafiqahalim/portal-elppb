<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-responsived table-striped" id='company-store-datatable'>
            <thead>
                <tr>
                    <th colspan="5">Maklumat Stor Sedia Ada</th>
                    <th>
                         <button alt="default" type="button" id="add-store" data-toggle="modal" data-target=".store-modal" class="btn btn-success float-right" >
                            Tambah
                        </button>
                    </th>
                </tr>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Hak Milik</th>
                    <th style='text-align:center'>Pilih</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('modal')
<div class="modal fade-scale store-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' method='post' action="{{ route('api.license_application.company-store.store') }}">
                <input type="hidden" id="company_store_company_id" name='company_id' value="" class="form-control" >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Stor</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <h3 class="box-title">Alamat Stor Perniagaan</h3>
                    <hr>
                    <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Alamat 1</label>
                                    <input type="text" value="" class="form-control" name='store_address_1' id='store_address_1'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 ">
                                <div class="form-group">
                                    <label>Alamat 2</label>
                                    <input type="text" value="" class="form-control" name='store_address_2' id='store_address_2'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Alamat 3</label>
                                    <input type="text" value="" class="form-control" name='store_address_3' id='store_address_3'>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id='store_postcode_label'>Poskod</label>
                                    <input type="text" name='store_postcode'  id='store_postcode'class="form-control"  >
                                </div>
                            </div>

                            <!--/span-->
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id='store_state_id_label'>Negeri</label>
                                    <select class="form-control date" id='store_state_id' name='store_state_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['states'] as $state)
                                            <option value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Daerah</label>
                                <select class="form-control date" id='store_district_id' name='store_district_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['districts'] as $district)
                                        <option value="{{ encrypt($district->id) }}">{{ $district->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div> 

                    <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="store_phone_number_label" >Nombor Telefon <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='phone_number' id='store_phone_number'>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="email_label" >Emel</label>
                                <input type="text" value="" class="form-control" name='email' id='store_email'>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="fax_number_label" >Faks</label>
                                <input type="text" value="" class="form-control" name='fax_number' id='fax_number'>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Jenis Bangunan</label>
                                <select class="form-control date" id='building_type_id' name='building_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['building_types'] as $buildingType)
                                        <option value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Jenis Hak Milik</label>
                                <select class="form-control date" id='store_ownership_type_id' name='store_ownership_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['store_ownership_types'] as $storeOwnershipType)
                                        <option value="{{ encrypt($storeOwnershipType->id) }}">{{ $storeOwnershipType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush
@push('js')
<script>
    function loadCompanyStoreConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        form["include_license_id"] = $('#include_license_id').val();
        form["store_id"] = $('#store_id').val();
        
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_application.company-store.buy-paddy-datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'company_name',
                    name: 'company_name'
                },
                {
                    data: 'address',
                    name: 'address'
                },
                {
                    data: 'building_type',
                    name: 'building_type'
                },
                {
                    data: 'store_ownership_type',
                    name: 'store_ownership_type'
                },
                
                {
                    data: 'action',
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
