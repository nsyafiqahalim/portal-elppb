<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6">Maklumat Stor Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil.</td>
                    <td style='text-align:left'>Nama Syarikat </td>
                    <td style='text-align:left'>Alamat </td>
                    <td style='text-align:center'>Jenis Bangunan</td>
                    <td style='text-align:center'>Jenis Hak Milik</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1. </td>
                    <td style='text-align:left'>{{ $inputParam['license_application_company']->name }} </td>
                    <td style='text-align:left'>
                        {{ $inputParam['license_application_store']->address_1 }}, <br/>
                        {{ $inputParam['license_application_store']->address_2 }},<br/>
                        @if($inputParam['license_application_store']->address_3)
                        {{ $inputParam['license_application_store']->address_2 }},<br/>
                        @endif
                        {{ $inputParam['license_application_store']->postcode }},<br/>
                        {{ $inputParam['license_application_store']->state_name }}
                    </td>
                    <td style='text-align:center'> {{ $inputParam['license_application_store']->building_type_name }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_store']->storeOwnershipType->name }}</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
