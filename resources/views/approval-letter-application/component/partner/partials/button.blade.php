
<button type="button" class="btn btn-md btn-info edit-company-partner" data-id="{{ $id }}">
    Kemaskini
</button>

<a class="deletion" href="{{ route('api.license_application.company-partner.delete',[$id]) }}">
    <button type="button" class="btn btn-md btn-danger">
        Padam
    </button>
</a>