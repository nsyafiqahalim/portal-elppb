 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="8">Maklumat Rakan Kongsi Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <thead>
                    <tr style='text-align:center'>
                        <td>Bil.</td>
                        <td style='text-align:center'>Nama</td>
                        <td style='text-align:center'>No<br/> K/P</td>
                        <td style='text-align:center'>No<br/> Telefon</td>
                        <td style='text-align:center'>Bangsa</td>
                        <td style='text-align:center'>Status<br/> Pemilikan</td>
                        <td style='text-align:center'>Warganegara</td>
                        <td style='text-align:center'>Jumlah<br/> Syer(%)</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($inputParam['license_application_partners'] as $index => $licenseApplicationPartner)
                <tr>
                    <td style='text-align:left'>{{ $index + 1 }}.</td>
                    <td style='text-align:center'>{{ $licenseApplicationPartner->name }}</td>
                    <td style='text-align:center'>{{ $licenseApplicationPartner->nric }}</td>
                    <td style='text-align:center'>{{ $licenseApplicationPartner->phone_number }}</td>
                    <td style='text-align:center'>{{ $licenseApplicationPartner->race_name }}</td>
                    <td style='text-align:center'>{{ $licenseApplicationPartner->race_type_name }}</td>
                    <td style='text-align:center'>{{ $licenseApplicationPartner->citizen_name }}</td>
                    <td style='text-align:center'>{{ number_format($licenseApplicationPartner->total_share,2) }}</td>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </tbody>
        </table>
        </div>
    </div>
</div>