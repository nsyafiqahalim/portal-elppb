<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped" id='company-partner-datatable'>
            <thead>
                <tr>
                    <th colspan="7">Maklumat Rakan Kongsi Sedia Ada</th>
                    <th>
                         <button alt="default" id="add-partner" type="button" data-toggle="modal" data-target=".rakan-kongsi" class="btn btn-success float-right" >
                            Tambah
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Bil.</th>
                    <th style='text-align:center'>Nama</th>
                    <th style='text-align:center'>No<br/> K/P</th>
                    <th style='text-align:center'>No<br/> Telefon</th>
                    <th style='text-align:center'>Bangsa</th>
                    <th style='text-align:center'>Warganegara</th>
                    <th style='text-align:center'>Jumlah<br/> Syer(%)</th>
                    <th style='text-align:center'>Tindakan</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('modal')
<div class="modal fade-scale rakan-kongsi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' method='post' action="{{ route('api.license_application.company-partner.store') }}">
                <input type="hidden" id="user_id" name='user_id' class="form-control" placeholder="" value="{{ encrypt(Auth::user()->id )}}" >
                <input type="hidden" id="company_partner_company_id" name='company_id' class="form-control" >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Rakan Kongsi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h3 class="card-title">Maklumat Rakan Kongsi</h3>
                    <hr>
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id='partner_name_label' class="control-label">Nama  <span class='required'>*</span></label>
                                <input type="text" class="form-control" name="partner_name" id="partner_name" />
                                <small class="form-control-feedback"> </small> </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" id="partner_race_type_id_label">Jenis Bangsa  <span class='required'>*</span></label>
                                <select class="form-control date" id='partner_race_type_id' name='partner_race_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['race_types'] as $raceType)
                                        <option value="{{ encrypt($raceType->id) }}">{{ $raceType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" id="partner_race_id_label">Bangsa  <span class='required'>*</span></label>
                                <select class="form-control date" id='partner_race_id' name='partner_race_id'>
                                    <option value="">Sila Pilih</option>
                                    <!---
                                    @foreach($inputParam['races'] as $race)
                                        <option value="{{ encrypt($race->id) }}">{{ $race->name }}</option>
                                    @endforeach
                                    --->
                                <select>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" id="partner_is_citizen_label"> Warganegara  <span class='required'>*</span></label>
                                <select class="form-control date" id='partner_is_citizen' name='partner_is_citizen'>
                                    <option value="">Sila Pilih</option>
                                    <option value='{{ encrypt(1) }}'>Warganegara</option>
                                    <option value='{{ encrypt(0) }}'>Bukan Warganegara</option>
                                <select>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label id='partner_ic_label' class="control-label">Kad Pengenalan  <span class='required'>*</span></label>
                                <input type="text" class="form-control" name="partner_ic" id="partner_ic" />
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label id='partner_phone_number_label' class="control-label">Nombor Telefon  <span class='required'>*</span></label>
                                <input type="text" class="form-control" name="partner_phone_number" id="partner_phone_number" />
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label id='partner_email_label' class="control-label">Emel  <span class='required'>*</span></label>
                                <input type="text" class="form-control" name="partner_email" id="partner_email"/>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" id="partner_total_share_label">Jumlah Syer (RM)  <span class='required'>*</span></label>
                                <input type="text" class="form-control" name="partner_total_share" id="partner_total_share" placeholder="" >
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" id="partner_share_percentage_label">Peratus Syer (%)  <span class='required'>*</span></label>
                                <input type="text" class="form-control" name="partner_share_percentage"  id="partner_share_percentage" placeholder="" >
                            </div>
                        </div>
                    </div>
                    <h3 class="box-title mt-5">Alamat</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                                <label id="partner_address_1_label">Alamat 1  <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='partner_address_1' id='partner_address_1'>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <div class="form-group">
                                <label id="partner_address_2_label">Alamat 2</label>
                                <input type="text" value="" class="form-control" name='partner_address_2' id='partner_address_2'>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label id="partner_address_3_label">Alamat 3</label>
                                <input type="text" value="" class="form-control" name='partner_address_3' id='partner_address_3'>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label id="partner_postcode_label" >Poskod  <span class='required'>*</span></label>
                                <input type="text" name='partner_postcode' id="partner_postcode" class="form-control"  >
                            </div>
                        </div>

                        <!--/span-->
                        <div class="col-md-12 col-lg-3 col-sm-12">
                            <div class="form-group">
                                <label id='partner_state_id_label'>Negeri  <span class='required'>*</span></label>
                                <select class="form-control date" id='partner_state_id' name='partner_state_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['states'] as $state)
                                        <option value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
                </div>
            </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush
@push('js')
<script>

    $('#company-partner-datatable').DataTable(
        loadCompanyPartnerConfig()
    );

    $( "#partner_race_type_id" ).change(function() {
        var raceTypeId = $("#partner_race_type_id option:selected").val();
        $('#partner_race_id').empty();
        $('#partner_race_id').append('<option value="">Sila Pilih</option>');
        $.ajax({
                type: "GET",
                url: "{{ route('api.race.find_race_by_race_type_id',['']) }}/"+raceTypeId,
                success: function(data){
                
                    // Use jQuery's each to iterate over the opts value
                    
                    $.each(data, function(i, d) {
                        console.log(1);
                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#partner_race_id').append('<option value="' + d.id + '">' + d.label + '</option>');
                    });
                }
            });
    });

    function  loadCompanyPartnerConfig(form) {
        if (form == null) {
            form = {};
        }

        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
            "ajax": {
                "url": "{{ route('api.license_application.company-partner.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex', className: "text-centre",
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name', className: "text-centre",
                    name: 'name'
                },
                {
                    data: 'nric', className: "text-centre",
                    name: 'nric'
                },
                {
                    data: 'phone_number', className: "text-centre",
                    name: 'phone_number'
                },
                {
                    data: 'race', className: "text-centre",
                    name: 'race'
                },
                {
                    data: 'citizen', className: "text-centre",
                    name: 'citizen'
                },
                {
                    data: 'share_percentage', className: "text-centre",
                    name: 'share_percentage'
                },
                {
                    data: 'action', className: "text-centre",
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
