<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped" id='company-premise-datatable'>
            <thead>
                <tr>
                    <th colspan="7">Maklumat Premis Sedia Ada</th>
                    <th>
                         <button alt="default" type="button" id="add-premise" data-toggle="modal" data-target=".premise-modal" class="btn btn-success float-right" >
                            Tambah
                        </button>
                    </th>
                </tr>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Syarikat</th>
                    <th style='text-align:center'>Dewan Undangan Negeri (DUN)</th>
                    <th style='text-align:center'>Parlimen</th>
                    <th style='text-align:center'>Pilih</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('modal')
<div class="modal fade-scale premise-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' method='post' action="{{ route('api.license_application.company-premise.store') }}">
                <input type="hidden" id="company_premise_company_id" name='company_id' value="" class="form-control" >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Premis</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <h3 class="box-title">Alamat Premis Perniagaan</h3>
                    <hr>
                    <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>Alamat 1</label>
                                    <input type="text" value="" class="form-control" name='address_1' id='address_1'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>Alamat 2</label>
                                    <input type="text" value="" class="form-control" name='address_2' id='address_2'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Alamat 3</label>
                                    <input type="text" value="" class="form-control" name='address_3' id='address_3'>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Poskod</label>
                                    <input type="text" name='postcode' class="form-control"  >
                                </div>
                            </div>

                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Negeri</label>
                                    <select class="form-control date" id='state_id' name='state_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['states'] as $state)
                                            <option value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Parlimen</label>
                                <select class="form-control date" id='parliament_id' name='parliament_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['parliaments'] as $parliament)
                                        <option value="{{ encrypt($parliament->id) }}">{{ $parliament->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Dewan Undangan Negeri (DUN)</label>
                                <select class="form-control date" id='dun_id' name='dun_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['duns'] as $dun)
                                        <option value="{{ encrypt($dun->id) }}">{{ $dun->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div> 


                    <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Jenis Bangunan</label>
                                <select class="form-control date" id='building_type_id' name='building_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['building_types'] as $buildingType)
                                        <option value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Jenis Perniagaan</label>
                                <select class="form-control date" id='business_type_id' name='business_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['business_types'] as $businessType)
                                        <option value="{{ encrypt($businessType->id) }}">{{ $businessType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-submit waves-effect text-left">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush
@push('js')
<script>    
    function loadCompanyPremiseConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        form["include_license_id"] = $('#include_license_id').val();
        form["premise_id"] = $('#premise_id').val();

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_application.company-premise.buy-paddy-datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'company_name',
                    name: 'company_name'
                },
                {
                    data: 'address',
                    name: 'address'
                },
                {
                    data: 'building_type',
                    name: 'building_type'
                },
                {
                    data: 'business_type',
                    name: 'business_type'
                },
                
                {
                    data: 'dun',
                    name: 'dun'
                },

                {
                    data: 'parliament',
                    name: 'parliament'
                },
                
                {
                    data: 'action',
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
