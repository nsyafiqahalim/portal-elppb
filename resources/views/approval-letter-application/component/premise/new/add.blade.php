<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
           <table class="table table-bordered table-responsived table-striped" id='company-premise-datatable' style='width:100%'>
            <thead>
                <tr>
                    <th colspan="9">Maklumat Premis Sedia Ada
                         <button alt="default" type="button" id="add-company-premise" data-toggle="modal" data-target=".premise-modal" class="btn btn-success float-right" >
                            Tambah
                        </button>
                    </th>
                </tr>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Syarikat</th>
                    <th style='text-align:center'>Dewan Undangan Negeri (DUN)</th>
                    <th style='text-align:center'>Parlimen</th>
                    <th style='text-align:center'>Pilih</th>
                    <th style='text-align:center'>Kemaskini</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('modal')
<div class="modal fade-scale premise-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' id='premise-form' method='post'>
                <input type="hidden" id="company_premise_company_id" name='company_id' class="form-control" >
                <input type="hidden" id="company_premise_method" name='_method' >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Premis</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                <h3 class="box-title">Alamat Premis Perniagaan</h3>
                    <hr>
                    <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label id="premise_address_1_label">Alamat 1 <span class='required'>*</span></label>
                                    <input type="text" value="" class="form-control" name='premise_address_1' id='premise_address_1'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 ">
                                <div class="form-group">
                                    <label id="premise_address_2_label">Alamat 2</label>
                                    <input type="text" value="" class="form-control" name='premise_address_2' id='premise_address_2'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id="premise_address_3_label">Alamat 3</label>
                                    <input type="text" value="" class="form-control" name='premise_address_3' id='premise_address_3'>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id="premise_postcode_label">Poskod <span class='required'>*</span></label>
                                    <input type="text" name='premise_postcode' id="premise_postcode" class="form-control" maxLength='5'  >
                                </div>
                            </div>

                            <!--/span-->
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id="premise_state_id_label">Negeri <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_state_id' name='premise_state_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['states'] as $state)
                                            <option data-code='{{ $state->code }}' value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label id="premise_district_id_label">Daerah <span class='required'>*</span></label>
                                <select class="form-control date" id='premise_district_id' name='premise_district_id'>
                                    <option value="">Sila Pilih</option>
                                <select>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label id="premise_parliament_id_label">Parlimen <span class='required'>*</span></label>
                                <select class="form-control date" id='premise_parliament_id' name='premise_parliament_id'>
                                    <option value="">Sila Pilih</option>
                                <select>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label id="premise_dun_id_label">Dewan Undangan Negeri (DUN) <span class='required'>*</span></label>
                                <select class="form-control date" id='premise_dun_id' name='premise_dun_id'>
                                    <option value="">Sila Pilih</option>
                                <select>
                            </div>
                        </div>
                    </div> 

                    <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="premise_phone_number_label" >Nombor Telefon <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='premise_phone_number' id='premise_phone_number' maxlength="10">
                                <small>Masukkan nombor telefon tanpa '-'  sebagai contoh : 0123033563</small><br/>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="premise_fax_number_label" >Nombor Faks</label>
                                <input type="text" value="" class="form-control" name='premise_fax_number' id='premise_fax_number' maxlength="10">
                                 <small>Masukkan nombor faks tanpa '-'  sebagai contoh : 0123033563</small><br/>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="premise_email_label" >Emel <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='premise_email' id='premise_email'>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label id="premise_building_type_id_label">Jenis Bangunan <span class='required'>*</span></label>
                                <select class="form-control date" id='premise_building_type_id' name='premise_building_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['building_types'] as $buildingType)
                                        <option data-code='{{ $buildingType->code }}' value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label id="premise_business_type_id_label">Jenis Perniagaan <span class='required'>*</span></label>
                                <select class="form-control date" id='premise_business_type_id' name='premise_business_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['business_types'] as $businessType)
                                        <option data-code='{{ $businessType->code }}' value="{{ encrypt($businessType->id) }}">{{ $businessType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush
@push('js')
<script>
    $(document).on('click','.edit-premise',function(){
        var premiseId = $(this).data('id');
        $('#premise-form').attr('action', "{{ route('api.license_application.company-premise.update',['']) }}/"+premiseId);
        $('#company_premise_method').val('PATCH');
        $('.premise-modal').modal('toggle');

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company-premise.find_company_premise_detail_by_premise_id',['']) }}/"+premiseId,
            success: function(data){
                $('#premise_name').val(data.name);
                $('#premise_mycoid').val(data.registration_number);
                $('#premise_expiry_date').val(data.formatted_expiry_date);
                $('#premise_paidup_capital').val(data.paidup_capital);
                $('#premise_address_1').val(data.address_1);
                $('#premise_address_2').val(data.address_2);
                $('#premise_address_3').val(data.address_3);
                $('#premise_postcode').val(data.postcode);
                $('#premise_phone_number').val(data.phone_number);
                $('#premise_fax_number').val(data.fax_number);
                $('#premise_email').val(data.email);
                $("#premise_building_type_id option[data-code='" + data.building_type_code +"']").attr("selected","selected");
                $("#premise_business_type_id option[data-code='" + data.business_type_code +"']").attr("selected","selected");
                $("#premise_state_id option[data-code='" + data.state_code +"']").attr("selected","selected");
                
                loadDistrictAndParliamentByStateId(data.state_id, data.district_code, data.parliament_code);
                loadDunBasedOnParliamentId(data.parliament_id, data.dun_code );
            }
        });
    })

    $('#add-company-premise').click(function(){
        userId = $('#premise_user_id').val();
        $('#premise-form').trigger("reset");
        $('#premise_user_id').val(userId);
        $('#premise-form').attr('action', "{{ route('api.license_application.company-premise.store') }}");
        $('#company_premise_method').val('POST');
        $('#premise_building_type_id').prop('selectedIndex',0);
        $('#premise_business_type_id').prop('selectedIndex',0);
        $('#premise_state_id').prop('selectedIndex',0);
        $('#premise_dun_id').empty();
        $('#premise_district_id').empty();
        $('#premise_parliament_id').empty();

        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');
        $('#premise_district_id').append('<option value="">Sila Pilih</option>');
        $('#premise_parliament_id').append('<option value="">Sila Pilih</option>');
    });


    $( "#premise_state_id" ).change(function() {
        var stateId = $("#premise_state_id option:selected").val();
        $('#premise_district_id').empty();
        $('#premise_district_id').append('<option value="">Sila Pilih</option>');
        
        $('#premise_parliament_id').empty();
        $('#premise_parliament_id').append('<option value="">Sila Pilih</option>');

        $('#premise_dun_id').empty();
        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');

        loadDistrictAndParliamentByStateId(stateId);
    });

    function loadDistrictAndParliamentByStateId(stateId, districtCode = null, parliamentCode = null){
        $.ajax({
            type: "GET",
            url: "{{ route('api.state.find_district_and_parliament_by_id',['']) }}/"+stateId,
            success: function(data){
                // Use jQuery's each to iterate over the opts value
                $.each(data['districts'], function(i, d) {
                    selected = null;
                    if(d.code == districtCode){
                        selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#premise_district_id').append('<option data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });

                $.each(data['parliaments'], function(i, d) {
                    selected = null;
                    if(d.code == parliamentCode){
                        selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#premise_parliament_id').append('<option  data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });
            }
        });
    }

    $( "#premise_parliament_id" ).change(function() {
        var parliamentId = $("#premise_parliament_id option:selected").val();
        $('#premise_dun_id').empty();
        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');
         loadDunBasedOnParliamentId(parliamentId);
    });

    function loadDunBasedOnParliamentId(parliamentId, dunCode = null){
        $.ajax({
            type: "GET",
            url: "{{ route('api.dun.find_dun_by_parliament_id',['']) }}/"+parliamentId,
            success: function(data){
                $.each(data, function(i, d) {
                    selected = null;
                    if(d.code == dunCode){
                            selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#premise_dun_id').append('<option  data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });
            }
        });
    }

    $('#company-premise-datatable').DataTable(
        {
             "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
         }
    );
    
    function loadCompanyPremiseConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        form["include_license_id"] = $('#include_license_id').val();

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_application.company-premise.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex', className: "text-centre",
                    name: 'DT_RowIndex'
                },
                {
                    data: 'company_name', className: "text-centre",
                    name: 'company_name'
                },
                {
                    data: 'address', className: "text-justify",
                    name: 'address'
                },
                {
                    data: 'building_type', className: "text-centre",
                    name: 'building_type'
                },
                {
                    data: 'business_type', className: "text-centre",
                    name: 'business_type'
                },
                
                {
                    data: 'dun', className: "text-centre",
                    name: 'dun'
                },

                {
                    data: 'parliament', className: "text-centre",
                    name: 'parliament'
                },
                
                {
                    data: 'action', className: "text-centre",
                    name: 'action'
                },
                {
                    data: 'edit', className: "text-centre",
                    name: 'edit'
                },
            ]

        };

        return json;
    }
</script>
@endpush