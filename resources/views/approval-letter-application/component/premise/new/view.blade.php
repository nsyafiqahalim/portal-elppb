<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="8">Maklumat Premis Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil.</td>
                    <td style='text-align:left'>Nama Syarikat </td>
                    <td style='text-align:left'>Alamat </td>
                    <td style='text-align:center'>Jenis Bangunan</td>
                    <td style='text-align:center'>Jenis Syarikat</td>
                    <td style='text-align:center'>Dewan Undangan Negeri (DUN)</td>
                    <td style='text-align:center'>Parlimen</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1. </td>
                    <td style='text-align:left'>{{ $inputParam['license_application_company']->name }} </td>
                    <td style='text-align:left'>
                        {{ $inputParam['license_application_premise']->address_1 }}, <br/>
                        {{ $inputParam['license_application_premise']->address_2 }},<br/>
                        @if($inputParam['license_application_premise']->address_3)
                        {{ $inputParam['license_application_premise']->address_2 }},<br/>
                        @endif
                        {{ $inputParam['license_application_premise']->postcode }},<br/>
                        {{ $inputParam['license_application_premise']->state_name }}
                    </td>
                    <td style='text-align:center'> {{ $inputParam['license_application_premise']->building_type_name }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_premise']->business_type_name }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_premise']->dun_name }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_premise']->parliament_name }}</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
