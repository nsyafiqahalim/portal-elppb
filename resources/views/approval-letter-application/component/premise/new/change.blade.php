<div class="form-body">
    <h3 class="box-title">Alamat Premis Perniagaan</h3>
    <hr>
    <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="form-group">
                    <label id="premise_address_1_label">Alamat 1 <span class='required'>*</span></label>
                    <input type="text"  class="form-control" value="{{ $inputParam['license_application_premise']->address_1 }}" name='premise_address_1' id='premise_address_1'>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12 ">
                <div class="form-group">
                    <label id="premise_address_2_label">Alamat 2</label>
                    <input type="text"  class="form-control" value="{{ $inputParam['license_application_premise']->address_2 }}" name='premise_address_2' id='premise_address_2'>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label id="premise_address_3_label">Alamat 3</label>
                    <input type="text"  class="form-control" value="{{ $inputParam['license_application_premise']->address_3 }}" name='premise_address_3' id='premise_address_3'>
                </div>
            </div>

            <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label id="premise_postcode_label">Poskod <span class='required'>*</span></label>
                    <input type="text" name='premise_postcode' id="premise_postcode" digit='5' value="{{ $inputParam['license_application_premise']->postcode }}" class="form-control"  >
                </div>
            </div>

            <!--/span-->
            <div class="col-md-4 col-xs-12">
                <div class="form-group">
                    <label id="premise_state_id_label">Negeri <span class='required'>*</span></label>
                    <select class="form-control date" id='premise_state_id' name='premise_state_id'>
                        <option value="">Sila Pilih</option>
                        @foreach($inputParam['states'] as $state)
                            <option 
                            @if($inputParam['license_application_premise']->state_id == $state->id)
                                selected
                            @endif
                            value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                        @endforeach
                    <select>
                </div>
            </div>
            <!--/span-->
        </div>
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label id="premise_district_id_label">Daerah <span class='required'>*</span></label>
                <select class="form-control date" id='premise_district_id' name='premise_district_id'>
                    <option value="">Sila Pilih</option>
                    @foreach($inputParam['districts'] as $district)
                        <option 
                            @if($inputParam['license_application_premise']->district_id == $district->id)
                                selected
                            @endif
                        value="{{ encrypt($district->id) }}">{{ $district->name }} {{ $inputParam['license_application_premise']->district_id }} </option>
                    @endforeach
                <select>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label id="premise_parliament_id_label">Parlimen <span class='required'>*</span></label>
                <select class="form-control date" id='premise_parliament_id' name='premise_parliament_id'>
                    <option value="">Sila Pilih</option>
                    @foreach($inputParam['parliaments'] as $parliament)
                        <option
                            @if($inputParam['license_application_premise']->parliament_id == $parliament->id)
                                selected
                            @endif
                        value="{{ encrypt($parliament->id) }}">{{ $parliament->name }}</option>
                    @endforeach
                <select>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="form-group">
                <label id="premise_dun_id_label">Dewan Undangan Negeri (DUN) <span class='required'>*</span></label>
                <select class="form-control date" id='premise_dun_id' name='premise_dun_id'>
                    <option value="">Sila Pilih</option>
                    @foreach($inputParam['duns'] as $dun)
                        <option 
                        
                        @if($inputParam['license_application_premise']->dun_id == $dun->id)
                            selected
                        @endif

                        value="{{ encrypt($dun->id) }}">{{ $dun->name }}</option>
                    @endforeach
                <select>
            </div>
        </div>
    </div> 


    <h3 class="box-title mt-5">Maklumat Tambahan</h3>
    <hr>
    <div class="row">
        <div class="col-md-4 col-lg-6 col-sm-12">
            <div class="form-group">
                <label id="premise_phone_number_label" >Nombor Telefon <span class='required'>*</span></label>
                <input type="text" class="form-control" name='premise_phone_number' id='premise_phone_number' value="{{ $inputParam['license_application_premise']->phone_number }}" maxlength="10">
                <small>Masukkan nombor telefon tanpa '-'  sebagai contoh : 0123033563</small><br/>
            </div>
        </div>

        <div class="col-md-4 col-lg-6 col-sm-12">
            <div class="form-group">
                <label id="premise_fax_number_label" >Nombor Faks</label>
                <input type="text" class="form-control" name='premise_fax_number' id='premise_fax_number' value="{{ $inputParam['license_application_premise']->fax_number }}" maxlength="10">
                    <small>Masukkan nombor faks tanpa '-'  sebagai contoh : 0123033563</small><br/>
            </div>
        </div>

        <div class="col-md-4 col-lg-6 col-sm-12">
            <div class="form-group">
                <label id="premise_email_label" >Emel <span class='required'>*</span></label>
                <input type="text" class="form-control" name='premise_email' value="{{ $inputParam['license_application_premise']->email }}" id='premise_email'>
            </div>
        </div>

        <div class="col-md-6 ">
            <div class="form-group">
                <label id="premise_building_type_id_label">Jenis Bangunan <span class='required'>*</span></label>
                <select class="form-control date" id='premise_building_type_id' name='premise_building_type_id'>
                    <option value="">Sila Pilih</option>
                    @foreach($inputParam['building_types'] as $buildingType)
                        <option 
                        @if($inputParam['license_application_premise']->building_type_id == $buildingType->id)
                            selected
                        @endif
                        value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                    @endforeach
                <select>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <label id="premise_business_type_id_label">Jenis Perniagaan <span class='required'>*</span></label>
                <select class="form-control date" id='premise_business_type_id' name='premise_business_type_id'>
                    <option value="">Sila Pilih</option>
                    @foreach($inputParam['business_types'] as $businessType)
                        <option 
                        @if($inputParam['license_application_premise']->business_type_id == $businessType->id)
                            selected
                        @endif

                        value="{{ encrypt($businessType->id) }}">{{ $businessType->name }}</option>
                    @endforeach
                <select>
            </div>
        </div>
    </div>
</div>
@push('js')
<script>
    $( "#premise_state_id" ).change(function() {
        var stateId = $("#premise_state_id option:selected").val();
        $('#premise_district_id').empty();
        $('#premise_district_id').append('<option value="">Sila Pilih</option>');
        
        $('#premise_parliament_id').empty();
        $('#premise_parliament_id').append('<option value="">Sila Pilih</option>');

        $('#premise_dun_id').empty();
        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');

        $.ajax({
                type: "GET",
                url: "{{ route('api.state.find_district_and_parliament_by_id',['']) }}/"+stateId,
                success: function(data){
                    // Use jQuery's each to iterate over the opts value
                    $.each(data['districts'], function(i, d) {
                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#premise_district_id').append('<option value="' + d.id + '">' + d.label + '</option>');
                    });

                    $.each(data['parliaments'], function(i, d) {
                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#premise_parliament_id').append('<option value="' + d.id + '">' + d.label + '</option>');
                    });
                }
            });
    });

    $( "#premise_parliament_id" ).change(function() {
        var raceTypeId = $("#premise_parliament_id option:selected").val();
        $('#premise_dun_id').empty();
        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');
        $.ajax({
                type: "GET",
                url: "{{ route('api.dun.find_dun_by_parliament_id',['']) }}/"+raceTypeId,
                success: function(data){
                
                    // Use jQuery's each to iterate over the opts value
                    
                    $.each(data, function(i, d) {
                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#premise_dun_id').append('<option value="' + d.id + '">' + d.label + '</option>');
                    });
                }
            });
    });
</script>
@endpush