 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="7">Maklumat Syarikat Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil. </td>
                    <td style='text-align:center'>Nama <br/>Syarikat.</td>
                    <td style='text-align:center'>Nombor <br/>Pendaftaran.</td>
                    <td style='text-align:center'>Jenis <br/>Syarikat</td>
                    <td style='text-align:center'>Tarikh <br/>Mansuh</td>
                    <td style='text-align:right'>Modal Berbayar <br/>(RM) </td>
                    <td style='text-align:center'>Alamat</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1.</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_company']->name }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_company']->registration_number }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_company']->company_type_name }}</td>
                    <td style='text-align:center'>{{ $inputParam['license_application_company']->expiry_date->format('d/m/Y') }}</td>
                    <td style='text-align:right'>{{ number_format($inputParam['license_application_company']->paidup_capital,2) }}</td>
                    <td style='text-align:center'>
                        {{ $inputParam['license_application_address']->address_1 }}, <br/>
                        {{ $inputParam['license_application_address']->address_2 }},<br/>
                        @if($inputParam['license_application_address']->address_3)
                        {{ $inputParam['license_application_address']->address_2 }},<br/>
                        @endif
                        {{ $inputParam['license_application_address']->postcode }},<br/>
                        {{ $inputParam['license_application_address']->state_name }}
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
