 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6">Maklumat Syarikat Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil. </td>
                    <td style='text-align:center'>Nama <br/>Syarikat.</td>
                    <td style='text-align:center'>Jenis <br/>Syarikat</td>
                    <td style='text-align:center'>Tarikh <br/>Mansuh</td>
                    <td style='text-align:right'>Modal Berbayar <br/>(RM) </td>
                    <td style='text-align:center'>Alamat</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1.</td>
                    <td style='text-align:center'>Segi Berhad</td>
                    <td style='text-align:center'>SDN BHD</td>
                    <td style='text-align:center'>10/10/2017</td>
                    <td style='text-align:right'>2,100.00</td>
                    <td style='text-align:center'>NO 10, <br/>KAMPONG,<br/>52100 BATU CAVES, KEDAH</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
<div class="modal fade-scale bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Kemaskini Syarikat</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h3 class="card-title">Maklumat Syarikat</h3>
                <hr>
                <div class="row pt-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Nama Syarikat</label>
                            <input type="text" id="firstName" class="form-control" value="Ali Baba">
                            <small class="form-control-feedback"> </small> </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">No Pendaftaran/ MYCOID</label>
                            <input type="text" class="form-control col-md-8" disabled value="1232422" >
                            -
                            <input type="text" class="form-control col-md-3" disabled value="D" >
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Jenis Syarikat</label>
                            <select class="form-control" >
                                <option>PERSENDIRIAN</option>
                                <option>SENDIRIAN BERHAD</option>
                                <option>KOPERASI</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Tarikh Mansuh (Cth: 23/12/2016)</label>
                            <input type="text" class="form-control" disabled value="10/10/2019" >
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Modal Berbayar (RM)</label>
                            <input type="text" class="form-control"  value="2,100.00" >
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 ">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th colspan="5">Maklumat Syarikat Sedia Ada</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style='text-align:center'>
                                    <td>Nama Syarikat</td>
                                    <td>No Pendaftaran/ MYCOID</td>
                                    <td>Jenis Syarikat</td>
                                    <td>Tarikh Mansuh (Ctd: 23/12/2016)</td>
                                    <td>Modal Berbayar (RM)</td>
                                </tr>
                                <tr style='text-align:center'>
                                    <td>Ali Baba</td>
                                    <td>1232422-D</td>
                                    <td>PERSENDIRIAN</td>
                                    <td>10/10/2019</td>
                                    <td>2,100.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Alamat 1</label>
                            <input type="text" value="NO 10" class="form-control" disabled >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Alamat 2</label>
                            <input type="text" value="KAMPONG" class="form-control" disabled >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Bandar</label>
                            <input type="text" value="BATU CAVES" class="form-control" disabled >
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Poskod</label>
                            <input type="text" VALUE="52100" class="form-control" disabled >
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Negeri</label>
                            <input type="text" value="KEDAH" class="form-control" disabled >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombor Telefon</label>
                            <input type="text" value="+601-232323" class="form-control" disabled >
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Emel</label>
                            <input type="text" VALUE="test@test.com" class="form-control" disabled >
                        </div>
                    </div>
                </div>

            <div id="overlayermap" class="gmaps"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success waves-effect text-left" data-dismiss="modal">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>