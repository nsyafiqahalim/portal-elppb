<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <table class="table display table-bordered table-responsived table-striped"id='company-datatable' style='width:100%'>
                <thead>
                    <tr>
                        <th colspan="8">
                            Maklumat Syarikat Sedia Ada
                            <button type="button" data-toggle="modal" data-target=".company-modal" id="add-company" class="btn btn-success float-right" >
                                Tambah
                            </button>
                        </th>
                    </tr>
                    <tr>
                        <th style='text-align:left'>Bil.</th>
                        <th style='text-align:center'>Nama <br/>Syarikat.</th>
                        <th style='text-align:center'>Nombor <br/>Pendaftara Syarikat.</th>
                        <th style='text-align:center'>Jenis <br/>Syarikat</th>
                        <th style='text-align:center'>Tarikh<br/> Mansuh</th>
                        <th style='text-align:right'>Modal Berbayar <br/>(RM) </th>
                        <th style='text-align:center'>Alamat</th>
                        <th style='text-align:center'>Pilih</th>
                        <th style='text-align:center'>Kemaskini</th>
                    </tr>
                </thead>
            </table>
    </div>
</div>

@push('modal')
<div class="modal fade-scale company-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' method='post' id="company-form">
                <input type="hidden" id="company_user_id" name='user_id' value="{{ encrypt(Auth::user()->id) }}" >
                <input type="hidden" id="company_method" name='_method' >
                @csrf
                
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Syarikat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h3 class="card-title">Maklumat Syarikat</h3>
                    <hr>
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="company_name_label" class="control-label">Nama Syarikat <span class='required'>*</span></label>
                                <input type="text" id="company_name" name='company_name' id="company_name" class="form-control" placeholder="" >
                                <small class="form-control-feedback"> </small> </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-6 col-sm-12">
                        <div class="form-group">
                            <label id="company_company_type_id_label" class="control-label">Jenis Syarikat <span class='required'>*</span></label>
                            <select class="form-control" id='company_company_type_id' name='company_company_type_id'>
                                <option value=''>Sila Pilih</option>
                                @foreach($inputParam['company_types'] as $companyType)
                                    <option data-code='{{ $companyType->code }}' value="{{ encrypt($companyType->id) }}" data-haspaidupcapital='{{ $companyType->has_paidup_capital }}' data-hasexpirydate='{{ $companyType->has_expiry_date }}'>{{ $companyType->name }}</option>
                                @endforeach
                            <select>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6 col-sm-12">
                        <div class="form-group">
                            <label id="company_mycoid_label" class="control-label">Nombor pendaftaran syarikat <span class='required'>*</span></label>
                            <input type="text" id='company_mycoid' name='company_mycoid' 
                            class="form-control mycoid-inputmask" 
                            data-inputmask-clearmaskonlostfocus="false" maxLength="12">
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6 col-sm-12">
                        <div class="form-group">
                            <label id="company_old_mycoid_label" class="control-label">Nombor pendaftaran syarikat Lama </label>
                            <input type="text" id='company_old_mycoid' name='company_old_mycoid' 
                            class="form-control mycoid-inputmask" 
                            data-inputmask-clearmaskonlostfocus="false" maxLength="12">
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="company_expiry_date_label" class="control-label">Tarikh Mansuh SSM/Trading License/IRD <span class='required'>*</span></label>
                                <input type="text" class="form-control date"  id='company_expiry_date' name="company_expiry_date" />
                            </div>
                        </div>

                    <div class="col-md-12 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="company_paidup_capital_label" class="control-label">Modal Berbayar (RM) <span class='required'>*</span></label>
                                <input type="number" class="form-control" placeholder="" name='company_paidup_capital' id='company_paidup_capital' style="text-align:right">
                            </div>
                        </div>
                    </div>

                    <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="form-group">
                                <label id="company_address_1_label" >Alamat 1 <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='company_address_1' id='company_address_1' >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="form-group">
                                <label id="company_address_2_label" >Alamat 2</label>
                                <input type="text" value="" class="form-control" name='company_address_2' id='company_address_2'>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="company_address_3_label" >Alamat 3</label>
                                <input type="text" value="" class="form-control" name='company_address_3' id='company_address_3'>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="company_postcode_label" >Poskod <span class='required'>*</span></label>
                                <input type="text" VALUE="" class="form-control" name='company_postcode' id='company_postcode' maxlength='5'>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="company_state_id_label" >Negeri <span class='required'>*</span></label>
                                <select class="form-control date" id='company_state_id' name='company_state_id'>
                                    <option value=''>Sila Pilih</option>
                                    @foreach($inputParam['states'] as $state)
                                        <option data-code='{{ $state->code }}' value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="company_phone_number_label" >Nombor Telefon <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='company_phone_number' id='company_phone_number' maxlength="10">
                                <small>Masukkan nombor telefon tanpa '-'  sebagai contoh : 0123033563</small><br/>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="company_fax_number_label" >Nombor Faks</label>
                                <input type="text" value="" class="form-control" name='company_fax_number' id='company_fax_number' maxlength="10">
                                 <small>Masukkan nombor telefon tanpa '-'  sebagai contoh : 0123033563</small><br/>
                            </div>
                        </div>

                        <div class="col-md-4 col-lg-6 col-sm-12">
                            <div class="form-group">
                                <label id="company_email_label" >Emel <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='company_email' id='company_email'>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush

@push('css')
<style>
     td{ text-align: center; }
</style>
@endpush
@push('js')
<script>
    $(document).on('click','.edit-company',function(){
        var companyId = $(this).data('id');
        $('#company-form').attr('action', "{{ route('api.license_application.company.update',['']) }}/"+companyId);
        $('#company_method').val('PATCH');
        $('.company-modal').modal('toggle');

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company.find_company_detail_by_company_id',['']) }}/"+companyId,
            success: function(data){
                $('#company_name').val(data.name);
                $('#company_mycoid').val(data.registration_number);
                $('#company_expiry_date').val(data.formatted_expiry_date);
                $('#company_paidup_capital').val(data.paidup_capital);
                $('#company_address_1').val(data.address_1);
                $('#company_address_2').val(data.address_2);
                $('#company_address_3').val(data.address_3);
                $('#company_postcode').val(data.postcode);
                $('#company_phone_number').val(data.phone_number);
                $('#company_email').val(data.email);
                $('#company_fax_number').val(data.fax_number);
                $('#company_old_mycoid').val(data.old_registration_number);
                $("#company_company_type_id option[data-code='" + data.company_type_code +"']").attr("selected","selected");
                $("#company_state_id option[data-code='" + data.state_code +"']").attr("selected","selected");
                $('#company_paidup_capital').prop('readonly',data.has_paidup_capital);
                $('#company_expiry_date').prop('readonly',data.has_expiry_date);
            }
        });
    })

    $('#add-company').click(function(){
        userId = $('#company_user_id').val();
        $('#company-form').trigger("reset");
        $('#company_user_id').val(userId);
        $('#company-form').attr('action', "{{ route('api.license_application.company.store') }}");
        $('#company_method').val('POST');
        $('#company_paidup_capital').prop('readonly',false);
        $('#company_expiry_date').prop('readonly',false);
    });

    $('#company_company_type_id').change(function(){
        var hasPaidupCapital = $("#company_company_type_id option:selected").data('haspaidupcapital');
        var hasExpiryDate = $("#company_company_type_id option:selected").data('hasexpirydate');
        
        if(hasPaidupCapital == 0){
            $('#company_paidup_capital').prop('readonly',true);
        }
        else{
            $('#company_paidup_capital').prop('readonly',false);
        }

        if(hasExpiryDate == 0){
            $('#company_expiry_date').datepicker().data('datepicker').destroy();
            $('#company_expiry_date').prop('readonly',true);
        }
        else{
            $('#company_expiry_date').datepicker({
                language: 'en',
                dateFormat: 'dd-mm-yyyy',
            }).data('datepicker');
            $('#company_expiry_date').prop('readonly',false);
        }

         $('#company_paidup_capital').val(null);
        $('#company_expiry_date').val(null);
    });

    $('#company-datatable').DataTable(
        loadCompanyConfig()
    );

    function loadCompanyConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["user_id"] = "{{ encrypt(Auth::user()->id) }}";

        @if(isset($inputParam['approval_letter_application_temporary']->company_id))
            form["company_id"] = $('#company_id').val();
        @endif

        @if(isset($inputParam['include']->company_id))
            form["company_id"] = $('#company_id').val();
        @endif

        var companyJson = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
            "ajax": {
                "url": "{{ route('api.license_application.company.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [
                {
                    data: 'DT_RowIndex', className: "text-centre",
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name', className: "text-centre",
                    name: 'name'
                },
                {
                    data: 'registration_number', className: "text-centre",
                    name: 'registration_number'
                },
                {
                    data: 'company_type', className: "text-centre",
                    name: 'company_type'
                },
                {
                    data: 'expiry_date', className: "text-centre",
                    name: 'expiry_date'
                },
                {
                    data: 'formatteed_paidup_capital', className: "text-right",
                    name: 'formatteed_paidup_capital'
                },
                {
                    data: 'address', className: "text-center",
                    name: 'address'
                },
                {
                    data: 'action', className: "text-centre",
                    name: 'action'
                },
                {
                    data: 'edit', className: "text-centre",
                    name: 'edit'
                },
            ]

        };

        return companyJson;
    }
</script>
@endpush