<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped table-responsived" id='generation-type-datatable' style='width:100%'>
            <thead>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:center'>Lesen Yang Ingin Dimohon</th>
                    <th style='text-align:center'>Pilih</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('js')
<script>
    $('#generation-type-datatable').DataTable(
        loadGenerationTypeConfig()
    );

    function loadGenerationTypeConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["license_application_license_generation_type_id"] = $('#license_application_license_generation_type_id').val();
        form["domain"] = $('#domain').val();

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "bFilter" : false,  
            "bInfo": false,     
            "bPaginate": false,        
            "bLengthChange": false,
            "ajax": {
                "url": "{{ route('api.license_application.generation_type.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',className: "text-centre",
                    name: 'name'
                },                
                {
                    data: 'action',className: "text-centre",
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
