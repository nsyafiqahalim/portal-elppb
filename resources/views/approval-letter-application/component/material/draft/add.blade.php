<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped table-responsived" id='attachment-datatable' style='width:100%'>
            <thead>
                <tr>
                    <th style='text-align:center'>Bil.</th>
                    <th style='text-align:center'>Nama </th>
                    <th style='text-align:center'>Status </th>
                    <th style='text-align:center'>Muat Naik</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('js')
<script>
    $('#attachment-datatable').DataTable(
        loadAttachmentConfig()
    );

    function loadAttachmentConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["material_domain"] = $('#material_domain').val();
        form["approval_letter_application_id"] = "{{ $inputParam['current_approval_letter_application_id'] }}";

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "bFilter" : false,  
            "bInfo": false,       
            "bPaginate": false,      
            "bLengthChange": false,
            "ajax": {
                "url": "{{ route('api.approval_letter_application.material.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'label', className: "text-left",
                    name: 'label'
                },
                {
                    data: 'status', className: "text-centre",
                    name: 'status'
                },                
                {
                    data: 'action',className: "text-centre",
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
