<header>
    <nav id="top-bar" class="navbar navbar-light bg-light" style="max-height:50px;">
        <!--<div class="d-flex justify-content-end" style="margin-right:-30pt;">
            <i class="fab fa-accessible-icon mr-2 fa-lg"></i>
            <div>
                <span>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button style="box-sizing:border-box; border: 1px solid black; height:25px; background-color:#a7a7a7;" type="button" onclick="toggleBlack()" class="btn">
                            <script type="text/javascript">
                                function toggleBlack() {
                                    var colors = ["#a7a7a7"],
                                        selectedColor = colors[Math.floor(Math.random() * colors.length)]
                                    $("body, .header-area, .header-top_area, .main-header-area").css("background-color", selectedColor);
                                }
                            </script>
                        </button>&ensp;
                        <button style="box-sizing:border-box; border: 1px solid black; background-color:#cfe5fc;" type="button" onclick="toggleBlue()" class="btn">
                            <script type="text/javascript">
                                function toggleBlue() {
                                    var colors = ["#cfe5fc"],
                                        selectedColor = colors[Math.floor(Math.random() * colors.length)]
                                    $("body, .header-area, .header-top_area, .main-header-area").css("background-color", selectedColor);
                                }
                            </script>
                        </button>&ensp;
                        <button type="button" onclick="togglePeach()" class="btn" style="background-color:#f2edcd; box-sizing:border-box; border: 1px solid black;">
                            <script type="text/javascript">
                                function togglePeach() {
                                    var colors = ["#f2edcd"],
                                        selectedColor = colors[Math.floor(Math.random() * colors.length)]
                                    $("body, .header-area, .header-top_area, .main-header-area").css("background-color", selectedColor);
                                }
                            </script>
                        </button>&ensp;
                        <button style="box-sizing:border-box; border: 1px solid black;" type="button" onclick="toggleWhite()" class="btn btn-light">
                            <script type="text/javascript">
                                function toggleWhite() {
                                    var colors = ["white"],
                                        selectedColor = colors[Math.floor(Math.random() * colors.length)]
                                    $("body, .header-area, .header-top_area, .main-header-area").css("background-color", selectedColor);
                                }
                            </script>
                        </button>
                    </div>
                </span>
            </div>
        </div>-->
        <div class="d-flex justify-content-start">
            <span class="navbar-text" id="date_time" style="font-size:9pt;"></span>
            <script type="text/javascript">
                window.onload = date_time('date_time');
            </script>
            <span class="navbar-text"><small class="mr-2 ml-2">|</small><small class="HijriDate" style="font-size:9pt;">
                    <script>
                        document.write(writeIslamicDate());
                    </script>
                </small><small class="mr-2 ml-2">|</small>
            </span>
            <span class="navbar-text" id="time" style="font-size:9pt;">
                <script type="text/javascript">
                    window.onload = time('time');
                </script>
            </span>
        </div>
    </nav> <!-- End Time Date -->

    <!-- Menu Bar -->
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="{{ route('website.index') }}" style="margin-left:25px;">
            <img src="{{ asset('img/logo_elppb.png') }}" width="250" height="80" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav w-100 justify-content-center">
                <li class="nav-item" style="padding-right:8px;">
                    <a class="nav-link" href="{{ route('website.index') }}">UTAMA </a>
                </li>
                <li class="nav-item dropdown menu" style="padding-right:8px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        INFO
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('website.info.chief') }}">Perutusan Ketua Pengarah</a>
                        <a class="dropdown-item" href="{{ route('website.info.history') }}">Sejarah</a>
                        <a class="dropdown-item" href="{{ route('website.info.role') }}">Peranan atau Fungsi</a>
                        <a class="dropdown-item" href="{{ route('website.info.rules') }}">Akta dan Peraturan</a>
                    </div>
                </li>
                <li class="nav-item" style="padding-right:8px;">
                    <a class="nav-link cl" href="{{ route('website.license') }}">LESEN</a>
                </li>
                <li class="nav-item" style="padding-right:8px;">
                    <a class="nav-link" href="{{ route('website.permit') }}">PERMIT</a>
                </li>
                <li class="nav-item" style="padding-right:8px;">
                    <a class="nav-link" href="{{ route('website.faq') }}">SOALAN LAZIM</a>
                </li>
                <li class="nav-item dropdown" style="padding-right:8px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        HUBUNGI KAMI
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('website.main-contact') }}">Alamat Ibu Pejabat</a>
                        <a class="dropdown-item" href="{{ route('website.contact') }}">Alamat Cawangan</a>
                    </div>
                </li>
                <li class="nav-item dropdown" style="padding-right:8px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        MANUAL PENGGUNA
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{route('website.userManual')}}">Pengurusan Pengguna</a>
                        <a class="dropdown-item" href="{{route('website.manualManagement')}}">Pengurusan Maklumat Asas</a>
                        <a class="dropdown-item" href="{{route('website.manual-license')}}">Permohonan Lesen</a>
                        <a class="dropdown-item" href="{{ route('website.manual-permit') }}">Permohonan Permit</a>
                    </div>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <a href="{{ route('login') }}" style="margin-right:25px;"><button class="btn btn-outline-success my-2 my-sm-0" type="submit">Log Masuk</button></a>
            </ul>
        </div>
    </nav>
</header>

@push('js')
<script>
    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML =
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }; // add zero in front of numbers < 10
        return i;
    }
</script>
@endpush

@push('js')
<script>
    jQuery(function($) {
        var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
        $('ul li a').each(function() {
            if (this.href === path) {
                $(this).addClass('active');
            }
        });
    });
</script>
@endpush
