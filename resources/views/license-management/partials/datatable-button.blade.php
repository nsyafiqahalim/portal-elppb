<div class="btn-group">
    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Tindakan
    </button>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="">Papar</a>
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.change_company.add',[$id]) }}">Perubahan Syarikat</a>
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.change_license_application.add',[$id]) }}">Perubahan Had Muatan</a>
        @if($licenseType != 'retail')
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.change_store.add',[$id]) }}">Perubahan Stor</a>
        @endif
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.change_premise.add',[$id]) }}">Perubahan Premis</a>
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.change_and_renew.add',[$id]) }}">Pembaharuan Dan Perubahan</a>
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.renew.add',[$id]) }}">Pembaharuan</a>
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.cancellation.add',[$id]) }}">Pembatalan</a>
        <a class="dropdown-item" href="{{ route('license_application.'.$licenseType.'.replacement.add',[$id]) }}">Penggantian</a>
    </div>
</div>