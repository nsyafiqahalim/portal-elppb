
<table class="table table-bordred table-stripted" style='width:100%'>
    <thead>
            <tr>
                <th>Bil.</th>
                <th>Nombor Rujukan Lesen</th>
                <th>Jenis Lesen</th>
                <th>Status</th>
            </tr>
    </thead>
    <tbody>
            @foreach($includeLicenseItems as $index => $includeLicenseItem)
                <tr>
                    <td>{{ $index+ 1}} </td>
                    <td>{{ $includeLicenseItem->currentLicense->license_number}} </td>
                    <td>{{ $includeLicenseItem->currentLicense->licenseType->name}} </td>
                    <td>{{ $includeLicenseItem->currentLicense->status->name}} </td>
                </tr>
            @endforeach
    </tbody>
</table>