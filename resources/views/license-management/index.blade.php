@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Senarai Lesen</h2>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'>Carian</b>
                <ul class="nav float-right panel_toolbox">
                    <li><a style='color:white;' data-toggle="collapse" href="#filter" license="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <div class="card-body collapse multi-collapse" id="filter">
                <div class="card-block">
                    <form class="form" id="filter-license">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='name'  class="form-control" placeholder="Nama" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='code' class="form-control" placeholder="Kod" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <select name='is_active'  class="form-control select2">
                                             <option value=''> Semua Status</option>
                                            <option value='1'>Aktif</option>
                                            <option value='0'>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-info">
                                <i class="icon-search"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <div class="card-body">
                <div class="m-t-40">
                    <table id="license-datatable" class="table display table-bordered table-striped table-responsived" style='text-align:center;width:100%'>
                        <thead>
                            <tr style="text-align:center">
                                <th style="vertical-align:middle">Bil.</th>
                                <th style="vertical-align:middle">Nama Syarikat</th>
                                <th style="vertical-align:middle">Maklumat Lesen Terlibat</th>
                                <th style="vertical-align:middle" width="300">Tindakan</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $("#filter-license").submit(function(event) {
        event.preventDefault();
        form = $('#filter-license').serializeArray();
        formdata = {}
        for (x = 0; x < form.length; x++) {
            formdata[form[x].name] = form[x].value;
        }
        $('#license-datatable').DataTable().destroy();
        $('#license-datatable').DataTable(
            loadConfig(formdata)
        );
    });

    $('#license-datatable').DataTable(
        loadConfig()
    );

    function loadConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_management.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'company',
                    name: 'company'
                },
                {
                    data: 'license_information',
                    name: 'license_information'
                },
                {
                    data: 'action',
                    name: 'Tindakan'
                },
            ]

        };

        return json;
    }
</script>
@endpush
