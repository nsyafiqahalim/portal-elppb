@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-home"></i> Laman Utama</h3>
        <ol class="breadcrumb">
        </ol>
    </div>
</div>

<div class="container" style="margin-left:70px;">
    <div class="row el-element-overlay">
        <div class="col-xs-6 col-md-4">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1">
                    <img src="{{asset('img/icon/icon-1.png')}}" alt="">
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1">
                    <img src="{{asset('img/icon/icon-2.png')}}" alt="">
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1">
                    <img src="{{asset('img/icon/icon-3.png')}}" alt="">
                    <div class="el-overlay scrl-up">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline" href="javascript:void(0);"><i class="icon-link"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
