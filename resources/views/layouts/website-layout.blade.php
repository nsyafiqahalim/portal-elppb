<!DOCTYPE doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="x-ua-compatible">
    <title>
        Portal Sistem Lesen Permit Padi Dan Beras (ELPPB 2.0)
    </title>
    <meta content="" name="description">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link href="img/ELPPB-favico.png" sizes="14x14" rel="shortcut icon" type="image/x-icon">
    <!-- Place favicon.ico in the root directory -->
    <!-- CSS here & Other Css-->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/timeline.css') }}" rel="stylesheet">
    <link href="{{ asset('css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/nice-select.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ asset('css/gijgo.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link href="{{ asset('css/slicknav.css') }}" rel="stylesheet">
    <link href="{{ asset('css/uikit-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/uikit.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script type="text/javascript" src="js/time.js"></script>
    <script type="text/javascript" src="js/date_time.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <link href="{{ asset('mapsvg/css/mapsvg.css') }}" rel="stylesheet">
    <link href="{{ asset('mapsvg/css/nanoscroller.css') }}" rel="stylesheet">
    <script src="{{ asset('mapsvg/js/jquery.js') }}"></script>
    <script src="{{ asset('mapsvg/js/jquery.mousewheel.min.js') }}"></script>
    <script src="{{ asset('mapsvg/js/jquery.nanoscroller.min.js') }}"></script>
    <script src="{{ asset('mapsvg/js/mapsvg.min.js') }}"></script>
    <script>
        function gmod(n, m) {
            return ((n % m) + m) % m;
        }

        function kuwaiticalendar(adjust) {
            var today = new Date();
            if (adjust) {
                adjustmili = 1000 * 60 * 60 * 24 * adjust;
                todaymili = today.getTime() + adjustmili;
                today = new Date(todaymili);
            }
            day = today.getDate();
            month = today.getMonth();
            year = today.getFullYear();
            m = month + 1;
            y = year;
            if (m < 3) {
                y -= 1;
                m += 12;
            }

            a = Math.floor(y / 100.);
            b = 2 - a + Math.floor(a / 4.);
            if (y < 1583) b = 0;
            if (y == 1582) {
                if (m > 10) b = -10;
                if (m == 10) {
                    b = 0;
                    if (day > 4) b = -10;
                }
            }

            jd = Math.floor(365.25 * (y + 4716)) + Math.floor(30.6001 * (m + 1)) + day + b - 1524;

            b = 0;
            if (jd > 2299160) {
                a = Math.floor((jd - 1867216.25) / 36524.25);
                b = 1 + a - Math.floor(a / 4.);
            }
            bb = jd + b + 1524;
            cc = Math.floor((bb - 122.1) / 365.25);
            dd = Math.floor(365.25 * cc);
            ee = Math.floor((bb - dd) / 30.6001);
            day = (bb - dd) - Math.floor(30.6001 * ee);
            month = ee - 1;
            if (ee > 13) {
                cc += 1;
                month = ee - 13;
            }
            year = cc - 4716;

            if (adjust) {
                wd = gmod(jd + 1 - adjust, 7) + 1;
            } else {
                wd = gmod(jd + 1, 7) + 1;
            }

            iyear = 10631. / 30.;
            epochastro = 1948084;
            epochcivil = 1948085;

            shift1 = 8.01 / 60.;

            z = jd - epochastro;
            cyc = Math.floor(z / 10631.);
            z = z - 10631 * cyc;
            j = Math.floor((z - shift1) / iyear);
            iy = 30 * cyc + j;
            z = z - Math.floor(j * iyear + shift1);
            im = Math.floor((z + 28.5001) / 29.5);
            if (im == 13) im = 12;
            id = z - Math.floor(29.5001 * im - 29);

            var myRes = new Array(8);

            myRes[0] = day; //calculated day (CE)
            myRes[1] = month - 1; //calculated month (CE)
            myRes[2] = year; //calculated year (CE)
            myRes[3] = jd - 1; //julian day number
            myRes[4] = wd - 1; //weekday number
            myRes[5] = id - 1; //islamic date
            myRes[6] = im - 1; //islamic month
            myRes[7] = iy; //islamic year

            return myRes;
        }

        function writeIslamicDate(adjustment) {
            var wdNames = new Array("Ahad", "Isnin", "Selasa", "Rabu", "Khamis", "Jumaat", "Sabtu");
            var iMonthNames = new Array("Muharram", "Safar", "Rabiul Awal", "Rabiul Akhir",
                "Jamadilawal", "Jamadilakhir", "Rejab", "Syaaban",
                "Ramadhan", "Syawal", "Zulkaedah", "Zulhijjah");
            var iDate = kuwaiticalendar(adjustment);
            var outputIslamicDate = iDate[5] + " " + iMonthNames[iDate[6]] + " " + iDate[7] + "H";
            return outputIslamicDate;
        }
    </script>
    @stack('css')
</head>

<body onload="startTime()">
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    @include('headers.website.index')

    <!-- header-end -->
    <!-- slider_area_start -->
    @yield('content')

    @stack('script')

    <!-- footer start -->
    @include('footers.website.index')
    <!--/ footer end  -->

    <script src="{{ asset('js/vendor/modernizr-3.5.0.min.js') }}">
    </script>
    <script src="{{ asset('js/vendor/jquery-1.12.4.min.js') }}">
    </script>
    <script src="{{ asset('js/popper.min.js') }}">
    </script>
    <script src="{{ asset('js/bootstrap.min.js') }}">
    </script>
    <script src="{{ asset('js/owl.carousel.min.js') }}">
    </script>
    <script src="{{ asset('js/isotope.pkgd.min.js') }}">
    </script>
    <script src="{{ asset('js/ajax-form.js') }}">
    </script>
    <script src="{{ asset('js/waypoints.min.js') }}">
    </script>
    <script src="{{ asset('js/jquery.counterup.min.js') }}">
    </script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}">
    </script>
    <script src="{{ asset('js/scrollIt.js') }}">
    </script>
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}">
    </script>
    <script src="{{ asset('js/wow.min.js') }}">
    </script>
    <script src="{{ asset('js/nice-select.min.js') }}">
    </script>
    <script src="{{ asset('js/jquery.slicknav.min.js') }}">
    </script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}">
    </script>
    <script src="{{ asset('js/plugins.js') }}">
    </script>
    <!-- <script src="{{ asset('js/gijgo.min.js') }}"></script> -->
    <script src="{{ asset('js/slick.min.js') }}">
    </script>
    <!--contact js-->
    <script src="{{ asset('js/contact.js') }}">
    </script>
    <script src="{{ asset('js/jquery.ajaxchimp.min.js') }}">
    </script>
    <script src="{{ asset('js/jquery.form.js') }}">
    </script>
    <script src="{{ asset('js/jquery.validate.min.js') }}">
    </script>
    <script src="{{ asset('js/mail-script.js') }}">
    </script>
    <script src="{{ asset('js/main.js') }}">
    </script>
    <script src="{{ asset('js/timeline.js') }}">
    </script>
    <script src="{{ asset('js/uikit.min.js') }}">
    </script>
    <script src="{{ asset('js/uikit-icons.min.js') }}">
    </script>
    <script src="https://kit.fontawesome.com/80f443d013.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
    <script data-account="3U2pd0hBtp" src="https://accessibilityserver.org/widget.js"></script>
    @stack('js')
</body>

</html>
