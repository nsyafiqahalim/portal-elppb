<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li><a href="{{ route('home') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Laman Utama</a></li>
        <!----
        <li><a href="{{ route('company.index') }}" aria-expanded="false"><i class="mdi mdi-archive"></i><span class="hide-menu">Maklumat Syarikat</a></li>
        ---->
        &nbsp;&nbsp;
        @if(Auth::user()->hasVerifiedEmail())
        
        <li><a href="{{ route('company_stock.index') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Penyata Stok</a></li>
        <li class="sidebarnav">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-plus"></i><span class="hide-menu">Permohonan Lesen</span></a>
            <ul aria-expanded="false" class="collapse">
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="mdi mdi-inbox"></i> Lesen</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('license_application.retail.new.add') }}" aria-expanded="false">Lesen Runcit</a></li>
                        <li><a href="{{ route('license_application.wholesale.new.add') }}" aria-expanded="false">Lesen  Borong</a></li>
                        <li><a href="{{ route('license_application.import.new.add') }}" aria-expanded="false">Lesen  Import</a></li>
                        <li><a href="{{ route('license_application.export.new.add') }}" aria-expanded="false">Lesen  Eksport</a></li>
                        <li><a href="{{ route('license_application.buy_paddy.new.add') }}" aria-expanded="false">Lesen  Membeli Padi</a></li>
                        <li><a href="{{ route('license_application.factory_paddy.new.add') }}" aria-expanded="false">Lesen  Kilang Padi (Komersial)</a></li>
                    </ul>
                </li>
                <!---
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="fas fa-paperclip"></i> Permit</a>
                    <ul class="dropright-menu">
                        <li><a href="#">Permindahan Padi</a></li>
                        <li><a href="#">Beras/Hasil Sampingan</a></li>
                    </ul>
                </li>
                ----->
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="fa fa-inbox"></i> Surat Kelulusan Bersyarat</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('approval_letter_application.buy_paddy.new.add') }}" aria-expanded="false">Surat Kelulusan Bersyarat  Membeli Padi</a></li>
                        <li><a href="{{ route('approval_letter_application.factory_paddy.new.add') }}" aria-expanded="false">Surat Kelulusan Bersyarat  Kilang Padi (Komersial)</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        
        <li class="sidebarnav">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu">Pengurusan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_application.license_management') }}" aria-expanded="false"><i class="mdi mdi-inbox"></i><span class="hide-menu"> Lesen</a></li>
                <!---
                <li><a href="{{ route('license_application.license_management') }}" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu"> Permit</a></li>
                ---->
                <li><a href="{{ route('approval_letter_application.approval_letter_management') }}" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu"> Surat Kelulusan Bersyarat</a></li>
            </ul>
        </li>
        <li class="sidebarnav">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-inbox"></i><span class="hide-menu">Status Permohonan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_application.list_of_application') }}" aria-expanded="false"><i class="mdi mdi-inbox"></i><span class="hide-menu"> Lesen</a></li>
                <!---
                <li><a href="{{ route('license_application.list_of_application') }}" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu"> Permit</a></li>
                --->
                <li><a href="{{ route('approval_letter_application.list_of_approval_letter_application') }}" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu"> Surat Kelulusan Bersyarat</a></li>
            </ul>
        </li>
        @endif
    </ul>
</nav>
