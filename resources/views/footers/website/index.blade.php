<footer style="background-color:#eeefeee0!important;">
    <div class="copy-right_text">
        <div class="container">
            <div class="row" style="padding-top:10px;">
                <div class="col-lg-5 my-0">
                    <p class="copy_right" style="font-size:90%;color:black;font-weight:600;">
                        Hak Cipta Terpelihara ©
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        eLPPB 2.0
                    </p>
                    <div class="row" style="margin-top:-22pt;">
                        <ul class="nav ml-2">
                            <li class="nav-item m-2">
                                <a href="{{ route('website.basic.privacy') }}">
                                    <small>Dasar Privasi</small>
                                </a>
                            </li>
                            <li class="nav-item m-2">
                                <a href="{{ route('website.basic.term') }}">
                                    <small>Terma & Syarat</small>
                                </a>
                            </li>
                            <li class="nav-item m-2">
                                <a href="{{ route('website.basic.security') }}">
                                    <small>Dasar Keselamatan</small>
                                </a>
                            </li>
                            <li class="nav-item m-2">
                                <a href="{{ route('website.basic.deny') }}">
                                    <small>Penafian</small>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 my-0">
                    <p class="copy_right text-left" style="font-size:90%;color:black;font-weight:600;">
                        Tarikh Kemaskini Terakhir :
                        <span id="updatedDate">8 Jun 2020</span>
                    </p>
                </div>
                <div class="col-lg-3 my-0">
                    <p class="copy_right text-right" style="font-size:90%;color:black;font-weight:600;">
                        Jumlah Pelawat: <span class="num" style="font-size:20px;">600</span>
                    </p>
                </div>
            </div>
            <p class="copy_right text-left" style="font-size:95%;font-weight:bold;color:#033e11;margin-top:-10pt;">
                Sesuai dipapar menggunakan Microsoft Edge, Google Chrome 9.0 dan
                Mozilla Firefox 3.0 & ke atas dengan resolusi 1024 x 768.
            </p>
        </div>
    </div>
</footer>

@push('js')
<script>
    $(".num").counterUp({
        delay: 10,
        time: 1000
    });
</script>
@endpush
