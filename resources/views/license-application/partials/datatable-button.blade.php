@if($status   == "NOT_SUBMITTED")
<a href="{{ route('license_application.'.$licenseType.'.'.$licenseApplicationType.'.draft',[$id]) }}">
    <button class="btn btn-sm btn-info">
        <i class="fa fa-edit"></i> Kemaskini 
    </button>
</a>
@elseif($status  == "SUBMITTED")
<a href="#">
    <button class="btn btn-sm btn-info">
        <i class="fa fa-eye"></i> papar 
    </button>
</a>
@endif
