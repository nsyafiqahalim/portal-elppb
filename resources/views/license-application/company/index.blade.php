@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Syarikat</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Maklumat Syarikat</a></li>
            <li class="breadcrumb-item active">Syarikat</li>
        </ol>
    </div>
     
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'>Carian</b>
                <ul class="nav float-right panel_toolbox">
                    <li><a style='color:white;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <div class="card-body collapse multi-collapse" id="filter">
                <div class="card-block">
                    <div class="alert alert-warning">
                        Pilihan carian ini tidak wajib
                    </div>
                    <form class="form" id="filter-user">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' class="form-control" placeholder="MYCOID"/>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                       <input type='text' class="form-control" placeholder="Nama Syarikat"/>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                            <select class="form-control select2">
                                                <option> Semua Jenis Syarikat</option>
                                                <option> Lesen Borong</option>
                                                <option> Lesen Runcit</option>
                                                <option> Lesen Import</option>
                                                <option> Lesen Eksport</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                            <select class="form-control select2">
                                                <option> Semua Jenis perniagaan</option>
                                                <option> Lesen Borong</option>
                                                <option> Lesen Runcit</option>
                                                <option> Lesen Import</option>
                                                <option> Lesen Eksport</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-search"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <div class="card-body">
                
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="table-responsive m-t-40">
                        <table id="datatable" class="table display table-bordered table-striped no-wrap">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Syarikat</th>
                                    <th>MYCOID</th>
                                    <th>Nama Pemilik</th>
                                    <th>Jenis Syarikat</th>
                                    <th>Jenis Perniagaan</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Ali Baba</td>
                                    <td>LCN-12ws</td>
                                    <td>Shamsul</td>
                                    <td>Perkongsian</td>
                                    <td>Kedai Runcit</td>
                                    <td>
                                        <a href="{{ route('license_application.company.view') }}">
                                            <button class="btn  btn-outline-info">
                                            <i class="mdi mdi-eye"></i> Papar
                                            </button>
                                        </a>
                                        <a href="{{ route('license_application.company.edit') }}">
                                            <button class="btn  btn-outline-primary">
                                            <i class="mdi mdi-pencil"></i> Kemaskini
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script>
     $('#datatable').DataTable({
        responsive: true,
        searching:  false,
        ordering: false,
    });
</script>
@endpush