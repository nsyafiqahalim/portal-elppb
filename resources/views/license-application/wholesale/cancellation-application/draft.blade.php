@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Borong</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Pembatalan</a></li>
            <li class="breadcrumb-item">Lesen Borong</li>
            <li class="breadcrumb-item active">Pembatalan</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="tab-wizard wizard-circle" id='submission-form' text="Anda Pasti?" method="post" >
                    @csrf
                    @method('PATCH')
                    <input type='hidden' name='license_type' id='license_type' value="{{ encrypt('BORONG') }}"/>
                    <input type='hidden' name='company_id' id='company_id' value="{{encrypt($inputParam['company_id'])}}" />
                    <input type='hidden' name='premise_id' id='premise_id' value="{{encrypt($inputParam['license_application_premise']->company_premise_id)}}" />
                    <input type='hidden' name='district_id' id='district_id' value="{{ encrypt($inputParam['license_application_premise']->district_id) }}"/>
                    <input type='hidden' name='license_application_type' id='license_application_type' value="{{ encrypt('PEMBATALAN') }}"/>
                    @if($inputParam['license_application_company']->companyType->code == 'PERTUBUHAN_PELADANG' || $inputParam['license_application_company']->companyType->code == 'KOPERASI')
                        <input type='hidden' name='material_domain' id='material_domain' value="{{ encrypt('PEMBATALAN_BORONG_KOPERASI') }}"/>
                    @else
                        <input type='hidden' name='material_domain' id='material_domain' value="{{ encrypt('PEMBATALAN_BORONG_SYARIKAT') }}"/>
                    @endif
                    <h6>Syarikat Dan Rakan Kongsi</h6>
                    <section>
                        @include('license-application.component.company.new.view')
                        @include('license-application.component.partner.new.view')
                    </section>

                     <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.premise.new.view')
                            </div>
                        </div>
                        <hr/>
                    </section>

                    <!-- Step 2 -->
                    <h6>Stor</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.store.new.view')
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.material.draft.change')
                            </div>
                        </div>
                    </section>

                    <h6>Maklumat Permohonan</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.wholesale.cancellation-application.summary-draft')
                            </div>
                        </div>
                    </section>
                    <!-- Step 4 -->
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script type='text/javascript'>   
    $(document).ready(function(){
        $(document).on('click','.selectCompany',function(){
            id = $(this).data('id');
            $('#company_id').val(id);
            $('#company_premise_company_id').val(id);
            $('#company_partner_company_id').val(id);

            $('#company-partner-datatable').DataTable().destroy();
            $('#company-partner-datatable').DataTable(loadCompanyPartnerConfig());
            $('#company-premise-datatable').DataTable().destroy();
            $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig());
        // $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig(form)).ajax.reload();
        })

        $(document).on('click','.selectPremise',function(){
            id = $(this).data('id');
            $('#premise_id').val(id);
        })

       $(document).on('click','.submitButton',function(){
            $("#submission-form").removeClass("form-data-submission-using-message-bag");
            $("#submission-form").addClass("form-data-submission-with-confirmation-using-message-bag");
            $('#submission-form').attr('action',"{{ route('api.license_application.wholesale.cancellation.draft.submit',[encrypt($inputParam['license_application']->id)]) }}");
        })

        $(document).on('click','.draftButton',function(){
            $("#submission-form").removeClass("form-data-submission-with-confirmation-using-message-bag");
            $("#submission-form").addClass("form-data-submission-using-message-bag");
            $('#submission-form').attr('action', "{{ route('api.license_application.wholesale.cancellation.draft.update',[encrypt($inputParam['license_application']->id)]) }}");
        })

    }); 
</script>
@endpush

@push('wizard')
<script type='text/javascript'>   
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "Simpan",
            previous: "Kembali",
            next: "Seterusnya"
        }
        , onFinished: function (event, currentIndex) {
            alert("Form Submitted!");

        },
        onStepChanged: function (event, currentIndex, priorIndex) { 
            if(currentIndex == 4){
            $('ul[aria-label=Pagination] a[href="#finish"]').remove();

            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-primary draftButton" value="Simpan"  /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));
            
            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-success submitButton" value="Hantar" /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));

            }
            else {
            $('ul[aria-label=Pagination] input[value="Hantar"]').remove();
            $('ul[aria-label=Pagination] input[value="Simpan"]').remove();
            }
        }
    });
    
</script>
@endpush