@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Borong</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Baharu</a></li>
            <li class="breadcrumb-item">Lesen Borong</li>
            <li class="breadcrumb-item active">Baharu</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="tab-wizard wizard-circle" id='submission-form' text="Anda Pasti?" method="post" >
                    @csrf
                    @method('PATCH')
                    <input type='hidden' name='company_id' id='company_id' />
                    <input type='hidden' name='premise_id' id='premise_id' />
                    <span class="store_parameter">
                    </span>
                    <input type='hidden' name='license_application_license_generation_type_id' id='license_application_license_generation_type_id' />
                    <input type='hidden' name='license_type' id='license_type' value="{{ encrypt('BORONG') }}"/>
                    <input type='hidden' name='license_application_type' id='license_application_type' value="{{ encrypt('BAHARU') }}"/>
                    <input type='hidden' name='user_id' id='user_id' value="{{ encrypt(Auth::user()->id) }}"/>
                    
                    
                    
                    <!-- Step 1 -->
                    <h6>Syarikat Dan Rakan Kongsi</h6>
                    <section>
                        @include('license-application.component.company.new.add')
                        @include('license-application.component.partner.new.add')
                    </section>

                    <h6>Jenis Janaan</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.generation-type.new.add')
                            </div>
                        </div>
                    </section>

                    <!-- Step 2 -->
                    <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.premise.new.add')
                            </div>
                        </div>
                    </section>

                    <!-- Step 2 -->
                    <h6>Stor</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.store.new.add')
                            </div>
                        </div>
                    </section>


                    <!-- Step 4 -->
                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.material.new.add')
                            </div>
                        </div>
                    </section>
                    <!-- Step 4 -->
                    
                    <h6>Maklumat Permohonan</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                 @include('license-application.wholesale.new-application.summary-add')
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script type='text/javascript'>   
    $(document).ready(function(){

        $('#add-company-partner').hide();
        $('#add-company-premise').hide();
        $('#add-company-store').hide();
        $(document).on('click','.selectCompany',async function(){
            id = $(this).data('id');
            key = $(this).data('key');

            $('#company_id').val(id);
            $('#company_premise_company_id').val(id);
            $('#company_partner_company_id').val(id);
            $('#company_store_company_id').val(id);

            await loadData();

            $('#add-company-partner').show();
            $('#add-company-premise').show();
            $('#add-company-store').show();
            $('.store_parameter').html('');
            $('#license_application_license_generation_type_id').html('');
            

            //await removeSummaryPartner();
            await removeSummaryPremise();
            await removeSummaryStore();
            await loadSummaryCompany(key);
        })

        async function loadData(){
            $('#summary-partner-datatable').DataTable().destroy();
            await $('#summary-partner-datatable').DataTable(loadSummaryompanyPartnerConfig());

            $('#company-partner-datatable').DataTable().destroy();
            await $('#company-partner-datatable').DataTable(loadCompanyPartnerConfig());


            $('#company-premise-datatable').DataTable().destroy();
            await $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig());
            $('#company-store-datatable').DataTable().destroy();
            await $('#company-store-datatable').DataTable(loadCompanyStoreConfig());
            $('#material-datatable').DataTable().destroy();
            await $('#material-datatable').DataTable(loadMaterialConfig());
            $('#generation-type-datatable').DataTable().destroy();
            await $('#generation-type-datatable').DataTable(loadGenerationTypeConfig());
        }

        $(document).on('click','.selectPremise',async function(){
            id = $(this).data('id');
            key = $(this).data('key');

            $('#premise_id').val(id);

            await removeSummaryPremise();
            loadSummaryPremise(key);
        })

        $(document).on('click','.selectStore', async function(){
            id = $(this).data('id');
            uuid = $(this).data('uuid');
            isChecked = $(this).prop("checked");

            if(isChecked == true){
                hiddenInputType = '<input type="hidden" class="store_id" name="store_id[]" id="'+uuid+'" value="'+id+'"/>';
                $('.store_parameter').append(hiddenInputType);

                loadSummaryStore(uuid);
            }
            else{
                $('#'+uuid).remove();
                $('#summary-store-col-'+uuid).remove();

            }
        })

        $(document).on('click','.selectGenerationType',async function(){
            id = $(this).data('id');
            key = $(this).data('key');
            $('#license_application_license_generation_type_id').val(id);

            loadSummaryGenerationType(key);
        })
        
        $(document).on('click','.submitButton',function(){
            $("#submission-form").removeClass("form-data-submission-using-message-bag");
            $("#submission-form").addClass("form-data-submission-with-confirmation-using-message-bag");
            $('#submission-form').attr('action',"{{ route('api.license_application.wholesale.draft.submit',[$inputParam['encrypted_license_application_id']]) }}");
        })

        $(document).on('click','.draftButton',function(){
            $("#submission-form").removeClass("form-data-submission-with-confirmation-using-message-bag");
            $("#submission-form").addClass("form-data-submission-using-message-bag");
            $('#submission-form').attr('action', "{{ route('api.license_application.wholesale.draft.update',[$inputParam['encrypted_license_application_id']]) }}");
        })
    }); 
</script>
@endpush

@push('wizard')
<script type='text/javascript'>   
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "Simpan",
            previous: "Kembali",
            next: "Seterusnya"
        }
        , onFinished: function (event, currentIndex) {
            alert("Form Submitted!");

        },
        onStepChanged: function (event, currentIndex, priorIndex) { 
            if(currentIndex == 5){
            $('ul[aria-label=Pagination] a[href="#finish"]').remove();

            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-primary draftButton" value="Simpan"  /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));
            
            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-success submitButton" value="Hantar" /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));

            }
            else {
            $('ul[aria-label=Pagination] input[value="Hantar"]').remove();
            $('ul[aria-label=Pagination] input[value="Simpan"]').remove();
            }
        }
    });
    
</script>
@endpush