

<div class="form-group row">
<label for="example-email-input" class="col-12 col-form-label"><h4>1. Maklumat Syarikat</h4></label>
   <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
        <table class="table no-border table-responsived">
            <tr>
                <td width="200">Nama Syarikat</td>
                <td>:</td>
                <td class="summary-name"></td>
            </tr>
            <tr>
                <td width="200">Nombor Pendaftaran Syarikat</td>
                <td>:</td>
                <td class="summary-registration-number"></td>
            </tr>
            <tr>
                <td width="200">Jenis Syarikat</td>
                <td>:</td>
                <td class="summary-company-type"></td>
            </tr>
             <tr>
                <td width="200">Alamat</td>
                <td>:</td>
                <td class="summary-address"></td>
            </tr>
        </table>
    </div>

    <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
        <table class="table no-border table-responsived">
            <tr>
                <td width="200">Tarikh Mansuh Syarikat</td>
                <td>:</td>
                <td class="summary-expiry-date"></td>
            </tr>
            <tr>
                <td width="200">Nombor Pendaftaran Syarikat Lama</td>
                <td>:</td>
                <td class="summary-old-registration-number"></td>
            </tr>
            <tr>
                <td width="200">Modal Berbayar</td>
                <td>:</td>
                <td class="summary-paidup-capital"></td>
            </tr>
        </table>
    </div>
</div>

<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>2. Rakan Kongsi</h4></label>
    <div class="col-12">
         <table class="table table-bordered table-striped summary-partner" id="summary-partner-datatable" style='text-align:center'>
            <thead>
                <tr>
                    <th>Bil.</th>
                    <th style='text-align:center'>Nama</th>
                    <th style='text-align:center'>No<br/> K/P</th>
                    <th style='text-align:center'>Passport</th>
                    <th style='text-align:center'>No<br/> Telefon</th>
                    <th style='text-align:center'>Bangsa</th>
                    <th style='text-align:center'>Warganegara</th>
                    <th style='text-align:center'>Jumlah<br/> Syer(%)</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>3. Premis</h4></label>
    <div class="col-12">
         <table class="table table-bordered table-striped summary-premise">
            <thead>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Syarikat</th>
                    <th style='text-align:center'>Dewan Undangan Negeri (DUN)</th>
                    <th style='text-align:center'>Parlimen</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>4. Stor</h4> <small>Sila tandakan pada had muatan yang diperlukan pada lesen yang bakal digunakan  </small></label>

    <div class="col-12">
        <table class="table table-bordered table-striped summary-store">
            <thead>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Hak Milik</th>
                    <th style='text-align:center'>Had Muatan</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!----
<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>5. Lampiran</h4></label>
    <div class="col-12">
         <table class="table table-bordered table-striped" class='summary-material'>
            <thead>
                <tr>
                    <th style='text-align:center'>Bil.</th>
                    <th style='text-align:center'>Fail</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
--->
@push('js')
<script>
    var licenseGenerationKey = null;
    async function loadSummaryCompany(key){
        var hiddenCompanyData =  $('#hidden-'+key).val();
        var companyData = JSON.parse(hiddenCompanyData);
        
        $('.summary-name').html(companyData.name);
        $('.summary-registration-number').html(companyData.registration_number);
        $('.summary-old-registration-number').html(companyData.old_registration_number);
        $('.summary-paidup-capital').html(companyData.paidup_capital);
        $('.summary-expiry-date').html(companyData.expiry_date);
        $('.summary-company-type').html(companyData.company_type.name);
        $('.summary-address').html(companyData.address);
       // await loadSummaryPartner();
    }

    async function removeSummaryPremise(){
        $(".summary-premise tbody > tr").remove();
    }

    async function loadSummaryPremise(key){
        var hiddenPremiseData =  $('#hidden-premise-'+key).val();
        var premise = JSON.parse(hiddenPremiseData);
       
        var cols = "<tr style='text-align:center'>>";

        cols += '<td>' + 1 + '</td>';
        cols += '<td>' +  $('.summary-name').html() + '</td>';
        cols += '<td>' + premise.address + '</td>';
        cols += '<td>' + premise.building_type.name + '</td>';
        cols += '<td>' + premise.business_type.name + '</td>';
        cols += '<td>' + premise.dun.name + '</td>';
        cols += '<td>' + premise.parliament.name + '</td>';

        cols += '</tr>';
        $(".summary-premise tbody").append(cols);
    }

    async function removeSummaryStore(){
        $(".summary-store tbody > tr").remove();
    }

    async function loadSummaryStore(key){
        var hiddenStoreData =  $('#hidden-store-'+key).val();
        var store = JSON.parse(hiddenStoreData);
       
        var cols = "<tr id='summary-store-col-"+key+"' style='text-align:center'>";

        cols += '<td>' + 1 + '</td>';
        cols += '<td>' +  $('.summary-name').html() + '</td>';
        cols += '<td>' + store.address + '</td>';
        cols += '<td>' + store.building_type.name + '</td>';
        cols += '<td>' + store.store_ownership_type.name + '</td>';
        cols += '<td>';
        cols += '<ul>';
        cols += '<li><input type="checkbox" checked disabled style="text-align:right">  Borong: &nbsp;<input type="integer" name="load-C-'+key+'"   style="text-align:right" value="'+store.load+'" disabled /></li>';
        cols += '<li class="list-import"><input type="checkbox" name="checkbox-A-'+key+'" style="text-align:right"> Import: &nbsp;&nbsp;<input type="integer" name="load-A-'+key+'" class="import-apply-load"  style="text-align:right" value="'+store.load+'" disabled /></li>';
        cols += '<li class="list-export"><input type="checkbox" name="checkbox-B-'+key+'" style="text-align:right"> Eksport: <input type="integer" name="load-B-'+key+'" class="export-apply-load"  style="text-align:right" value="'+store.load+'" disabled /></li>';

        cols +='</ul>';
        cols +='</td>';
        cols +='</tr>';
        $(".summary-store tbody").append(cols);
        loadSummaryGenerationType();
    }
    
    async function removeSummaryPartner(){
        $(".summary-partner tbody > tr").remove();
    }

    async function loadSummaryGenerationType(key = null){

        if(key  != null){
            licenseGenerationKey = key;
        }

        var generationType =  $('#hidden-generation-type-'+licenseGenerationKey).val();
        var gt = JSON.parse(generationType);
        
        imp = gt.includes("A");
        exp = gt.includes("B");

        if(imp == true){
            $('.list-import').show();
            $('.import-apply-load').prop('disabled',false);
            $('.import-apply-load').prop('readonly',true);
        }
        else{
            $('.list-import').hide();
            $('.import-apply-load').prop('readonly',false);
            $('.import-apply-load').prop('disabled',true);
        }

        if(exp == true){
            $('.list-export').show();
            $('.export-apply-load').prop('disabled',false);
            $('.export-apply-load').prop('readonly',true);
        }
        else{
            $('.list-export').hide();
            $('.export-apply-load').prop('readonly',false);
            $('.export-apply-load').prop('disabled',true);
        }
    }
    function loadSummaryPartner(){
        $.each($(".summary-partner-hidden" ), function(i) {
            partner = JSON.parse($(this).val());
            var cols = "<tr style='text-align:center'>";

            cols += '<td>' + (i + 1) + '</td>';
            cols += '<td>' + partner.name + '</td>';
            cols += '<td>' + partner.nric + '</td>';
            cols += '<td>' + partner.passport + '</td>';
            cols += '<td>' + partner.phone_number + '</td>';
            cols += '<td>' + partner.race.name + '</td>';
            cols += '<td>' + partner.citizen + '</td>';
            cols += '<td>' + partner.share_percentage + '%</td>';

            cols += '</tr>';
            $(".summary-partner tbody").append(cols);
        });
    }

    function  loadSummaryompanyPartnerConfig(form) {
        if (form == null) {
            form = {};
        }

        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": false,
            "searching": false,
            "ordering": false,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
            "ajax": {
                "url": "{{ route('api.license_application.company-partner.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "async": false,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex', className: "text-centre",
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name', className: "text-centre",
                    name: 'name'
                },
                {
                    data: 'nric', className: "text-centre",
                    name: 'nric'
                },
                {
                    data: 'passport', className: "text-centre",
                    name: 'passport'
                },
                {
                    data: 'phone_number', className: "text-centre",
                    name: 'phone_number'
                },
                {
                    data: 'race', className: "text-centre",
                    name: 'race'
                },
                {
                    data: 'citizen', className: "text-centre",
                    name: 'citizen'
                },
                {
                    data: 'share_percentage', className: "text-centre",
                    name: 'share_percentage'
                }
            ]

        };

        return json;
    }
</script>
@endpush
