

<div class="form-group row">
<label for="example-email-input" class="col-12 col-form-label"><h4>1. Maklumat Syarikat</h4></label>
   <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
        <table class="table no-border table-responsived">
            <tr>
                <td width="200">Nama Syarikat</td>
                <td>:</td>
                <td class="summary-name">{{ $inputParam['license_application_company']->name  ?? null}}</td>
            </tr>
            <tr>
                <td width="200">Nombor Pendaftaran Syarikat</td>
                <td>:</td>
                <td class="summary-registration-number">{{ $inputParam['license_application_company']->registration_number ?? null }}</td>
            </tr>
            <tr>
                <td width="200">Jenis Syarikat</td>
                <td>:</td>
                <td class="summary-company-type">{{ $inputParam['license_application_company']['companyType']->name ?? null }}</td>
            </tr>
             <tr>
                <td width="200">Alamat</td>
                <td>:</td>
                <td class="summary-address">
                 @if(isset($inputParam['license_application']->companyAddress))
                 {!! convertAddressIntoString($inputParam['license_application']->companyAddress) !!}
                @endif
               </td>
            </tr>
        </table>
    </div>

    <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
        <table class="table no-border table-responsived">
            <tr>
                <td width="200">Tarikh Mansuh Syarikat</td>
                <td>:</td>
                <td class="summary-expiry-date">
                @if(isset($inputParam['license_application_company']->expiry_date))
                {{ $inputParam['license_application_company']->expiry_date->format('d/m/Y') }}
                @endif
                </td>
            </tr>
            <tr>
                <td width="200">Nombor Pendaftaran Syarikat Lama</td>
                <td>:</td>
                <td class="summary-old-registration-number">{{ $inputParam['license_application_company']->old_registration_number ?? null}}</td>
            </tr>
            <tr>
                <td width="200">Modal Berbayar</td>
                <td>:</td>
                <td class="summary-paidup-capital">
                @if(isset($inputParam['license_application_company']->paidup_capital))
                {{ number_format($inputParam['license_application_company']->paidup_capital,2) }}
                @endif
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>2. Rakan Kongsi</h4></label>
    <div class="col-12">
         <table class="table table-bordered table-striped summary-partner" style='text-align:center'>
            <thead>
                <tr>
                    <th>Bil.</th>
                    <th style='text-align:center'>Nama</th>
                    <th style='text-align:center'>No<br/> K/P</th>
                    <th style='text-align:center'>Passport</th>
                    <th style='text-align:center'>No<br/> Telefon</th>
                    <th style='text-align:center'>Bangsa</th>
                    <th style='text-align:center'>Warganegara</th>
                    <th style='text-align:center'>Jumlah<br/> Syer(%)</th>
                </tr>
            </thead>
            <tbody>
            @if(isset($inputParam['license_application_partners']))
                @foreach($inputParam['license_application_partners'] as $index   => $partner)
                    <tr>
                        <td>{{  ($index +1) }}</td>
                        <td>{{  $partner->name }}</td>
                        <td>{{  $partner->nric }}</td>
                        <td>{{  $partner->passport }}</td>
                        <td>{{  $partner->phone_number }}</td>
                        <td>{{  $partner->race->name }}</td>
                        <td>@if($partner->is_citizen == 1) 
                            Warganegara 
                            @else
                            Bukan Warganegara
                            @endif
                        </td>
                        <td>{{  $partner->share_percentage }}%</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>3. Premis</h4></label>
    <div class="col-12">
         <table class="table table-bordered table-striped summary-premise">
            <thead>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Syarikat</th>
                    <th style='text-align:center'>Dewan Undangan Negeri (DUN)</th>
                    <th style='text-align:center'>Parlimen</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($inputParam['license_application_premise']))
                    @if(isset($inputParam['license_application_premise']))
                    <tr>
                        <td>  1 </td>
                        <td>{{ $inputParam['license_application_company']->name }}</td>
                        <td>{!!  convertAddressIntoString($inputParam['license_application_premise'])    !!}  </td>
                        <td>{{  $inputParam['license_application_premise']['buildingType']->name ?? null }}   </td>
                        <td>{{  $inputParam['license_application_premise']['businessType']->name ?? null }}   </td>
                        <td>{{  $inputParam['license_application_premise']['dun']->name ?? null }} </td>
                        <td>{{  $inputParam['license_application_premise']['parliament']->name ?? null    }}  </td>
                    </tr>
                    @endif
                @endif
            </tbody>
        </table>
    </div>
</div>

<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>4. Stor</h4></label>

    <div class="col-12">
        <table class="table table-bordered table-striped summary-store">
            <thead>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Nama Syarikat </th>
                    <th style='text-align:left'>Alamat </th>
                    <th style='text-align:center'>Jenis Bangunan</th>
                    <th style='text-align:center'>Jenis Hak Milik</th>
                    <th style='text-align:center'>Had Muatan</th>
                </tr>
            </thead>
            <tbody>
            @if(isset($inputParam['license_application_stores']))
                @foreach($inputParam['license_application_stores'] as  $index => $store)
                <tr>
                    <td>{{ ($index+ 1) }}</td>
                    <td>{{  $inputParam['license_application_company']->name }}</td>
                    <td>{!! convertAddressIntoString($store)!!}</td>
                    <td>{{ $store['buildingType']->name }}</td>
                    <td>{{ $store['storeOwnershipType']->name }}</td>
                    <td>
                    <ul>
                        @foreach($store->storeItems as $item)
                            <li><input type="checkbox" checked disabled style="text-align:right">  {{ $item->includeLicenseItem->licenseType->name }}: &nbsp;<input type="integer"  style="text-align:right" value="{{ $item->apply_load  }}" disabled /></li>
                        @endforeach
                    </ul>
                    </td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>

<!----
<div class="form-group row">
    <label for="example-email-input" class="col-12 col-form-label"><h4>5. Lampiran</h4></label>
    <div class="col-12">
         <table class="table table-bordered table-striped" class='summary-material'>
            <thead>
                <tr>
                    <th style='text-align:center'>Bil.</th>
                    <th style='text-align:center'>Fail</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
--->
@push('js')
<script>

    @if(isset($inputParam['license_application']->license_application_license_generation_type_id))
    var licenseGenerationKey = '{{ md5($inputParam['license_application']->license_application_license_generation_type_id) }}';
    
    @else
     var licenseGenerationKey = null;
    @endif
    $(document).on('change','#company_name',function(){
        $('.summary-name').html($(this).val());
    });

    async function loadSummaryCompany(key){
        var hiddenCompanyData =  $('#hidden-'+key).val();
        var companyData = JSON.parse(hiddenCompanyData);
        
        $('.summary-name').html(companyData.name);
        $('.summary-registration-number').html(companyData.registration_number);
        $('.summary-old-registration-number').html(companyData.old_registration_number);
        $('.summary-paidup-capital').html(companyData.paidup_capital);
        $('.summary-expiry-date').html(companyData.expiry_date);
        $('.summary-company-type').html(companyData.company_type.name);
        $('.summary-address').html(companyData.address);
        await loadSummaryPartner();
        await loadSummaryGenerationType();
    }

    async function removeSummaryPremise(){
        $(".summary-premise tbody > tr").remove();
    }

    async function loadSummaryPremise(key){
        var hiddenPremiseData =  $('#hidden-premise-'+key).val();
        var premise = JSON.parse(hiddenPremiseData);
       
        var cols = "<tr style='text-align:center'>>";

        cols += '<td>' + 1 + '</td>';
        cols += '<td>' +  $('.summary-name').html() + '</td>';
        cols += '<td>' + premise.address + '</td>';
        cols += '<td>' + premise.building_type.name + '</td>';
        cols += '<td>' + premise.business_type.name + '</td>';
        cols += '<td>' + premise.dun.name + '</td>';
        cols += '<td>' + premise.parliament.name + '</td>';

        cols += '</tr>';
        $(".summary-premise tbody").append(cols);
    }

    async function removeSummaryStore(){
        $(".summary-store tbody > tr").remove();
    }

    async function loadSummaryStore(key){
        var hiddenStoreData =  $('#hidden-store-'+key).val();
        var store = JSON.parse(hiddenStoreData);
       
        var cols = "<tr id='summary-store-col-"+key+"' style='text-align:center'>";

        cols += '<td>' + 1 + '</td>';
        cols += '<td>' +  $('.summary-name').html() + '</td>';
        cols += '<td>' + store.address + '</td>';
        cols += '<td>' + store.building_type.name + '</td>';
        cols += '<td>' + store.store_ownership_type.name + '</td>';
        cols += '<td>';
        cols += '<ul>';
        cols += '<li>Borong: &nbsp;<input type="integer" name="load-C-'+key+'"   style="text-align:right" value="'+store.load+'" disabled /></li>';
        cols += '<li class="list-import">Import: &nbsp;&nbsp;<input type="integer" name="load-A-'+key+'" class="import-apply-load"  style="text-align:right" value="'+store.load+'" disabled /></li>';
        cols += '<li class="list-export">Eksport: <input type="integer" name="load-B-'+key+'" class="export-apply-load"  style="text-align:right" value="'+store.load+'" disabled /></li>';

        cols +='</ul>';
        cols +='</td>';
        cols +='</tr>';
        $(".summary-store tbody").append(cols);
        loadSummaryGenerationType();
    }
    
    async function removeSummaryPartner(){
        $(".summary-partner tbody > tr").remove();
    }

    async function loadSummaryGenerationType(key = null){

        if(key  != null){
            licenseGenerationKey = key;
        }
        
        var generationType =  $('#hidden-generation-type-'+licenseGenerationKey).val();
        console.log($('#hidden-generation-type-'+licenseGenerationKey).val());
        var gt = JSON.parse(generationType);
        
        imp = gt.includes("A");
        exp = gt.includes("B");

        if(imp == true){
            $('.list-import').show();
            $('.import-apply-load').prop('disabled',false);
            $('.import-apply-load').prop('readonly',true);
        }
        else{
            $('.list-import').hide();
            $('.import-apply-load').prop('readonly',false);
            $('.import-apply-load').prop('disabled',true);
        }

        if(exp == true){
            $('.list-export').show();
            $('.export-apply-load').prop('disabled',false);
            $('.export-apply-load').prop('readonly',true);
        }
        else{
            $('.list-export').hide();
            $('.export-apply-load').prop('readonly',false);
            $('.export-apply-load').prop('disabled',true);
        }
    }
    function loadSummaryPartner(){
        $.each($(".summary-partner-hidden" ), function(i) {
            partner = JSON.parse($(this).val());
            var cols = "<tr style='text-align:center'>";

            cols += '<td>' + (i + 1) + '</td>';
            cols += '<td>' + partner.name + '</td>';
            cols += '<td>' + partner.nric + '</td>';
            cols += '<td>' + partner.passport + '</td>';
            cols += '<td>' + partner.phone_number + '</td>';
            cols += '<td>' + partner.race.name + '</td>';
            cols += '<td>' + partner.citizen + '</td>';
            cols += '<td>' + partner.share_percentage + '%</td>';

            cols += '</tr>';
            $(".summary-partner tbody").append(cols);
        });
    }
</script>
@endpush
