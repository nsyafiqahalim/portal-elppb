@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Borong</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Perubahan Dan Pembaharuan</a></li>
            <li class="breadcrumb-item">Lesen Borong</li>
            <li class="breadcrumb-item active">Perubahan dan pembaharuan</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="tab-wizard wizard-circle" id='submission-form' text="Anda Pasti?" method="post" >
                    @csrf
                    <input type='hidden' name='original_license_application_id' id='original_license_application_id' value="{{encrypt($inputParam['license_application']->id)}}" />
                    <input type='hidden' name='company_id' id='company_id' value="{{encrypt($inputParam['company_id'])}}" />
                    <input type='hidden' name='premise_id' id='premise_id' value="{{encrypt($inputParam['premise_id'])}}"/>
                    <input type='hidden' name='user_id' id='user_id' value="{{ encrypt(Auth::user()->id) }}"/>
                     <input type='hidden' name='include_license_id' id='include_license_id' value="{{ encrypt($inputParam['include_license_id']) }}"/>
                    <input type='hidden' name='license_application_license_generation_type_id' id='license_application_license_generation_type_id' />>
                    <input type='hidden' name='change_type_code' id='change_type_code' value="{{ encrypt('CHANGE_RENEW') }}"/>
                    <input type='hidden' name='store_id' id='store_id' value="{{encrypt($inputParam['store_id'])}}"/>
                    <input type='hidden' name='license_type' id='license_type' value="{{ encrypt('BORONG') }}"/>
                    <input type='hidden' name='license_application_type' id='license_application_type' value="{{ encrypt('PEMBAHARUAN_DAN_PERUBAHAN') }}"/>
                    @if($inputParam['license_application_company']->companyType->code == 'PERTUBUHAN_PELADANG' || $inputParam['license_application_company']->companyType->code == 'KOPERASI')
                        <input type='hidden' name='material_domain' id='material_domain' value="{{ encrypt('PERUBAHAN_DAN_PEMBAHARUAN_BORONG_KOPERASI') }}"/>
                    @else
                        <input type='hidden' name='material_domain' id='material_domain' value="{{ encrypt('PERUBAHAN_DAN_PEMBAHARUAN_BORONG_SYARIKAT') }}"/>
                    @endif
                    <!-- Step 1 -->
                    <h6>Syarikat</h6>
                    <section>
                        @include('license-application.component.company.new.change')
                        <br/>
                    </section>

                    <h6>Rakan Kongsi</h6>
                    <section>
                        @include('license-application.component.partner.new.view')
                    </section>

                    <!-- Step 2 -->
                    <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.premise.new.change')
                            </div>
                        </div>
                    </section>

                    <h6>Stor</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.store.new.change')
                            </div>
                        </div>
                    </section>
                    
                    <h6>Penyata Stok</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.stock-statement.new.renew')
                            </div>
                        </div>
                    </section>

                    <!-- Step 3 -->
                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.material.new.change')
                            </div>
                        </div>
                    </section>
                    <!-- Step 4 -->
                    <h6>Maklumat Permohonan</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="example-url-input" class="col-2 col-form-label">Tempoh Permohonan</label>
                                    <div class="col-6">
                                        <input class="form-control" type="integer" value="{{ $inputParam['license_application']->apply_duration }}" disabled/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-url-input" class="col-2 col-form-label">Had Muatan (TAN)</label>
                                    <div class="col-6">
                                        <input class="form-control" type="integer" name='apply_load' value="{{ $inputParam['license_application']->apply_load }}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-url-input" class="col-2 col-form-label">Lesen Yang Mahu Dijana</label>
                                        <div class="col-6">
                                            @include('license-application.component.generation-type.draft.renew')
                                        </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script type='text/javascript'>   
    $(document).ready(function(){
        $(document).on('click','.selectCompany',function(){
            id = $(this).data('id');
            $('#company_id').val(id);
            $('#company_premise_company_id').val(id);
            $('#company_partner_company_id').val(id);
            $('#company_store_company_id').val(id);

            $('#company-partner-datatable').DataTable().destroy();
            $('#company-partner-datatable').DataTable(loadCompanyPartnerConfig());
            $('#company-premise-datatable').DataTable().destroy();
            $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig());
            $('#company-store-datatable').DataTable().destroy();
            $('#company-store-datatable').DataTable(loadCompanyStoreConfig());
        // $('#company-premise-datatable').DataTable(loadCompanyPremiseConfig(form)).ajax.reload();
        })

        $(document).on('click','.selectPremise',function(){
            id = $(this).data('id');
            $('#premise_id').val(id);
        })

        $(document).on('click','.selectGenerationType',function(){
            id = $(this).data('id');
            $('#license_application_license_generation_type_id').val(id);
        })

        $(document).on('click','.submitButton',function(){
            $("#submission-form").removeClass("form-data-submission-using-message-bag");
            $("#submission-form").addClass("form-data-submission-with-confirmation-using-message-bag");
            $('#submission-form').attr('action', "{{ route('api.license_application.wholesale.change_and_renew.store') }}");
        })

        $(document).on('click','.draftButton',function(){
            $("#submission-form").removeClass("form-data-submission-with-confirmation-using-message-bag");
            $("#submission-form").addClass("form-data-submission-using-message-bag");
            $('#submission-form').attr('action', "{{ route('api.license_application.wholesale.change_and_renew.draft') }}");
        })

    }); 
</script>
@endpush

@push('wizard')
<script type='text/javascript'>   
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "Simpan",
            previous: "Kembali",
            next: "Seterusnya"
        }
        , onFinished: function (event, currentIndex) {
            alert("Form Submitted!");

        },
        onStepChanged: function (event, currentIndex, priorIndex) { 
            if(currentIndex == 6){
            $('ul[aria-label=Pagination] a[href="#finish"]').remove();

            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-primary draftButton" value="Simpan"  /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));
            
            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-success submitButton" value="Hantar" /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));

            }
            else {
            $('ul[aria-label=Pagination] input[value="Hantar"]').remove();
            $('ul[aria-label=Pagination] input[value="Simpan"]').remove();
            }
        }
    });
    
</script>
@endpush