@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Beli Padi</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Beli Padi</a></li>
            <li class="breadcrumb-item active">Lesen Beli Padi</li>
            <li class="breadcrumb-item active">Baharu</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form action="#" class="tab-wizard wizard-circle">

                    <h6>Butiran Surat Kelulusan Beli Padi</h6>
                    <section>
                        @include('license-application.component.buy-paddy')
                    </section>
                    <!-- Step 1 -->
                    <h6>Butiran Syarikat Dan Rakan Kongsi</h6>
                    <section>
                        @include('license-application.component.company')
                        @include('license-application.component.partner')
                    </section>
                    <!-- Step 2 -->
                    <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.premise')
                            </div>
                        </div>
                    </section>

                    <h6>Stor</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.store')
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                    <h6>Senarai Semak</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">#</th>
                                            <th>Fail</th>
                                            <th>Muat Naik</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                            Salinan SSM
                                            </td>
                                            <td>
                                            <input type="file" class="form-control"/>
                                            </td>
                                        </tr>                            
                                        <tr>
                                            <td>2</td>
                                            <td>
                                            Salinan lesen perniagaan (Sabah/Sarawak)
                                            </td>
                                            <td>
                                            <input type="file" class="form-control"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!-- Step 4 -->
                    <h6>Tindakan Seterusnya</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form">
                                    <div class="form-group row">
                                        <label for="example-email-input" class="col-2 col-form-label">Tempoh Permohonan</label>
                                        <div class="col-6">
                                            <select class="form-control" readonly>
                                                    <option selected> 1 Tahun</option>
                                                    <option> 2 Tahun </option>
                                                    <option> 3 Tahun</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-url-input" class="col-2 col-form-label">Had Muatan (TAN)</label>
                                        <div class="col-6">
                                            <input class="form-control" type="text" value="10" readonly/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-email-input" class="col-2 col-form-label">Status Permohonan</label>
                                        <div class="col-6">
                                            <select class="form-control">
                                                    <option selected>Sila Pilih</option>
                                                    <option> Simpan</option>
                                                    <option>Hantar</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script>
     $('#datatable').DataTable({
        responsive: true,
        searching:  false,
        ordering: false,
    });
</script>
@endpush