<!---
<h3 class="box-title mt-5">Alamat Premis Perniagaan</h3>
<hr>
<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <label>Alamat 1</label>
            <input type="text" value="NO 10" class="form-control">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <label>Alamat 2</label>
            <input type="text" value="KAMPONG" class="form-control">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Bandar</label>
            <input type="text" value="BATU CAVES" class="form-control">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Poskod</label>
            <input type="text" VALUE="52100" class="form-control">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Negeri</label>
            <input type="text" value="KEDAH" class="form-control">
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Parlimen</label>
            <input type="text" value="BATU" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Dewan Undangan Negeri (DUN)</label>
            <input type="text" value="LANGAT" class="form-control">
        </div>
    </div>
</div> 


<h3 class="box-title mt-5">Maklumat Tambahan</h3>
<hr>
<div class="row">
    <div class="col-md-6 ">
        <div class="form-group">
            <label>Jenis Bangunan</label>
            <input type="text" value="RUMAH KEDAI" class="form-control">
        </div>
    </div>
    <div class="col-md-6 ">
        <div class="form-group">
            <label>Jenis Perniagaan</label>
            <input type="text" value="STESEN MINYAK" class="form-control">
        </div>
    </div>
</div>
<div id="overlayermap" class="gmaps"></div>
--->

<div class="row">
    <!---
    <div class="col-md-12 ">
        <div class="form-group">
            <button alt="default" data-toggle="modal" data-target=".premise" class="btn btn-success float-right" >
                <i class="fa fa-plus"></i> Kemaskini Premis
            </button>
            <br/><br/>
        </div>
    </div>
    ----->
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6">Maklumat Premis Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil.</td>
                    <td style='text-align:left'>Nama Syarikat </td>
                    <td style='text-align:left'>Alamat </td>
                    <td style='text-align:center'>Jenis Bangunan</td>
                    <td style='text-align:center'>Jenis Syarikat</td>
                    <td style='text-align:center'>Dewan Undangan Negeri (DUN)</td>
                    <td style='text-align:center'>Parlimen</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1. </td>
                    <td style='text-align:left'>Segi Sdn Bhd </td>
                    <td style='text-align:left'>NO 10, <br/>KAMPONG,<br/>52100 BATU CAVES, KEDAH</td>
                    <td style='text-align:center'>RUMAH KEDAI</td>
                    <td style='text-align:center'>STESEN MINYAK</td>
                    <td style='text-align:center'>LANGAT</td>
                    <td style='text-align:center'>BATU</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('assets/plugins/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/gmaps/jquery.gmaps.js') }}"></script>
@endpush