<div id="premise_exist" class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-body">
            <div class="card">
                <div class="card-body">
                    <div id="premise-form">
                        <h3 class="box-title">Alamat Premis Perniagaan</h3>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Alamat 1 <span class='required'>*</span></label>
                                    <input type="text"  class="form-control" value="" name='premise_address_1' id='premise_address_1'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 ">
                                <div class="form-group">
                                    <label>Alamat 2</label>
                                    <input type="text"  class="form-control" value="" name='premise_address_2' id='premise_address_2'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Alamat 3</label>
                                    <input type="text"  class="form-control" value="" name='premise_address_3' id='premise_address_3'>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Poskod <span class='required'>*</span></label>
                                    <input type="text" name='premise_postcode' id="premise_postcode" digit='5' value="" class="form-control"  >
                                </div>
                            </div>

                            <!--/span-->
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Negeri <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_state_id' name='premise_state_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['states'] as $state)
                                            <option 
                                            data-code="{{ $state->code }}"
                                            value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Daerah <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_district_id' name='premise_district_id'>
                                        <option value="">Sila Pilih</option>
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Parlimen <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_parliament_id' name='premise_parliament_id'>
                                        <option value="">Sila Pilih</option>
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Dewan Undangan Negeri (DUN) <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_dun_id' name='premise_dun_id'>
                                        <option value="">Sila Pilih</option>
                                    <select>
                                </div>
                            </div>
                        </div> 

                        <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-4 col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Nombor Telefon <span class='required'>*</span></label>
                                    <input type="text" class="form-control" name='premise_phone_number' id='premise_phone_number' value="" maxlength="10">
                                    <small>Masukkan nombor telefon tanpa '-'  sebagai contoh : 0123033563</small><br/>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Nombor Faks</label>
                                    <input type="text" class="form-control" name='premise_fax_number' id='premise_fax_number' value="" maxlength="10">
                                        <small>Masukkan nombor faks tanpa '-'  sebagai contoh : 0123033563</small><br/>
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Emel <span class='required'>*</span></label>
                                    <input type="text" class="form-control" name='premise_email' value="" id='premise_email'>
                                </div>
                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Jenis Bangunan <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_building_type_id' name='premise_building_type_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['building_types'] as $buildingType)
                                            <option 
                                            data-code="{{ $buildingType->code }}"
                                            value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Jenis Perniagaan <span class='required'>*</span></label>
                                    <select class="form-control date" id='premise_business_type_id' name='premise_business_type_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['business_types'] as $businessType)
                                            <option 

                                            data-code="{{ $businessType->code }}"
                                            value="{{ encrypt($businessType->id) }}">{{ $businessType->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <input type="checkbox" class="check" id="is_premise_true" name="is_premise_true">
                                <label for="minimal-checkbox-2">Saya {{ Auth::user()->name }}, dengan ini mengaku bahawa segala maklumat premis yang tertera adalah yang <b style="color:green">BENAR</b></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $(document).on('click','.edit-premise',function(){
        var premiseId = $(this).data('id');
        $('#modal-premise-form').attr('action', "{{ route('api.license_application.company-premise.update',['']) }}/"+premiseId);
        $('#company_premise_method').val('PATCH');
        $('.premise-modal').modal('toggle');

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company-premise.find_company_premise_detail_by_premise_id',['']) }}/"+premiseId,
            success: function(data){
                $('#modal_premise_name').val(data.name);
                $('#modal_premise_mycoid').val(data.registration_number);
                $('#modal_premise_expiry_date').val(data.formatted_expiry_date);
                $('#modal_premise_paidup_capital').val(data.paidup_capital);
                $('#modal_premise_address_1').val(data.address_1);
                $('#modal_premise_address_2').val(data.address_2);
                $('#modal_premise_address_3').val(data.address_3);
                $('#modal_premise_postcode').val(data.postcode);
                $('#modal_premise_phone_number').val(data.phone_number);
                $('#modal_premise_fax_number').val(data.fax_number);
                $('#modal_premise_email').val(data.email);
                $("#modal_premise_building_type_id option[data-code='" + data.building_type_code +"']").attr("selected","selected");
                $("#modal_premise_business_type_id option[data-code='" + data.business_type_code +"']").attr("selected","selected");
                $("#modal_premise_state_id option[data-code='" + data.state_code +"']").attr("selected","selected");
                
                loadModalDistrictAndParliamentByStateId(data.state_id, data.district_code, data.parliament_code);
                loadModalDunBasedOnParliamentId(data.parliament_id, data.dun_code );
            }
        });
    })

    $('#add-company-premise').click(function(){
        userId = $('#modal_premise_user_id').val();
        $('#modal-premise-form').trigger("reset");
        $('#modal_premise_user_id').val(userId);
        $('#modal-premise-form').attr('action', "{{ route('api.license_application.company-premise.store') }}");
        $('#company_premise_method').val('POST');
        $('#modal_premise_building_type_id').prop('selectedIndex',0);
        $('#modal_premise_business_type_id').prop('selectedIndex',0);
        $('#modal_premise_state_id').prop('selectedIndex',0);
        $('#modal_premise_dun_id').empty();
        $('#modal_premise_district_id').empty();
        $('#modal_premise_parliament_id').empty();

        $('#modal_premise_dun_id').append('<option value="">Sila Pilih</option>');
        $('#modal_premise_district_id').append('<option value="">Sila Pilih</option>');
        $('#modal_premise_parliament_id').append('<option value="">Sila Pilih</option>');
    });

    $( "#modal_premise_state_id" ).change(function() {
        var stateId = $("#modal_premise_state_id option:selected").val();
        $('#modal_premise_district_id').empty();
        $('#modal_premise_district_id').append('<option value="">Sila Pilih</option>');
        
        $('#modal_premise_parliament_id').empty();
        $('#modal_premise_parliament_id').append('<option value="">Sila Pilih</option>');

        $('#modal_premise_dun_id').empty();
        $('#modal_premise_dun_id').append('<option value="">Sila Pilih</option>');

        loadModalDistrictAndParliamentByStateId(stateId);
    });

    function loadModalDistrictAndParliamentByStateId(stateId, districtCode = null, parliamentCode = null){
        $.ajax({
            type: "GET",
            url: "{{ route('api.state.find_district_and_parliament_by_id',['']) }}/"+stateId,
            success: function(data){
                // Use jQuery's each to iterate over the opts value
                $.each(data['districts'], function(i, d) {
                    selected = null;
                    if(d.code == districtCode){
                        selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#modal_premise_district_id').append('<option data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });

                $.each(data['parliaments'], function(i, d) {
                    selected = null;
                    if(d.code == parliamentCode){
                        selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#modal_premise_parliament_id').append('<option  data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });
            }
        });
    }

    function loadModalDunBasedOnParliamentId(parliamentId, dunCode = null){
        $.ajax({
            type: "GET",
            url: "{{ route('api.dun.find_dun_by_parliament_id',['']) }}/"+parliamentId,
            success: function(data){
                $.each(data, function(i, d) {
                    selected = null;
                    if(d.code == dunCode){
                            selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#modal_premise_dun_id').append('<option  data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });
            }
        });
    }

    $("#modal_premise_parliament_id" ).change(function() {
        var parliamentId = $("#modal_premise_parliament_id option:selected").val();
        $('#modal_premise_dun_id').empty();
        $('#modal_premise_dun_id').append('<option value="">Sila Pilih</option>');
         loadModalDunBasedOnParliamentId(parliamentId);
    });

    $( "#premise_state_id" ).change(function() {
        var stateId = $("#premise_state_id option:selected").val();
        $('#premise_district_id').empty();
        $('#premise_district_id').append('<option value="">Sila Pilih</option>');
        
        $('#premise_parliament_id').empty();
        $('#premise_parliament_id').append('<option value="">Sila Pilih</option>');

        $('#premise_dun_id').empty();
        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');

        loadDistrictAndParliamentByStateId(stateId);
    });

    function loadPremise(premiseId){
        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company-premise.find_company_premise_detail_by_premise_id',['']) }}/"+premiseId,
            success: function(data){
                $('#premise_name').val(data.name);
                $('#premise_mycoid').val(data.registration_number);
                $('#premise_expiry_date').val(data.formatted_expiry_date);
                $('#premise_paidup_capital').val(data.paidup_capital);
                $('#premise_address_1').val(data.address_1);
                $('#premise_address_2').val(data.address_2);
                $('#premise_address_3').val(data.address_3);
                $('#premise_postcode').val(data.postcode);
                $('#premise_phone_number').val(data.phone_number);
                $('#premise_fax_number').val(data.fax_number);
                $('#premise_email').val(data.email);
                $("#premise_building_type_id option[data-code='" + data.building_type_code +"']").attr("selected","selected");
                $("#premise_business_type_id option[data-code='" + data.business_type_code +"']").attr("selected","selected");
                $("#premise_state_id option[data-code='" + data.state_code +"']").attr("selected","selected");
                
                loadDistrictAndParliamentByStateId(data.state_id, data.district_code, data.parliament_code);
                loadDunBasedOnParliamentId(data.parliament_id, data.dun_code );
            }
        });
    }
    function resetPremise(){
        userId = $('#premise_user_id').val();
        $('#premise-form').trigger("reset");
        $('#premise_user_id').val(userId);
        $('#premise-form').attr('action', "{{ route('api.license_application.company-premise.store') }}");
        $('#premise_building_type_id').prop('selectedIndex',0);
        $('#premise_business_type_id').prop('selectedIndex',0);
        $('#premise_state_id').prop('selectedIndex',0);
        $('#premise_dun_id').empty();
        $('#premise_district_id').empty();
        $('#premise_parliament_id').empty();

        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');
        $('#premise_district_id').append('<option value="">Sila Pilih</option>');
        $('#premise_parliament_id').append('<option value="">Sila Pilih</option>');
    }

    function loadDistrictAndParliamentByStateId(stateId, districtCode = null, parliamentCode = null){
        $.ajax({
            type: "GET",
            url: "{{ route('api.state.find_district_and_parliament_by_id',['']) }}/"+stateId,
            success: function(data){
                // Use jQuery's each to iterate over the opts value
                $.each(data['districts'], function(i, d) {
                    selected = null;
                    if(d.code == districtCode){
                        selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#premise_district_id').append('<option data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });

                $.each(data['parliaments'], function(i, d) {
                    selected = null;
                    if(d.code == parliamentCode){
                        selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#premise_parliament_id').append('<option  data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });
            }
        });
    }

    $( "#premise_parliament_id" ).change(function() {
        var parliamentId = $("#premise_parliament_id option:selected").val();
        $('#premise_dun_id').empty();
        $('#premise_dun_id').append('<option value="">Sila Pilih</option>');
         loadDunBasedOnParliamentId(parliamentId);
    });

    function loadDunBasedOnParliamentId(parliamentId, dunCode = null){
        $.ajax({
            type: "GET",
            url: "{{ route('api.dun.find_dun_by_parliament_id',['']) }}/"+parliamentId,
            success: function(data){
                $.each(data, function(i, d) {
                    selected = null;
                    if(d.code == dunCode){
                            selected = 'selected';
                    }
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#premise_dun_id').append('<option  data-code="'+d.code+'" '+selected+' value="' + d.id + '">' + d.label + '</option>');
                });
            }
        });
    }

    $('#company-premise-datatable').DataTable(
        {
             "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "columnDefs": [
                {"className": "dt-center", "targets": "_all"}
            ],
         }
    );
</script>
@endpush