 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped" id='company-partner-datatable'>
            <thead>
                <tr>
                    <th colspan="7">Maklumat Pemilik, Rakan Kongsi dan Ahli Lembaga Pengarah Sedia Ada</th>
                    <th>
                         <button alt="default" id="add-partner" type="button" data-toggle="modal" data-target=".rakan-kongsi" class="btn btn-success float-right" >
                            Tambah
                        </button>
                    </th>
                </tr>
                <tr>
                    <th>Bil.</th>
                    <th style='text-align:center'>Nama</th>
                    <th style='text-align:center'>No<br/> K/P</th>
                    <th style='text-align:center'>No<br/> Telefon</th>
                    <th style='text-align:center'>Bangsa</th>
                    <th style='text-align:center'>Warganegara</th>
                    <th style='text-align:center'>Jumlah<br/> Syer(%)</th>
                    <th style='text-align:center'>Tindakan</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>

@push('modal')
<div class="modal fade-scale rakan-kongsi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' method='post' action="{{ route('api.license_application.company-partner.store') }}">
                <input type="hidden" id="user_id" name='user_id' class="form-control" placeholder="" value="{{ Auth::user()->id }}" >
                <input type="hidden" id="company_partner_company_id" name='company_id' value="" class="form-control" >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Rakan Kongsi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h3 class="card-title">Maklumat Rakan Kongsi</h3>
                    <hr>
                    <div class="row pt-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id='name_label' class="control-label">Nama </label>
                                <input type="text" class="form-control" name="name" />
                                <small class="form-control-feedback"> </small> </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label id='ic_label' class="control-label">Kad Pengenalan </label>
                                <input type="text" class="form-control" name="ic" />
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label id='ic_label' class="control-label">Nombor Telefon </label>
                                <input type="text" class="form-control" name="phone_number" />
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label id='email_label' class="control-label">Emel </label>
                                <input type="text" class="form-control" name="email" />
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Bangsa</label>
                                <select class="form-control date" id='race_id' name='race_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['races'] as $race)
                                        <option value="{{ encrypt($race->id) }}">{{ $race->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Jenis Bangsa</label>
                                <select class="form-control date" id='race_type_id' name='race_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['race_types'] as $raceType)
                                        <option value="{{ encrypt($raceType->id) }}">{{ $raceType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Bumiputera</label>
                                <select class="form-control date" id='is_citizen' name='is_citizen'>
                                    <option value="">Sila Pilih</option>
                                    <option value='{{ encrypt(1) }}'>Warganegara</option>
                                    <option value='{{ encrypt(0) }}'>Bukan Warganegara</option>
                                <select>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Jumlah Syer (RM)</label>
                                <input type="text" class="form-control" name="total_share" placeholder="" >
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">Peratus Syer(%)</label>
                                <input type="text" class="form-control" name="share_percentage" placeholder="" >
                            </div>
                        </div>
                    </div>
                    <h3 class="box-title mt-5">Alamat</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="form-group">
                                <label>Alamat 1</label>
                                <input type="text" value="" class="form-control" name='address_1' id='address_1'>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="form-group">
                                <label>Alamat 2</label>
                                <input type="text" value="" class="form-control" name='address_2' id='address_2'>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Alamat 3</label>
                                <input type="text" value="" class="form-control" name='address_3' id='address_3'>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Poskod</label>
                                <input type="text" name='postcode' class="form-control" maxLength='5' >
                            </div>
                        </div>

                        <!--/span-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Negeri</label>
                                <select class="form-control date" id='state_id' name='state_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['states'] as $state)
                                        <option value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
                </div>
            </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush
@push('js')
<script>
    function  loadCompanyPartnerConfig(form) {
        if (form == null) {
            form = {};
        }

        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_application.company-partner.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'nric',
                    name: 'nric'
                },
                {
                    data: 'phone_number',
                    name: 'phone_number'
                },
                {
                    data: 'race',
                    name: 'race'
                },
                {
                    data: 'citizen',
                    name: 'citizen'
                },
                {
                    data: 'share_percentage',
                    name: 'share_percentage'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
