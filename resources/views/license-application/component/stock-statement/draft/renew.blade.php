<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped table-responsived" id='company-stock-statement-datatable' style='width:100%'>
            <thead>
                <tr>
                    <th style='text-align:center'>Bil.</th>
                    <th style='text-align:center'>Penggal</th>
                    <th style='text-align:center'>Kategori Penyata Stok</th>
                    <th style='text-align:center'>Jenis Penyata Stok</th>
                    <th style='text-align:center'>Jenis Gred</th>
                    <th style='text-align:right'>Stok Mula</th>
                    <th style='text-align:right'>Stok Akhir</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('js')
<script>
    $('#company-stock-statement-datatable').DataTable(
        loadCompanyStockStatementConfig()
    );

    function loadCompanyStockStatementConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["company_id"] = $('#company_id').val();
        
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_application.company_stock_statement.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'stock_category',className: "text-centre",
                    name: 'stock_category'
                },  
                {
                    data: 'stock_type',className: "text-centre",
                    name: 'stock_type'
                },        
                {
                    data: 'total_grade',className: "text-centre",
                    name: 'total_grade'
                },
                {
                    data: 'initial_stock',className: "text-centre",
                    name: 'initial_stock'
                },
                {
                    data: 'balance_stock',className: "text-centre",
                    name: 'balance_stock'
                }
            ]

        };

        return json;
    }
</script>
@endpush
