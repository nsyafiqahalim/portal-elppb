 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6">Maklumat Syarikat Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil. </td>
                    <td style='text-align:center'>Nama <br/>Syarikat.</td>
                    <td style='text-align:center'>Jenis <br/>Syarikat</td>
                    <td style='text-align:center'>Tarikh <br/>Mansuh</td>
                    <td style='text-align:right'>Modal Berbayar <br/>(RM) </td>
                    <td style='text-align:center'>Alamat</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1.</td>
                    <td style='text-align:center'>Segi Berhad</td>
                    <td style='text-align:center'>SDN BHD</td>
                    <td style='text-align:center'>10/10/2017</td>
                    <td style='text-align:right'>2,100.00</td>
                    <td style='text-align:center'>NO 10, <br/>KAMPONG,<br/>52100 BATU CAVES, KEDAH</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>

@push('js')
    <script src="{{ asset('assets/plugins/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/gmaps/jquery.gmaps.js') }}"></script>
@endpush