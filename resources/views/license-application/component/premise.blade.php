<!---
<h3 class="box-title mt-5">Alamat Premis Perniagaan</h3>
<hr>
<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <label>Alamat 1</label>
            <input type="text" value="NO 10" class="form-control">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <label>Alamat 2</label>
            <input type="text" value="KAMPONG" class="form-control">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Bandar</label>
            <input type="text" value="BATU CAVES" class="form-control">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Poskod</label>
            <input type="text" VALUE="52100" class="form-control">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Negeri</label>
            <input type="text" value="KEDAH" class="form-control">
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Parlimen</label>
            <input type="text" value="BATU" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Dewan Undangan Negeri (DUN)</label>
            <input type="text" value="LANGAT" class="form-control">
        </div>
    </div>
</div> 


<h3 class="box-title mt-5">Maklumat Tambahan</h3>
<hr>
<div class="row">
    <div class="col-md-6 ">
        <div class="form-group">
            <label>Jenis Bangunan</label>
            <input type="text" value="RUMAH KEDAI" class="form-control">
        </div>
    </div>
    <div class="col-md-6 ">
        <div class="form-group">
            <label>Jenis Perniagaan</label>
            <input type="text" value="STESEN MINYAK" class="form-control">
        </div>
    </div>
</div>
<div id="overlayermap" class="gmaps"></div>
--->

<div class="row">
    <!--
    <div class="col-md-12 ">
        <div class="form-group">
            <button alt="default" data-toggle="modal" data-target=".premise" class="btn btn-success float-right" >
                <i class="fa fa-plus"></i> Tambah Premis
            </button>
            <br/><br/>
        </div>
    </div>
    --->
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="7">Maklumat Premis Sedia Ada</th>
                    <th colspan="1">
                         <button type='button' alt="default" data-toggle="modal" data-target=".premise" class="btn btn-md btn-success float-right" >
                            Tambah 
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil.</td>
                    <td style='text-align:left'>Nama Syarikat </td>
                    <td style='text-align:left'>Alamat </td>
                    <td style='text-align:center'>Jenis Bangunan</td>
                    <td style='text-align:center'>Jenis Syarikat</td>
                    <td style='text-align:center'>Dewan Undangan Negeri (DUN)</td>
                    <td style='text-align:center'>Parlimen</td>
                    <td style='text-align:center'>Pilih</td>
                </tr>
                <tr>
                    <td style='text-align:left'>1. </td>
                    <td style='text-align:left'>Segi Sdn Bhd </td>
                    <td style='text-align:left'>NO 10, <br/>KAMPONG,<br/>52100 BATU CAVES, KEDAH</td>
                    <td style='text-align:center'>RUMAH KEDAI</td>
                    <td style='text-align:center'>STESEN MINYAK</td>
                    <td style='text-align:center'>LANGAT</td>
                    <td style='text-align:center'>BATU</td>
                    <td style='text-align:center'>
                        <input type="radio" name="a" checked />
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
<div class="modal fade-scale premise" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Tambah Premis</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h3 class="box-title mt-5">Alamat Premis Perniagaan</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Alamat 1</label>
                            <input type="text" value="NO 10" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <label>Alamat 2</label>
                            <input type="text" value="KAMPONG" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Bandar</label>
                            <input type="text" value="BATU CAVES" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Poskod</label>
                            <input type="text" VALUE="52100" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Negeri</label>
                            <input type="text" value="KEDAH" class="form-control">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Parlimen</label>
                            <input type="text" value="BATU" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Dewan Undangan Negeri (DUN)</label>
                            <input type="text" value="LANGAT" class="form-control">
                        </div>
                    </div>
                </div> 


                <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                <hr>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Jenis Bangunan</label>
                            <input type="text" value="RUMAH KEDAI" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Jenis Perniagaan</label>
                            <input type="text" value="STESEN MINYAK" class="form-control">
                        </div>
                    </div>
                </div>
                <div id="overlayermap" class="gmaps"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success waves-effect text-left" data-dismiss="modal">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>