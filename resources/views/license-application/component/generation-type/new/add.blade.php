<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped table-responsived" id='generation-type-datatable' style='width:100%'>
            <thead>
                <tr>
                    <th style='text-align:left'>Bil.</th>
                    <th style='text-align:left'>Lesen Yang Bakal Dijana </th>
                    <th style='text-align:center'>Pilih</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
</div>
@push('js')
<script>
    // $('#company-premise-datatable').DataTable(
    //     loadCompanyPremiseConfig()
    // );

    function loadGenerationTypeConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        form["license_type"] = $('#license_type').val();
        form["license_application_type"] = $('#license_application_type').val();
        form["company_id"] = $('#company_id').val();
        form["include_license_id"] = $('#include_license_id').val();

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "responsive": true,
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "bFilter" : false,  
            "bInfo": false,       
            "bPaginate": false,      
            "bLengthChange": false,
            "ajax": {
                "url": "{{ route('api.license_application.generation_type.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name', className: "text-left",
                    name: 'name'
                },                
                {
                    data: 'action',className: "text-centre",
                    name: 'action'
                },
            ]

        };

        return json;
    }
</script>
@endpush
