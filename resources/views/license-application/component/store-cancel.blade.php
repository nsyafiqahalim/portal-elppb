 <div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="5">Maklumat Alamat Stor Perniagaan Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil. </td>
                    <td style='text-align:left'>Nama Syarikat </td>
                    <td style='text-align:left'>Alamat </td>
                    <td style='text-align:center'>Had Muatan (TAN/METRIK)</td>
                    <td style='text-align:center'>Status Stor</td>
                </tr>
                <tr>
                    <td style='text-align:center'>2.</td>
                    <td style='text-align:center'>TIda SDN BHD</td>
                    <td style='text-align:left'>NO 10, <br/>KAMPONG,<br/>52100 BATU CAVES, KEDAH</td>
                    <td style='text-align:center'>1</td>
                    <td style='text-align:center'>Aktif</td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>

 <div class="col-md-12 ">
     <h3 class="box-title mt-5">Maklumat Tambahan</h3>
        <hr>
        <div class="row">
            <div class="col-md-6 ">
                <div class="form-group">
                    <label for="example-url-input" class="col-form-label">Lesen Dibatalkan</label>
                    <input class="form-control" type="text" value="D00001" readonly/>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label for="example-url-input" class="col-form-label">Tujuan Pembatalan</label>
                    <select class="form-control">
                            <option>Muflis</option>
                            <option>Tutup Syarikat</option>
                            <option>Syarikat Dibeli</option>
                            <option>Lain - lain</option>
                    </select>
                    <small>Jika pilih 'lain-lain'. Nyatakan di ulasan</small>
                </div>
                
            </div>
            <div class="col-md-12 ">
                <div class="form-group">
                    <label for="example-url-input" class="col-form-label">Ulasan</label>
                    <textarea class="form-control" rows='10'></textarea>
                </div>
            </div>
        </div>
    </div>
    
@push('js')
    <script src="{{ asset('assets/plugins/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/gmaps/jquery.gmaps.js') }}"></script>
@endpush