<div class="form-body">
        <h3 class="card-title">Maklumat Syarikat</h3>
        <hr>
        <div class="row">
            <div class="col-md-4 col-lg-6 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Nama Syarikat</label>
                    <input type="text" id="company_name" name='company_name' class="form-control" value="{{ $inputParam['license_application_company']->name }}" >
                </div>
            </div>
            <div class="col-md-4 col-lg-6 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Jenis Syarikat</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fas fa-building"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" value="{{ $inputParam['license_application_company']->company_type_name }}" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">No Pendaftaran Baharu / Trading License / IRD</label>
                    <input type="text" class="form-control" value="{{ $inputParam['license_application_company']->registration_number }}" disabled>
                </div>
            </div>
                <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">No Pendaftaran Lama</label>
                    <input type="text" class="form-control" value="{{ $inputParam['license_application_company']->old_registration_number }}" disabled>
                </div>
            </div>
            <div class="col-md-3 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Tarikh Mansuh</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="mdi mdi-calendar-question"></i>
                            </span>
                        </div>
                            <input type="text" class="form-control" value="{{ \Carbon\Carbon::parse($inputParam['license_application_company']->expiry_date)->format('d/m/Y') }}" disabled>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Modal Berbayar</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">RM</span>
                        </div>
                        <input type="text" class="form-control" value="{{ number_format($inputParam['license_application_company']->paidup_capital,2) }}" disabled style='text-align:right'>
                    </div>
                </div>
            </div>
        </div>
        <!-- Company Address -->
        <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
        <hr>
        <div class="row">
            <div class="col-md-6 col-lg-12 col-xs-12">
                <div class="form-group">
                    <label>Alamat 1</label>
                    <input type="text" value="{{ $inputParam['license_application_address']->address_1 }}" disabled class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12">
                <div class="form-group">
                    <label>Alamat 2</label>
                    <input type="text" value="{{ $inputParam['license_application_address']->address_2}}" disabled class="form-control">
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-12">
                <div class="form-group">
                    <label>Alamat 3</label>
                    <input type="text" value="{{ $inputParam['license_application_address']->address_3 }}" disabled class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-6 col-xs-12">
                <div class="form-group">
                    <label>Poskod</label>
                    <input type="text" value="{{ $inputParam['license_application_address']->postcode }}" disabled class="form-control">
                </div>
            </div>
            <div class="col-md-4 col-lg-6 col-xs-12">
                <div class="form-group">
                    <label>Negeri</label>
                    <input type="text" value="{{ $inputParam['license_application_address']->state_name }}" disabled class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label>No. Telefon</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="mdi mdi-cellphone"></i>
                            </span>
                        </div>
                        <input type="text" value="{{ $inputParam['license_application_address']->phone_number }}" disabled class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label>No. Faks</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="mdi mdi-fax"></i>
                            </span>
                        </div>
                        <input type="text" disabled value="{{ $inputParam['license_application_company']->fax_number }}" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-xs-12">
                <div class="form-group">
                    <label>E-mel</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="mdi mdi-email"></i>
                            </span>
                        </div>
                        <input type="text" value="{{ $inputParam['license_application_company']->email }}" disabled class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>