<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            <table class="table table-bordered table-striped" id='company-store-datatable'>
                <thead>
                    <tr>
                        <th colspan="7">Maklumat Stor Sedia Ada <small></th>
                    </tr>
                    <tr>
                        <th style='text-align:left'>Bil.</th>
                        <th style='text-align:left'>Nama Syarikat </th>
                        <th style='text-align:left'>Alamat </th>
                        <th style='text-align:center'>Jenis Bangunan</th>
                        <th style='text-align:center'>Jenis Hak Milik</th>
                        <th style='text-align:center'>Kemaskini</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@push('modal')
<div class="modal fade-scale store-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form class='url-encoded-submission' method='post' id='store-form'>
                <input type="hidden" id="company_store_company_id" name='company_id' value="{{ encrypt($inputParam['company_id']) }}" class="form-control" >
                <input type="hidden" id="company_store_method" name='_method' >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Stor</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h3 class="box-title">Alamat Stor Perniagaan</h3>
                    <hr>
                     <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label id="store_address_1_label">Alamat 1 <span class='required'>*</span></label>
                                    <input type="text" value="" class="form-control" name='store_address_1' id='store_address_1'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 ">
                                <div class="form-group">
                                    <label id="store_address_2_label">Alamat 2 </label>
                                    <input type="text" value="" class="form-control" name='store_address_2' id='store_address_2'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id="store_address_3_label">Alamat 3 <span class='required'>*</span></label>
                                    <input type="text" value="" class="form-control" name='store_address_3' id='store_address_3'>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id='store_postcode_label'>Poskod <span class='required'>*</span></label>
                                    <input type="text" name='store_postcode'  id='store_postcode'class="form-control"  >
                                </div>
                            </div>

                            <!--/span-->
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id='store_state_id_label'>Negeri <span class='required'>*</span></label>
                                    <select class="form-control date" id='store_state_id' name='store_state_id'>
                                        <option value="">Sila Pilih</option>
                                        @foreach($inputParam['states'] as $state)
                                            <option 
                                            data-code="{{ $state->code }}"
                                            value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label id='store_district_id_label'>Daerah <span class='required'>*</span></label>
                                <select class="form-control date" id='store_district_id' name='store_district_id'>
                                    <option value="">Sila Pilih</option>
                                <select>
                            </div>
                        </div>
                    </div> 

                    <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="store_phone_number_label" >Nombor Telefon <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='store_phone_number' id='store_phone_number'>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="store_email_label" >Emel <span class='required'>*</span></label>
                                <input type="text" value="" class="form-control" name='store_email' id='store_email'>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="store_faks_number_label" >Faks</label>
                                <input type="text" value="" class="form-control" name='store_faks_number' id='store_faks_number'>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label id="store_building_type_id_label">Jenis Bangunan <span class='required'>*</span></label>
                                <select class="form-control date" id='store_building_type_id' name='store_building_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['building_types'] as $buildingType)
                                        <option 
                                        data-code="{{ $buildingType->code }}"
                                        value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label id="store_store_ownership_type_id_label">Jenis Hak Milik <span class='required'>*</span></label>
                                <select class="form-control date" id='store_store_ownership_type_id' name='store_store_ownership_type_id'>
                                    <option value="">Sila Pilih</option>
                                    @foreach($inputParam['store_ownership_types'] as $storeOwnershipType)
                                        <option data-code="{{ $storeOwnershipType->code }}" value="{{ encrypt($storeOwnershipType->id) }}">{{ $storeOwnershipType->name }}</option>
                                    @endforeach
                                <select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect text-left">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endpush
@push('js')
<script>
    $('#company-store-datatable').DataTable(
        loadCompanyStoreConfig()
    );

    $(document).on('click','.edit-store',function(){
        var premiseId = $(this).data('id');
        $('#store-form').attr('action', "{{ route('api.license_application.license-application-company-store.update',['']) }}/"+premiseId);
        $('#company_store_method').val('PATCH');
        $('.store-modal').modal('toggle');
        $('#store_district_id').empty();
        $('#store_district_id').append('<option value="">Sila Pilih</option>');

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company-store.find_company_store_detail_by_store_id',['']) }}/"+premiseId,
            success: function(data){
                $('#store_name').val(data.name);
                $('#store_address_1').val(data.address_1);
                $('#store_address_2').val(data.address_2);
                $('#store_address_3').val(data.address_3);
                $('#store_postcode').val(data.postcode);
                $('#store_phone_number').val(data.phone_number);
                $('#store_fax_number').val(data.fax_number);
                $('#store_email').val(data.email);
                $("#store_building_type_id option[data-code='" + data.building_type_code +"']").attr("selected","selected");
                $("#store_store_ownership_type_id option[data-code='" + data.store_ownership_type_code +"']").attr("selected","selected");
                $("#store_state_id option[data-code='" + data.state_code +"']").attr("selected","selected");
                
                loadDistrictByStateId(data.state_id, data.district_code);
            }
        });
    })

    $('#add-company-store').click(function(){
        userId = $('#store_user_id').val();
        $('#store-form').trigger("reset");
        $('#store_user_id').val(userId);
        $('#store-form').attr('action', "{{ route('api.license_application.company-store.store') }}");
        $('#company_store_method').val('POST');
        $('#store_building_type_id').prop('selectedIndex',0);
        $('#store_store_ownership_type_id').prop('selectedIndex',0);
        $('#store_state_id').prop('selectedIndex',0);
        $('#store_district_id').empty();

        $('#store_dun_id').append('<option value="">Sila Pilih</option>');
        $('#store_district_id').append('<option value="">Sila Pilih</option>');
        $('#store_parliament_id').append('<option value="">Sila Pilih</option>');
    });
    
    $( "#store_state_id" ).change(function() {
        var stateId = $("#store_state_id option:selected").val();
        $('#store_district_id').empty();
        $('#store_district_id').append('<option value="">Sila Pilih</option>');
        loadDistrictByStateId(stateId);
    });

    function loadDistrictByStateId(stateId, districtCode = null){
         $.ajax({
                type: "GET",
                url: "{{ route('api.state.find_district_and_parliament_by_id',['']) }}/"+stateId,
                success: function(data){
                    // Use jQuery's each to iterate over the opts value
                    $.each(data['districts'], function(i, d) {
                        selected = null;
                        if(d.code == districtCode){
                            selected = 'selected';
                        }

                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#store_district_id').append('<option '+selected+' data-code="'+d.code+'" value="' + d.id + '">' + d.label + '</option>');
                    });
                }
            });
    }

    function loadCompanyStoreConfig(form) {
        if (form == null) {
            form = {};
        }
        //form["company_id"] = $('#company_id').val();

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.license_application.license-application-company-store.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "async": false,
                "data": (d)=>{
                     d.src           =   "datatable";
                    d.company_id    =   $('#company_id').val();
                    d.license_application_id    =   "{{ $inputParam['encrypted_license_application_id'] }}";
                    d.store_id      =   $('.store_id').map(function(){ 
                                            return this.value; 
                                        }).get();
                   
                },
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex', className: "text-centre",
                    name: 'DT_RowIndex'
                },
                {
                    data: 'company_name', className: "text-centre",
                    name: 'company_name'
                },
                {
                    data: 'address',className: "text-centre",
                    name: 'address'
                },
                {
                    data: 'building_type',className: "text-centre",
                    name: 'building_type'
                },
                {
                    data: 'store_ownership_type',className: "text-centre",
                    name: 'store_ownership_type'
                },
                {
                    data: 'edit',className: "text-centre",
                    name: 'edit'
                },
            ]

        };

        return json;
    }
</script>
@endpush
