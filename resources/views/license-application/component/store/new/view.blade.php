<div class="row">
    <div class="col-md-12 ">
        <div class="form-group">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="6">Maklumat Stor Sedia Ada</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style='text-align:left'>Bil.</td>
                    <td style='text-align:left'>Nama Syarikat </td>
                    <td style='text-align:left'>Alamat </td>
                    <td style='text-align:center'>Jenis Bangunan</td>
                    <td style='text-align:center'>Jenis Hak Milik</td>
                </tr>
                
                @foreach($inputParam['license_application_stores'] as $index    => $store)
                <tr>
                    <td style='text-align:left'>{{ $index + 1  }} </td>
                    <td style='text-align:left'>{{ $inputParam['license_application_company']->name }} </td>
                    <td style='text-align:left'>
                        {{ $store->address_1 }}, <br/>
                        {{ $store->address_2 }},<br/>
                        @if($store->address_3)
                        {{ $store->address_2 }},<br/>
                        @endif
                        {{ $store->postcode }},<br/>
                        {{ $store->state_name }}
                    </td>
                    <td style='text-align:center'> {{ $store->building_type_name }}</td>
                    <td style='text-align:center'>{{ $store->storeOwnershipType->name }}</td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        </div>
    </div>
</div>
