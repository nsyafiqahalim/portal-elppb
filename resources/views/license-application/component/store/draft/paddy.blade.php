<div id="store_exist" class="row" >
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-body">
            <div class="card">
                <div class="card-body">
                    <div id="store-form">
                        <h3 class="box-title">Alamat Stor Perniagaan</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Alamat 1 <span class='required'>*</span></label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->address_1 ?? null }}" class="form-control" name='store_address_1' id='store_address_1'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 ">
                                <div class="form-group">
                                    <label>Alamat 2 </label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->address_2 ?? null}}" class="form-control" name='store_address_2' id='store_address_2'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Alamat 3 <span class='required'>*</span></label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->address_3 ?? null }}" class="form-control" name='store_address_3' id='store_address_3'>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Poskod <span class='required'>*</span></label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->postcode ?? null}}" name='store_postcode'  id='store_postcode' maxLength='5' class="form-control"  >
                                </div>
                            </div>

                            <!--/span-->
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Negeri<span class='required'>*</span></label>
                                    <select class="form-control date" id='store_state_id' name='store_state_id'>
                                        <option value="">Sila Pilih</option>
                                        @if(isset($inputParam['store_states']))
                                            @foreach($inputParam['store_states'] as $state)
                                                <option 
                                                @if(isset($inputParam['license_application_store']) &&  $inputParam['license_application_store']->state_id == $state->id)
                                                    selected
                                                @endif
                                                value="{{ encrypt($state->id) }}">{{ $state->name }}</option>
                                            @endforeach
                                        @endif
                                    <select>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Daerah<span class='required'>*</span></label>
                                    <select class="form-control date" id='store_district_id' name='store_district_id'>
                                        <option value="">Sila Pilih</option>
                                        @if(isset($inputParam['store_districts']))
                                            @foreach($inputParam['store_districts'] as $district)
                                                <option 
                                                @if(isset($inputParam['license_application_store']) && $inputParam['license_application_store']->district_id == $district->id)
                                                        selected
                                                @endif

                                                value="{{ encrypt($district->id) }}">{{ $district->name }} </option>
                                            @endforeach
                                        @endif
                                    <select>
                                </div>
                            </div>
                        </div> 

                        <h3 class="box-title mt-5">Maklumat Tambahan</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label >Nombor Telefon <span class='required'>*</span></label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->phone_number ?? null}}"  class="form-control" name='store_phone_number' id='store_phone_number'>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Emel <span class='required'>*</span></label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->email ?? null}}"  class="form-control" name='store_email' id='store_email'>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Faks</label>
                                    <input type="text" value="{{ $inputParam['license_application_store']->fax_number ?? null}}" class="form-control" name='store_fax_number' id='store_fax_number'>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Jenis Bangunan <span class='required'>*</span></label>
                                    <select class="form-control date" id='store_building_type_id' name='store_building_type_id'>
                                        <option value="">Sila Pilih</option>
                                        @if(isset($inputParam['store_building_types']))
                                            @foreach($inputParam['store_building_types'] as $buildingType)
                                                <option 
                                                @if(isset($inputParam['license_application_store']) &&  $inputParam['license_application_store']->building_type_id == $buildingType->id)
                                                    selected
                                                @endif
                                                value="{{ encrypt($buildingType->id) }}">{{ $buildingType->name }}</option>
                                            @endforeach
                                        @endif
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>Jenis Hak Milik <span class='required'>*</span></label>
                                    <select class="form-control date" id='store_store_ownership_type_id' name='store_store_ownership_type_id'>
                                        <option value="">Sila Pilih</option>
                                        @if(isset($inputParam['store_store_ownership_types']))
                                            @foreach($inputParam['store_store_ownership_types'] as $storeOwnershipType)
                                                <option 
                                                @if(isset($inputParam['license_application_store']) &&  $inputParam['license_application_store']->store_ownership_type_id == $storeOwnershipType->id)
                                                    selected
                                                @endif
                                                value="{{ encrypt($storeOwnershipType->id) }}">{{ $storeOwnershipType->name }}</option>
                                            @endforeach
                                        @endif
                                    <select>
                                </div>
                            </div>
                    </div>
                    </div>
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <input type="checkbox" class="check" 
                                @if(isset($inputParam['license_application_store']->is_store_validate) && $inputParam['license_application_store']->is_store_validate == 1)
                                    checked
                                @endif
                                id="is_store_true" name="is_store_true" >
                                <label for="minimal-checkbox-2">Saya {{ Auth::user()->name }}, dengan ini mengaku bahawa segala maklumat stor yang tertera adalah yang <b style="color:green">BENAR</b></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
   function loadStore(storeId){
        $('#store_district_id').empty();
        $('#store_district_id').append('<option value="">Sila Pilih</option>');

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company-store.find_company_store_detail_by_store_id',['']) }}/"+storeId,
            success: function(data){
                $('#store_name').val(data.name);
                $('#store_address_1').val(data.address_1);
                $('#store_address_2').val(data.address_2);
                $('#store_address_3').val(data.address_3);
                $('#store_postcode').val(data.postcode);
                $('#store_phone_number').val(data.phone_number);
                $('#store_fax_number').val(data.fax_number);
                $('#store_email').val(data.email);
                $("#store_building_type_id option[data-code='" + data.building_type_code +"']").attr("selected","selected");
                $("#store_store_ownership_type_id option[data-code='" + data.store_ownership_type_code +"']").attr("selected","selected");
                $("#store_state_id option[data-code='" + data.state_code +"']").attr("selected","selected");
                
                loadDistrictByStateId(data.state_id, data.district_code);
            }
        });
   }

    function resetStore(){
        userId = $('#store_user_id').val();
        $('#store-form').trigger("reset");
        $('#store_user_id').val(userId);
        $('#store-form').attr('action', "{{ route('api.license_application.company-store.store') }}");
        $('#company_store_method').val('POST');
        $('#store_building_type_id').prop('selectedIndex',0);
        $('#store_business_type_id').prop('selectedIndex',0);
        $('#store_state_id').prop('selectedIndex',0);
        $('#store_district_id').empty();

        $('#store_dun_id').append('<option value="">Sila Pilih</option>');
        $('#store_district_id').append('<option value="">Sila Pilih</option>');
        $('#store_parliament_id').append('<option value="">Sila Pilih</option>');
    }
    
    $( "#store_state_id" ).change(function() {
        var stateId = $("#store_state_id option:selected").val();
        $('#store_district_id').empty();
        $('#store_district_id').append('<option value="">Sila Pilih</option>');
        loadDistrictByStateId(stateId);
    });

    function loadDistrictByStateId(stateId, districtCode = null){
         $.ajax({
                type: "GET",
                url: "{{ route('api.state.find_district_and_parliament_by_id',['']) }}/"+stateId,
                success: function(data){
                    // Use jQuery's each to iterate over the opts value
                    $.each(data['districts'], function(i, d) {
                        selected = null;
                        console.log(districtCode);
                        if(d.code == districtCode){
                            selected = 'selected';
                        }

                        // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                        $('#store_district_id').append('<option '+selected+' data-code="'+d.code+'" value="' + d.id + '">' + d.label + '</option>');
                    });
                }
            });
    }
</script>
@endpush
