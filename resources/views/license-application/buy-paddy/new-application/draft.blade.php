@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Beli Padi</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Baharu</a></li>
            <li class="breadcrumb-item">Lesen Beli Padi</li>
            <li class="breadcrumb-item active">Baharu</li>
        </ol>
    </div>
</div>
<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="tab-wizard wizard-circle" id='submission-form' text="Anda Pasti?" method="post" >
                    @csrf
                    @method('PATCH')
                    <input type='hidden' name='company_id' id='company_id' value="@if(isset($inputParam['license_application_temporary']->company_id)){{ encrypt($inputParam['license_application_temporary']->company_id)}}@endif" />
                    <input type='hidden' name='premise_id' id='premise_id' value="@if(isset($inputParam['license_application_temporary']->company_premise_id)){{encrypt($inputParam['license_application_temporary']->company_premise_id)}}@endif"/>
                    <input type='hidden' name='store_id' id='store_id' value="@if(isset($inputParam['license_application_temporary']->company_store_id)){{encrypt($inputParam['license_application_temporary']->company_store_id)}}@endif"/>
                    <input type='hidden' name='user_id' id='user_id' value="{{ encrypt(Auth::user()->id) }}"/>
                    <input type='hidden' name='license_application_buy_paddy_condition_id' id='license_application_buy_paddy_condition_id' value="@if(isset($inputParam['license_application_buy_paddy_condition_id'])){{encrypt($inputParam['license_application_buy_paddy_condition_id'])}}@endif" />
                    <input type='hidden' name='approval_letter_id' id='approval_letter_id' value="@if(isset($inputParam['approval_letter_id'])){{encrypt($inputParam['approval_letter_id'])}}@endif" />
                    <input type='hidden' name='domain' id='domain' value="{{ encrypt('BORONG') }}"/>
                    <input type='hidden' name='material_domain' id='material_domain' value="{{ encrypt('BUY_PADDY_NEW') }}"/>

                    <!-- Step 1 -->
                    <h6>Syarikat Dan Rakan Kongsi</h6>
                    <section>
                        @include('license-application.component.company.draft.paddy')
                        @include('license-application.component.partner.draft.add')
                    </section>
                    <!-- Step 2 -->
                    <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.premise.draft.paddy')
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                     <h6>Stor</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.store.draft.paddy')
                            </div>
                        </div>
                    </section>

                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.material.draft.add')
                            </div>
                        </div>
                    </section>
                    <!-- Step 4 -->
                    <h6>Maklumat Permohonan</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="example-email-input" class="col-2 col-form-label">Tempoh Permohonan</label>
                                    <div class="col-6">
                                        <select class="form-control" name='apply_duration' readonly>
                                                <option selected> 1 Tahun</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-url-input" class="col-2 col-form-label">Had Muatan (TAN)</label>
                                    <div class="col-6">
                                        <input class="form-control" type="text" name='apply_load'  value="100" readonly/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-url-input" class="col-2 col-form-label">Syarat Membeli Padi</label>
                                    <div class="col-6">
                                        <input class="form-control" type="text" id="license_application_buy_paddy_condition_name" value="{{ $inputParam['license_application_buy_paddy_condition_name'] }}" readonly/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script type='text/javascript'>   
    $(document).ready(function(){
        $(document).on('click','.selectCompany',function(){
            id = $(this).data('id');
            $('#company_id').val(id);
            $('#company_premise_company_id').val(id);
            $('#company_partner_company_id').val(id);
            $('#company_store_company_id').val(id);

            checkApprovalLetterExist(id);
                $('#add-company-partner').show();
                $('#add-company-premise').show();
                $('#add-company-store').show();
            });
        
        async function checkApprovalLetterExist(id){
            $.ajax({
                    type: 'GET',
                    url: "{{ route('api.approval_letter_management.is_buy_paddy_approval_letter_exist') }}",
                    data: {
                        company_id:id,
                        domain: $('#domain').val()
                    },
                    success: function (data) {
                       if(data.status == 'EXIST'){
                            $('#add-partner').hide();
                            $('#add-premise').hide();
                            $('#add-store').hide();
                            $('#include_license_id').val(data.license_id);
                            $('#license').val(data.license);
                            $('#premise_id').val(data.premise_id);
                            $('#store_id').val(data.store_id);
                            $('#has_previous_license').val(data.status);
                            $('#license_application_buy_paddy_condition_name').val(data.license_application_buy_paddy_condition_name);
                            $('#license_application_buy_paddy_condition_id').val(data.license_application_buy_paddy_condition_id);
                            $('#approval_letter_id').val(data.approval_letter_id);
                            $('#premise_not_exist').hide();
                            $('#premise_exist').show();
                            $('#store_not_exist').hide();
                            $('#store_exist').show();
                            loadPremise(data.premise_id);
                            loadStore(data.store_id);
                       }
                       else{
                            $('#add-partner').show();
                            $('#add-premise').show();
                            $('#add-store').show();
                            $('#include_license_id').val(null);
                            $('#license').val(data.license);
                            $('#premise_id').val(null);
                            $('#store_id').val(null);
                            $('#has_previous_license').val(data.status);
                            $('#license_application_buy_paddy_condition_name').val(null);
                            $('#license_application_buy_paddy_condition_id').val(null);
                            $('#approval_letter_id').val(null);
                            $('#premise_not_exist').show();
                            $('#premise_exist').hide();
                            $('#store_not_exist').show();
                            $('#store_exist').hide();
                            resetPremise();
                            resetStore();
                       }
                        reloadAllDatataTable();
                    },
                    error: function (data) {

                    }
                });
        }

        function reloadAllDatataTable(){
            $('#company-partner-datatable').DataTable().destroy();
            $('#company-partner-datatable').DataTable(loadCompanyPartnerConfig());
        }

        $(document).on('click','.selectPremise',function(){
            id = $(this).data('id');
            $('#premise_id').val(id);
        })

        $(document).on('click','.selectStore',function(){
            id = $(this).data('id');
            $('#store_id').val(id);
        })

        $(document).on('click','.selectGenerationType',function(){
            id = $(this).data('id');
            $('#license_application_license_generation_type_id').val(id);
        })

        $(document).on('click','.submitButton',function(){
            $("#submission-form").removeClass("form-data-submission-using-message-bag");
            $("#submission-form").addClass("form-data-submission-with-confirmation-using-message-bag");
            $('#submission-form').attr('action',"{{ route('api.license_application.buy_paddy.draft.submit',[$inputParam['current_license_application_id']]) }}");
        })

        $(document).on('click','.draftButton',function(){
            $("#submission-form").removeClass("form-data-submission-with-confirmation-using-message-bag");
            $("#submission-form").addClass("form-data-submission-using-message-bag");
            $('#submission-form').attr('action', "{{ route('api.license_application.buy_paddy.draft.update',[$inputParam['current_license_application_id']]) }}");
        })
    }); 
</script>
@endpush

@push('wizard')
<script type='text/javascript'>   
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "Simpan",
            previous: "Kembali",
            next: "Seterusnya"
        }
        , onFinished: function (event, currentIndex) {
            alert("Form Submitted!");

        },
        onStepChanged: function (event, currentIndex, priorIndex) { 
            if(currentIndex == 4){
            $('ul[aria-label=Pagination] a[href="#finish"]').remove();

            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-primary draftButton" value="Simpan"  /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));
            
            var $input = $('<li aria-hidden="false" style=""><input type="submit" style="font-size:16px" class="btn btn-success submitButton" value="Hantar" /></li>');
            $input.appendTo($('ul[aria-label=Pagination]'));

            }
            else {
            $('ul[aria-label=Pagination] input[value="Hantar"]').remove();
            $('ul[aria-label=Pagination] input[value="Simpan"]').remove();
            }
        }
    });
    
</script>
@endpush