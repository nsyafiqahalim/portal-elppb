


@extends('layouts.license-application-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Runcit</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Lesen</a></li>
            <li class="breadcrumb-item active">Lesen Runcit</li>
            <li class="breadcrumb-item active">Permohonan Penggantian</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs customtab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile2" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Butiran Syarikat</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#partner" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Rakan Kongsi</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Bukti Penggantian</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#action" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Tindakan</span></a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane  p-3 active" id="profile2" role="tabpanel">
                    <div class="form-group row">
                        <label for="example-email-input" class="col-2 col-form-label">Syarikat</label>
                        <div class="col-6">
                            <select class="form-control">
                                    <option> Syarikat A</option>
                                    <option> Syarikat B</option>
                                    <option> Syarikat C</option>
                            </select>
                        </div>
                    </div>
                    @include('license-management.application.component.company-detail')
                </div>
                <div class="tab-pane p-3" id="partner" role="tabpanel">
                    
                </div>
                <div class="tab-pane p-3" id="messages2" role="tabpanel">
                    <table class="table no-wrap table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="10">#</th>
                                <th>Fail</th>
                                <th>Muat Naik</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    Bukti 1
                                </td>
                                <td>
                                <input type="file" class="form-control"/>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>
                                    Bukti 2
                                </td>
                                <td>
                                <input type="file" class="form-control"/>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>
                                    Bukti 3
                                </td>
                                <td>
                                <input type="file" class="form-control"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane p-3" id="action" role="tabpanel">
                    <h3>Tindakan Seterusnya</h3>
                    <form class="form">
                        <div class="form-group row">
                            <label for="example-email-input" class="col-2 col-form-label">Jangka Masa</label>
                            <div class="col-6">
                                <select class="form-control">
                                        <option> 1 Tahun</option>
                                        <option> 2 Tahun </option>
                                        <option> 3 Tahun</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-url-input" class="col-2 col-form-label">Ulasan</label>
                            <div class="col-6">
                                <textarea class="form-control" rows="10" type="url"></textarea>
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-success mr-2">Hantar</button>
                        <button type="submit" class="btn btn-secondary mr-2 float-right">Hantar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script>
     $('#datatable').DataTable({
        responsive: true,
        searching:  false,
        ordering: false,
    });
</script>
@endpush