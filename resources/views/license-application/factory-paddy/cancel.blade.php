@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Permohonan Lesen Runcit</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Permohonan Runcit</a></li>
            <li class="breadcrumb-item active">Lesen Runcit</li>
            <li class="breadcrumb-item active">Pembatalan</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form action="#" class="tab-wizard wizard-circle">
                    <!-- Step 1 -->
                    <h6>Butiran Syarikat Dan Rakan Kongsi</h6>
                    <section>
                        @include('license-application.component.company-view')
                        @include('license-application.component.partner-view')
                    </section>
                    <!-- Step 2 -->
                    <h6>Premis</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                @include('license-application.component.premise-renew')
                            </div>
                        </div>
                    </section>
                    <!-- Step 3 -->
                    <h6>Senarai Semak</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">#</th>
                                            <th>Fail</th>
                                            <th>Muat Naik</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                            Salinan SSM
                                            </td>
                                            <td>
                                            <input type="file" class="form-control"/>
                                            </td>
                                        </tr>                            
                                        <tr>
                                            <td>2</td>
                                            <td>
                                            Salinan lesen perniagaan (Sabah/Sarawak)
                                            </td>
                                            <td>
                                            <input type="file" class="form-control"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script>
     $('#datatable').DataTable({
        responsive: true,
        searching:  false,
        ordering: false,
    });
</script>
@endpush