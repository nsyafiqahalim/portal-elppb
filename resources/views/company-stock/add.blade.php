@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-4 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Penyata Stok</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Penyata Stok</a></li>
            <li class="breadcrumb-item active">Tambah</li>
        </ol>
    </div>
</div>
<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="form-sample url-encoded-submission-and-redirect-to-other-page" method="post" action="{{ route('api.license_application.company_stock_statement.store') }}">
                    @csrf
                    <div class="form-body">
                        <h3 class="card-title">1. Pilih Syarikat</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" id="company_id_label">Syarikat</label>
                                    <select class="select2" id="company_id" name="company_id" style="width: 100%">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Jenis Syarikat</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fas fa-building"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" id="company_type_name" name="company_type_name" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">No Pendaftaran Baharu / Trading License / IRD</label>
                                    <input type="text" class="form-control"  id="company_mycoid" name="company_mycoid" disabled>
                                </div>
                            </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">No Pendaftaran Lama</label>
                                    <input type="text" class="form-control" id="company_old_registration_number" name="company_old_registration_number" disabled>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Tarikh Mansuh</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="mdi mdi-calendar-question"></i>
                                            </span>
                                        </div>
                                            <input type="text" class="form-control date" id="company_expiry_date" name="company_expiry_date" disabled>         
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Modal Berbayar</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">RM</span>
                                        </div>
                                        <input type="text" class="form-control" id="company_paidup_capital" name="company_paidup_capital" style='text-align:right' disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions float-right">
                        <button type="submit" class="btn btn-success">
                            Tambah Penyata Stok
                        </button>
                        <a href="{{ route ('company_stock.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                    </div>
                    <h3 class="box-title mt-5">Maklumat Penyata Stok</h3>
<hr>
<div class="row">
    <table class="table table-bordered table-striped summary-premise">
        <thead>
            <tr>
                <th style='text-align:left'>Bil.</th>
                <th style='text-align:left'>Jenis Lesen </th>
                <th style='text-align:left'>Jenis Beras </th>
                <th style='text-align:center'>Gred Beras</th>
                <th style='text-align:center'>Stok Mula</th>
                <th style='text-align:center'>Stok Akhir</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td style='text-align:left'>1</td>
                    <td style='text-align:left'>Borong </td>
                    <td style='text-align:left'>Basmathi</td>
                    <td style='text-align:center'>A</td>
                    <td style='text-align:center'>1000</td>
                    <td style='text-align:center'>2000</td>
                </tr>
                <tr>
                    <td style='text-align:left'>2</td>
                    <td style='text-align:left'>Kilang </td>
                    <td style='text-align:left'>Jasmin Super 5</td>
                    <td style='text-align:center'>A</td>
                    <td style='text-align:center'>2</td>
                    <td style='text-align:center'>1089</td>
                </tr>
        </tbody>
    </table>
</div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@push('js')
<script>
$(document).ready(function(){

    $("#company_id").select2({
        ajax: {
            url: "{{ route('api.license_application.company.find_company_by_query') }}",
            dataType: 'json',
            delay: 250,
            minimumInputLength: 3,
            placeholder: "Sila Masukkan Nama Syarikat/ Nombor Pendaftaran Syarikat/ Nombor Pendaftaran Syarikat Lama",
            data: function(params) {
                return {
                    q: params.term,
                    user_id: "{{ encrypt(Auth::user()->id) }}"
                };
            },
            processResults: function (data) {
					return {
						results: data
					};
			},
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        //templateResult: formatRepo, // omitted for brevity, see the source of this page
        //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    $("#company_id").change(function(){
        companyId = $(this).val();

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company.find_company_detail_and_active_license_by_company_id',['']) }}/"+companyId,
            success: function(data){
                $('#company_name').val(data.name);
                $('#company_mycoid').val(data.registration_number);
                $('#company_expiry_date').val(data.formatted_expiry_date);
                $('#company_paidup_capital').val(data.paidup_capital);
                $('#company_old_mycoid').val(data.old_registration_number);
                $('#company_type_name').val(data.company_type_name);
                /*
                $.each(data.licenses, function(i, d) {
                    selected = null;
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#license_id').append('<option  data-code="'+d.license_type_code+'" '+selected+' value="' + d.id + '">' + d.label+ ' : Lesen ' + d.license_type + '</option>');
                });
                */
            }
        });
    });

});
</script>
@endpush
