@extends('layouts.internal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-4 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Penyata Stok</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Penyata Stok</a></li>
            <li class="breadcrumb-item active">Tambah</li>
        </ol>
    </div>
</div>
<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content"><div class="alert alert-danger error-message-bag"></div>
                <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.license_application.company_stock_statement.store') }}">
                    @csrf
                    <div class="form-body">
                        <h3 class="card-title">Maklumat Syarikat</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label" id="company_id_label">Syarikat</label>
                                    <select class="select2" id="company_id" name="company_id" style="width: 100%">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Jenis Syarikat</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fas fa-building"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" id="company_type_name" name="company_type_name" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">No Pendaftaran Baharu / Trading License / IRD</label>
                                    <input type="text" class="form-control"  id="company_mycoid" name="company_mycoid" disabled>
                                </div>
                            </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">No Pendaftaran Lama</label>
                                    <input type="text" class="form-control" id="company_old_registration_number" name="company_old_registration_number" disabled>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Tarikh Mansuh</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="mdi mdi-calendar-question"></i>
                                            </span>
                                        </div>
                                            <input type="text" class="form-control date" id="company_expiry_date" name="company_expiry_date" disabled>         
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Modal Berbayar</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">RM</span>
                                        </div>
                                        <input type="text" class="form-control" id="company_paidup_capital" name="company_paidup_capital" style='text-align:right' disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label id="license_id_label">Lesen <span class='required'>*</span></label>
                                    <select class="form-control date" id='license_id' name='license_id'>
                                        <option>Sila Pilih</option>
                                        <option data-code='BORONG' value="{{ encrypt('C') }}">Borong</option>
                                        <option data-code='KILANG_PADI' value="{{ encrypt('F') }}">Kilang Padi Komersial</option>
                                    <select>
                                </div>
                            </div>
                        </div>
                        <!-- Company Address -->
                        <h3 class="box-title mt-5">Maklumat Penyata Stok</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-xs-6" id="stock_type_id_input">
                                <div class="form-group">
                                    <label id="stock_type_id_label">Jenis Penyata Stok Kilang</label>
                                    <select class="form-control date" id='stock_type_id' name='stock_type_id'>
                                        <option>Sila Pilih</option>
                                        @foreach($stockTypes as $stockType)
                                        <option data-code='{{ $stockType->code }}' value="{{ encrypt($stockType->id) }}">{{ $stockType->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-6" id="stock_at_input">
                                <div class="form-group">
                                    <label id="stock_at_label">Tarikh Penyata Stok</label>
                                    <input type="text" class="form-control date" id='stock_at' name='stock_at'>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-6" id="grade_type_id_input">
                                <div class="form-group">
                                    <label id="grade_type_id_label"> Jenis gred Beras</label>
                                    <select class="form-control date" id='grade_type_id' name='grade_type_id'>
                                        <option>Sila Pilih</option>
                                        @foreach($riceGrades as $riceGrade)
                                        <option value="{{ encrypt($riceGrade->id) }}">{{ $riceGrade->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="initial_stock_input">
                                <div class="form-group">
                                    <label class="control-label" id="initial_stock_label">Stok Mula</label>
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" id="initial_stock" name="initial_stock" style='text-align:right' >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">KG</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="buying_input">
                                <div class="form-group">
                                    <label class="control-label" id="buying_label">Belian</label>
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" id="buying" name="buying" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="selling_input">
                                <div class="form-group">
                                    <label class="control-label" id="selling_label">Jualan</label>
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" id="selling" name="selling" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="total_buy_input">
                                <div class="form-group">
                                    <label class="control-label" id="total_buy_label">Jumlah Belian</label>
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" id="total_buy" name="total_buy" style='text-align:right' >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">KG</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="buying_price_input">
                                <div class="form-group">
                                    <label class="control-label" id="buying_price_label">Harga Belian</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">RM</span>
                                        </div>
                                        <input type="number" step="0.01" class="form-control" id="buying_price" name="buying_price" style='text-align:right' >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="total_spin_input">
                                <div class="form-group">
                                    <label class="control-label" id="total_spin_label">Jumlah Kisaran</label>
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" id="total_spin" name="total_spin" style='text-align:right' >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">KG</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-6" id="spin_result_input">
                                <div class="form-group">
                                    <label id="spin_result_label">Hasil Kisaran</label>
                                    <select class="form-control date" id='spin_result' name='spin_result'>
                                        <option>Sila Pilih</option>
                                        @foreach($spinTypes as $spinType)
                                        <option value="{{ encrypt($spinType->id) }}">{{ $spinType->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-6" id="total_grade_input">
                                <div class="form-group">
                                    <label id="total_grade_label">Jumlah Gred</label>
                                    <select class="form-control date" id='total_grade' name='total_grade'>
                                        <option>Sila Pilih</option>
                                        @foreach($totalGrades as $totalGrade)
                                        <option value="{{ encrypt($totalGrade->id) }}">{{ $totalGrade->name }}</option>
                                        @endforeach
                                    <select>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="total_sell_input">
                                <div class="form-group">
                                    <label class="control-label" id="total_sell_label">Jumlah Jualan</label>
                                    <div class="input-group">
                                        <input type="number" step="0.01" class="form-control" id="total_sell" name="total_sell" style='text-align:right' >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">KG</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12" id="selling_price_input">
                                <div class="form-group">
                                    <label class="control-label" id="selling_price_label">Harga Jualan</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">RM</span>
                                        </div>
                                        <input type="number" step="0.01" class="form-control" id="selling_price" name="selling_price" style='text-align:right' >
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4 col-lg-4 col-xs-12" id="stock_balance_input">
                                <div class="form-group">
                                    <label class="control-label" id="stock_balance_label">Baki Stok</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="stock_balance_display" disabled>
                                        <input type="hidden" class="form-control" id="stock_balance" name="stock_balance">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">KG</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions float-right">
                        <button type="submit" class="btn btn-success">
                            Hantar
                        </button>
                        <a href="{{ route ('company_stock.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script>
$(document).ready(function(){
    hideAll();
    
    $("#company_id").select2({
        ajax: {
            url: "{{ route('api.license_application.company.find_company_by_query') }}",
            dataType: 'json',
            delay: 250,
            minimumInputLength: 3,
            placeholder: "Sila Masukkan Nama Syarikat/ Nombor Pendaftaran Syarikat/ Nombor Pendaftaran Syarikat Lama",
            data: function(params) {
                return {
                    q: params.term,
                    user_id: "{{ encrypt(Auth::user()->id) }}"
                };
            },
            processResults: function (data) {
					return {
						results: data
					};
			},
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        //templateResult: formatRepo, // omitted for brevity, see the source of this page
        //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    $("#company_id").change(function(){
        companyId = $(this).val();

        $.ajax({
            type: "GET",
            url: "{{ route('api.license_application.company.find_company_detail_and_active_license_by_company_id',['']) }}/"+companyId,
            success: function(data){
                $('#company_name').val(data.name);
                $('#company_mycoid').val(data.registration_number);
                $('#company_expiry_date').val(data.formatted_expiry_date);
                $('#company_paidup_capital').val(data.paidup_capital);
                $('#company_old_mycoid').val(data.old_registration_number);
                $('#company_type_name').val(data.company_type_name);
                /*
                $.each(data.licenses, function(i, d) {
                    selected = null;
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    $('#license_id').append('<option  data-code="'+d.license_type_code+'" '+selected+' value="' + d.id + '">' + d.label+ ' : Lesen ' + d.license_type + '</option>');
                });
                */
            }
        });
    });

    $(document).on('change','#initial_stock',function(){
        calculateStockBalance();
    });

    $(document).on('change','#total_buy',function(){
        calculateStockBalance();
    });

    $(document).on('change','#total_sell',function(){
        calculateStockBalance();
    });

    function calculateStockBalance(){

        // $('#initial_stock').val().toFixed(2);
        // $('#total_buy').val().toFixed(2);
        // $('#total_sell').val().toFixed(2);

        initialStock =  parseFloat($('#initial_stock').val());
        totalBuy   =  $('#total_buy').val();
        totalSell  =  $('#total_sell').val();
        licenseTypeCode = $('#license_id option:selected').data('code');

        if(licenseTypeCode == 'BORONG'){
            totalBuy = (totalBuy.length > 0 )?parseFloat(totalBuy):0;
            totalSell = (totalSell.length > 0 )?parseFloat(totalSell):0;
        }
        else{
            stockTypeCode = $('#stock_type_id option:selected').data('code');
            if(stockTypeCode == 'JENIS_PADI'){
                totalBuy = (totalBuy.length > 0 )? parseFloat(totalBuy):0;
                totalSell = 0;

            }
            else{
                $('.error-message').remove();
                totalSell = (totalSell.length > 0 )?parseFloat(totalSell):0;
                totalBuy : 0;
            }
        }
       
        result = initialStock + totalBuy - totalSell;

        $('#stock_balance').val(result);
        $('#stock_balance_display').val(result.toFixed(2));
    }

    $(document).on('change','#license_id',function(){
        licenseTypeCode = $('#license_id option:selected').data('code');
        //licenseTypeCode = $('#license_id').val();
        if(licenseTypeCode == 'BORONG'){
            $('.error-message').remove();
            hideAll();
            wholesale();
        }
        else{
            hideAll();
            factoryPaddy();
        }
    });

     $(document).on('change','#stock_type_id',function(){
        stockTypeCode = $('#stock_type_id option:selected').data('code');
        if(stockTypeCode == 'JENIS_PADI'){
            $('.error-message').remove();
            hideFactory();
            paddyType();
        }
        else{
            $('.error-message').remove();
            hideFactory();
            riceGrade();
        }
    });

    function factoryPaddy(){
         $('#stock_type_id_input').show();
    }

    function paddyType(){
        $('#initial_stock_input').show();
        $('#total_buy_input').show();
        $('#buying_price_input').show();
        $('#total_spin_input').show();
        $('#spin_result_input').show();
        $('#stock_at_input').show();
        $('#stock_balance_input').show();
    }

    function riceGrade(){
        $('#initial_stock_input').show();
        $('#total_grade_input').show();
        $('#selling_price_input').show();
        $('#total_sell_input').show();
        $('#stock_at_input').show();
        $('#stock_balance_input').show();
    }

    function wholesale(){
        $('#grade_type_id_input').show();
        $('#initial_stock_input').show();
        //$('#selling_input').show();
        //$('#buying_input').show();
        $('#total_sell_input').show();
        $('#total_buy_input').show();
        $('#selling_price_input').show();
        $('#buying_price_input').show();
        $('#stock_balance_input').show();
        $('#stock_at_input').show();
    }

    function hideFactory(){
        $('#grade_type_id_input').hide();
        $('#initial_stock_input').hide();
        $('#selling_input').hide();
        $('#buying_input').hide();
        $('#selling_price_input').hide();
        $('#buying_price_input').hide();
        $('#stock_balance_input').hide();
        $('#total_buy_input').hide();
        $('#total_spin_input').hide();
        $('#spin_result_input').hide();
        $('#total_sell_input').hide();
        $('#total_grade_input').hide();  
        $('#stock_at_input').hide();
    }

    function hideAll(){
        $('#stock_type_id').prop('selectedIndex',0);
        $('#stock_type_id_input').hide();
        $('#grade_type_id_input').hide();
        $('#initial_stock_input').hide();
        $('#selling_input').hide();
        $('#buying_input').hide();
        $('#selling_price_input').hide();
        $('#buying_price_input').hide();
        $('#stock_balance_input').hide();
        $('#total_buy_input').hide();
        $('#total_spin_input').hide();
        $('#spin_result_input').hide();
        $('#total_sell_input').hide();
        $('#total_grade_input').hide();  
        $('#stock_at_input').hide();
    }
});
</script>
@endpush
