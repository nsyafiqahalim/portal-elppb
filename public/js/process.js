$(document).ready(function () {
    $(document).on('submit', '.url-encoded-submission-without-clear-field', function (e) {
        e.preventDefault();
        $(".se-pre-con").fadeIn("fast");
        $('.error-message').remove();

        var formData = $(this).serialize();
        form = $(this);

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            success: function (data) {
                successMessage(data);
            },
            error: function (data) {
                errorMessage(data);
            }
        });
    });

    $(document).on('submit', '.url-encoded-submission', function (e) {
        e.preventDefault();

        $('.error-message').remove();
        $(".se-pre-con").fadeIn("fast");
        var formData = $(this).serialize();
        form = $(this);

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            success: function (data) {
                form[0].reset();
                successMessage(data);
            },
            error: function (data) {
                errorMessage(data);
            }
        });
    });

    $(document).on('submit', '.url-encoded-submission-and-redirect-to-other-page', function (e) {
        e.preventDefault();

        $('.error-message').remove();
        $(".se-pre-con").fadeIn("fast");
        var formData = $(this).serialize();
        form = $(this);

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            success: function (data) {
                window.location = data['route'];
            },
            error: function (data) {
                errorMessage(data);
            }
        });
    });

    $(document).on('submit', '.form-data-submission', function (e) {
        e.preventDefault();

        $('.error-message').remove();

        var formData = new FormData(this);
        form = $(this);
        $(".se-pre-con").fadeIn("fast");
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                form[0].reset();
                successMessage(data);
            },
            error: function (data) {
                errorMessage(data);
            }
        });
    });

    $(document).on('submit', '.form-data-submission-with-confirmation', function (e) {
        e.preventDefault();
        $(".se-pre-con").fadeIn("fast");
        var method = $(this).attr('method');
        var action = $(this).attr('action');
        var text = $(this).attr('text');
        var formData = new FormData(this);
        form = $(this);

        Swal.fire({
            title: 'Pengesahan',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            preConfirm: function () {
                $('.error-message').remove();
                e.preventDefault();
                $.ajax({
                    type: method,
                    url: action,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        form[0].reset();
                        successMessage(data);
                    },
                    error: function (data) {
                        errorMessage(data);
                    }
                });
            },
        },
        );
    });

    $(document).on('submit', '.form-data-submission-using-message-bag', function (e) {
        e.preventDefault();

        $('.error-message-bag').html(null);
        $('.error-message-bag').hide();

        var formData = new FormData(this);
        form = $(this);
        $(".se-pre-con").fadeIn("fast");
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                form[0].reset();
                successMessage(data);
            },
            error: function (data) {
                errorMessageBag(data);
            }
        });
    });

    $(document).on('submit', '.form-data-submission-with-confirmation-using-message-bag', function (e) {
        e.preventDefault();
        $(".se-pre-con").fadeIn("fast");
        var method = $(this).attr('method');
        var action = $(this).attr('action');
        var text = $(this).attr('text');
        var formData = new FormData(this);
        form = $(this);

        Swal.fire({
            title: 'Pengesahan',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            preConfirm: function () {
                $('.error-message-bag').html(null);
                $('.error-message-bag').hide();
                e.preventDefault();
                $.ajax({
                    type: method,
                    url: action,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        form[0].reset();
                        successMessage(data);
                    },
                    error: function (data) {
                        errorMessageBag(data);
                    }
                });
            },
        },
        );
    });

    $(document).on('submit', '.url-encoded-submission-with-confirmation', function (e) {
        e.preventDefault();
        var method = $(this).attr('method');
        var action = $(this).attr('action');
        var text = $(this).attr('text');
        var formData = $(this).serialize();
        form = $(this);

        Swal.fire({
            title: 'Pengesahan',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya",
            cancelButtonText: "Cancel",
            preconfirm: function () {
                $('.error-message').remove();
                $(".se-pre-con").fadeIn("fast");

                $.ajax({
                    type: method,
                    url: action,
                    data: formData,
                    success: function (data) {
                        form[0].reset();
                        successMessage(data);
                    },
                    error: function (data) {
                        errorMessage(data);
                    }
                });
            },
        },

        );
    });

    $(document).on('click', '.confirmation', function (e) {
        e.preventDefault();
        var formData = {};
        formData['_method'] = "PATCH";

        var href = $(this).attr('href');

        Swal.fire({
            title: 'Pengesahan',
            text: 'Anda Pasti ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya",
            cancelButtonText: "Cancel",
            preConfirm: function () {
                $(".se-pre-con").fadeIn("fast");
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: formData,
                    success: function (data) {
                        successMessage(data);
                    },
                    error: function (data) {
                        errorMessage(data);
                    }
                });
            },
        }
        );
    });

    $(document).on('click', '.deletion', function (e) {
        e.preventDefault();
        var formData = {};
        formData['_method'] = "DELETE";

        var href = $(this).attr('href');

        Swal.fire({
            title: 'Pengesahan',
            text: 'Anda Pasti ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Ya",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            preConfirm: function () {
                $(".se-pre-con").fadeIn("fast");
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: formData,
                    success: function (data) {
                        successMessage(data);
                    },
                    error: function (data) {
                        errorMessage(data);
                    }
                });
            }
        });
    });

    function successMessage(data) {
        $(".se-pre-con").fadeOut("slow");
        if (data['success'] == true) {
            type = "success";
            title = "Berjaya";
        }
        else {
            type = "error"
            title = "Amaran";
        }
        Swal.fire({
            title: title,
            text: data['message'],
            type: type,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ya",
            preConfirm: function (isConfirm) {
                $('.modal').modal('hide');
                if (isConfirm && data['success'] == true && data['route'] !== null) {
                    window.location = data['route'];
                }
                else if (isConfirm && data['success'] == true && data['datatable'] !== null) {
                    $('#' + data['datatable']).DataTable().destroy();
                    $('#' + data['datatable']).DataTable(eval(data['loadDatatableConfig']));
                    //$('#' + data['datatable']).DataTable(eval(data['loadDatatableConfig'])).ajax.reload();
                }

            }
        }
        );
    }

    async function errorMessageBag(data) {

        $(".se-pre-con").fadeOut("slow");
        var err = data.responseJSON;
        Swal.close();
        $('.error-message-bag').append('<ol type="1">');
        await $.each(err['errors'], function (index, value) {
            $('.error-message-bag').append('<li>' + value + '</li>');
        });
        $('.error-message-bag').append('</ol>');
        $('.error-message-bag').show();
    }
    function errorMessage(data) {

        $(".se-pre-con").fadeOut("slow");
        var err = data.responseJSON;
        if (err.errors !== undefined) {
            Swal.close();
            $.each(err['errors'], function (index, value) {
                message = '<span class="error-message" >' + value + '</span>';
                index = index.replace(".", "_");
                $('#' + index + '_label').parent().append(message);
            });
        }
        else if (err.message !== undefined) {
            $(".se-pre-con").fadeOut("slow");
            Swal.fire("Warning", err.message, "warning");
        }
        else {
            $(".se-pre-con").fadeOut("slow");
            Swal.fire("Warning", "Unexpected Error", "warning");
        }

    }
});