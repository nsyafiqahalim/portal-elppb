<?php

if (! function_exists('convertDecryptedArrayInputType')) {
    function convertDecryptedArrayInputType($paramsTobeConverted){

        foreach($paramsTobeConverted as $index => $paramTobeConverted){
            $par[] =  decrypt($paramTobeConverted);
        }
        
        return $par;
    }
}


if (! function_exists('convertAddressIntoString')) {
    function convertAddressIntoString($param){
        $address = $param->address_1;

        if(isset($param->address_2)){
            $address = $address.',<br/>'.$param->address_2;
        }

        if(isset($param->address_3)){
            $address = $address.',<br/>'.$param->address_3;
        }
        
        $address = $address.',<br/>'.$param->postcode;
        $address = $address.',<br/>'.$param->state->name;

        return $address;
    }
}