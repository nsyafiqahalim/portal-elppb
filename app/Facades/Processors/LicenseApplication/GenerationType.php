<?php

namespace App\Facades\Processors\LicenseApplication;

use App\DataObjects\ReferenceData\CompanyDataObject;
use App\DataObjects\LicenseApplication\IncludelicenseDataObject;
use App\DataObjects\LicenseDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;

use App\Models\Admin\LicenseType;

class GenerationType {
    
   public static function findGenerationTypeDomainByCompanyId($companyId,$licenseType ,$licenseApplicationType,$includelicenseId = null) {
        if($companyId == null){
            return null;
        }

        $company    =   CompanyDataObject::findCompanyById($companyId);
        $generationTypeDomain = null;

        if($licenseType == "BORONG"){
            return  self::wholesale($licenseType ,$licenseApplicationType, $includelicenseId);
        }
        elseif($licenseType == "IMPORT"){
            return  self::import($companyId,$licenseType ,$licenseApplicationType, $includelicenseId);
        }
        elseif($licenseType == "EKSPORT"){
            return  self::export($companyId,$licenseType ,$licenseApplicationType, $includelicenseId);
        }
        
        return $generationTypeDomain;
    }

    public static function wholesale($licenseType ,$licenseApplicationType, $includelicenseId){
        if($licenseApplicationType == "BAHARU"){
            return 'NEW_WHOLESALE';
        }
        elseif($licenseApplicationType == "PEMBAHARUAN" || $licenseApplicationType == "PEMBAHARUAN_DAN_PERUBAHAN"){

            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);

            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'RENEW_WHOLESALE_RENEW_EXPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'RENEW_WHOLESALE_RENEW_IMPORT';
            }
            else{
                return 'RENEW_WHOLESALE';
            }
        }
        elseif($licenseApplicationType == "PEMBATALAN"){
            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);
            
            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'CANCEL_WHOLESALE_ALL';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'CANCEL_WHOLESALE_EXPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                
                return 'CANCEL_WHOLESALE_IMPORT';
            }
            else{
                return 'CANCEL_WHOLESALE';
            }
        }
         elseif($licenseApplicationType == "PENGGANTIAN"){
            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);
            
            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'REPLACEMENT_WHOLESALE_ALL';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'REPLACEMENT_WHOLESALE_EXPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                
                return 'REPLACEMENT_WHOLESALE_IMPORT';
            }
            else{
                return 'REPLACEMENT_WHOLESALE';
            }
        }
    }

    public static function import($companyId, $licenseType ,$licenseApplicationType, $includelicenseId){
        if($licenseApplicationType == "BAHARU"){

            $param['company_id']        =   $companyId;
            $param['status_id']         =   StatusDataObject::findStatusByCode('LESEN_AKTIF')->id;
            $param['license_type_id']   =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $license =  LicenseDataObject::findActiveLicenseExistByCompanyIdAndLicenseTypeId($param);
            if (isset($license)) {
                return 'NEW_IMPORT';
                
            }
            else{
                return 'NEW_IMPORT_WHOLESALE';
            }
            
        }
        elseif($licenseApplicationType == "PEMBAHARUAN" || $licenseApplicationType == "PEMBAHARUAN_DAN_PERUBAHAN"){

            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);

            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'RENEW_IMPORT_EXPORT';
            }
            else{
                return 'RENEW_IMPORT';
            }
        }
        elseif($licenseApplicationType == "PEMBATALAN"){
            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);
            
            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'CANCEL_IMPORT_ALL';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'CANCEL_IMPORT_EXPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                
                return 'CANCEL_IMPORT_IMPORT';
            }
            else{
                return 'CANCEL_IMPORT';
            }
        }
         elseif($licenseApplicationType == "PENGGANTIAN"){
            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);
            
            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'REPLACEMENT_IMPORT_ALL';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'REPLACEMENT_IMPORT_EXPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                
                return 'REPLACEMENT_IMPORT_IMPORT';
            }
            else{
                return 'REPLACEMENT_IMPORT';
            }
        }
    }

    public static function export($companyId, $licenseType ,$licenseApplicationType, $includelicenseId){
        if($licenseApplicationType == "BAHARU"){

            $param['company_id']        =   $companyId;
            $param['status_id']         =   StatusDataObject::findStatusByCode('LESEN_AKTIF')->id;
            $param['license_type_id']   =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $license =  LicenseDataObject::findActiveLicenseExistByCompanyIdAndLicenseTypeId($param);
            if (isset($license)) {
                return 'NEW_EXPORT';
            }
            else{
                return 'NEW_EXPORT_WHOLESALE';
            }
            
        }
        elseif($licenseApplicationType == "PEMBAHARUAN" || $licenseApplicationType == "PEMBAHARUAN_DAN_PERUBAHAN"){

            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);

            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'RENEW_EXPORT_IMPORT';
            }
            else{
                return 'RENEW_EXPORT';
            }
        }
        elseif($licenseApplicationType == "PEMBATALAN"){
            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);
            
            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'CANCEL_EXPORT_ALL';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'CANCEL_EXPORT_IMPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                
                return 'CANCEL_EXPORT_EXPORT';
            }
            else{
                return 'CANCEL_EXPORT';
            }
        }
         elseif($licenseApplicationType == "PENGGANTIAN"){
            $includeLicense =   IncludelicenseDataObject::findIncludeLicenseById($includelicenseId);
            $licensesFound  =   self::findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense);
            
            if (in_array("C", $licensesFound) && in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                return 'REPLACEMENT_EXPORT_ALL';
            }
            elseif (in_array("C", $licensesFound) && in_array("B", $licensesFound) && !in_array("A", $licensesFound)){
                return 'REPLACEMENT_EXPORT_IMPORT';
            }
            elseif (in_array("C", $licensesFound) && !in_array("B", $licensesFound) && in_array("A", $licensesFound)){
                
                return 'REPLACEMENT_EXPORT_EXPORT';
            }
            else{
                return 'REPLACEMENT_EXPORT';
            }
        }
    }

    public static function findGenerationTypeSubDomainBasedOnIncludeLicense($includeLicense){
        $includelicenseItems = $includeLicense->includeLicenseItems;

        $licenseFound = [];
        
        foreach($includelicenseItems as $includelicenseItem){
            $licenseFound[] = $includelicenseItem->licenseType->code;
        }

        return $licenseFound;
    }
}