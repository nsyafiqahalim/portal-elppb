<?php

namespace App\Facades\Processors\LicenseApplication;

use App\DataObjects\ReferenceData\CompanyDataObject;

class Material {
    
   public static function findMaterialDomainByCompanyId($companyId,$licenseType ,$licenseApplicationType) {
        if($companyId == null){
            return null;
        }

        $company        =   CompanyDataObject::findCompanyById($companyId);
        $companyType    =   $company->companyType;
        $materialDomain = null;
        
        if($companyType->code != "PERTUBUHAN_PELADANG" &&  $companyType->code != "KOPERASI"){
            $materialExtension = 'SYARIKAT';
        }
        else{
            $materialExtension = 'KOPERASI';
        }

        $configName = 'material.'.$licenseType.'.'.$licenseApplicationType.'.'.$materialExtension;
        
        $materialDomain = config($configName);
        return $materialDomain;
    }

    public static function findMaterialExtensionNameByCompanyCode($companyTypeCode){
        
    }
}