<?php

namespace App\Facades\LicenseApplication;

use Illuminate\Support\Facades\Facade;

use App\DataObjects\CompanyDataObject;

class GenerationTypeFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'license-application-generation-type';
    }
}