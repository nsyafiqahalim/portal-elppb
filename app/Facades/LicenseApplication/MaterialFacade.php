<?php

namespace App\Facades\LicenseApplication;

use Illuminate\Support\Facades\Facade;

use App\DataObjects\CompanyDataObject;

class MaterialFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'license-application-material';
    }
}