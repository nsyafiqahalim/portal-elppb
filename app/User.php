<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

use App\Models\User\Profile;
use App\Models\LicenseApplication;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\VerifyEmail;

use App\Notifications\ResetPassword;
use App\Notifications\EmailVerification;

//use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,HasApiTokens, HasRoles;

    public const USER_TYPE_MOA = 'MOA';
    public const USER_TYPE_APPLICANT = 'APPLICANT';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    //protected $fillable = [
    //'name', 'email', 'password',
    //];

    protected $guarded = [
        'id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public function moaLicenseApplications()
    {
        return $this->hasMany(LicenseApplication::class, 'user_id');
    }

    public function applicantLicenseApplications()
    {
        return $this->hasMany(LicenseApplication::class, 'applicant_id');
    }

    public function sendPasswordResetNotification($token)
    {
      $this->notify(new ResetPassword($token));
    }

    public function sendEmailVerificationNotification()
    {
      $this->notify(new EmailVerification());
    }
}
