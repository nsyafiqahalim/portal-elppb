<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_settings';
}
