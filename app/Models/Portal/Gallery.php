<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_galleries';
}
