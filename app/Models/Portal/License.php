<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_licenses';

    public const PUBLISHED = 1;

    public function scopePublishedOnly($query)
    {
        return $query->where('is_published', self::PUBLISHED);
    }
}
