<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public $guarded = ['id'];

    public $table = 'portal_faqs';

    public const PUBLISHED = 1;

    public function scopePublishedOnly($query)
    {
        return $query->where('is_published', self::PUBLISHED);
    }
}
