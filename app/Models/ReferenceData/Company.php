<?php

namespace App\Models\ReferenceData;

use Illuminate\Database\Eloquent\Model;

use App\Models\ReferenceData\CompanyAddress;
use App\Models\ReferenceData\CompanyPartner;
use App\Models\ReferenceData\CompanyPremise;
use App\Models\Admin\CompanyType;
use App\Models\Admin\BusinessType;
use App\Models\ApprovalLetter;

class Company extends Model
{
    public $guarded = ['id'];
    
    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function address()
    {
        return $this->hasOne(CompanyAddress::class, 'company_id')->with(['state']);
    }

    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type_id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }

    public function  approvalLetters(){
        return $this->hasMany(ApprovalLetter::class,'company_id');
    }

    public function  partners(){
        return $this->hasMany(CompanyPartner::class,'company_id');
    }

    public function premise(){
        return $this->hasOne(CompanyPremise::class,'company_id');
    }
}
