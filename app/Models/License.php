<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\Company;
use App\Models\Admin\LicenseApplicationType;
use App\Models\LicenseApplication;
use App\Models\Admin\LicenseType;
use App\Models\Admin\Status;

class License extends Model
{
    public $guarded = ['id'];
    public $table = 'licenses';

    private const ACTIVE = 1;

    public function licenseType()
    {
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

    public function licenseApplicationType()
    {
        return $this->belongsTo(LicenseApplicationType::class, 'license_application_type_id');
    }
}
