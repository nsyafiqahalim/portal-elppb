<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\CompanyAddress;
use App\Models\Admin\CompanyType;

class Temporary extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_temporaries';
    public $timestamps = false;
    public $dates = [
        'expiry_date'
    ];
}
