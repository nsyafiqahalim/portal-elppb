<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\Company;
use App\Models\Admin\State;

class CompanyAddress extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_company_addresses';

    private const ACTIVE = 1;

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
}
