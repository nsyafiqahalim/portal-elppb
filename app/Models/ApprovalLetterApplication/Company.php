<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\CompanyAddress;
use App\Models\Admin\CompanyType;

class Company extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_companies';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function address()
    {
        return $this->hasOne(CompanyAddress::class, 'company_id');
    }

    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type_id');
    }
}
