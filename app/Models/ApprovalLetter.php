<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\Company;
use App\Models\Admin\ApprovalLetterApplicationType;
use App\Models\ApprovalLetterApplication;
use App\Models\Admin\ApprovalLetterType;
use App\Models\Admin\Status;

class ApprovalLetter extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letters';
    public $dates = ['expiry_date'];

    private const ACTIVE = 1;

    public function approvalLetterType()
    {
        return $this->belongsTo(ApprovalLetterType::class, 'approval_letter_type_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function approvalLetterApplication()
    {
        return $this->belongsTo(ApprovalLetterApplication::class, 'approval_letter_application_id');
    }

    public function approvalLetterApplicationType()
    {
        return $this->belongsTo(ApprovalLetterApplicationType::class, 'approval_letter_application_type_id');
    }
}
