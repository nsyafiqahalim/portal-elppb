<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public $guarded = ['id'];
    public $table = 'user_profiles';
}
