<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\ReferenceData\Company;
use App\Models\ReferenceData\CompanyStock as ReferenceDataCompanyStock;

class CompanyStock extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_company_stocks';

    private const ACTIVE = 1;

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function companyStock()
    {
        return $this->belongsTo(ReferenceDataCompanyStock::class, 'company_stock_id');
    }
}
