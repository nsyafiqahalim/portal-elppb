<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication;
use App\Models\ApprovalLetter as ApprovalLetter2;

class ApprovalLetter extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_approval_letters';

    private const ACTIVE = 1;

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

    public function approvalLetter()
    {
        return $this->belongsTo(ApprovalLetter2::class, 'approval_letter_id');
    }
}
