<?php

namespace App\Models\LicenseApplication;
use Illuminate\Database\Eloquent\Model;

class SerialNumber extends Model
{
    public $guarded = ['id'];
    public $table = 'license_applications_serial_numbers';
    public $timestamps = false;
}
