<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\GenerateLicense;
use App\Models\LicenseApplication;
use App\Models\Admin\LicenseType;
use App\Models\License;

class IncludeLicenseItem extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_include_license_items';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function includeLicense()
    {
        return $this->belongsTo(IncludeLicense::class, 'license_application_include_license_id');
    }

    public function currentLicense()
    {
        return $this->belongsTo(License::class, 'current_license_id');
    }

    public function oldLicense()
    {
        return $this->belongsTo(License::class, 'old_license_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

    public function licenseType(){
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }
}
