<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\LicenseApplicationBuyPaddyCondition;

class BuyPaddy extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_buy_paddies';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function buyPaddyCondition(){
        return $this->belongsTo(LicenseApplicationBuyPaddyCondition::class,'license_application_buy_paddy_condition_id');
    }
}
