<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\CompanyAddress;
use App\Models\Admin\CompanyType;

class Attachment extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_attachments';

    public $dates = [
        'expiry_date'
    ];

    public const SSM = 'SSM';
    public const ORIGINAL_ATTACHMENT = 'ORIGINAL_ATTACHMENT';
    public const MINUTE_MEETING = 'MINUTE_MEETING';
    public const ACCOUNT_STATEMENT = 'ACCOUNT_STATEMENT';
    public const SALE_AGREEMENT = 'SALE_AGREEMENT';

    public function address()
    {
        return $this->hasOne(CompanyAddress::class, 'company_id');
    }

    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type_id');
    }
}
