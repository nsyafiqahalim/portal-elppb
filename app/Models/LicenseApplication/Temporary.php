<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\CompanyAddress;
use App\Models\Admin\CompanyType;

use App\Models\ReferenceData\Company;
use App\Models\ReferenceData\CompanyPremise;

class Temporary extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_temporaries';
    public $timestamps = false;
    public $dates = [
        'expiry_date'
    ];

    public function company(){
        return $this->belongsTo(Company::class,'company_id');
    }

    public function premise(){
        return $this->hasOne(CompanyPremise::class,'premise_id');
    }
}
