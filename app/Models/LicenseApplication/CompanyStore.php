<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\BuildingType;
use App\Models\Admin\BusinessType;
use App\Models\Admin\StoreOwnershipType;
use App\Models\Admin\Dun;
use App\Models\Admin\Parliament;
use App\Models\Admin\District;
use App\Models\Admin\State;

use App\Models\LicenseApplication\IncludeLicenseItemStore;

use App\Models\ReferenceData\CompanyStore as ReferenceCompanyStore;

class CompanyStore extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_company_stores';

    private const ACTIVE = 1;

    public function storeItems()
    {
        return $this->hasMany(IncludeLicenseItemStore::class, 'license_application_company_store_id');
    }

    public function buildingType()
    {
        return $this->belongsTo(BuildingType::class, 'building_type_id');
    }

    public function storeOwnershipType()
    {
        return $this->belongsTo(StoreOwnershipType::class, 'store_ownership_type_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }

    public function dun()
    {
        return $this->belongsTo(Dun::class, 'dun_id');
    }

    public function parliament()
    {
        return $this->belongsTo(Parliament::class, 'parliament_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function store()
    {
        return $this->belongsTo(ReferenceCompanyStore::class, 'company_store_id');
    }
    
}
