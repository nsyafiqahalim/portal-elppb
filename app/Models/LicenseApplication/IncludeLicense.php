<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\IncludeLicenseItem;
use App\Models\LicenseApplication;
use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

class IncludeLicense extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_include_licenses';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function includeLicenseItems()
    {
        return $this->hasMany(IncludeLicenseItem::class, 'license_application_include_license_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

    public function licenseApplicationType()
    {
        return $this->belongsTo(LicenseApplicationType::class, 'license_application_type_id');
    }

    public function licenseType()
    {
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }
    
}
