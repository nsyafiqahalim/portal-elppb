<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\GenerateLicense;
use App\Models\LicenseApplication;
use App\Models\LicenseApplication\IncludeLicenseItem;

use App\Models\Admin\LicenseType;
use App\Models\License;

class IncludeLicenseItemStore extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_store_include_license_items';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function includeLicenseItem()
    {
        return $this->belongsTo(IncludeLicenseItem::class, 'license_application_include_license_item_id');
    }

    public function currentLicense()
    {
        return $this->belongsTo(License::class, 'current_license_id');
    }

    public function oldLicense()
    {
        return $this->belongsTo(License::class, 'old_license_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

    public function licenseType(){
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }
}
