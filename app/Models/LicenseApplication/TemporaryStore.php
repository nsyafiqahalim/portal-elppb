<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\CompanyAddress;


use App\Models\ReferenceData\CompanyStore;

class TemporaryStore extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_temporary_stores';
    public $timestamps = false;
    public $dates = [
        'expiry_date'
    ];

    public function store(){
        return $this->belongsTo(CompanyStore::class,'store_id');
    }
}
