<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\LicenseApplicationChangeType;

class Change extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_changes';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function changeType()
    {
        return $this->belongsTo(LicenseApplicationChangeType::class, 'license_application_change_type_id');
    }
}
