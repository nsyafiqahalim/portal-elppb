<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\Company;
use App\Models\ApprovalLetterApplication\CompanyAddress;
use App\Models\ApprovalLetterApplication\CompanyPartner;
use App\Models\ApprovalLetterApplication\CompanyPremise;
use App\Models\ApprovalLetterApplication\CompanyStore;
use App\Models\ApprovalLetterApplication\Attachment;
use App\Models\ApprovalLetterApplication\Temporary;
use App\Models\ApprovalLetterApplication\BuyPaddy;

use App\Models\Admin\ApprovalLetterApplicationType;
use App\Models\Admin\ApprovalLetterType;
use App\Models\Admin\Status;
use App\Models\ApprovalLetter;

class ApprovalLetterApplication extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_applications';

    private const ACTIVE = 1;

    public function companyAddress()
    {
        return $this->hasOne(CompanyAddress::class, 'approval_letter_application_id');
    }
    public function approvalLetterType()
    {
        return $this->belongsTo(ApprovalLetterType::class, 'approval_letter_type_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function approvalLetterApplicationType()
    {
        return $this->belongsTo(ApprovalLetterApplicationType::class, 'approval_letter_application_type_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'approval_letter_application_id');
    }

    public function companyPartners()
    {
        return $this->hasMany(CompanyPartner::class, 'approval_letter_application_id');
    }

    public function companyStore()
    {
        return $this->hasOne(CompanyStore::class, 'approval_letter_application_id');
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'approval_letter_application_id');
    }

    public function companyPremise()
    {
        return $this->hasOne(CompanyPremise::class, 'approval_letter_application_id');
    }

    public function temporary()
    {
        return $this->hasOne(Temporary::class, 'approval_letter_application_id');
    }

    public function cancellation()
    {
        return $this->hasOne(Cancellation::class, 'approval_letter_application_id');
    }

    public function buyPaddy()
    {
        return $this->hasOne(BuyPaddy::class, 'approval_letter_application_id');
    }

    public function approvalLetters()
    {
        return $this->hasMany(ApprovalLetter::class, 'approval_letter_application_id');
    }
}
