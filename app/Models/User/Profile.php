<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Profile extends Model
{
    public $guarded = ['id'];
    public $table = 'user_profiles';
    public $dates = ['expiry_date'];

    private const ACTIVE = 1;

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
