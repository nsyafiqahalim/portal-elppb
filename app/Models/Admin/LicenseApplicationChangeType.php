<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationChangeType extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_change_types';
}
