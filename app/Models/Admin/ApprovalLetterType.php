<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ApprovalLetterType extends Model
{
    public $guarded = ['id'];

    public const BELI_PADI  = 'E';
    public const KILANG_PADI_KOMERSIAL  = 'F';
}
