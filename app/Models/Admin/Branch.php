<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\District;
use App\Models\Admin\AreaCoverage;

class Branch extends Model
{
    public $guarded = ['id'];

    public function areaCoverages()
    {
        return $this->hasMany(AreaCoverage::class, 'branch_id');
    }
}
