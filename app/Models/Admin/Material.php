<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_materials';

    private const ACTIVE = 1;
    
    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
