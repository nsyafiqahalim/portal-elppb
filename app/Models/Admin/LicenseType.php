<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseType extends Model
{
    public $guarded = ['id'];

    public const RUNCIT  = 'D';
    public const BORONG  = 'C';
    public const BELI_PADI  = 'E';
    public const KILANG_PADI_KOMERSIAL  = 'F';
    public const IMPORT  = 'A';
    public const EKSPORT  = 'B';
}
