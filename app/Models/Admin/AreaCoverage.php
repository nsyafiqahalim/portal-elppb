<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Branch;

class AreaCoverage extends Model
{
    public $guarded = ['id'];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id','id');
    }
}
