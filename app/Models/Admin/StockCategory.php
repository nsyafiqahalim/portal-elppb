<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class StockCategory extends Model
{
    public $guarded = ['id'];

    public $table = 'stock_categories';
}
