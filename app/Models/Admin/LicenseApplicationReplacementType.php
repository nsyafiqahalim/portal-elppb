<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationReplacementType extends Model
{
    private const ACTIVE = 1;
    
    public $guarded = ['id'];
    public $table = 'license_application_replacement_types';

    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
