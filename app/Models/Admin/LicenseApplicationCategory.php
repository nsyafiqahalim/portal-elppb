<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationCategory extends Model
{
    public $guarded = ['id'];

    public const INDIVIDUAL = 'INDIVIDUAL';
    public const BERSEKALI  = 'BERSEKALI';
}
