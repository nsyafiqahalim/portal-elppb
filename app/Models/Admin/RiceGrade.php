<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class RiceGrade extends Model
{
    public $guarded = ['id'];

    private const ACTIVE = 1;
    
    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
