<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

use App\Traits\BasicModelTrait;

class BuildingType extends Model
{
    use BasicModelTrait;
    
    public $guarded = ['id'];

    private const ACTIVE = 1;
    
    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
