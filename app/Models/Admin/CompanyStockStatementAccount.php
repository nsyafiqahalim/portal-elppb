<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class CompanyStockStatmentAccount extends Model
{
    
    public $guarded = ['id'];
    public $table   =   'company_stock_statement_accounts';

    private const ACTIVE = 1;
    
    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
