<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationBuyPaddyCondition extends Model
{
    private const ACTIVE = 1;
    
    public $guarded = ['id'];
    public $table = 'license_application_buy_paddy_conditions';

    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
