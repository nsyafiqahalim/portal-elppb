<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationType extends Model
{
    public $guarded = ['id'];

    public const BAHARU  = 'BAHARU';
    public const PEMBAHARUAN  = 'PEMBAHARUAN';
    public const PERUBAHAN  = 'PERUBAHAN';
    public const PEMBATALAN  = 'PEMBATALAN';
    public const PENGGANTIAN  = 'PENGGANTIAN';
    public const PEMBAHARUAN_DAN_PERUBAHAN  = 'PEMBAHARUAN_DAN_PERUBAHAN';
}
