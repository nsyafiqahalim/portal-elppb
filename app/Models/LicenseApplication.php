<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\Company;
use App\Models\LicenseApplication\CompanyAddress;
use App\Models\LicenseApplication\CompanyPartner;
use App\Models\LicenseApplication\CompanyPremise;
use App\Models\LicenseApplication\CompanyStore;
use App\Models\LicenseApplication\Attachment;
use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\TemporaryStore;
use App\Models\LicenseApplication\ApprovalLetter;
use App\Models\LicenseApplication\BuyPaddy;
use App\Models\LicenseApplication\Cancellation;
use App\Models\LicenseApplication\Replacement;
use App\Models\LicenseApplication\Change;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;
use App\Models\Admin\Status;
use App\Models\License;
 
use App\Models\LicenseApplication\IncludeLicense;
use App\Models\LicenseApplication\IncludeLicenseItemStore;

class LicenseApplication extends Model
{
    public $guarded = ['id'];
    public $table = 'license_applications';

    private const ACTIVE = 1;

    public function change()
    {
        return $this->hasOne(Change::class, 'license_application_id');
    }

    public function companyAddress()
    {
        return $this->hasOne(CompanyAddress::class, 'license_application_id');
    }

    public function referenceLicense()
    {
        return $this->hasOne(ReferenceLicense::class, 'license_application_id');
    }

    public function includeLicense()
    {
        return $this->hasOne(IncludeLicense::class, 'license_application_id');
    }

    public function licenseType()
    {
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function licenseApplicationType()
    {
        return $this->belongsTo(LicenseApplicationType::class, 'license_application_type_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'license_application_id');
    }

    public function storeItems()
    {
        return $this->hasMany(IncludeLicenseItemStore::class, 'license_application_id');
    }

    public function companyPartners()
    {
        return $this->hasMany(CompanyPartner::class, 'license_application_id');
    }

    public function companyStores()
    {
        return $this->hasMany(CompanyStore::class, 'license_application_id')->with(['storeItems']);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'license_application_id');
    }

    public function companyPremise()
    {
        return $this->hasOne(CompanyPremise::class, 'license_application_id');
    }

    public function temporary()
    {
        return $this->hasOne(Temporary::class, 'license_application_id');
    }

    public function cancellation()
    {
        return $this->hasOne(Cancellation::class, 'license_application_id');
    }

    public function replacement()
    {
        return $this->hasOne(Replacement::class, 'license_application_id');
    }

    public function approvalLetter()
    {
        return $this->hasOne(ApprovalLetter::class, 'license_application_id');
    }

    public function buyPaddy()
    {
        return $this->hasOne(BuyPaddy::class, 'license_application_id');
    }
    
    public function licenses()
    {
        return $this->hasMany(License::class, 'license_application_id');
    }

    public function temporaryStores()
    {
        return $this->hasMany(TemporaryStore::class, 'license_application_id');
    }
    
}
