<?php

namespace App\DataObjects;

use DB;
use Carbon\Carbon;
use App\Models\ApprovalLetterApplication;

class ApprovalLetterApplicationDataObject
{
    public static function createNewApprovalLetterApplication($param)
    {
        return ApprovalLetterApplication::create([
            'reference_number'          =>  $param['reference_number'],
            'status_id'                 =>  $param['status_id'],
            'company_name'                 =>  $param['company_name'],
            'approval_letter_application_type_id'           =>  $param['approval_letter_application_type_id'],
            'approval_letter_type_id'=>  $param['approval_letter_type_id'],
            //'branch_id'                 =>  $param['branch_id'],
            'user_id'                 =>  $param['user_id'],
            'apply_date'                =>  Carbon::now()->format('Y-m-d')

        ]);
    }

    public static function updateApprovalLetterApplication($param, $id)
    {
        return ApprovalLetterApplication::find($id)->update([
            'reference_number'          =>  $param['reference_number'],
            'status_id'                 =>  $param['status_id'],
            'company_name'                 =>  $param['company_name'],
            'approval_letter_application_type_id'           =>  $param['approval_letter_application_type_id'],
            'approval_letter_type_id'=>  $param['approval_letter_type_id'],
            //'branch_id'                 =>  $param['branch_id'],
            'user_id'                 =>  $param['user_id'],
            'apply_date'                =>  Carbon::now()->format('Y-m-d')
        ]);
    }
    public static function findApprovalLetterApplicationById($id)
    {
        return ApprovalLetterApplication::find($id);
    }
    public static function findApprovalLetterApplicationByLicenseId($licenseId)
    {
        return ApprovalLetterApplication::whereHas('licenses', function ($license) use ($licenseId) {
            return $license->where('id', $licenseId);
        })->first();
    }


    public static function createDraftApprovalLetterApplication($param)
    {
        return ApprovalLetterApplication::create([
            'status_id'                 =>  $param['status_id'],
            'user_id'                 =>  $param['user_id'],
            'company_name'                 =>  $param['company_name'],
            'approval_letter_application_type_id'           =>  $param['approval_letter_application_type_id'],
            'approval_letter_type_id'=>  $param['approval_letter_type_id'],
            //'approval_letter_license_generation_type_id'   =>  $param['approval_letter_license_generation_type_id'] ?? null,
        ]);
        }
        
    public static function findMyApprovalLetterApplicationUsingDatatableFormat($param)
    {
        $approvalLetterApplications =  ApprovalLetterApplication::distinct()
                    ->when(isset($param['user_id']), function ($approvalLetterApplication) use ($param) {
                        $approvalLetterApplication->where('user_id', $param['user_id']);
                    })->orderby('id', 'desc');
                
        return datatables()->of($approvalLetterApplications)
        ->addIndexColumn()
        ->addColumn('approval_letter_type', function ($approvalLetterApplication) {
            return $approvalLetterApplication->approvalLetterType->name ?? '-';
        })->addColumn('approval_letter_application_type', function ($approvalLetterApplication) {
            return $approvalLetterApplication->approvalLetterApplicationType->name ?? '-';
        })->addColumn('status', function ($approvalLetterApplication) {
            return $approvalLetterApplication->status->name ?? '-';
        })
        ->addColumn('company', function ($approvalLetterApplication) {
            return $approvalLetterApplication->company_name?? '-';
        })
        ->addColumn('reference_number', function ($approvalLetterApplication) {
            return $approvalLetterApplication->reference_number ?? '-';
        })
        ->addColumn('action', function ($approvalLetterApplication) {
            $approvalLetterTypeName = $approvalLetterApplication->approvalLetterType->name;
            $approvalLetterApplicationTypeName = $approvalLetterApplication->approvalLetterApplicationType->name;
            if ($approvalLetterTypeName == 'Membeli Padi') {
                $approvalLetterType = 'buy_paddy';
            } elseif ($approvalLetterTypeName == 'Kilang Padi Komersil') {
                $approvalLetterType = 'factory_paddy';
            }

            if ($approvalLetterApplicationTypeName == 'BAHARU') {
                $approvalLetterApplicationType = 'new';
            }

            return view(
                'approval-letter-application.partials.datatable-button',
                [
                    'id'    =>  encrypt($approvalLetterApplication->id),
                    'approvalLetterType'   =>  $approvalLetterType,
                    'approvalLetterApplicationType'    =>  $approvalLetterApplicationType
                ]
            )->render();
        })
    ->make(true);
    }
}
