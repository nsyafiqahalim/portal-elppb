<?php

namespace App\DataObjects\LicenseApplication;

use DB;

use App\Models\LicenseApplication\IncludeLicenseItem;
use App\Models\LicenseApplication\IncludeLicenseItemStore;
use App\Models\LicenseApplication\IncludeLicense;

use App\DataObjects\Admin\GenerationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use Carbon\Carbon;

class IncludeLicenseItemDataObject
{
    public static function addIncludeLicenseItem($param)
    {
        return  IncludeLicenseItem::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_include_license_id'    =>  $param['license_application_include_license_id'],
                'status_id'                                 =>  $param['status_id'],
                'license_type_id'                           =>  $param['license_type_id'],
                'current_license_number'                    =>  $param['current_license_number'],
                'current_license_id'                        =>  $param['current_license_id'],
                'old_license_number'                        =>  $param['old_license_number'],
                'old_license_id'                            =>  $param['old_license_id']
            ]);

    }

    public static function copyAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($includeLicenseItemParam, $includeLicenseId){
        $includeLicense = IncludeLicense::find($includeLicenseId);
        $arrayedIncludeLicenseItems = $includeLicense->includeLicenseItems->toArray();
        
        foreach($arrayedIncludeLicenseItems as $arrayedIncludeLicenseItem){
            IncludeLicenseItem::create([
                'license_application_id'                    =>  $includeLicenseItemParam['license_application_id'],
                'license_application_include_license_id'    =>  $includeLicenseItemParam['license_application_include_license_id'],
                'status_id'                                 =>  $includeLicenseItemParam['status_id'],
                'license_type_id'                           =>  $arrayedIncludeLicenseItem['license_type_id'],
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $arrayedIncludeLicenseItem['current_license_number'],
                'old_license_id'                            =>  $arrayedIncludeLicenseItem['current_license_id']
            ]);
        }
    }

    public static function cancelAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($includeLicenseItemParam, $includeLicenseId){
        $includeLicense = IncludeLicense::find($includeLicenseId);
        $arrayedIncludeLicenseItems = $includeLicense->includeLicenseItems->toArray();
        
        $status         =   StatusDataObject::findStatusByCode('LESEN_BAKAL_DIJANA_PERLU_DIBATALKAN');

        foreach($arrayedIncludeLicenseItems as $arrayedIncludeLicenseItem){
            IncludeLicenseItem::create([
                'license_application_id'                    =>  $includeLicenseItemParam['license_application_id'],
                'license_application_include_license_id'    =>  $includeLicenseItemParam['license_application_include_license_id'],
                'status_id'                                 =>  $status->id,
                'license_type_id'                           =>  $arrayedIncludeLicenseItem['license_type_id'],
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $arrayedIncludeLicenseItem['current_license_number'],
                'old_license_id'                            =>  $arrayedIncludeLicenseItem['current_license_id']
            ]);
        }
    }

    public static function replaceAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($includeLicenseItemParam, $includeLicenseId){
        $includeLicense = IncludeLicense::find($includeLicenseId);
        $arrayedIncludeLicenseItems = $includeLicense->includeLicenseItems->toArray();
        
        $status         =   StatusDataObject::findStatusByCode('LESEN_BAKAL_DIJANA_PERLU_DIGANTIKAN');

        foreach($arrayedIncludeLicenseItems as $arrayedIncludeLicenseItem){
            IncludeLicenseItem::create([
                'license_application_id'                    =>  $includeLicenseItemParam['license_application_id'],
                'license_application_include_license_id'    =>  $includeLicenseItemParam['license_application_include_license_id'],
                'status_id'                                 =>  $status->id,
                'license_type_id'                           =>  $arrayedIncludeLicenseItem['license_type_id'],
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $arrayedIncludeLicenseItem['current_license_number'],
                'old_license_id'                            =>  $arrayedIncludeLicenseItem['current_license_id']
            ]);
        }
    }
    
    public static function  replacementIncludeLicenseBasedOnMaterial($param,$existingLiceses = null){

        $generation = GenerationTypeDataObject::findGenerationTypeById($param['license_application_license_generation_type_id']);
        $arrayedDomain  =   $generation->toArray();

        $configName = $generation['domain'].'.'.$generation['code'];
        $generatedLicenses  = config('includelicense.'.$configName);
        foreach($generatedLicenses as $item){

            $licenseType    =   LicenseTypeDataObject::findLicenseTypeByCode($item);
            $status         =   StatusDataObject::findStatusByCode('LESEN_BAKAL_DIJANA_PERLU_DIGANTIKAN');
            
            $oldLicenseNumber = null;
            $oldLicenseId = null;

            IncludeLicenseItem::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_include_license_id'    =>  $param['license_application_include_license_id'],
                'status_id'                                 =>  $status->id,
                'license_type_id'                           =>  $licenseType->id,
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $oldLicenseNumber,
                'old_license_id'                            =>  $oldLicenseId
            ]);
        }
    }

    public static function  cancelIncludeLicenseBasedOnMaterial($param,$existingLiceses = null){

        $generation = GenerationTypeDataObject::findGenerationTypeById($param['license_application_license_generation_type_id']);
        $arrayedDomain  =   $generation->toArray();

        $configName = $generation['domain'].'.'.$generation['code'];
        $generatedLicenses  = config('includelicense.'.$configName);
        foreach($generatedLicenses as $item){

            $licenseType    =   LicenseTypeDataObject::findLicenseTypeByCode($item);
            $status         =   StatusDataObject::findStatusByCode('LESEN_BAKAL_DIJANA_PERLU_DIBATALKAN');
            
            $oldLicenseNumber = null;
            $oldLicenseId = null;

            IncludeLicenseItem::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_include_license_id'    =>  $param['license_application_include_license_id'],
                'status_id'                                 =>  $status->id,
                'license_type_id'                           =>  $licenseType->id,
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $oldLicenseNumber,
                'old_license_id'                            =>  $oldLicenseId
            ]);
        }
    }

    public static function addIncludeLicenseBasedOnMaterial($param,$existingLiceses = null){

        $generation = GenerationTypeDataObject::findGenerationTypeById($param['license_application_license_generation_type_id']);
        $arrayedDomain  =   $generation->toArray();

        $configName = $generation['domain'].'.'.$generation['code'];
        $generatedLicenses  = config('includelicense.'.$configName);
        foreach($generatedLicenses as $item){

            $licenseType    =   LicenseTypeDataObject::findLicenseTypeByCode($item);
            $status         =   StatusDataObject::findStatusByCode('LESEN_BAKAL_DIJANA_PERLU_DICETAK');
            
            $oldLicenseNumber = null;
            $oldLicenseId = null;

            IncludeLicenseItem::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_include_license_id'    =>  $param['license_application_include_license_id'],
                'status_id'                                 =>  $status->id,
                'license_type_id'                           =>  $licenseType->id,
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $oldLicenseNumber,
                'old_license_id'                            =>  $oldLicenseId
            ]);
        }
    }

    public static function addIncludeLicenseBasedOnMaterialWithStore($param,$store, $existingLiceses = null){

        $generation = GenerationTypeDataObject::findGenerationTypeById($param['license_application_license_generation_type_id']);
        $arrayedDomain  =   $generation->toArray();

        $configName = $generation['domain'].'.'.$generation['code'];
        $generatedLicenses  = config('includelicense.'.$configName);
        foreach($generatedLicenses as $item){

            $licenseType    =   LicenseTypeDataObject::findLicenseTypeByCode($item);
            $status         =   StatusDataObject::findStatusByCode('LESEN_BAKAL_DIJANA_PERLU_DICETAK');
            
            $oldLicenseNumber = null;
            $oldLicenseId = null;

            $includeLicenseItem =  IncludeLicenseItem::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_include_license_id'    =>  $param['license_application_include_license_id'],
                'status_id'                                 =>  $status->id,
                'license_type_id'                           =>  $licenseType->id,
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $oldLicenseNumber,
                'old_license_id'                            =>  $oldLicenseId
            ]);
            
            if($item == "C"){
                IncludeLicenseItemStore::create([
                    'license_application_id'                        =>  $param['license_application_id'],
                    'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                    'license_application_company_store_id'          =>  $store->id,
                    'apply_load'                                    => 10
                ]);
            }
            else{
                if(isset($param['checkbox-'.$item.'-'.$store->store->unique_id_identifier])){

                    IncludeLicenseItemStore::create([
                        'license_application_id'                        =>  $param['license_application_id'],
                        'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                        'license_application_company_store_id'          =>  $store->id,
                        'apply_load'                                    =>  $param['load-'.$item.'-'.$store->store->unique_id_identifier]
                    ]);
                }
                
            }
        }
    }

    public static function copyExistingIncludeLicenseItemsForANewLicenseApplication($oldStore,$newStore,$param)
    {
        foreach($oldStore->storeItems as $item){
            $includeLicenseItem =   IncludeLicenseItem::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_include_license_id'    =>  $param['license_application_include_license_id'],
                'status_id'                                 =>  StatusDataObject::findStatusIdByCode('LESEN_BAKAL_DIJANA_PERLU_DICETAK'),
                'license_type_id'                           =>  $item->includeLicenseItem->license_type_id,
                'current_license_number'                    =>  null,
                'current_license_id'                        =>  null,
                'old_license_number'                        =>  $item->includeLicenseItem->current_license_number,
                'old_license_id'                            =>  $item->includeLicenseItem->current_license_id
            ]);

            IncludeLicenseItemStore::create([
                    'license_application_id'                        =>  $param['license_application_id'],
                    'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                    'license_application_company_store_id'          =>  $newStore->id,
                    'apply_load'                                    => 10
                ]);
        }
    }
}
