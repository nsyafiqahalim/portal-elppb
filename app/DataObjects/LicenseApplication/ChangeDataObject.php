<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Illuminate\Support\Facades\Storage;

use App\Models\LicenseApplication\Change;
use App\Models\Admin\Material;

class ChangeDataObject

{
    public static function UpdateOrCreateChange($param)
    {
        return    Change::UpdateOrCreate([
                'license_application_id'    =>  $param['license_application_id']
            ],
            [
                'license_application_change_type_id'                    =>  $param['license_application_change_type_id'] ?? null,
                'license_application_company_id'                        =>  $param['license_application_company_id'] ?? null,
                'license_application_company_store_id'                  =>  $param['license_application_company_store_id'] ?? null,
                'license_application_company_premise_id'                =>  $param['license_application_company_premise_id'] ?? null,
                'original_load'                                         =>  $param['original_load'] ?? null
            ]);
    }
}
