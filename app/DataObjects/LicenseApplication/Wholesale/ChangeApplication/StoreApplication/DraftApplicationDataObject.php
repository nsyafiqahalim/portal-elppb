<?php

namespace App\DataObjects\LicenseApplication\Wholesale\ChangeApplication\StoreApplication;

use DB;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\Attachment;
 
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\ChangeDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\Models\LicenseApplicationCancellationType;

class DraftApplicationDataObject
{
    public static function store($param)
    {
        $licenseApplicationParam = [
                'status_id'                     =>  $param['status_id'],
                'applicant_id'                       =>  $param['applicant_id'],
                'company_name'                  =>  $param['company_name'],
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'branch_id'                     =>  $param['branch']->id,
                'apply_load'                    =>  $param['apply_load'],
        ];

        $licenseApplication = LicenseApplicationDataObject::createDraftLicenseApplication($licenseApplicationParam);
        $param['license_application_id']    =   $licenseApplication->id;

        $companyDAOParam = $param['original_company']->toArray();
        $companyDAOParam['company_type_name'] = $param['original_company']->companyType->name;
        $companyDAOParam['company_id'] = $param['original_company']->id;
        $companyDAOParam['license_application_id'] = $param['license_application_id'];
        unset($companyDAOParam['id']);
        $company = CompanyDataObject::createNewCompany($companyDAOParam);

        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);

        $changeParam = [
                'license_application_change_type_id'                    =>  $param['license_application_change_type']->id,
                'license_application_company_id'                        =>  null,
                'license_application_company_store_id'                  =>  null,
                'license_application_company_premise_id'                =>  null,
                'original_load'                                         =>  null,
                'license_application_id'                                =>  $licenseApplication->id,
        ];

        ChangeDataObject::UpdateOrCreateChange($changeParam);

        return Temporary::create([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'company_store_id'            =>  $param['store_id'],
                'license_application_include_license_id'    =>  $param['include_license_id'],
                'license_application_id'        =>  $licenseApplication->id,
        ]);
    }

    public static function update($param, $id)
    {
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);
    }
}
