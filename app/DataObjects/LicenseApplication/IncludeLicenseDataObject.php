<?php

namespace App\DataObjects\LicenseApplication;

use DB;

use App\Models\LicenseApplication\IncludeLicense;
use App\DataObjects\Admin\StatusDataObject;

use Carbon\Carbon;

class IncludeLicenseDataObject
{
   
    public static function  copyExistingIncludeLicenseForANewLicenseApplication($includeLicense,$param){
        return  IncludeLicense::create([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_type_id'               =>  $param['license_application_type_id'],
                'status_id'                                 =>  StatusDataObject::findStatusIdByCode('KUMPULAN_LESEN_SEDANG_PROSES'),
                'license_type_id'                           =>  $param['license_type_id'],
            ]);
    }

    public static function addIncludeLicense($param)
    {
        return  IncludeLicense::firstOrCreate([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_type_id'               =>  $param['license_application_type_id'],
                'status_id'                                 =>  $param['status_id'],
                'license_type_id'                           =>  $param['license_type_id'],
            ]);
    }

    public static function findIncludeLicenseById($id)
    {
        return IncludeLicense::find($id);
    }

    public static function findActiveWholesaleLicenseAndFactoryLicenseByCompanyId($companyId)
    {
        return IncludeLicense::distinct()
        ->where('company_id',$companyId)
        ->whereHas('licenseType',function($licenseType){
            $licenseType->where('code','C')
            ->orWhere('code','F');
        })
        ->whereHas('status',function($status){
            $status->where('code','LESEN_AKTIF');
        })
        ->get();
    }

    public static function findMyIncludeLicenseUsingDatatableFormat($param)
    {
        $includeLicense =  IncludeLicense::distinct()->with(['licenseApplication','licenseApplicationType','includeLicenseItems','includeLicenseItems.currentLicense','includeLicenseItems.oldLicense'])
        ->whereHas('includeLicenseItems',function($includeLiceseItem) use($param){
            $includeLiceseItem->whereHas('currentLicense',function($license)use($param){
                $license->when(isset($param['applicant_id']), function ($License) use ($param) {
                        $License->where('user_id', $param['user_id']);
                    });
            });
        })
        ->orderby('id', 'desc');
                
        return datatables()->of($includeLicense)
        ->addIndexColumn()
       ->addColumn('license_application_type', function ($includeLicense) {
            return $includeLicense->licenseApplicationType->name ?? '-';
        })
        ->addColumn('reference_number', function ($includeLicense) {
            //return $includeLicense->licenseApplicationType->name ?? '-';
            return $includeLicense->licenseApplication->reference_number ?? '-';
        })
        ->addColumn('status', function ($includeLicense) {
            return 'Aktif';
        })
        ->addColumn('company', function ($includeLicense) {
            return $includeLicense->licenseApplication->company->name?? '-';
        })
        ->addColumn('license_number', function ($includeLicense) {
            //return $includeLicense->license_number ?? '-';
            return '-';
        })
        ->addColumn('license_information', function ($includeLicense) {
            $includeLicenseItems = $includeLicense->includeLicenseItems;

            return view(
                'license-management.partials.license-info',
                compact('includeLicenseItems')
            )->render();
        })
        ->addColumn('action', function ($includeLicense) {
            $licenseTypeName = $includeLicense->licenseType->name;

            if ($licenseTypeName == 'Runcit') {
                $licenseType = 'retail';
            } elseif ($licenseTypeName == 'Borong') {
                $licenseType = 'wholesale';
            } elseif ($licenseTypeName == 'Import') {
                $licenseType = 'import';
            } elseif ($licenseTypeName == 'Eksport') {
                $licenseType = 'export';
            } elseif ($licenseTypeName == 'Membeli Padi') {
                $licenseType = 'buy_paddy';
            } elseif ($licenseTypeName == 'Kilang Padi Komersil') {
                $licenseType = 'factory_paddy';
            }
            

            return view(
                'license-management.partials.datatable-button',
                [
                    'id'    =>  encrypt($includeLicense->license_application_id),
                    'licenseType'   =>  $licenseType
                ]
            )->render();
        })
        ->rawColumns(['status','action','license_information'])
    ->make(true);
    }
}
