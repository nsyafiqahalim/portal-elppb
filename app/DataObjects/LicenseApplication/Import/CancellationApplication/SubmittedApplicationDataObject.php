<?php

namespace App\DataObjects\LicenseApplication\Import\CancellationApplication;

use DB;
use Carbon\Carbon;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\Attachment;
 
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\Models\LicenseApplication\Cancellation;
use App\DataObjects\LicenseApplication\SerialNumberDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseItemDataObject;

class SubmittedApplicationDataObject
{
    public static function store($param)
    {

        $serialNumber = SerialNumberDataObject::generateReferenceNumber($param['branch']['short_code'],$param['license_type_code']);
        
        $param['license_application']['reference_number']       =   $serialNumber['reference_number'];
        $licenseApplication = LicenseApplicationDataObject::createNewLicenseApplication($param['license_application']);

        SerialNumberDataObject::updateSerialNumber($serialNumber['object']->id, $serialNumber['object']->counter+1);
        
        $param['license_application_id']                        =   $licenseApplication->id;
        $param['original_company']['license_application_id']    =   $licenseApplication->id;
        $param['original_address']['license_application_id']    =   $licenseApplication->id;
        $param['original_premise']['license_application_id']    =   $licenseApplication->id;
        $param['original_store']['license_application_id']    =   $licenseApplication->id;
        $param['include_license']['license_application_id']     =   $licenseApplication->id;

        $company = CompanyDataObject::createNewCompany($param['original_company']);
        $addess = CompanyAddressDataObject::createNewCompanyAddress($param['original_address']);
        $premise = CompanyPremiseDataObject::addNewCompanyPremise($param['original_premise']);
        $store = CompanyStoreDataObject::addNewCompanyStore($param['original_store']);

        foreach ($param['original_partners'] as $item) {
            $item['license_application_id']                     = $licenseApplication->id;
            CompanyPartnerDataObject::addNewCompanyPartner($item);
        }

        Cancellation::updateOrCreate([
                'license_application_id'   =>$licenseApplication->id
            ], [
                'license_application_cancellation_type_id'    =>  $param['license_application_cancellation_type_id'] ?? null,
                'remarks'                                      =>  $param['remarks'] ?? null
            ]);

        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);

        $includeLicense = IncludeLicenseDataObject::addIncludeLicense($param['include_license']);
        $param['license_application_include_license_id']    =   $includeLicense->id;
        IncludeLicenseItemDataObject::cancelAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($param,$param['include_license_id']);
    }
    public static function update($param, $id)
    {
        $currentLiceseApplication                               =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        $arrayedCurrentLicenseApplication                       =   $currentLiceseApplication->toArray();
        $serialNumber                                           =   SerialNumberDataObject::generateReferenceNumber($param['branch']['short_code'],$param['license_type_code']);
        
        $param['license_application']['reference_number']       =   $serialNumber['reference_number'];
        $licenseApplication                                     =   LicenseApplicationDataObject::updateLicenseApplication($param['license_application'], $id);
        
        SerialNumberDataObject::updateSerialNumber($serialNumber['object']->id, $serialNumber['object']->counter+1);
        
        Temporary::where('license_application_id', $id)->first()->delete();

        $param['license_application_id']                        =   $id;
        $param['original_company']['license_application_id']    =   $id;
        $param['original_address']['license_application_id']    =   $id;
        $param['original_premise']['license_application_id']    =   $id;
        $param['original_store']['license_application_id']      =   $id;
        $param['include_license']['license_application_id']     =   $id;
        
        $company = CompanyDataObject::createNewCompany($param['original_company']);
        $addess = CompanyAddressDataObject::createNewCompanyAddress($param['original_address']);
        $premise = CompanyPremiseDataObject::addNewCompanyPremise($param['original_premise']);
        $store = CompanyStoreDataObject::addNewCompanyStore($param['original_store']);

        foreach ($param['original_partners'] as $item) {
            $item['license_application_id']                     = $id;
            CompanyPartnerDataObject::addNewCompanyPartner($item);
        }  

        Cancellation::updateOrCreate([
                'license_application_id'    =>$id
            ], [
                'license_application_cancellation_type_id'    =>  $param['license_application_cancellation_type_id'] ?? null,
                'remarks'                                      =>  $param['remarks'] ?? null
            ]);

        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],$arrayedCurrentLicenseApplication['material_domain'],$param);

        $includeLicense = IncludeLicenseDataObject::addIncludeLicense($param['include_license']);
        $param['license_application_include_license_id']    =   $includeLicense->id;
        IncludeLicenseItemDataObject::cancelAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($param,$param['include_license_id']);
    }
}
