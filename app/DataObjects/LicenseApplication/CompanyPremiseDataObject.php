<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Auth;

use App\Models\LicenseApplication\CompanyPremise;

class CompanyPremiseDataObject
{   
    
    public static function updateCompanyPremiseByLicenseApplicationId($param,$licenseApplicationId){
        return CompanyPremise::where('license_application_id',$licenseApplicationId)->first()->update($param);
    }

    public static function copyExistingCompanyPremiseIntoNewALicenseApplication($companyPremise,$param){
        return CompanyPremise::create([
                'business_type_id'          =>  $companyPremise['business_type_id'],
                'business_type_name'        =>  $companyPremise['business_type_name'],
                'building_type_id'          =>  $companyPremise['building_type_id'],
                'building_type_name'        =>  $companyPremise['building_type_name'],
                'dun_id'                    =>  $companyPremise['dun_id'],
                'dun_name'                  =>  $companyPremise['dun']['name'],
                'company_id'                =>  $companyPremise['company_id'],
                'parliament_id'             =>  $companyPremise['parliament_id'],
                'district_id'               =>  $companyPremise['district_id'],
                'district_name'             =>  $companyPremise['district_name'],
                'parliament_name'           =>  $companyPremise['parliament_name'],
                'address_1'                 =>  $companyPremise['address_1'],
                'address_2'                 =>  $companyPremise['address_2'],
                'address_3'                 =>  $companyPremise['address_3'],
                'postcode'                  =>  $companyPremise['postcode'],
                'state_id'                  =>  $companyPremise['state_id'],
                'state_name'                =>  $companyPremise['state_name'],
                'company_premise_id'        =>  $companyPremise['company_premise_id'],
                'license_application_id'    =>  $param['license_application_id'],
                'phone_number'              =>  $companyPremise['phone_number'],
                'fax_number'                =>  $companyPremise['fax_number'],
                'email'                     =>  $companyPremise['email'],
            ]);
    }

    public static function addNewCompanyPremise($param)
    {
        return CompanyPremise::create([
            //'name'                  =>  $param['name'],
            'business_type_id'          =>  $param['business_type_id'],
            'business_type_name'        =>  $param['business_type_name'],
            'building_type_id'          =>  $param['building_type_id'],
            'building_type_name'        =>  $param['building_type_name'],
            'dun_id'                    =>  $param['dun_id'],
            'dun_name'                  =>  $param['dun_name'],
            'company_id'                =>  $param['company_id'],
            'parliament_id'             =>  $param['parliament_id'],
            'district_id'               =>  $param['district_id'],
            'district_name'             =>  $param['district_name'],
            'parliament_name'           =>  $param['parliament_name'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'state_name'                =>  $param['state_name'],
            'company_premise_id'        =>  $param['company_premise_id'],
            'license_application_id'    =>  $param['license_application_id'],
            'phone_number'              =>  $param['phone_number'],
            'fax_number'                =>  $param['fax_number'],
            'email'                     =>  $param['email'],
            //'user_id'               =>  $param['user_id'],
        ]);
    }

    public static function UpdateOrCreateCompanyPremise($param){
        return CompanyPremise::UpdateOrCreate([
             'license_application_id'                  =>  $param['license_application_id'],
        ],[
            //'name'                  =>  $param['name'],
            'business_type_id'       =>  $param['business_type_id'],
            'business_type_name'       =>  $param['business_type_name'],
            'building_type_id'       =>  $param['building_type_id'],
            'building_type_name'       =>  $param['building_type_name'],
            'dun_id'                =>  $param['dun_id'],
            'dun_name'                =>  $param['dun_name'],
            'company_id'                =>  $param['company_id'],
            'parliament_id'         =>  $param['parliament_id'],
            'district_id'         =>  $param['district_id'],
            'district_name'         =>  $param['district_name'],
            'parliament_name'         =>  $param['parliament_name'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'state_name'                  =>  $param['state_name'],
            'company_premise_id'                  =>  $param['company_premise_id'],
           
            'phone_number'              =>  $param['phone_number'],
            'fax_number'              =>  $param['fax_number'],
            'email'                     =>  $param['email'],
            'is_premise_validate'            =>  $param['is_premise_validate']??null,
        ]);
    }

    public static function updateCompanyPremise($param, $id){
        return CompanyPremise::find($id)->update([
            //'name'                  =>  $param['name'],
            'business_type_id'       =>  $param['business_type_id'],
            'business_type_name'       =>  $param['business_type_name'],
            'building_type_id'       =>  $param['building_type_id'],
            'building_type_name'       =>  $param['building_type_name'],
            'dun_id'                =>  $param['dun_id'],
            'dun_name'                =>  $param['dun_name'],
            'company_id'                =>  $param['company_id'],
            'parliament_id'         =>  $param['parliament_id'],
            'district_id'         =>  $param['district_id'],
            'district_name'         =>  $param['district_name'],
            'parliament_name'         =>  $param['parliament_name'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'state_name'                  =>  $param['state_name'],
            'company_premise_id'                  =>  $param['company_premise_id'],
            'license_application_id'                  =>  $param['license_application_id'],
            'is_premise_validate'            =>  $param['is_premise_validate']??null,
            //'user_id'               =>  $param['user_id'],
        ]);
    }

    public static function findLicenseApplicationCompanyPremiseById($id)
    {
        $CompanyPremise = CompanyPremise::find($id);

        return $CompanyPremise;
    }

    public static function findAllCompanyPremisesUsingDatatableFormat($param)
    {
        $companies =  CompanyPremise::distinct()
                    ->when(isset($param['user_id']), function ($company) {
                        $company->where('user_id', $param['user_id']);
                    });
                
        return datatables()->of($companies)
        ->addColumn('action', function ($company) {
            // return view('admin.company-type.partials.datatable-button', compact('company'))->render();
        })
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
        })->addColumn('business_type', function ($company) {
            return $company->businessType->name ?? '-';
        })->addColumn('building_type', function ($company) {
            return $company->buildingType->name ?? '-';
        })
        ->addColumn('dun', function ($company) {
            return $company->dun->name ?? '-';
        })
        ->addColumn('parliament', function ($company) {
            return $company->parliament->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return view(
                'license-application.component.premise.partials.radiobutton',
                [
                'id'    =>  encrypt($company->id)
            ]
            )->render();
        })
        ->addColumn('name', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyPremiseById($id)
    {
        return CompanyPremise::find($id);
    }
}
