<?php

namespace App\DataObjects\LicenseApplication\BuyPaddy\CancellationApplication;

use DB;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\Cancellation;
use App\Models\LicenseApplication\Attachment;
 
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\Models\LicenseApplicationCancellationType;

class DraftApplicationDataObject
{
    public static function store($param)
    {
        $licenseApplicationParam = [
                'status_id'                     =>  $param['status_id'],
                'user_id'                       =>  $param['user_id'],
                'company_name'                  =>  $param['company_name'],
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_application_license_generation_type_id'   =>  $param['license_application_license_generation_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
                'branch_id'                     =>null,
        ];

        $licenseApplication = LicenseApplicationDataObject::createDraftLicenseApplication($licenseApplicationParam);
        $param['license_application_id']    =   $licenseApplication->id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);

        Cancellation::updateOrCreate([
                'license_application_id'    =>$licenseApplication->id
            ], [
                'license_application_cancellation_type_id'    =>  $param['license_application_cancellation_type_id'] ?? null,
                'remarks'                                      =>  $param['remarks'] ?? null
            ]);

        return Temporary::create([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'company_store_id'              =>  $param['store_id'],
                'license_application_id'        =>  $licenseApplication->id,
        ]);
    }

    public static function update($param, $id)
    {
        $licenseApplicationParam = [
                'status_id'                     =>  $param['status_id'],
                'user_id'                       =>  $param['user_id'],
                'company_name'                  =>  $param['company_name'],
                'reference_number'              =>  null,
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_application_license_generation_type_id'   =>  $param['license_application_license_generation_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
                'branch_id'                     =>null,
        ];
        
        $licenseApplication = LicenseApplicationDataObject::updateLicenseApplication($licenseApplicationParam, $id);
        
         $param['license_application_id']    =   $id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


          

        Temporary::where('license_application_id', $id)->first()->update([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'company_store_id'              =>  $param['store_id'],
        ]);

        Cancellation::updateOrCreate([
                'license_application_id'    =>$id
            ], [
                'license_application_cancellation_type_id'    =>  $param['license_application_cancellation_type_id'] ?? null,
                'remarks'                                      =>  $param['remarks'] ?? null
            ]);
    }
}
