<?php

namespace App\DataObjects\LicenseApplication\BuyPaddy\NewApplication;

use DB;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\Attachment;
use App\Models\LicenseApplication\BuyPaddy;
use App\Models\LicenseApplication\ApprovalLetter;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\ChangeDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\Models\LicenseApplicationCancellationType;

class DraftApplicationDataObject
{
    public static function store($param)
    {
        $licenseApplicationParam = [
                'status_id'                     =>  $param['status_id'],
                'applicant_id'                       =>  $param['applicant_id'],
                'company_name'                  =>  $param['company_name'],
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'branch_id'                     =>  null,
                'apply_load'                    =>  $param['apply_load'],
        ];

        $licenseApplication = LicenseApplicationDataObject::createDraftLicenseApplication($licenseApplicationParam);
        $param['license_application_id']    =   $licenseApplication->id;

        if(isset($param['premise_id'])){
            $companyPremiseDAOParam                                 = $param['updated_premise'];
            $companyPremiseDAOParam['license_application_id']       = $licenseApplication->id;
            $premise = CompanyPremiseDataObject::UpdateOrCreateCompanyPremise($companyPremiseDAOParam);
        }
        
        if(isset($param['store_id'])){
            $companyStoreDAOParam                              = $param['updated_store'];
            $companyStoreDAOParam['license_application_id'] = $licenseApplication->id;
            $store = CompanyStoreDataObject::UpdateOrCreateCompanyStore($companyStoreDAOParam);
        }

        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


        if (isset($param['license_application_buy_paddy_condition_id'])) {
            BuyPaddy::updateOrCreate([
                'license_application_id'    =>$licenseApplication->id
            ], [
                'license_application_buy_paddy_condition_id'    =>  $param['license_application_buy_paddy_condition_id']
            ]);
        }

        if (isset($param['approval_letter_id'])) {
            ApprovalLetter::updateOrCreate([
                'license_application_id'    =>$licenseApplication->id,
            ], [
                'approval_letter_id'    =>  $param['approval_letter_id']
            ]);
        } 
        
        return Temporary::create([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'company_store_id'                    =>  $param['store_id'],
                'license_application_id'        =>  $licenseApplication->id,
        ]);
    }

    public static function update($param, $id)
    {
        $licenseApplicationParam = [
                'status_id'                     =>  $param['status_id'],
                'applicant_id'                       =>  $param['applicant_id'],
                'company_name'                  =>  $param['company_name'],
                'reference_number'              =>  null,
                'branch_id'                     =>  null,
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
        ];
        $licenseApplication = LicenseApplicationDataObject::updateLicenseApplication($licenseApplicationParam, $id);
        $param['license_application_id']    =   $id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


       if(isset($param['premise_id'])){
            $companyPremiseDAOParam                                 = $param['updated_premise'];
            $companyPremiseDAOParam['license_application_id']       = $id;
            $premise = CompanyPremiseDataObject::UpdateOrCreateCompanyPremise($companyPremiseDAOParam);
        }
        
        if(isset($param['store_id'])){
            $companyStoreDAOParam                              = $param['updated_store'];
            $companyStoreDAOParam['license_application_id'] = $id;
            $store = CompanyStoreDataObject::UpdateOrCreateCompanyStore($companyStoreDAOParam);
        }

        if (isset($param['license_application_buy_paddy_condition_id'])) {
            BuyPaddy::updateOrCreate([
                'license_application_id'    =>$id
            ], [
                'license_application_buy_paddy_condition_id'    =>  $param['license_application_buy_paddy_condition_id']
            ]);
        }
        
        if (isset($param['approval_letter_id'])) {
            ApprovalLetter::updateOrCreate([
                'license_application_id'    =>$id,
            ], [
                'approval_letter_id'    =>  $param['approval_letter_id']
            ]);
        } 

        Temporary::where('license_application_id', $id)->first()->update([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'company_store_id'                    =>  $param['store_id'],
        ]);
    }
}
