<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Auth;

use App\Models\LicenseApplication\CompanyPartner;

class CompanyPartnerDataObject
{
    public static function copyExistingCompanyPartnersForANewLicenseApplication($companyPartners,$param){
        foreach($companyPartners as $companyPartner){
            CompanyPartner::create([
                'name'                      =>  $companyPartner['name'],
                'race_type_id'              =>  $companyPartner['race_type_id'],
                'race_type_name'            =>  $companyPartner['race_type_name'],
                'is_citizen'                =>  $companyPartner['is_citizen'],
                'citizen_name'              =>  $companyPartner['citizen_name'],
                'race_id'                   =>  $companyPartner['race_id'],
                'race_name'                 =>  $companyPartner['race_name'],
                'total_share'               =>  $companyPartner['total_share'],
                'share_percentage'          =>  $companyPartner['share_percentage'],
                'phone_number'              =>  $companyPartner['phone_number'],
                'email'                     =>  $companyPartner['email'],
                'nric'                      =>  $companyPartner['nric'],
                'address_1'                 =>  $companyPartner['address_1'],
                'address_2'                 =>  $companyPartner['address_2'],
                'address_3'                 =>  $companyPartner['address_3'],
                'postcode'                  =>  $companyPartner['postcode'],
                'state_id'                  =>  $companyPartner['state_id'],
                'license_application_id'    =>  $param['license_application_id'],
                'state_name'                =>  $companyPartner['state_name'],
                'company_partner_id'        =>  $companyPartner['company_partner_id']
            ]);
        }

        return CompanyPartner::where('license_application_id',$param['license_application_id'])->get();
    }

    public static function addNewCompanyPartner($param)
    {
        return CompanyPartner::create([
            'name'                      =>  $param['name'],
            'race_type_id'              =>  $param['race_type_id'],
            'race_type_name'            =>  $param['race_type_name'],
            'is_citizen'                =>  $param['is_citizen'],
            'citizen_name'              =>  $param['citizen_name'],
            'race_id'                   =>  $param['race_id'],
            'race_name'                 =>  $param['race_name'],
            'total_share'               =>  $param['total_share'],
            'share_percentage'          =>  $param['share_percentage'],
            'phone_number'              =>  $param['phone_number'],
            'email'                     =>  $param['email'],
            'nric'                      =>  $param['nric'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'license_application_id'    =>  $param['license_application_id'],
            'state_name'                =>  $param['state_name'],
            'company_partner_id'        =>  $param['company_partner_id']
        ]);
    }

    public static function findAllActiveCompanyPartners()
    {
        return CompanyPartner::get();
    }

    public static function findLicenseCompanyPartnerByLicenseApplicationId($id)
    {
        return CompanyPartner::where('license_application_id', $id)->get();
    }

    public static function findAllCompanyPartnersUsingDatatableFormat($param)
    {
        $companies =  CompanyPartner::distinct()
                    ->when(isset($param['company_id']), function ($company) {
                        $company->where('company_id', $param['company_id']);
                    });
                
        return datatables()->of($companies)
        ->addColumn('action', function ($company) {
            // return view('admin.company-type.partials.datatable-button', compact('company'))->render();
        })
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
            } else {
                return '-';
            }
        })->addColumn('ownership_type', function ($company) {
            return $company->ownershipType->name ?? '-';
        })
        ->addColumn('race', function ($company) {
            return $company->race->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
