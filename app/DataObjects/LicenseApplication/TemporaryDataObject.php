<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Carbon\Carbon;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\TemporaryStore;
use App\Models\LicenseApplication\Attachment;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;

class TemporaryDataObject
{
    public static function saveAsDraft($param)
    {
        $licenseApplicationParam = [
                'status_id'                     =>  $param['status_id'],
                'user_id'                       =>  $param['user_id'],
                'company_name'                  =>  $param['company_name'],
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
        ];

        return Temporary::create([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'license_application_id'        =>  $licenseApplication->id,
        ]);
    }

    public static function updateLicenseTemporaryWithStore($param){
        $temporary = Temporary::create([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'license_application_id'        =>  $param['license_application_id'],
        ]);

        foreach($param['store_id'] as $store){
            TemporaryStore::UpdateOrCreate([
                'store_id'                  =>  decrypt($store),
                'license_application_id'    =>  $param['license_application_id']
            ],[
                'created_at'                =>  Carbon::now()
            ]);
        }
    }
}
