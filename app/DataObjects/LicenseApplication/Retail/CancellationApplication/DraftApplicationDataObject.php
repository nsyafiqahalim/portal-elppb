<?php

namespace App\DataObjects\LicenseApplication\Retail\CancellationApplication;

use DB;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\Attachment;
 
use App\DataObjects\LicenseApplicationDataObject;
use App\Models\LicenseApplication\Cancellation;
use App\DataObjects\LicenseApplication\AttachmentDataObject;

class DraftApplicationDataObject
{
    public static function store($param)
    {
        $licenseApplication = LicenseApplicationDataObject::createDraftLicenseApplication($param['license_application']);

        $param['license_application_id']    =   $licenseApplication->id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);

          

        Cancellation::updateOrCreate([
                'license_application_id'   =>$licenseApplication->id
            ], [
                'license_application_cancellation_type_id'    =>  $param['license_application_cancellation_type_id'] ?? null,
                'remarks'                                      =>  $param['remarks'] ?? null
            ]);

        return Temporary::create([
                'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'license_application_include_license_id'    =>  $param['include_license_id'],
                'license_application_id'        =>  $licenseApplication->id,
        ]);
    }

    public static function update($param, $id)
    {
        $currentLiceseApplication           =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        $arrayedCurrentLicenseApplication   =   $currentLiceseApplication->toArray();
        $licenseApplication                 =   LicenseApplicationDataObject::updateLicenseApplication($param['license_application'], $id);
        $param['license_application_id']    =   $id;
        
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],$arrayedCurrentLicenseApplication['material_domain'],$param);

           

        Cancellation::updateOrCreate([
                'license_application_id'    =>$id
            ], [
                'license_application_cancellation_type_id'    =>  $param['license_application_cancellation_type_id'] ?? null,
                'remarks'                                      =>  $param['remarks'] ?? null
            ]);

        return Temporary::where('license_application_id', $id)->first()->update([
                'company_premise_id'            =>  $param['premise_id'],
                'license_application_include_license_id'    =>  $param['include_license_id'],
                'company_id'                    =>  $param['company_id'],
        ]);
    }
}
