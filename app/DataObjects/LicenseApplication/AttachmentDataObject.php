<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Illuminate\Support\Facades\Storage;
use LicenseApplicationMaterialFacade;

use App\Models\LicenseApplication\Attachment;
use App\Models\Admin\Material;

class AttachmentDataObject
{
    public static function addNewAttachment($param)
    {
        $path = Storage::disk('fileURL')->put('license_application_attachment/'.$param['license_application_id'], $param['file']);

        Attachment::create([
            'file_path'                 =>  $path,
            'code'                      =>  $param['code'],
            'file_original_name'        =>  $param['file_original_name'],
            'license_application_id'    =>  $param['license_application_id']
        ]);
    }


    public static function findAttachmentByCodeAndLicenseApplicationID($code, $licenseApplicationId)
    {
        return Attachment::where('code', $code)->where('license_application_id', $licenseApplicationId)->first();
    }

    public static function findAttachmentByLicenseApplicationId( $licenseApplicationId)
    {
        return Attachment::where('license_application_id', $licenseApplicationId)->get();
    }

    public static function addDraftValidateAttachments($param){
        $rules                          =   [];
        $messages                       =   [];
        $companyId                      =   (isset($param['company_id']))?decrypt($param['company_id']):null;
        $licenseType                    =   (isset($param['license_type']))?decrypt($param['license_type']):null;
        $licenseApplicationType         =   (isset($param['license_application_type']))?decrypt($param['license_application_type']):null;

        if(isset($companyId)){
            $domain  = LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($companyId, $licenseType, $licenseApplicationType);

            $materials  = Material::where('domain', $domain)->get();
            foreach ($materials as $material) {
                $rules[$material->code]  =    ['nullable', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'];
                $messages[$material->code.'.max']  =    ['"'.$material->label.'" perlu 20MB dan kebawah'];
                $messages[$material->code.'.mimes']  =    ['Format "'.$material->label.'" yang dibenarkan adalah jpeg,png,jpg,pdf'];
            }
        }
    
        return [
            'rules'     =>  $rules,
            'messages'  =>  $messages
        ];
    }

    public static function addOrUpdateAttachmentsByMaterialDomain($currentDomain,$oldDomain,$param){
            self::removeAttachmentWhenMaterialDomainWhenMaterialDomainHasChanged($param['license_application_id'], $currentDomain, $oldDomain);

            $materials  = Material::where('domain', $currentDomain)->get();
            foreach ($materials as $material) {
                if (isset($param[$material->code])) {

                    $attachment = self::findAttachmentByCodeAndLicenseApplicationID($material->code, $param['license_application_id']);
                    if (isset($attachment)) {
                        try{
                         //   unlink(public_path().'//file/'.$attachment->file_path);
                        }
                        catch(Exception $err){}
                
                        $attachment->delete();
                    }

                    $storedParameter = [
                        'file'                                  =>  $param[$material->code],
                        'file_original_name'                    =>  $param[$material->code]->getClientOriginalName(),
                        'code'                                  =>  $material->code,
                        'license_application_id'                =>  $param['license_application_id'],
                    ];

                    self::addNewAttachment($storedParameter);
                }
            }
    }

    public static function removeAttachmentWhenMaterialDomainWhenMaterialDomainHasChanged($id, $currentDomain, $odDomain){
        if(isset($currentDoamin) && isset($oldDomain) && $currentDoamin != $oldDomain){
            $attachments = self::findAttachmentByLicenseApplicationId($id);
            foreach ($attachments as $attachment) {
                try{
                    unlink(public_path().'//file/'.$attachment->file_path);
                }
                catch(Exception $err){}
                
                $attachment->delete();
            }
        }
    }

    public static function validateAttachments($validator,$request, $id){
        $param = $request->all();

        $companyId                      =   (isset($param['company_id']))?decrypt($param['company_id']):null;
        $licenseType                    =   (isset($param['license_type']))?decrypt($param['license_type']):null;
        $licenseApplicationType         =   (isset($param['license_application_type']))?decrypt($param['license_application_type']):null;
        if(isset($companyId)){
            $domain  = LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($companyId, $licenseType, $licenseApplicationType);
            
            $materials  = Material::where('domain', $domain)->get();
            foreach ($materials as $material) {
                $attachment = AttachmentDataObject::findAttachmentByCodeAndLicenseApplicationID($material->code, $id);
                //dd($request->file($material->code));
                if ($attachment == null && $request->file($material->code) == null) {
                        $validator->errors()->add($material->code, '"'.$material->label.'" diperlukan');
                }
            }
        }
        return $validator;
    }

    public static function addValidateAttachments($param){
        $rules                          =   [];
        $messages                       =   [];
        $companyId                      =   (isset($param['company_id']))?decrypt($param['company_id']):null;
        $licenseType                    =   (isset($param['license_type']))?decrypt($param['license_type']):null;
        $licenseApplicationType         =   (isset($param['license_application_type']))?decrypt($param['license_application_type']):null;
        if(isset($companyId)){
            $domain  = LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($companyId, $licenseType, $licenseApplicationType);
            
            $materials  = Material::where('domain', $domain)->get();
            foreach ($materials as $material) {
                $rules[$material->code]  =    ['required', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'];
                $messages[$material->code.'.required']  =    ['"'.$material->label.'" diperlukan'];
                $messages[$material->code.'.max']  =    ['"'.$material->label.'" perlu 20MB dan kebawah'];
                $messages[$material->code.'.mimes']  =    ['Format "'.$material->label.'" yang dibenarkan adalah jpeg,png,jpg,pdf'];
            }
        }
    
        return [
            'rules'     =>  $rules,
            'messages'  =>  $messages
        ];
    }

    public static function updateValidateAttachments($domain){
        $rules = [];
        $messages = [];
        $rules                          =   [];
        $messages                       =   [];
        $companyId                      =   (isset($param['company_id']))?decrypt($param['company_id']):null;
        $licenseType                    =   (isset($param['license_type']))?decrypt($param['license_type']):null;
        $licenseApplicationType         =   (isset($param['license_application_type']))?decrypt($param['license_application_type']):null;

        if(isset($companyId)){
            $domain  = LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($companyId, $licenseType, $licenseApplicationType);

            $materials  = Material::where('domain', $domain)->get();
            foreach ($materials as $material) {
                $rules[$material->code]  =    ['nullable', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'];
                $messages[$material->code.'.max']  =    ['"'.$material->label.'" perlu 20MB dan kebawah'];
                $messages[$material->code.'.mimes']  =    ['Format "'.$material->label.'" yang dibenarkan adalah jpeg,png,jpg,pdf'];
            }
        }
        
        return [
            'rules'     =>  $rules,
            'messages'  =>  $messages
        ];
    }
}
