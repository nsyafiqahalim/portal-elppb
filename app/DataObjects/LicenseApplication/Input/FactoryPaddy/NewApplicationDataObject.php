<?php

namespace App\DataObjects\LicenseApplication\Input\FactoryPaddy;

use DB;

use App\DataObjects\LicenseA\CompanyDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;
use App\DataObjects\Admin\SettingDataObject;

class NewApplicationDataObject
{
    public static function newInputParameter()
    {
        $inputParam = [];

        $inputParam['company_types']            =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                   =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']          =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                    =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']               =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                     =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']              =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']           =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']           =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']    =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        $inputParam['load_setting']             =   SettingDataObject::findSettingByDomainAndCode('wholesale_license_setting','load_application');
        $inputParam['apply_load']           =   $inputParam['load_setting']->value.' '.$inputParam['load_setting']->metric;
        $inputParam['duration_setting']         =   SettingDataObject::findSettingByDomainAndCode('wholesale_license_setting','application_duration');
        $inputParam['apply_duration']           =   $inputParam['duration_setting']->value.' '.$inputParam['duration_setting']->metric;

        return $inputParam;
    }

    public static function draftInputParameter($id)
    {
        $inputParam = [];
        $inputParam['license_application']              =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        $inputParam['current_license_application_id']   =   encrypt($inputParam['license_application']->id);
        $inputParam['license_application_temporary']    =   $inputParam['license_application']->temporary;
        $inputParam['company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                           =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']                  =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                             =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']                      =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                        =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']            =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        $inputParam['company_id']                       =   $inputParam['license_application_temporary']->company_id;
        $inputParam['premise_id']                       =   $inputParam['license_application_temporary']->company_premise_id;
        $inputParam['store_id']                         =   $inputParam['license_application_temporary']->company_store_id;
        $inputParam['load_setting']                     =   SettingDataObject::findSettingByDomainAndCode('wholesale_license_setting','load_application');
        $inputParam['apply_load']                       =   $inputParam['load_setting']->value.' '.$inputParam['load_setting']->metric;
        $inputParam['duration_setting']                 =   SettingDataObject::findSettingByDomainAndCode('wholesale_license_setting','application_duration');
        $inputParam['apply_duration']                   =   $inputParam['duration_setting']->value.' '.$inputParam['duration_setting']->metric;

        return $inputParam;
    }
}
