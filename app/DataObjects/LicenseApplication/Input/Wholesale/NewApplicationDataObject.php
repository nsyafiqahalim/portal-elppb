<?php

namespace App\DataObjects\LicenseApplication\Input\Wholesale;

use DB;
use Auth;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;
use App\DataObjects\Admin\SettingDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;

class NewApplicationDataObject
{
    public static function newInputParameter()
    {
        $inputParam = [];

        $inputParam['company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                           =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']                  =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                             =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']                      =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                        =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']            =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        $inputParam['load_setting']                     =   SettingDataObject::findSettingByDomainAndCode('PENETAPAN_LESEN_BORONG','PERMOHONAN_HAD_MUATAN');
        $inputParam['apply_load']                       =   $inputParam['load_setting']->value.' '.$inputParam['load_setting']->metric;
        $inputParam['duration_setting']                 =   SettingDataObject::findSettingByDomainAndCode('PENETAPAN_LESEN_BORONG','PERMOHONAN_TAMAT_TEMPOH');
        $inputParam['apply_duration']                   =   $inputParam['duration_setting']->value.' '.$inputParam['duration_setting']->metric;

        $statusId                                       =   StatusDataObject::findStatusIdByCode('PERMOHONAN_BAHARU_BORONG_BAHARU');
        $outputParam['license_application_type_id']     =   LicenseApplicationTypeDataObject::findLicenseApplicationTypeIdByCode(LicenseApplicationType::BAHARU);
        $outputParam['license_type_id']                 =   LicenseTypeDataObject::findLicenseTypeIdByCode(LicenseType::BORONG);

        $licenseApplicationParam = [
                'status_id'                     =>  $statusId,
                'license_application_type_id'   =>  $outputParam['license_application_type_id'],
                'license_type_id'               =>  $outputParam['license_type_id'],
                'applicant_id'                  =>  Auth::user()->id,
        ];

        $licenseApplication                             =   LicenseApplicationDataObject::createDraftLicenseApplication($licenseApplicationParam);
        $inputParam['encrypted_license_application_id'] =   encrypt($licenseApplication->id);
        
        return $inputParam;
    }

    public static function draftInputParameter($id)
    {
        $inputParam = [];
        $inputParam['license_application']              =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        $inputParam['current_license_application_id']   =   encrypt($inputParam['license_application']->id);
        $inputParam['license_application_temporary']    =   (isset($inputParam['license_application']->temporary))?$inputParam['license_application']->temporary: null;
        $inputParam['license_application_temporary_stores']    =   (isset($inputParam['license_application']->temporaryStores))?$inputParam['license_application']->temporaryStores: null;
        $inputParam['company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                           =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']                  =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                             =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']                      =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                        =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']            =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        $inputParam['company_id']                       =   (isset($inputParam['license_application_temporary']->company_id))?$inputParam['license_application_temporary']->company_id: null;
        $inputParam['company']                          =   (isset($inputParam['license_application_temporary']->company))?$inputParam['license_application_temporary']->company: null;
        $inputParam['premise_id']                       =   (isset($inputParam['license_application_temporary']->company_premise_id))?$inputParam['license_application_temporary']->company_premise_id: null;
        //$inputParam['store_id']                         =   $inputParam['license_application_temporary']->company_store_id;
        $inputParam['load_setting']                     =   SettingDataObject::findSettingByDomainAndCode('PENETAPAN_LESEN_BORONG','PERMOHONAN_HAD_MUATAN');
        $inputParam['apply_load']                       =   $inputParam['load_setting']->value.' '.$inputParam['load_setting']->metric;
        $inputParam['duration_setting']                 =   SettingDataObject::findSettingByDomainAndCode('PENETAPAN_LESEN_BORONG','PERMOHONAN_TAMAT_TEMPOH');
        $inputParam['apply_duration']                   =   $inputParam['duration_setting']->value.' '.$inputParam['duration_setting']->metric;

        return $inputParam;
    }
}
