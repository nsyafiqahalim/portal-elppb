<?php

namespace App\DataObjects\LicenseApplication\Input\Wholesale;

use DB;

use App\DataObjects\LicenseA\CompanyDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

class RenewApplicationDataObject
{
    public static function newInputParameter($id, $licenseApplicationDataSource)
    {
        $inputParam = [];

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $inputParam['license_application']                              =   LicenseApplicationDataObject::findLicenseApplicationById($id);
            $inputParam['include_license_id']                               =   $inputParam['license_application']->includeLicense->id;
            $inputParam['license_application_company']                      =   $inputParam['license_application']->company;
            $inputParam['license_application_address']                      =   $inputParam['license_application']->companyAddress;
            $inputParam['license_application_partners']                     =   $inputParam['license_application']->companyPartners;
            $inputParam['license_application_premise']                      =   $inputParam['license_application']->companyPremise;
            $inputParam['license_application_store']                        =   $inputParam['license_application']->companyStore;
            $inputParam['company_id']                                       =   $inputParam['license_application_company']->company_id;
            $inputParam['premise_id']                                       =   $inputParam['license_application_premise']->company_premise_id;
            $inputParam['store_id']                                         =   $inputParam['license_application_store']->company_store_id;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $inputParam['license_application']                              =   LicenseApplicationDataObject::findLicenseApplicationById($id);
            $inputParam['include_license_id']                               =   $inputParam['license_application']->includeLicense->id;
            $inputParam['license_application_company']                      =   $inputParam['license_application']->company;
            $inputParam['license_application_address']                      =   $inputParam['license_application']->companyAddress;
            $inputParam['license_application_partners']                     =   $inputParam['license_application']->companyPartners;
            $inputParam['license_application_premise']                      =   $inputParam['license_application']->companyPremise;
            $inputParam['license_application_store']                        =   $inputParam['license_application']->companyStore;
            $inputParam['company_id']                                       =   $inputParam['license_application_company']->company_id;
            $inputParam['premise_id']                                       =   $inputParam['license_application_premise']->company_premise_id;
            $inputParam['store_id']                                         =   $inputParam['license_application_store']->company_store_id;
        }
        $inputParam['durations']                                             =   DurationDataObject::findAllDurations();
        
        return $inputParam;
    }

    public static function draftInputParameter($id, $licenseApplicationDataSource)
    {
        $inputParam = [];

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $inputParam['current_license_application']                      =   LicenseApplicationDataObject::findLicenseApplicationById($id);
            $inputParam['current_license_application_temporary']            =   $inputParam['current_license_application']->temporary;
             
            $inputParam['current_license_application_id']   =   encrypt($inputParam['current_license_application']->id);
            
            //below is the origianl license application
            $inputParam['license_application']                              =    LicenseApplicationDataObject::findLicenseApplicationByPreviouseLicenseApplicationIncludeLicenseId($inputParam['current_license_application_temporary']->license_application_include_license_id);
            $inputParam['include_license_id']                               =   $inputParam['current_license_application_temporary']->license_application_include_license_id;
            $inputParam['license_application_company']                      =   $inputParam['license_application']->company;

            $inputParam['license_application_address']                      =   $inputParam['license_application']->companyAddress;
            $inputParam['license_application_partners']                     =   $inputParam['license_application']->companyPartners;
            $inputParam['license_application_premise']                      =   $inputParam['license_application']->companyPremise;
            $inputParam['license_application_store']                        =   $inputParam['license_application']->companyStore;
            $inputParam['company_id']                                       =   $inputParam['license_application_company']->company_id;
            $inputParam['premise_id']                                       =   $inputParam['license_application_premise']->company_premise_id;
            $inputParam['store_id']                                         =   $inputParam['license_application_store']->company_store_id;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $inputParam['license_application']                              =   LicenseApplicationDataObject::findLicenseApplicationById($id);
            $inputParam['include_license_id']                               =   $inputParam['license_application']->includeLicense->id;
            $inputParam['license_application_company']                      =   $inputParam['license_application']->company;
            $inputParam['license_application_address']                      =   $inputParam['license_application']->companyAddress;
            $inputParam['license_application_partners']                     =   $inputParam['license_application']->companyPartners;
            $inputParam['license_application_premise']                      =   $inputParam['license_application']->companyPremise;
            $inputParam['license_application_store']                        =   $inputParam['license_application']->companyStore;
            $inputParam['company_id']                                       =   $inputParam['license_application_company']->company_id;
            $inputParam['premise_id']                                       =   $inputParam['license_application_premise']->company_premise_id;
            $inputParam['store_id']                                         =   $inputParam['license_application_store']->company_store_id;
        }
        $inputParam['durations']                                             =   DurationDataObject::findAllDurations();

        return $inputParam;
    }
}
