<?php

namespace App\DataObjects\LicenseApplication\Input\Wholesale;

use DB;
use Auth;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\CompanyDataObject as LicenseApplicationCompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseItemDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseItemStore;
use App\DataObjects\LicenseApplication\ChangeDataObject;

use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationChangeTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationCancellationTypeDataObject;

class CancellationApplicationDataObject
{
    public static function newInputParameter($id)
    {
        $inputParam = [];

        $statusId                                       =   StatusDataObject::findStatusIdByCode('DRAF_BORONG_PEMBATALAN');
        $inputParam['license_application_cancellation_types']           =   LicenseApplicationCancellationTypeDataObject::findAllActiveLicenseApplicationCancellationTypes();
        $inputParam['license_application_type_id']      =   LicenseApplicationTypeDataObject::findLicenseApplicationTypeIdByCode('PEMBATALAN');
        $inputParam['license_type_id']                  =   LicenseTypeDataObject::findLicenseTypeIdByCode(LicenseType::BORONG);
        $outputParam['original_license_application']    =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        //$param['license_application_change_type']       = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode('COMPANY_INFORMATION');
        $inputParam['old_license_application_id']      =   $outputParam['original_license_application']->id;

        $licenseApplicationParam = [
                'status_id'                     =>  $statusId,
                'license_application_type_id'   =>  $inputParam['license_application_type_id'],
                'license_type_id'               =>  $inputParam['license_type_id'],
                'applicant_id'                  =>  Auth::user()->id,
                'company_name'                  =>  $outputParam['original_license_application']->company_name,
        ];

        $inputParam['license_application']                  =   LicenseApplicationDataObject::createDraftLicenseApplication($licenseApplicationParam);
        $inputParam['license_application_id']               =   $inputParam['license_application']->id;
        $inputParam['user_id']                              =   Auth::user()->id;

        $inputParam['license_application_company']          =   LicenseApplicationCompanyDataObject::copyReferenceCompanyIntoLiceseApplicationCompany($outputParam['original_license_application']->company,$inputParam);
        $inputParam['license_application_address']          =   CompanyAddressDataObject::copyReferenceCompanyAddressIntoLiceseApplicationCompany($outputParam['original_license_application']->companyAddress,$inputParam);
        $inputParam['license_application_partners']              =   CompanyPartnerDataObject::copyExistingCompanyPartnersForANewLicenseApplication($outputParam['original_license_application']->companyPartners,$inputParam);
        $inputParam['license_application_premise']               =   CompanyPremiseDataObject::copyExistingCompanyPremiseIntoNewALicenseApplication($outputParam['original_license_application']->companyPremise,$inputParam);
        $inputParam['include_license']                           =   IncludeLicenseDataObject::copyExistingIncludeLicenseForANewLicenseApplication($outputParam['original_license_application']->includeLicense,$inputParam);
        $inputParam['license_application_include_license_id']    =   $inputParam['include_license'] ->id;
        $inputParam['license_application_stores']                =   CompanyStoreDataObject::copyExistingCompanyStoresForANewLicenseApplication($outputParam['original_license_application']->companyStores,$inputParam);
        
        $inputParam['encrypted_license_application_id']     =   encrypt($inputParam['license_application_id']);
        $inputParam['company_id']                           =   $inputParam['license_application_company']->company_id;
            
        return $inputParam;
    }

    public static function draftInputParameter($id)
    {
        $inputParam = [];
        $inputParam['license_application']                      =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        $inputParam['license_application_cancellation']                 =   $inputParam['license_application']->cancellation;
        $inputParam['license_application_cancellation_types']           =   LicenseApplicationCancellationTypeDataObject::findAllActiveLicenseApplicationCancellationTypes();
        $inputParam['license_application_company']              =   $inputParam['license_application']->company;
        $inputParam['license_application_address']              =   $inputParam['license_application']->companyAddress;
        $inputParam['license_application_partners']              =   $inputParam['license_application']->companyPartners;
        $inputParam['license_application_premise']               =  $inputParam['license_application']->companyPremise;
        $inputParam['license_application_stores']               =  $inputParam['license_application']->companyStores;
        $inputParam['license_application_id']                   =   $inputParam['license_application']->id;
        $inputParam['company_id']                               =   $inputParam['license_application_company']->company_id;
        $inputParam['encrypted_license_application_id']     =   encrypt($inputParam['license_application_id']);
        
        return $inputParam;
    }
}
