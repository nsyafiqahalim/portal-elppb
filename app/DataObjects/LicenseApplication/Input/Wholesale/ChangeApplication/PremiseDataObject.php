<?php

namespace App\DataObjects\LicenseApplication\Input\Wholesale\ChangeApplication;

use DB;
use Auth;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\CompanyDataObject as LicenseApplicationCompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseItemDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseItemStore;
use App\DataObjects\LicenseApplication\ChangeDataObject;

use App\DataObjects\LicenseApplicationDataObject as OriginalLicenseApplicationDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationChangeTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;

use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

class PremiseDataObject
{
    public static function newInputParameter($id)
    {
        $inputParam = [];

        $statusId                                       =   StatusDataObject::findStatusIdByCode('DRAF_BORONG_PERUBAHAN');
        $inputParam['license_application_type_id']      =   LicenseApplicationTypeDataObject::findLicenseApplicationTypeIdByCode('PERUBAHAN_MAKLUMAT_PREMIS');
        $inputParam['license_type_id']                  =   LicenseTypeDataObject::findLicenseTypeIdByCode(LicenseType::BORONG);
        $outputParam['original_license_application']    =   OriginalLicenseApplicationDataObject::findLicenseApplicationById($id);
        $param['license_application_change_type']       = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode('PREMISE_INFORMATION');
        $inputParam['old_license_application_id']      =   $outputParam['original_license_application']->id;
        $licenseApplicationParam = [
                'status_id'                     =>  $statusId,
                'license_application_type_id'   =>  $inputParam['license_application_type_id'],
                'license_type_id'               =>  $inputParam['license_type_id'],
                'company_name'                  =>  $outputParam['original_license_application']->company_name,
                'applicant_id'                  =>  Auth::user()->id,
        ];

        $inputParam['license_application']                  =   OriginalLicenseApplicationDataObject::createDraftLicenseApplication($licenseApplicationParam);
        $inputParam['license_application_id']               =   $inputParam['license_application']->id;
        $inputParam['user_id']                              =   Auth::user()->id;
        
        $changeParam = [
                'license_application_change_type_id'                    =>  $param['license_application_change_type']->id,
                'license_application_company_id'                        =>  null,
                'license_application_company_store_id'                  =>  null,
                'license_application_company_premise_id'                =>  null,
                'original_load'                                         =>  null,
                'license_application_id'                                =>  $inputParam['license_application_id'],
        ];

        ChangeDataObject::UpdateOrCreateChange($changeParam);

        $inputParam['license_application_company']          =   LicenseApplicationCompanyDataObject::copyReferenceCompanyIntoLiceseApplicationCompany($outputParam['original_license_application']->company,$inputParam);
        $inputParam['license_application_address']          =   CompanyAddressDataObject::copyReferenceCompanyAddressIntoLiceseApplicationCompany($outputParam['original_license_application']->companyAddress,$inputParam);
        $inputParam['license_application_partners']              =   CompanyPartnerDataObject::copyExistingCompanyPartnersForANewLicenseApplication($outputParam['original_license_application']->companyPartners,$inputParam);
        $inputParam['license_application_premise']               =   CompanyPremiseDataObject::copyExistingCompanyPremiseIntoNewALicenseApplication($outputParam['original_license_application']->companyPremise,$inputParam);
        $inputParam['include_license']                           =   IncludeLicenseDataObject::copyExistingIncludeLicenseForANewLicenseApplication($outputParam['original_license_application']->includeLicense,$inputParam);
        $inputParam['license_application_include_license_id']    =   $inputParam['include_license'] ->id;
        $inputParam['license_application_stores']                =   CompanyStoreDataObject::copyExistingCompanyStoresForANewLicenseApplication($outputParam['original_license_application']->companyStores,$inputParam);
        
        $inputParam['encrypted_license_application_id']     =   encrypt($inputParam['license_application_id']);
        $inputParam['company_id']                           =   $inputParam['license_application_company']->company_id;
        
        $inputParam['premise_company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['premise_states']                           =   StateDataObject::findAllActiveStates();
        $inputParam['premise_ownership_types']                   =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['premise_races']                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['premise_race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['premise_duns']                     =   DunDataObject::findDunsByParliamentId($inputParam['license_application_premise']->parliament_id);
        $inputParam['premise_parliaments']              =   ParliamentDataObject::findParliamentsByStateId($inputParam['license_application_premise']->state_id);
        $inputParam['premise_building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['premise_business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['premise_districts']                =   DistrictDataObject::findDistrictByStateId($inputParam['license_application_premise']->state_id);

        return $inputParam;
    }

    public static function draftInputParameter($id)
    {
        $inputParam = [];
        $inputParam['license_application']                      =   OriginalLicenseApplicationDataObject::findLicenseApplicationById($id);
        $inputParam['license_application_company']              =   $inputParam['license_application']->company;
        $inputParam['license_application_address']              =   $inputParam['license_application']->companyAddress;
        $inputParam['license_application_partners']              =   $inputParam['license_application']->companyPartners;
        $inputParam['license_application_premise']               =  $inputParam['license_application']->companyPremise;
        $inputParam['license_application_stores']               =  $inputParam['license_application']->companyStores;
        $inputParam['license_application_id']                   =   $inputParam['license_application']->id;
        $inputParam['company_id']                               =   $inputParam['license_application_company']->company_id;
        $inputParam['encrypted_license_application_id']     =   encrypt($inputParam['license_application_id']);
        
        $inputParam['premise_company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['premise_states']                           =   StateDataObject::findAllActiveStates();
        $inputParam['premise_ownership_types']                   =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['premise_races']                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['premise_race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['premise_duns']                     =   DunDataObject::findDunsByParliamentId($inputParam['license_application_premise']->parliament_id);
        $inputParam['premise_parliaments']              =   ParliamentDataObject::findParliamentsByStateId($inputParam['license_application_premise']->state_id);
        $inputParam['premise_building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['premise_business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['premise_districts']                =   DistrictDataObject::findDistrictByStateId($inputParam['license_application_premise']->state_id);
            
        return $inputParam;
    }
}
