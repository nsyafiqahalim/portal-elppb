<?php

namespace App\DataObjects\LicenseApplication\Input\Buypaddy;

use DB;

use App\Models\Admin\LicenseType;
use App\DataObjects\LicenseA\CompanyDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;
use App\DataObjects\LicenseDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\GenerationTypeDataObject;
use App\DataObjects\Admin\SettingDataObject;

class NewApplicationDataObject
{
    public static function newInputParameter()
    {
        $inputParam = [];

        $inputParam['company_types']                                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                                           =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']                                  =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']                                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                                             =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']                                      =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']                                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']                                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                                        =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']                            =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();

        return $inputParam;
    }

    public static function draftInputParameter($id)
    {
        $inputParam['current_license_application']                      =   LicenseApplicationDataObject::findLicenseApplicationById($id);
        $inputParam['current_license_application_id']                   =   encrypt($inputParam['current_license_application']->id);
        $inputParam['license_application_temporary']                    =   $inputParam['current_license_application_temporary']            =   $inputParam['current_license_application']->temporary;
        $inputParam['approval_letter_id']                               =   (isset($inputParam['current_license_application']->approvalLetter))?$inputParam['current_license_application']->approvalLetter->approval_letter_id : null;
        $inputParam['license_application_buy_paddy_condition_id']       =   (isset($inputParam['current_license_application']->buyPaddy))?$inputParam['current_license_application']->buyPaddy->license_application_buy_paddy_condition_id: null;
        $inputParam['license_application_buy_paddy_condition_name']       =   (isset($inputParam['current_license_application']->buyPaddy))?$inputParam['current_license_application']->buyPaddy->buyPaddyCondition->name: null;


        $inputParam['company_id']                                       =   $inputParam['current_license_application_temporary']->company_id;
        $inputParam['premise_id']                                       =   $inputParam['current_license_application_temporary']->company_premise_id;
        $inputParam['store_id']                                         =   $inputParam['current_license_application_temporary']->company_store_id;
        $inputParam['company_types']                                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                                           =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']                                  =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']                                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                                             =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']                                      =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']                                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']                                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                                        =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['has_previous_license']                             =   null;

        $inputParam['premise_company_types']                            =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['premise_states']                                   =   StateDataObject::findAllActiveStates();
        $inputParam['premise_ownership_types']                          =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['premise_races']                                    =   RaceDataObject::findAllActiveRaces();
        $inputParam['premise_race_types']                               =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['premise_building_types']                           =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['premise_business_types']                           =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        
        $inputParam['store_states']                                     =   StateDataObject::findAllActiveStates();
        $inputParam['store_building_types']                             =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['store_business_types']                             =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['store_store_ownership_types']                      =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();

        if (isset($inputParam['company_id'])) {
            $param = [];
            $param['status_id']                                         =   StatusDataObject::findStatusByCode('LESEN_AKTIF')->id;
            $param['company_id']                                        =   $inputParam['company_id'];
            $param['license_type_id']                                   =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $license                                                    =   LicenseDataObject::findActiveLicenseExistByCompanyIdAndLicenseTypeId($param);

            $inputParam['license_application_company']                  =   $inputParam['current_license_application']->company;
            $inputParam['license_application_address']                  =   $inputParam['current_license_application']->companyAddress;
            $inputParam['license_application_partners']                 =   $inputParam['current_license_application']->companyPartners;
            $inputParam['license_application_premise']                  =   $inputParam['current_license_application']->companyPremise;
            $inputParam['license_application_store']                    =   $inputParam['current_license_application']->companyStore;
            
            //$inputParam['store_ownership_types']                      =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
            $inputParam['premise_duns']                                 =   DunDataObject::findDunsByParliamentId($inputParam['license_application_premise']->parliament_id);
            $inputParam['premise_parliaments']                          =   ParliamentDataObject::findParliamentsByStateId($inputParam['license_application_premise']->state_id);
            $inputParam['premise_districts']                            =   DistrictDataObject::findDistrictByStateId($inputParam['license_application_premise']->state_id);
            $inputParam['store_districts']                              =   DistrictDataObject::findDistrictByStateId($inputParam['license_application_store']->state_id);
        }

        return $inputParam;
    }
}
