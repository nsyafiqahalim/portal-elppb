<?php

namespace App\DataObjects\LicenseApplication\Input\Retail;

use DB;

use App\DataObjects\License\CompanyDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

class ChangeApplicationDataObject
{
    public static function newInputParameter($id, $licenseApplicationDataSource)
    {
        $inputParam = [];

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $inputParam['license_application']                              =   LicenseApplicationDataObject::findLicenseApplicationById($id);
            $inputParam['include_license_id']                               =   $inputParam['license_application']->includeLicense->id;
            $inputParam['license_application_company']                      =   $inputParam['license_application']->company;
            $inputParam['license_application_address']                      =   $inputParam['license_application']->companyAddress;
            $inputParam['license_application_partners']                     =   $inputParam['license_application']->companyPartners;
            $inputParam['license_application_premise']                      =   $inputParam['license_application']->companyPremise;
            $inputParam['company_id']                                       =   $inputParam['license_application_company']->company_id;
            $inputParam['premise_id']                                       =   $inputParam['license_application_premise']->company_premise_id;

            $inputParam['company_types']            =   CompanyTypeDataObject::findAllActiveCompanyTypes();
            $inputParam['states']                   =   StateDataObject::findAllActiveStates();
            $inputParam['ownership_types']          =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
            $inputParam['races']                    =   RaceDataObject::findAllActiveRaces();
            $inputParam['race_types']               =   RaceTypeDataObject::findAllActiveRaceTypes();
            $inputParam['duns']                     =   DunDataObject::findAllActiveDuns();
            $inputParam['parliaments']              =   ParliamentDataObject::findAllActiveParliaments();
            $inputParam['building_types']           =   BuildingTypeDataObject::findAllActiveBuildingTypes();
            $inputParam['business_types']           =   BusinessTypeDataObject::findAllActiveBusinessTypes();
            $inputParam['districts']                =   DistrictDataObject::findAllActiveDistricts();
            $inputParam['store_ownership_types']    =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
        }
        return $inputParam;
    }

    public static function draftInputParameter($id, $licenseApplicationDataSource)
    {
        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $inputParam['current_license_application']                      =   LicenseApplicationDataObject::findLicenseApplicationById($id);
            $inputParam['current_license_application_id']                   =   encrypt($inputParam['current_license_application']->id);
            $inputParam['current_license_application_temporary']            =   $inputParam['current_license_application']->temporary;
             
            $inputParam['current_license_application_change']               =   $inputParam['current_license_application']->change;
            
            //below is the origianl license application
            $inputParam['license_application']                              =    LicenseApplicationDataObject::findLicenseApplicationByPreviouseLicenseApplicationIncludeLicenseId($inputParam['current_license_application_temporary']->license_application_include_license_id);
            $inputParam['include_license_id']                               =   $inputParam['current_license_application_temporary']->license_application_include_license_id;
            $inputParam['license_application_company']                      =   $inputParam['license_application']->company;
            $inputParam['license_application_address']                      =   $inputParam['license_application']->companyAddress;
            $inputParam['license_application_partners']                     =   $inputParam['license_application']->companyPartners;
            $inputParam['license_application_premise']                      =   $inputParam['license_application']->companyPremise;
            $inputParam['company_id']                                       =   $inputParam['current_license_application_temporary']->company_id;
            $inputParam['premise_id']                                       =   $inputParam['current_license_application_temporary']->company_premise_id;
            $inputParam['store_id']                                         =   $inputParam['current_license_application_temporary']->company_store_id;

            $inputParam['company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
            $inputParam['states']                           =   StateDataObject::findAllActiveStates();
            $inputParam['ownership_types']                   =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
            $inputParam['races']                            =   RaceDataObject::findAllActiveRaces();
            $inputParam['race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
            $inputParam['duns']                             =   DunDataObject::findAllActiveDuns();
            $inputParam['parliaments']                      =   ParliamentDataObject::findAllActiveParliaments();
            $inputParam['building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
            $inputParam['business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
            $inputParam['districts']                        =   DistrictDataObject::findAllActiveDistricts();
            $inputParam['store_ownership_types']            =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
        }

        return $inputParam;
    }
}
