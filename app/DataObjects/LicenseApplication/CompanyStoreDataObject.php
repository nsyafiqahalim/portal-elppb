<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Auth;

use App\Models\LicenseApplication\CompanyStore;
Use App\DataObjects\LicenseApplication\IncludeLicenseItemDataObject;

class CompanyStoreDataObject
{
    public static function copyExistingCompanyStoresForANewLicenseApplication($companyStores,$param){
 
        foreach($companyStores as $companyStore){
            $newStore   =   companyStore::create([
            'store_ownership_type_id'       =>  $companyStore['store_ownership_type_id'],
            'store_ownership_type_name'       =>  $companyStore['store_ownership_type_name'],
            'building_type_id'       =>  $companyStore['building_type_id'],
            'phone_number'       =>  $companyStore['phone_number'],
            'email'                     =>  $companyStore['email'],
            'fax_number'                    =>  $companyStore['fax_number'],
            'building_type_name'            =>  $companyStore['building_type_name'],
            'district_id'                   =>  $companyStore['district_id'],
            'district_name'                =>  $companyStore['district_name'],
            'company_store_id'         =>  $companyStore['company_store_id'],
            'address_1'                 =>  $companyStore['address_1'],
            'address_2'                 =>  $companyStore['address_2'],
            'address_3'                 =>  $companyStore['address_3'],
            'postcode'                  =>  $companyStore['postcode'],
            'state_id'                  =>  $companyStore['state_id'],
            'state_name'                  =>  $companyStore['state_name'],
            'license_application_id'                  =>  $param['license_application_id'],
            'company_id'               =>  $companyStore['company_id'],
            'is_store_validate'            =>  $companyStore['is_store_validate']??null,
            ]);
            
            IncludeLicenseItemDataObject::copyExistingIncludeLicenseItemsForANewLicenseApplication($companyStore,$newStore, $param);
        }
        return companyStore::where('license_application_id',$param['license_application_id'])->get();
    }

    public static function addNewCompanyStore($param)
    {
        return CompanyStore::create([
            //'name'                  =>  $param['name'],
            'store_ownership_type_id'       =>  $param['store_ownership_type_id'],
            'store_ownership_type_name'       =>  $param['store_ownership_type_name'],
            //'business_type_name'       =>  $param['business_type_name'],
            'building_type_id'       =>  $param['building_type_id'],
            'phone_number'       =>  $param['phone_number'],
            'email'       =>  $param['email'],
            'fax_number'       =>  $param['fax_number'],
            'building_type_name'       =>  $param['building_type_name'],
            'district_id'                =>  $param['district_id'],
            'district_name'                =>  $param['district_name'],
            'company_store_id'         =>  $param['company_store_id'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'state_name'                  =>  $param['state_name'],
            'license_application_id'                  =>  $param['license_application_id'],
            'company_id'               =>  $param['company_id'],
            'is_store_validate'            =>  $param['is_store_validate']??null,
        ]);
    }

    public static function  UpdateOrCreateCompanyStore($param)
    {
        return CompanyStore::UpdateOrCreate(
        [
            'license_application_id'                  =>  $param['license_application_id'],
        ],
            [
            //'name'                  =>  $param['name'],
            'store_ownership_type_id'       =>  $param['store_ownership_type_id'],
            'store_ownership_type_name'       =>  $param['store_ownership_type_name'],
            //'business_type_name'       =>  $param['business_type_name'],
            'building_type_id'       =>  $param['building_type_id'],
            'phone_number'       =>  $param['phone_number'],
            'email'       =>  $param['email'],
            'fax_number'       =>  $param['fax_number'],
            'building_type_name'       =>  $param['building_type_name'],
            'district_id'                =>  $param['district_id'],
            'district_name'                =>  $param['district_name'],
            'company_store_id'         =>  $param['company_store_id'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'state_name'                  =>  $param['state_name'],
            'is_store_validate'            =>  $param['is_store_validate'] ?? null,
            'company_id'               =>  $param['company_id'],   
        ]);
    }

    public static function findAllActiveCompanyStores()
    {
        $Companys = CompanyStore::activeOnly()->get();

        return $Companys;
    }

    public static function findAllCompanyStoresUsingDatatableFormat($param)
    {
        $companies =  CompanyStore::distinct()
                    ->with(
                    'buildingType',
                    'storeOwnershipType',
                    'state',
                    'businessType',
                    'district')
                    ->when(isset($param['license_application_id']), function ($companyStore) {
                        $companyStore->where('license_application_id', $param['license_application_id']);
                    });
                
        return datatables()->of($companies)
        ->addColumn('action', function ($companyStore) {
            // return view('admin.companyStore-type.partials.datatable-button', compact('companyStore'))->render();
        })
        ->addColumn('status', function ($companyStore) {
            return ($companyStore->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($companyStore) {
            return $companyStore->address_1.','
                    .$companyStore->address_2.','
                    .$companyStore->address_3.','
                    .$companyStore->postcode.','
                    .$companyStore->state->name.',';
        })->addColumn('business_type', function ($companyStore) {
            return $companyStore->businessType->name ?? '-';
        })->addColumn('building_type', function ($companyStore) {
            return $companyStore->buildingType->name ?? '-';
        })
        ->addColumn('dun', function ($companyStore) {
            return $companyStore->dun->name ?? '-';
        })
        ->addColumn('parliament', function ($companyStore) {
            return $companyStore->parliament->name ?? '-';
        })
        ->addColumn('action', function ($companyStore) {
            return view(
                'license-application.component.premise.partials.radiobutton',
                [
                'id'    =>  encrypt($companyStore->id)
            ]
            )->render();
        })
        ->addColumn('name', function ($companyStore) {
            return '-';
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
