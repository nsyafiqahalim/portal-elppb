<?php

namespace App\DataObjects\LicenseApplication;

use DB;

use App\Models\LicenseApplication\CompanyAddress;

class CompanyAddressDataObject
{
    public static function findLicenseApplicationAddressByLicenseApplicationId($id)
    {
        return CompanyAddress::where('license_application_id', $id)->first();
    }

    public static function copyReferenceCompanyAddressIntoLiceseApplicationCompany($address, $param){
        return CompanyAddress::create([
            'address_1'                 =>  $address['address_1'],
            'address_2'                 =>  $address['address_2'],
            'address_3'                 =>  $address['address_3'],
            'postcode'                  =>  $address['postcode'],
            'state_id'                  =>  $address['state_id'],
            'phone_number'              =>  $address['phone_number'],
            'fax_number'              =>  $address['fax_number'],
            'email'                     =>  $address['email'],
            'state_name'                     =>  $address['state_name'],
            'license_application_id'   =>  $param['license_application_id'],
           // 'company_id'                =>  $param['company_id'],
        ]);
    }

    public static function createNewCompanyAddress($param)
    {
        return CompanyAddress::create([
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'phone_number'              =>  $param['phone_number'],
            'fax_number'              =>  $param['fax_number'],
            'email'                     =>  $param['email'],
            'state_name'                     =>  $param['state_name'],
            'license_application_id'   =>  $param['license_application_id'],
           // 'company_id'                =>  $param['company_id'],
        ]);
    }
}
