<?php

namespace App\DataObjects\LicenseApplication;

use DB;

use App\Models\LicenseApplication\SerialNumber;

use Carbon\Carbon;

class SerialNumberDataObject
{
    public static function findLatestSerialNumber($branchCode,$licenseCode)
    {
        return  SerialNumber::firstOrCreate([
                'year'          => Carbon::now()->format('Y'),
                'branch_code'   =>  $branchCode,
                'license_code'  =>  $licenseCode
            ]);

    }

    public static function updateSerialNumber($id,$counter)
    {
        return  SerialNumber::where('id',$id)->update([
            'counter' =>  $counter
        ]);

    }
    
    public static function generateReferenceNumber($branchCode,$licenseCode){
        $serialNumberObject = self::findLatestSerialNumber($branchCode,$licenseCode);
        $counter = $serialNumberObject->counter + 1;
        $currentCounter  = str_pad($counter, 6, "0", STR_PAD_LEFT);

        $formatted = '326-'.$licenseCode.'-'.$branchCode.'-'.Carbon::now()->format('Y').'-'.$currentCounter;
        return [
            'reference_number'  =>  $formatted,
            'object'            =>  $serialNumberObject
        ];
    }
}
