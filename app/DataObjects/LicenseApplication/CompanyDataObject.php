<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Auth;

use App\Models\LicenseApplication\Company;
use App\Models\ReferenceData\Company as ReferenceDataCompany;
class CompanyDataObject
{
    public static function copyReferenceCompanyIntoLiceseApplicationCompany($company, $param){
        return Company::UpdateOrCreate([
            'name'                  =>  $company['name'],
            'company_type_id'       =>  $company['company_type_id'],
            'company_id'            =>  $company['company_id'],
            'company_type_name'          =>  $company['company_type_name'],
            'registration_number'   =>  $company['registration_number'],
            'expiry_date'           =>  $company['expiry_date'],
            'paidup_capital'        =>  $company['paidup_capital'],
            'user_id'               =>  $param['user_id'],
            'license_application_id'   =>  $param['license_application_id'],
        ]);
    }
    
    
    
    public static function updateCompanyNameByLicenseApplicationId($name,$id)
    {
        return Company::UpdateOrCreate([
            'license_application_id'   =>  $id,
        ],
        [
            'name'                  =>  $name,
        ]);
    }

    public static function createNewCompany($param)
    {
        return Company::UpdateOrCreate([
            'license_application_id'   =>  $param['license_application_id'],
        ],
        [
            'name'                  =>  $param['name'],
            'company_type_id'       =>  $param['company_type_id'],
            'company_id'            =>  $param['company_id'],
            'company_type_name'          =>  $param['company_type_name'],
            'registration_number'   =>  $param['registration_number'],
            'expiry_date'           =>  $param['expiry_date'],
            'paidup_capital'        =>  $param['paidup_capital'],
            'user_id'               =>  $param['user_id'],
        ]);
    }

    public static function findAllActiveCompanys()
    {
        $Companys = Company::activeOnly()->get();

        return $Companys;
    }

    public static function findLicenseApplicationCompanyById($id)
    {
        $companies = Company::find($id);

        return $companies;
    }

    public static function findAllCompanyUsingDatatableFormat($param)
    {
        $companies =  Company::distinct()
                    ->when(isset($param['user_id']), function ($company) {
                        $company->where('user_id', $param['user_id']);
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return $company->address->address_1.','
                    .$company->address->address_2.','
                    .$company->address->address_3.','
                    .$company->address->postcode.','
                    .$company->address->state->name.',';
            } else {
                return '-';
            }
        })->addColumn('company_type', function ($company) {
            return $company->companyType->name ?? '-';
        })
        ->addColumn('expiry_date', function ($company) {
            return $company->expiry_date->format('d/m/Y') ?? '-';
        })
        ->addColumn('action', function ($company) use ($param) {
            return view(
                'license-application.component.company.partials.radiobutton',
                [
                    'id'    =>  encrypt($company->id)
                ]
            )->render();
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
