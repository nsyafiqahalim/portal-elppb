<?php

namespace App\DataObjects\LicenseApplication\Output;

use DB;

use App\DataObjects\LicenseApplicationDataObject;

use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;

use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\SettingDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\Admin\UserDataObject;

use App\Transformers\Licensepplication\CompanyTransformer;
use App\Transformers\Licensepplication\CompanyPremiseTransformer;
use App\Transformers\Licensepplication\CompanyPartnerTransformer;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

class RetailOutputDataObject
{
    public static function newOutputParameter($param)
    {
        $outputParam = $param;

        $outputParam['premise_id']                      = (isset($outputParam['premise_id']))? decrypt($outputParam['premise_id']) : null;
        $outputParam['company_id']                      = (isset($outputParam['company_id']))? decrypt($outputParam['company_id']) : null;
        $outputParam['applicant_id']                    = (isset($outputParam['user_id']))? decrypt($outputParam['user_id']) : null;

        $outputParam['original_company']                = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
        $outputParam['original_premise']                = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
        $outputParam['original_partners']               = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
        $outputParam['branch']                          = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;
        $setting                                        = SettingDataObject::findSettingByDomain('findSettingByDomain');
        $outputParam['company_name']                    = $outputParam['original_company']->name;
        $outputParam['original_address']                = $outputParam['original_company']->address;
        $outputParam['duration_id']                     = $setting->id;
        $outputParam['apply_duration']                  = $setting->name;
        $licenseType                                    = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::RUNCIT);
        $param['license_application_type_id']           = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
        $param['license_type_id']                       = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::RUNCIT)->id;
        $param['status_id']                             = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_RUNCIT_BAHARU')->id;
        $param['license_type_id']                       = $licenseType->id;
        $param['license_type_code']                     = $licenseType->code;
    }

    public static function draftOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;
        $outputParam['company_id']              = (isset($outputParam['company_id']))? decrypt($outputParam['company_id']) : null;
        $outputParam['company_name']            = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
        $outputParam['status_id']                     = StatusDataObject::findStatusByCode('DRAF_RUNCIT_BAHARU')->id;
        $outputParam['license_application_type_id']   = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
        $outputParam['license_type_id']               = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::RUNCIT)->id;

        return $outputParam;
    }
}
