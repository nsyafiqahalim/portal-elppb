<?php

namespace App\DataObjects\LicenseApplication\Output\ChangeApplication\Export;

use DB;

use App\DataObjects\LicenseApplicationDataObject;

use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;

use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;

class StoreOutputDataObject
{
    public static function newOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['store_store_ownership_type_id']        = (isset($outputParam['store_store_ownership_type_id']))?decrypt($param['store_store_ownership_type_id']):null;
            $outputParam['store_building_type_id']        = (isset($outputParam['store_building_type_id']))?decrypt($param['store_building_type_id']):null;
            $outputParam['store_dun_id']                  = (isset($outputParam['store_dun_id']))?decrypt($param['store_dun_id']):null;
            $outputParam['store_parliament_id']           = (isset($outputParam['store_parliament_id']))?decrypt($param['store_parliament_id']):null;
            $outputParam['store_district_id']             = (isset($outputParam['store_district_id']))?decrypt($param['store_district_id']):null;
            $outputParam['store_state_id']                = (isset($outputParam['store_state_id']))?decrypt($param['store_state_id']):null;

            $outputParam['original_company']                = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']                = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']               = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                = OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                    = $outputParam['original_company']->name;
            $outputParam['original_address']                = $outputParam['original_company']->address;
            $outputParam['branch']                          = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_store']->district_id)->branch;
            $outputParam['updated_store']                 = [
                                                            'store_ownership_type_id'          =>  $outputParam['store_store_ownership_type_id'],
                                                            'store_ownership_type_name'        =>  (isset($outputParam['store_store_ownership_type_id']))?StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($outputParam['store_store_ownership_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['store_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['store_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['store_building_type_id'])->name:null,
                                                            'company_id'                =>  $outputParam['original_company'],
                                                            'parliament_id'             =>  $outputParam['store_parliament_id'],
                                                            'district_id'               =>  $outputParam['store_district_id'],
                                                            'district_name'             =>   (isset($outputParam['store_district_id']))?DistrictDataObject::findDistrictById($outputParam['store_district_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['store_address_1'],
                                                            'address_2'                 =>  $outputParam['store_address_2'],
                                                            'address_3'                 =>  $outputParam['store_address_3'],
                                                            'postcode'                  =>  $outputParam['store_postcode'],
                                                            'state_id'                  =>  $outputParam['store_state_id'],
                                                            'state_name'                =>   (isset($outputParam['store_state_id']))?StateDataObject::findStateById($outputParam['store_state_id'])->name:null,
                                                            'company_store_id'        =>  $outputParam['original_store']->id,
                                                            'company_id'                =>  $outputParam['original_company']->id,
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['store_phone_number'],
                                                            'fax_number'                =>  $outputParam['store_fax_number'],
                                                            'email'                     =>  $outputParam['store_email'],
                                                            ];
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['original_company']              = CompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = CompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = CompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
        }

        return $outputParam;
    }

    public static function draftOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;
        
        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['store_store_ownership_type_id']        = (isset($outputParam['store_store_ownership_type_id']))?decrypt($param['store_store_ownership_type_id']):null;
            $outputParam['store_building_type_id']        = (isset($outputParam['store_building_type_id']))?decrypt($param['store_building_type_id']):null;
            $outputParam['store_dun_id']                  = (isset($outputParam['store_dun_id']))?decrypt($param['store_dun_id']):null;
            $outputParam['store_parliament_id']           = (isset($outputParam['store_parliament_id']))?decrypt($param['store_parliament_id']):null;
            $outputParam['store_district_id']             = (isset($outputParam['store_district_id']))?decrypt($param['store_district_id']):null;
            $outputParam['store_state_id']                = (isset($outputParam['store_state_id']))?decrypt($param['store_state_id']):null;
            
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                = OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_store']->district_id)->branch;
            $outputParam['updated_store']                 = [
                                                            'store_ownership_type_id'          =>  $outputParam['store_store_ownership_type_id'],
                                                            'store_ownership_type_name'        =>  (isset($outputParam['store_store_ownership_type_id']))?StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($outputParam['store_store_ownership_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['store_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['store_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['store_building_type_id'])->name:null,
                                                            'company_id'                =>  $outputParam['original_company'],
                                                            'district_id'               =>  $outputParam['store_district_id'],
                                                            'district_name'             =>   (isset($outputParam['store_district_id']))?DistrictDataObject::findDistrictById($outputParam['store_district_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['store_address_1'],
                                                            'address_2'                 =>  $outputParam['store_address_2'],
                                                            'address_3'                 =>  $outputParam['store_address_3'],
                                                            'postcode'                  =>  $outputParam['store_postcode'],
                                                            'state_id'                  =>  $outputParam['store_state_id'],
                                                            'state_name'                =>   (isset($outputParam['store_state_id']))?StateDataObject::findStateById($outputParam['store_state_id'])->name:null,
                                                            'company_store_id'        =>  $outputParam['original_store']->id,
                                                            'company_id'                =>  $outputParam['original_company']->id,
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['store_phone_number'],
                                                            'fax_number'                =>  $outputParam['store_fax_number'],
                                                            'email'                     =>  $outputParam['store_email'],
                                                            ];
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['company_name']                   = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;
        }
        return $outputParam;
    }
}
