<?php

namespace App\DataObjects\LicenseApplication\Output\ChangeApplication\Retail;

use DB;

use App\DataObjects\LicenseApplicationDataObject;

use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;

use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;

use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

class LicenseApplicationOutputDataObject
{
    public static function newOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['original_company']              = CompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = CompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = CompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
        }

        $outputParam['include_license'] = [
                //'license_application_id'                    =>  $outputParam['license_application_id'],
                'license_application_type_id'               =>  $outputParam['license_application_type_id'],
                'status_id'                                 =>  $outputParam['include_license_status_id'],
                'license_type_id'                           =>  $outputParam['license_type_id'],
            ];

        return $outputParam;
    }

    public static function draftOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;
        
        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['company_name']                   = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['company_name']                   = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;
        }


        return $outputParam;
    }
}
