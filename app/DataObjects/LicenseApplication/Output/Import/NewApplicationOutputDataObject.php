<?php

namespace App\DataObjects\LicenseApplication\Output\Import;

use DB;

use App\DataObjects\LicenseApplicationDataObject;

use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;

use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\SettingDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\Admin\UserDataObject;

use App\Transformers\LicenseApplication\CompanyTransformer;
use App\Transformers\LicenseApplication\CompanyAddressTransformer;
use App\Transformers\LicenseApplication\CompanyPremiseTransformer;
use App\Transformers\LicenseApplication\CompanyPartnerTransformer;
use App\Transformers\LicenseApplication\CompanyStoreTransformer;


use App\DataObjects\Admin\LicenseApplicationCategoryDataObject;
use App\DataObjects\Admin\GenerationTypeDataObject;
use App\Models\Admin\LicenseApplicationCategory;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;
use LicenseApplicationMaterialFacade;


class NewApplicationOutputDataObject
{
    public static function submitApplication($param)
    {
        $outputParam = $param;
        $outputParam['premise_id']                      =   (isset($outputParam['premise_id']))? decrypt($outputParam['premise_id']) : null;
        $outputParam['company_id']                      =   (isset($outputParam['company_id']))? decrypt($outputParam['company_id']) : null;
        $outputParam['store_id']                        =   (isset($outputParam['store_id']))? decrypt($outputParam['store_id']) : null;
        $outputParam['applicant_id']                    =   (isset($outputParam['user_id']))? decrypt($outputParam['user_id']) : null;
        $outputParam['license_application_license_generation_type_id']                    =   (isset($outputParam['license_application_license_generation_type_id']))? decrypt($outputParam['license_application_license_generation_type_id']) : null;
        $materialLiceenseType                           =   (isset($outputParam['license_type']))? decrypt($outputParam['license_type']) : null;
        $licenseApplicationType                         =   (isset($outputParam['license_application_type']))? decrypt($outputParam['license_application_type']) : null;
        $LicenseApplicationCategory                     =   LicenseApplicationCategoryDataObject::findLicenseApplicationCategoryByCode(LicenseApplicationCategory::INDIVIDUAL);
        
        
        $outputParam['material_domain']                 =   LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($outputParam['company_id'], $materialLiceenseType, $licenseApplicationType);
        $setting                                        =   SettingDataObject::findSettingByDomainAndCode('PENETAPAN_LESEN_IMPORT','PERMOHONAN_TAMAT_TEMPOH');
        $duration                                       =   DurationDataObject::findDurationById($setting->value);
        $licenseType                                    =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::IMPORT);
        $outputParam['original_company']                =   OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
        $outputParam['original_premise']                =   OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
        $outputParam['original_partners']               =   OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
        $outputParam['original_store']                  =   OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
        $outputParam['branch']                          =   AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;  
        $outputParam['license_application_type_id']     =   LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
        $outputParam['license_type_id']                 =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::IMPORT)->id;
        $outputParam['status_id']                       =   StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_IMPORT_BAHARU')->id;
        $outputParam['include_license_status_id']       =   StatusDataObject::findStatusByCode('KUMPULAN_LESEN_SEDANG_PROSES')->id;
        $outputParam['moa_staff_id']                    =   UserDataObject::findUserWithLowestLicenseApplicationTasksBasedOnPermission('Permohonan Baharu Lesen Import (Cawangan)', $outputParam['branch']->id)->id;

        $outputParam['apply_duration']                  =   $duration->name;
        $outputParam['duration_id']                     =   $duration->value;
        $outputParam['license_type_id']                 =   $licenseType->id;
        $outputParam['license_type_code']               =   $licenseType->code;
        $outputParam['company_name']                    =   $outputParam['original_company']->name;
        $outputParam['original_address']                =   $outputParam['original_company']->address;
        $outputParam['original_company']['user_id']     =   $outputParam['applicant_id'];
        
        $outputParam['original_company']                =   CompanyTransformer::transfromReferenceDataCompany($outputParam['original_company']);
        $outputParam['original_address']                =   CompanyAddressTransformer::transfromReferenceDataCompanyAddress($outputParam['original_address']);
        $outputParam['original_premise']                =   CompanyPremiseTransformer::transfromReferenceDataCompanyPremise($outputParam['original_premise']);
        $outputParam['original_partners']               =   CompanyPartnerTransformer::transfromReferenceDataCompanyPartner($outputParam['original_partners']);
        $outputParam['original_store']                  =   CompanyStoreTransformer::transfromReferenceDataCompanyStore($outputParam['original_store']);
    
        $outputParam['license_application'] = [
                'branch_id'                     =>  $outputParam['branch']['id'],
                'status_id'                     =>  $outputParam['status_id'],
                'license_application_type_id'   =>  $outputParam['license_application_type_id'],
                'license_type_id'               =>  $outputParam['license_type_id'],
                'applicant_id'                  =>  $outputParam['applicant_id'],
                'user_id'                       =>  $outputParam['moa_staff_id'],
                'material_domain'               =>  $outputParam['material_domain'],
                'company_name'                  =>  $outputParam['company_name'],
                'apply_duration'                =>  $outputParam['apply_duration'],
                'apply_load'                    =>  $outputParam['apply_load'],
                'license_application_license_generation_type_id'    =>  $outputParam['license_application_license_generation_type_id'],
        ];

        $outputParam['include_license'] = [
                //'license_application_id'                    =>  $outputParam['license_application_id'],
                'license_application_type_id'               =>  $outputParam['license_application_type_id'],
                'status_id'                                 =>  $outputParam['include_license_status_id'],
                'license_type_id'                           =>  $outputParam['license_type_id'],
        ];

        return $outputParam;
    }

    public static function draftApplication($param)
    {
        $outputParam = $param;
        $outputParam['premise_id']                      =   (isset($outputParam['premise_id']))? decrypt($outputParam['premise_id']) : null;
        $outputParam['company_id']                      =   (isset($outputParam['company_id']))? decrypt($outputParam['company_id']) : null;
        $outputParam['store_id']                        =   (isset($outputParam['store_id']))? decrypt($outputParam['store_id']) : null;
        $outputParam['applicant_id']                    =   (isset($outputParam['user_id']))? decrypt($outputParam['user_id']) : null;
        $outputParam['license_application_license_generation_type_id']                    =   (isset($outputParam['license_application_license_generation_type_id']))? decrypt($outputParam['license_application_license_generation_type_id']) : null;
        
        $materialLiceenseType                           =   (isset($outputParam['license_type']))? decrypt($outputParam['license_type']) : null;
        $licenseApplicationType                         =   (isset($outputParam['license_application_type']))? decrypt($outputParam['license_application_type']) : null;
        
        $setting                                        =   SettingDataObject::findSettingByDomainAndCode('PENETAPAN_LESEN_IMPORT','PERMOHONAN_TAMAT_TEMPOH');
        $duration                                       =   DurationDataObject::findDurationById($setting->value);
        $outputParam['material_domain']                 =   LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($outputParam['company_id'], $materialLiceenseType, $licenseApplicationType);
        $outputParam['company_name']                    =   (isset($outputParam['company_id']))?OriginalCompanyDataObject::findCompanyById($outputParam['company_id'])->name : null;
        $outputParam['status_id']                       =   StatusDataObject::findStatusByCode('DRAF_IMPORT_BAHARU')->id;
        $outputParam['license_application_type_id']     =   LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
        $outputParam['license_type_id']                 =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::IMPORT)->id;
        $outputParam['duration_id']                     =   $duration->value;
        $outputParam['apply_duration']                  =   $duration->name;

        $outputParam['license_application'] = [
                'status_id'                             =>  $outputParam['status_id'],
                'applicant_id'                          =>  $outputParam['applicant_id'],
                'material_domain'                       =>  $outputParam['material_domain'],
                'company_name'                          =>  $outputParam['company_name'],
                'license_application_type_id'           =>  $outputParam['license_application_type_id'],
                'license_type_id'                       =>  $outputParam['license_type_id'],
                'apply_duration'                        =>  $outputParam['apply_duration'],
                'apply_load'                            =>  $outputParam['apply_load'],
                'branch_id'                             =>  null,
                'reference_number'                      =>  null,
                'license_application_license_generation_type_id'    =>  $outputParam['license_application_license_generation_type_id'],
        ];

        return $outputParam;
    }
}
