<?php

namespace App\DataObjects\LicenseApplication\Output;

use DB;

use App\DataObjects\LicenseApplicationDataObject;

use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;

use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;


use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;

class ImportOutputDataObject
{
    public static function newOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['premise_business_type_id']        = (isset($outputParam['premise_business_type_id']))?decrypt($param['premise_business_type_id']):null;
            $outputParam['premise_building_type_id']        = (isset($outputParam['premise_building_type_id']))?decrypt($param['premise_building_type_id']):null;
            $outputParam['premise_dun_id']                  = (isset($outputParam['premise_dun_id']))?decrypt($param['premise_dun_id']):null;
            $outputParam['premise_parliament_id']           = (isset($outputParam['premise_parliament_id']))?decrypt($param['premise_parliament_id']):null;
            $outputParam['premise_district_id']             = (isset($outputParam['premise_district_id']))?decrypt($param['premise_district_id']):null;
            $outputParam['premise_state_id']                = (isset($outputParam['premise_state_id']))?decrypt($param['premise_state_id']):null;

            $outputParam['store_store_ownership_type_id']        = (isset($outputParam['store_store_ownership_type_id']))?decrypt($param['store_store_ownership_type_id']):null;
            $outputParam['store_building_type_id']        = (isset($outputParam['store_building_type_id']))?decrypt($param['store_building_type_id']):null;
            $outputParam['store_dun_id']                  = (isset($outputParam['store_dun_id']))?decrypt($param['store_dun_id']):null;
            $outputParam['store_parliament_id']           = (isset($outputParam['store_parliament_id']))?decrypt($param['store_parliament_id']):null;
            $outputParam['store_district_id']             = (isset($outputParam['store_district_id']))?decrypt($param['store_district_id']):null;
            $outputParam['store_state_id']                = (isset($outputParam['store_state_id']))?decrypt($param['store_state_id']):null;

            $outputParam['original_company']                = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']                = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']               = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                  = OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                    = $outputParam['original_company']->name;
            $outputParam['original_address']                = $outputParam['original_company']->address;
            $outputParam['branch']                          = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_store']->district_id)->branch;
            if(isset($outputParam['has_previous_license']) && $outputParam['has_previous_license'] == 'EXIST'){
                $outputParam['updated_premise']              = [
                                                            'business_type_id'          =>  $outputParam['premise_business_type_id'],
                                                            'business_type_name'        =>  (isset($outputParam['premise_business_type_id']))?BusinessTypeDataObject::findBusinessTypeById($outputParam['premise_business_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['premise_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['premise_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['premise_building_type_id'])->name:null,
                                                            'dun_id'                    =>  $outputParam['premise_dun_id'],
                                                            'dun_name'                  =>  (isset($outputParam['premise_dun_id']))?DunDataObject::findDunById($outputParam['premise_dun_id'])->name:null,
                                                            'company_id'                =>  $outputParam['original_company'],
                                                            'parliament_id'             =>  $outputParam['premise_parliament_id'],
                                                            'district_id'               =>  $outputParam['premise_district_id'],
                                                            'district_name'             =>   (isset($outputParam['premise_district_id']))?DistrictDataObject::findDistrictById($outputParam['premise_district_id'])->name:null,
                                                            'parliament_name'           =>   (isset($outputParam['premise_parliament_id']))?ParliamentDataObject::findParliamentById($outputParam['premise_parliament_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['premise_address_1'],
                                                            'address_2'                 =>  $outputParam['premise_address_2'],
                                                            'address_3'                 =>  $outputParam['premise_address_3'],
                                                            'postcode'                  =>  $outputParam['premise_postcode'],
                                                            'state_id'                  =>  $outputParam['premise_state_id'],
                                                            'state_name'                =>   (isset($outputParam['premise_state_id']))?StateDataObject::findStateById($outputParam['premise_state_id'])->name:null,
                                                            'company_premise_id'        =>  $outputParam['original_premise']->id,
                                                            'company_id'                =>  $outputParam['original_company']->id,
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['premise_phone_number'],
                                                            'fax_number'                =>  $outputParam['premise_fax_number'],
                                                            'email'                     =>  $outputParam['premise_email'],
                                                            ];
                $outputParam['updated_store']                 = [
                                                            'store_ownership_type_id'   =>  $outputParam['store_store_ownership_type_id'],
                                                            'store_ownership_type_name' =>  (isset($outputParam['store_store_ownership_type_id']))?StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($outputParam['store_store_ownership_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['store_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['store_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['store_building_type_id'])->name:null,
                                                            'company_id'                =>  $outputParam['original_company'],
                                                            'parliament_id'             =>  $outputParam['store_parliament_id'],
                                                            'district_id'               =>  $outputParam['store_district_id'],
                                                            'district_name'             =>   (isset($outputParam['store_district_id']))?DistrictDataObject::findDistrictById($outputParam['store_district_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['store_address_1'],
                                                            'address_2'                 =>  $outputParam['store_address_2'],
                                                            'address_3'                 =>  $outputParam['store_address_3'],
                                                            'postcode'                  =>  $outputParam['store_postcode'],
                                                            'state_id'                  =>  $outputParam['store_state_id'],
                                                            'state_name'                =>   (isset($outputParam['store_state_id']))?StateDataObject::findStateById($outputParam['store_state_id'])->name:null,
                                                            'company_store_id'        =>  $outputParam['original_store']->id,
                                                            'company_id'                =>  $outputParam['original_company']->id,
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['store_phone_number'],
                                                            'fax_number'                =>  $outputParam['store_fax_number'],
                                                            'email'                     =>  $outputParam['store_email'],
                                                            ];
            }
            else{
                $outputParam['updated_store']                 = [
                                                            'store_ownership_type_id'           =>  $outputParam['original_store']->store_ownership_type_id,
                                                            'store_ownership_type_name'         =>  (isset($outputParam['original_store']->store_ownership_type_id))?StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($outputParam['original_store']->store_ownership_type_id)->name:null,
                                                            'building_type_id'                  =>  $outputParam['original_store']->building_type_id,
                                                            'building_type_name'                =>  (isset($outputParam['original_store']->building_type_id))?BuildingTypeDataObject::findBuildingTypeById($outputParam['original_store']->building_type_id)->name:null,
                                                            'company_id'                        =>  $outputParam['original_store']->company_id,
                                                            //'parliament_id'                     =>  $outputParam['original_store']->parliament_id,
                                                            
                                                            'district_id'                       =>  $outputParam['original_store']->district_id,
                                                            'district_name'                     =>  (isset($outputParam['original_store']->district_id))?DistrictDataObject::findDistrictById($outputParam['original_store']->district_id)->name:null,
                                                            'address_1'                         =>  $outputParam['original_store']->address_1,
                                                            'address_2'                         =>  $outputParam['original_store']->address_2,
                                                            'address_3'                         =>  $outputParam['original_store']->address_3,
                                                            'postcode'                          =>  $outputParam['original_store']->postcode,
                                                            'state_id'                          =>  $outputParam['original_store']->state_id,
                                                            'state_name'                        =>  (isset($outputParam['original_store']->state_id))?StateDataObject::findStateById($outputParam['original_store']->state_id)->name:null,
                                                            'company_store_id'                  =>  $outputParam['store_id'],
                                                            //'license_application_id'          =>  $outputParam['original_store']->email,
                                                            'phone_number'                      =>  $outputParam['original_store']->phone_number,
                                                            'fax_number'                        =>  $outputParam['original_store']->fax_number,
                                                            'email'                             =>  $outputParam['original_store']->email,
                ];
                $outputParam['updated_premise']            = [
                                                            'business_type_id'          =>  $outputParam['original_premise']->business_type_id,
                                                            'business_type_name'        =>  (isset($outputParam['original_premise']->business_type_id))?BusinessTypeDataObject::findBusinessTypeById($outputParam['original_premise']->business_type_id)->name:null,
                                                            'building_type_id'          =>  $outputParam['original_premise']->building_type_id,
                                                            'building_type_name'        =>  (isset($outputParam['original_premise']->building_type_id))?BuildingTypeDataObject::findBuildingTypeById($outputParam['original_premise']->building_type_id)->name:null,
                                                            'dun_id'                    =>  $outputParam['original_premise']->dun_id,
                                                            'dun_name'                  =>  (isset($outputParam['original_premise']->dun_id))?DunDataObject::findDunById($outputParam['original_premise']->dun_id)->name:null,
                                                            'company_id'                =>  $outputParam['original_premise']->company_id,
                                                            'parliament_id'             =>  $outputParam['original_premise']->parliament_id,
                                                            'district_id'               =>  $outputParam['original_premise']->district_id,
                                                            'district_name'             =>   (isset($outputParam['original_premise']->district_id))?DistrictDataObject::findDistrictById($outputParam['original_premise']->district_id)->name:null,
                                                            'parliament_name'           =>   (isset($outputParam['original_premise']->parliament_id))?ParliamentDataObject::findParliamentById($outputParam['original_premise']->parliament_id)->name:null,
                                                            'address_1'                 =>  $outputParam['original_premise']->address_1,
                                                            'address_2'                 =>  $outputParam['original_premise']->address_2,
                                                            'address_3'                 =>  $outputParam['original_premise']->address_3,
                                                            'postcode'                  =>  $outputParam['original_premise']->postcode,
                                                            'state_id'                  =>  $outputParam['original_premise']->state_id,
                                                            'state_name'                =>   (isset($outputParam['original_premise']->state_id))?StateDataObject::findStateById($outputParam['original_premise']->state_id)->name:null,
                                                            'company_premise_id'        =>  $outputParam['premise_id'],
                                                            'phone_number'              =>  $outputParam['original_premise']->phone_number,
                                                            'fax_number'                =>  $outputParam['original_premise']->fax_number,
                                                            'email'                     =>  $outputParam['original_premise']->email,
                ];
            }
            $outputParam['updated_premise']['is_premise_validate']              = (isset($outputParam['is_premise_true']))?1:0;
            $outputParam['updated_store']['is_store_validate']              = (isset($outputParam['is_store_true']))?1:0;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['original_company']              = CompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = CompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = CompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
        }

        return $outputParam;
    }

    public static function draftOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;
        
        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {

            $outputParam['premise_business_type_id']        = (isset($outputParam['premise_business_type_id']))?decrypt($param['premise_business_type_id']):null;
            $outputParam['premise_building_type_id']        = (isset($outputParam['premise_building_type_id']))?decrypt($param['premise_building_type_id']):null;
            $outputParam['premise_dun_id']                  = (isset($outputParam['premise_dun_id']))?decrypt($param['premise_dun_id']):null;
            $outputParam['premise_parliament_id']           = (isset($outputParam['premise_parliament_id']))?decrypt($param['premise_parliament_id']):null;
            $outputParam['premise_district_id']             = (isset($outputParam['premise_district_id']))?decrypt($param['premise_district_id']):null;
            $outputParam['premise_state_id']                = (isset($outputParam['premise_state_id']))?decrypt($param['premise_state_id']):null;
            
            $outputParam['store_store_ownership_type_id']        = (isset($outputParam['store_store_ownership_type_id']))?decrypt($param['store_store_ownership_type_id']):null;
            $outputParam['store_building_type_id']        = (isset($outputParam['store_building_type_id']))?decrypt($param['store_building_type_id']):null;
            $outputParam['store_dun_id']                  = (isset($outputParam['store_dun_id']))?decrypt($param['store_dun_id']):null;
            $outputParam['store_parliament_id']           = (isset($outputParam['store_parliament_id']))?decrypt($param['store_parliament_id']):null;
            $outputParam['store_district_id']             = (isset($outputParam['store_district_id']))?decrypt($param['store_district_id']):null;
            $outputParam['store_state_id']                = (isset($outputParam['store_state_id']))?decrypt($param['store_state_id']):null;

            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']                = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']               = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                  = OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                  = $outputParam['original_company']->name ?? null;
            $outputParam['original_address']              = $outputParam['original_company']->address ?? null;
            
            if(isset($outputParam['has_previous_license']) && $outputParam['has_previous_license'] == 'EXIST'){
                $outputParam['updated_premise']              = [
                                                            'business_type_id'          =>  $outputParam['premise_business_type_id'],
                                                            'business_type_name'        =>  (isset($outputParam['premise_business_type_id']))?BusinessTypeDataObject::findBusinessTypeById($outputParam['premise_business_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['premise_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['premise_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['premise_building_type_id'])->name:null,
                                                            'dun_id'                    =>  $outputParam['premise_dun_id'],
                                                            'dun_name'                  =>  (isset($outputParam['premise_dun_id']))?DunDataObject::findDunById($outputParam['premise_dun_id'])->name:null,
                                                            'company_id'                =>  $outputParam['original_company'],
                                                            'parliament_id'             =>  $outputParam['premise_parliament_id'],
                                                            'district_id'               =>  $outputParam['premise_district_id'],
                                                            'district_name'             =>   (isset($outputParam['premise_district_id']))?DistrictDataObject::findDistrictById($outputParam['premise_district_id'])->name:null,
                                                            'parliament_name'           =>   (isset($outputParam['premise_parliament_id']))?ParliamentDataObject::findParliamentById($outputParam['premise_parliament_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['premise_address_1'],
                                                            'address_2'                 =>  $outputParam['premise_address_2'],
                                                            'address_3'                 =>  $outputParam['premise_address_3'],
                                                            'postcode'                  =>  $outputParam['premise_postcode'],
                                                            'state_id'                  =>  $outputParam['premise_state_id'],
                                                            'state_name'                =>   (isset($outputParam['premise_state_id']))?StateDataObject::findStateById($outputParam['premise_state_id'])->name:null,
                                                            'company_premise_id'        =>  $outputParam['original_premise']->id,
                                                            'company_id'                =>  $outputParam['original_company']->id,
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['premise_phone_number'],
                                                            'fax_number'                =>  $outputParam['premise_fax_number'],
                                                            'email'                     =>  $outputParam['premise_email'],
                                                            ];
                $outputParam['updated_store']                 = [
                                                            'store_ownership_type_id'   =>  $outputParam['store_store_ownership_type_id'],
                                                            'store_ownership_type_name' =>  (isset($outputParam['store_store_ownership_type_id']))?StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($outputParam['store_store_ownership_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['store_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['store_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['store_building_type_id'])->name:null,
                                                            'company_id'                =>  $outputParam['original_company'],
                                                            'parliament_id'             =>  $outputParam['store_parliament_id'],
                                                            'district_id'               =>  $outputParam['store_district_id'],
                                                            'district_name'             =>   (isset($outputParam['store_district_id']))?DistrictDataObject::findDistrictById($outputParam['store_district_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['store_address_1'],
                                                            'address_2'                 =>  $outputParam['store_address_2'],
                                                            'address_3'                 =>  $outputParam['store_address_3'],
                                                            'postcode'                  =>  $outputParam['store_postcode'],
                                                            'state_id'                  =>  $outputParam['store_state_id'],
                                                            'state_name'                =>   (isset($outputParam['store_state_id']))?StateDataObject::findStateById($outputParam['store_state_id'])->name:null,
                                                            'company_store_id'        =>  $outputParam['original_store']->id,
                                                            'company_id'                =>  $outputParam['original_company']->id,
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['store_phone_number'],
                                                            'fax_number'                =>  $outputParam['store_fax_number'],
                                                            'email'                     =>  $outputParam['store_email'],
                                                            ];
            }
            else{
                if(isset($outputParam['original_store'])){
                    $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_store']->district_id)->branch;
                    $outputParam['updated_store']                 = [
                                                            'store_ownership_type_id'           =>  $outputParam['original_store']->store_ownership_type_id,
                                                            'store_ownership_type_name'         =>  (isset($outputParam['original_store']->store_ownership_type_id))?StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($outputParam['original_store']->store_ownership_type_id)->name:null,
                                                            'building_type_id'                  =>  $outputParam['original_store']->building_type_id,
                                                            'building_type_name'                =>  (isset($outputParam['original_store']->building_type_id))?BuildingTypeDataObject::findBuildingTypeById($outputParam['original_store']->building_type_id)->name:null,
                                                            'company_id'                        =>  $outputParam['original_store']->company_id,
                                                            //'parliament_id'                     =>  $outputParam['original_store']->parliament_id,
                                                            
                                                            'district_id'                       =>  $outputParam['original_store']->district_id,
                                                            'district_name'                     =>  (isset($outputParam['original_store']->district_id))?DistrictDataObject::findDistrictById($outputParam['original_store']->district_id)->name:null,
                                                            'address_1'                         =>  $outputParam['original_store']->address_1,
                                                            'address_2'                         =>  $outputParam['original_store']->address_2,
                                                            'address_3'                         =>  $outputParam['original_store']->address_3,
                                                            'postcode'                          =>  $outputParam['original_store']->postcode,
                                                            'state_id'                          =>  $outputParam['original_store']->state_id,
                                                            'state_name'                        =>  (isset($outputParam['original_store']->state_id))?StateDataObject::findStateById($outputParam['original_store']->state_id)->name:null,
                                                            'company_store_id'                  =>  $outputParam['store_id'],
                                                            //'license_application_id'          =>  $outputParam['original_store']->email,
                                                            'phone_number'                      =>  $outputParam['original_store']->phone_number,
                                                            'fax_number'                        =>  $outputParam['original_store']->fax_number,
                                                            'email'                             =>  $outputParam['original_store']->email,
                ];
                }
                else{
                    $outputParam['branch']                  =   null;
                    $outputParam['updated_store']            = [];
                }
                

                if(isset($outputParam['original_premise'])){
                    $outputParam['updated_premise']            = [
                                                            'business_type_id'          =>  $outputParam['original_premise']->business_type_id,
                                                            'business_type_name'        =>  (isset($outputParam['original_premise']->business_type_id))?BusinessTypeDataObject::findBusinessTypeById($outputParam['original_premise']->business_type_id)->name:null,
                                                            'building_type_id'          =>  $outputParam['original_premise']->building_type_id,
                                                            'building_type_name'        =>  (isset($outputParam['original_premise']->building_type_id))?BuildingTypeDataObject::findBuildingTypeById($outputParam['original_premise']->building_type_id)->name:null,
                                                            'dun_id'                    =>  $outputParam['original_premise']->dun_id,
                                                            'dun_name'                  =>  (isset($outputParam['original_premise']->dun_id))?DunDataObject::findDunById($outputParam['original_premise']->dun_id)->name:null,
                                                            'company_id'                =>  $outputParam['original_premise']->company_id,
                                                            'parliament_id'             =>  $outputParam['original_premise']->parliament_id,
                                                            'district_id'               =>  $outputParam['original_premise']->district_id,
                                                            'district_name'             =>   (isset($outputParam['original_premise']->district_id))?DistrictDataObject::findDistrictById($outputParam['original_premise']->district_id)->name:null,
                                                            'parliament_name'           =>   (isset($outputParam['original_premise']->parliament_id))?ParliamentDataObject::findParliamentById($outputParam['original_premise']->parliament_id)->name:null,
                                                            'address_1'                 =>  $outputParam['original_premise']->address_1,
                                                            'address_2'                 =>  $outputParam['original_premise']->address_2,
                                                            'address_3'                 =>  $outputParam['original_premise']->address_3,
                                                            'postcode'                  =>  $outputParam['original_premise']->postcode,
                                                            'state_id'                  =>  $outputParam['original_premise']->state_id,
                                                            'state_name'                =>   (isset($outputParam['original_premise']->state_id))?StateDataObject::findStateById($outputParam['original_premise']->state_id)->name:null,
                                                            'company_premise_id'        =>  $outputParam['premise_id'],
                                                            'phone_number'              =>  $outputParam['original_premise']->phone_number,
                                                            'fax_number'                =>  $outputParam['original_premise']->fax_number,            
                                                            'email'                     =>  $outputParam['original_premise']->email,
                    ];
                }
                else{
                    $outputParam['updated_premise']            = [];
                }
            }
            $outputParam['updated_premise']['is_premise_validate']          = (isset($outputParam['is_premise_true']))?1:0;
            $outputParam['updated_store']['is_store_validate']              = (isset($outputParam['is_store_true']))?1:0;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['company_name']                   = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_premise']->district_id)->branch;
        }
        return $outputParam;
    }
}
