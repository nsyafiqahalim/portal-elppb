<?php

namespace App\DataObjects\LicenseApplication\Output;

use DB;

use App\DataObjects\LicenseApplicationDataObject;

use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;

use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;

use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;

class BuyPaddyOutputDataObject
{
    public static function newOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                = OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
            $outputParam['branch']                        = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['original_store']->district_id)->branch;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['original_company']              = CompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = CompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = CompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                = CompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
        }

        return $outputParam;
    }

    public static function draftOutputParameter($param, $licenseApplicationDataSource)
    {
        $outputParam = $param;

        if ($licenseApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['company_name'] = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
        } elseif ($licenseApplicationDataSource == 'LICENSE_APPLICATION_DATA') {
            $outputParam['company_name'] = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
        }
        return $outputParam;
    }
}
