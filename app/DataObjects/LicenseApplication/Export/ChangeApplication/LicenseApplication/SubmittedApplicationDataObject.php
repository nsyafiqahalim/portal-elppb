<?php

namespace App\DataObjects\LicenseApplication\Export\ChangeApplication\LicenseApplication;

use DB;
use Carbon\Carbon;

use App\Models\LicenseApplication\Temporary;
use App\Models\LicenseApplication\Attachment;
 
use App\Models\LicenseApplication\Cancellation;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\ChangeDataObject;
use App\DataObjects\LicenseApplication\SerialNumberDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseItemDataObject;

class SubmittedApplicationDataObject
{
    public static function store($param)
    {
        $serialNumber = SerialNumberDataObject::generateReferenceNumber($param['branch']->short_code,$param['license_type_code']);
        
        $licenseApplicationparam = [
                'reference_number'              =>  $serialNumber['reference_number'],
                'duration_id'                     =>  $param['duration_id'],
                'user_id'                     =>  $param['user_id'],
                'branch_id'                     =>  $param['branch']->id,
                'status_id'                     =>  $param['status_id'],
                'duration_id'                       =>  $param['duration_id'],
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'applicant_id'                       =>  $param['applicant_id'],
                'company_name'                  =>  $param['company_name'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
            ];
        $licenseApplication = LicenseApplicationDataObject::createNewLicenseApplication($licenseApplicationparam);
        SerialNumberDataObject::updateSerialNumber($serialNumber['object']->id, $serialNumber['object']->counter+1);
        
        $companyAddressParam                            = $param['original_address']->toArray();
        $companyAddressParam['state_name']                   = $param['original_address']->state->name;
        $companyAddressParam['license_application_id']  = $licenseApplication->id;
        $companyAddressParam['company_id']              = $param['original_company']->id;
        unset($param['original_address']['id']);

        $companyDAOParam                                = $param['original_company']->toArray();
        $companyDAOParam['company_type_name']                = $param['original_company']->companyType->name;
        $companyDAOParam['company_id']                  = $param['original_company']->id;
        $companyDAOParam['license_application_id']      = $licenseApplication->id;
        unset($companyDAOParam['id']);

        $companyPremiseDAOParam                              = $param['original_premise']->toArray();
        $companyPremiseDAOParam['business_type_name']        = $param['original_premise']->businessType->name;
        $companyPremiseDAOParam['building_type_name']        = $param['original_premise']->buildingType->name;
        $companyPremiseDAOParam['state_name']                = $param['original_premise']->state->name;
        $companyPremiseDAOParam['parliament_name']           = $param['original_premise']->parliament->name;
        $companyPremiseDAOParam['dun_name']                  = $param['original_premise']->dun->name;
        $companyPremiseDAOParam['district_name'] = $param['original_premise']->district->name;
        $companyPremiseDAOParam['company_premise_id']       = $companyPremiseDAOParam['id'];

        $companyPremiseDAOParam['license_application_id'] = $licenseApplication->id;
        unset($companyPremiseDAOParam['id']);

        $company = CompanyDataObject::createNewCompany($companyDAOParam);
        $addess = CompanyAddressDataObject::createNewCompanyAddress($companyAddressParam);
        $premise = CompanyPremiseDataObject::addNewCompanyPremise($companyPremiseDAOParam);

        foreach ($param['original_partners'] as $item) {
            $companyPartnerDAOParam = $item->toArray();
            //$companyPartnerDAOParam['ownership_type'] = $item->ownershipType->name;
             $companyPartnerDAOParam['race_name'] = (isset($item->race))?$item->race->name:null;
            $companyPartnerDAOParam['race_type_name'] = (isset($item->raceType))?$item->raceType->name:null;
            $companyPartnerDAOParam['citizen_name'] = ($item->is_citizen  == 1)? 'Warganegara' : 'Bukan Warganegara';
            $companyPartnerDAOParam['state_name'] = $item->state->name;

            $companyPartnerDAOParam['company_id'] = $param['original_company']->id;
            $companyPartnerDAOParam['license_application_id'] = $licenseApplication->id;
            $companyPartnerDAOParam['company_partner_id'] = $companyPartnerDAOParam['id'];
            unset($companyPartnerDAOParam['id']);

            CompanyPartnerDataObject::addNewCompanyPartner($companyPartnerDAOParam);
        }

        $param['license_application_id']    =   $licenseApplication->id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


        $changeParam = [
                'license_application_change_type_id'                    =>  $param['license_application_change_type']->id,
                'license_application_company_id'                        =>  $param['original_company']->id,
                'license_application_company_store_id'                  =>  null,
                'license_application_company_premise_id'                =>  $param['original_premise']->id,
                'original_load'                                         =>  null,
                'license_application_id'                                =>  $licenseApplication->id,
        ];

        ChangeDataObject::UpdateOrCreateChange($changeParam);

        $param['include_license']['license_application_id']                         =   $licenseApplication->id;
        $includeLicense                                                             =   IncludeLicenseDataObject::addIncludeLicense($param['include_license']);
        $param['include_license_item']  =   [
            'license_application_id'                   =>   $licenseApplication->id,
            'status_id'                                =>   $param['include_license_item_status_id'],
            'license_application_include_license_id'   =>   $includeLicense->id
        ];
        
        IncludeLicenseItemDataObject::copyAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($param['include_license_item'],$param['include_license_id']);
    }
    public static function update($param, $id)
    {
        $serialNumber = SerialNumberDataObject::generateReferenceNumber($param['branch']->short_code,$param['license_type_code']);
        
        $licenseApplicationparam = [
                'reference_number'              =>  $serialNumber['reference_number'],
                'duration_id'                     =>  $param['duration_id'],
                'user_id'                     =>  $param['user_id'],
                'branch_id'                     =>  $param['branch']->id,
                'status_id'                 =>  $param['status_id'],
                'duration_id'                       =>  $param['duration_id'],
                'license_application_type_id'   =>  $param['license_application_type_id'],
                'license_type_id'               =>  $param['license_type_id'],
                'applicant_id'                       =>  $param['applicant_id'],
                'company_name'                  =>  $param['company_name'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
            ];
        
        $licenseApplication = LicenseApplicationDataObject::updateLicenseApplication($licenseApplicationparam, $id);
        SerialNumberDataObject::updateSerialNumber($serialNumber['object']->id, $serialNumber['object']->counter+1);
        Temporary::where('license_application_id', $id)->first()->delete();

        $companyAddressParam = $param['original_address']->toArray();
        $companyAddressParam['state_name'] = $param['original_address']->state->name;
        $companyAddressParam['license_application_id'] = $id;
        $companyAddressParam['company_id'] = $param['original_company']->id;
        unset($param['original_address']['id']);

        $companyDAOParam = $param['original_company']->toArray();
        $companyDAOParam['company_type_name'] = $param['original_company']->companyType->name;
        $companyDAOParam['company_id'] = $param['original_company']->id;
        $companyDAOParam['license_application_id'] = $id;
        unset($companyDAOParam['id']);

        $companyPremiseDAOParam = $param['original_premise']->toArray();
        $companyPremiseDAOParam['business_type_name'] = $param['original_premise']->businessType->name;
        $companyPremiseDAOParam['building_type_name'] = $param['original_premise']->buildingType->name;
        $companyPremiseDAOParam['district_name'] = $param['original_premise']->district->name;
        $companyPremiseDAOParam['state_name'] = $param['original_premise']->state->name;
        $companyPremiseDAOParam['parliament_name'] = $param['original_premise']->parliament->name;
        $companyPremiseDAOParam['dun_name'] = $param['original_premise']->dun->name;
        $companyPremiseDAOParam['company_id'] = $param['original_company']->id;
        $companyPremiseDAOParam['company_premise_id'] =$companyPremiseDAOParam['id'];

        $companyPremiseDAOParam['license_application_id'] = $id;
        unset($companyPremiseDAOParam['id']);

        $company = CompanyDataObject::createNewCompany($companyDAOParam);
        $addess = CompanyAddressDataObject::createNewCompanyAddress($companyAddressParam);
        $premise = CompanyPremiseDataObject::addNewCompanyPremise($companyPremiseDAOParam);

        foreach ($param['original_partners'] as $item) {
            $companyPartnerDAOParam = $item->toArray();
            //$companyPartnerDAOParam['ownership_type'] = $item->ownershipType->name;
             $companyPartnerDAOParam['race_name'] = (isset($item->race))?$item->race->name:null;
            $companyPartnerDAOParam['race_type_name'] = (isset($item->raceType))?$item->raceType->name:null;
            $companyPartnerDAOParam['citizen_name'] = ($item->is_citizen  == 1)? 'Warganegara' : 'Bukan Warganegara';
            $companyPartnerDAOParam['state_name'] = $item->state->name;

            $companyPartnerDAOParam['company_id'] = $param['original_company']->id;
            $companyPartnerDAOParam['license_application_id'] = $id;
            $companyPartnerDAOParam['company_partner_id'] = $companyPartnerDAOParam['id'];
            unset($companyPartnerDAOParam['id']);

            CompanyPartnerDataObject::addNewCompanyPartner($companyPartnerDAOParam);
        }

       $param['license_application_id']    =   $id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


          

        $changeParam = [
                'license_application_change_type_id'                    =>  $param['license_application_change_type']->id,
                'license_application_company_id'                        =>  $param['original_company']->id,
                'license_application_company_store_id'                  =>  null,
                'license_application_company_premise_id'                =>  $param['original_premise']->id,
                'original_load'                                         =>  null,
                'license_application_id'                                =>  $id,
        ];

        ChangeDataObject::UpdateOrCreateChange($changeParam);
        
        $param['include_license']['license_application_id']                         =   $id;
        $includeLicense                                                             =   IncludeLicenseDataObject::addIncludeLicense($param['include_license']);
        $param['include_license_item']  =   [
            'license_application_id'                   =>   $id,
            'status_id'                                =>   $param['include_license_item_status_id'],
            'license_application_include_license_id'   =>   $includeLicense->id
        ];
        
        IncludeLicenseItemDataObject::copyAllIncludeLicenseItemFromPreviousIncludeLicenseIdByIncludeLiceseId($param['include_license_item'],$param['include_license_id']);
    }
}
