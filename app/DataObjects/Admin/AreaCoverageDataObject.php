<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\AreaCoverage;

class AreaCoverageDataObject
{
    public static function findAreaCoverageById($id)
    {
        return AreaCoverage::find($id);
    }

    public static function findAreaCoverageByDistrictId($districtId)
    {
        return AreaCoverage::where('district_id',$districtId)->first();
    }
}
