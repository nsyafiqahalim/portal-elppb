<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseType;

class LicenseTypeDataObject
{
    public static function findLicenseTypeByCode($code)
    {
        return LicenseType::where('code', $code)->first();
    }
    
    public static function findLicenseTypeIdByCode($code)
    {
        return LicenseType::select('id')->where('code', $code)->first()->id;
    }

    public static function findAllLicenseTypes()
    {
        return LicenseType::select('id')->where('code', $code)->first()->id;
    }
}
