<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\BuildingType;

class BuildingTypeDataObject
{
    public static function findAllActiveBuildingTypes()
    {
        $buildingTypes = BuildingType::activeOnly()->get();

        return $buildingTypes;
    }

    public static function findAllBuildingTypeUsingDatatableFormat()
    {
        return datatables()->of(BuildingType::query())
        ->addColumn('action', function ($buildingTypes) {
            return view('admin.company-type.partials.datatable-button', compact('buildingTypes'))->render();
        })
        ->addColumn('status', function ($buildingTypes) {
            return ($buildingTypes->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findBuildingTypeById($id)
    {
        return BuildingType::find($id);
    }
}
