<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseApplicationReplacementType;

class LicenseApplicationReplacementTypeDataObject
{
    public static function findLicenseApplicationReplacementTypeById($code)
    {
        return LicenseApplicationReplacementType::where('code', $code)->first();
    }

    public static function findAllActiveLicenseApplicationReplacementTypes()
    {
        $Duns = LicenseApplicationReplacementType::activeOnly()->get();

        return $Duns;
    }
}
