<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseApplicationType;

class LicenseApplicationTypeDataObject
{
    public static function findLicenseApplicationTypeByCode($code)
    {
        return LicenseApplicationType::where('code', $code)->first();
    }

    public static function findLicenseApplicationTypeIdByCode($code)
    {
        return LicenseApplicationType::select('id')->where('code', $code)->first()->id;
    }
}
