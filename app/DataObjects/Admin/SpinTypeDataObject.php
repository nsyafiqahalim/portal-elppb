<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\SpinType;

class SpinTypeDataObject
{
    public static function findAllActiveSpinTypes()
    {
        return SpinType::activeOnly()->get();
    }

    public static function findSpinTypeById($id)
    {
        return SpinType::find($id);
    }
}
