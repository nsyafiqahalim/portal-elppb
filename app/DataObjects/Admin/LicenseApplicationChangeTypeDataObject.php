<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseApplicationChangeType;

class LicenseApplicationChangeTypeDataObject
{
    public static function findLicenseApplicationChangeTypeByCode($code)
    {
        return LicenseApplicationChangeType::where('code', $code)->first();
    }

    public static function findAllActiveLicenseApplicationChangeTypes()
    {
        $Duns = LicenseApplicationChangeType::activeOnly()->get();

        return $Duns;
    }
}
