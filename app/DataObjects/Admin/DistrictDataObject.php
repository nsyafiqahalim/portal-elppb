<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\District;

class DistrictDataObject
{
    public static function findAllActiveDistricts()
    {
        $Districts = District::activeOnly()->get();

        return $Districts;
    }

    public static function findDistrictByStateId($stateId)
    {
        return District::activeOnly()->where('state_id', $stateId)->get();
    }

    public static function findAllDistrictUsingDatatableFormat()
    {
        return datatables()->of(District::query())
        ->addColumn('action', function ($Districts) {
            return view('admin.company-type.partials.datatable-button', compact('districts'))->render();
        })
        ->addColumn('status', function ($Districts) {
            return ($Districts->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findDistrictById($id)
    {
        return District::find($id);
    }
}
