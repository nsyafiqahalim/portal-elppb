<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseApplicationBuyPaddyCondition;

class LicenseApplicationBuyPaddyConditionDataObject
{
    public static function findLicenseApplicationBuyPaddyConditionById($code)
    {
        return LicenseApplicationBuyPaddyCondition::where('code', $code)->first();
    }

    public static function findActiveLicenseApplicationBuyPaddyConditionsOnly()
    {
       return LicenseApplicationBuyPaddyCondition::activeOnly()->get();
    }
}
