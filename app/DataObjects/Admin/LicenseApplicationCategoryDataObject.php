<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseApplicationCategory;

class LicenseApplicationCategoryDataObject
{
    public static function findLicenseApplicationCategoryByCode($code)
    {
        return LicenseApplicationCategory::where('code', $code)->first();
    }
}
