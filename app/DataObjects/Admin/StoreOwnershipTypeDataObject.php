<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\StoreOwnershipType;

class StoreOwnershipTypeDataObject
{
    public static function findAllActiveStoreOwnershipTypes()
    {
        $storeOwnershipTypes = StoreOwnershipType::activeOnly()->get();

        return $storeOwnershipTypes;
    }

    public static function findAllStoreOwnershipTypeUsingDatatableFormat()
    {
        return datatables()->of(StoreOwnershipType::query())
        ->addColumn('action', function ($storeOwnershipType) {
            return view('admin.company-type.partials.datatable-button', compact('storeOwnershipType'))->render();
        })
        ->addColumn('status', function ($storeOwnershipType) {
            return ($storeOwnershipType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findStoreOwnershipTypeById($id)
    {
        return StoreOwnershipType::find($id);
    }
}
