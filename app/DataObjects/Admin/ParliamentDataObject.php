<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\Parliament;

class ParliamentDataObject
{
    public static function findAllActiveParliaments()
    {
        $Parliaments = Parliament::activeOnly()->get();

        return $Parliaments;
    }

    public static function findAllParliamentUsingDatatableFormat()
    {
        return datatables()->of(Parliament::query())
        ->addColumn('action', function ($Parliaments) {
            return view('admin.company-type.partials.datatable-button', compact('Parliaments'))->render();
        })
        ->addColumn('status', function ($Parliaments) {
            return ($Parliaments->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findParliamentsByStateId($stateId)
    {
        return Parliament::activeOnly()->where('state_id', $stateId)->get();
    }
    
    public static function findParliamentById($id)
    {
        return Parliament::find($id);
    }
}
