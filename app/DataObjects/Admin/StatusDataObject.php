<?php
namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\Status;

class StatusDataObject
{
    public static function findAllActiveStatuss()
    {
        $Status = Status::activeOnly()->get();

        return $Status;
    }

    public static function findStatusById($id)
    {
        return Status::find($id);
    }

    public static function findStatusByCode($code)
    {
        return Status::where('code', $code)->first();
        ;
    }

    public static function findStatusIdByCode($code)
    {
        return Status::select('id')->where('code', $code)->first()->id;
        ;
    }
}
