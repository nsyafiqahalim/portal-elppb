<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\PremiseType;

class PremiseTypeDataObject
{
    public static function findAllActivePremiseTypes()
    {
        $premiseTypes = PremiseType::activeOnly()->get();

        return $premiseTypes;
    }

    public static function findAllPremiseTypeUsingDatatableFormat()
    {
        return datatables()->of(PremiseType::query())
        ->addColumn('action', function ($premiseType) {
            return view('admin.company-type.partials.datatable-button', compact('premiseType'))->render();
        })
        ->addColumn('status', function ($premiseType) {
            return ($premiseType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findPremiseTypeById($id)
    {
        return PremiseType::find($id);
    }
}
