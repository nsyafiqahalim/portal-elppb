<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\Dun;

class DunDataObject
{
    public static function findAllActiveDuns()
    {
        $Duns = Dun::activeOnly()->get();

        return $Duns;
    }

    public static function findAllDunUsingDatatableFormat()
    {
        return datatables()->of(Dun::query())
        ->addColumn('action', function ($Duns) {
            return view('admin.company-type.partials.datatable-button', compact('Duns'))->render();
        })
        ->addColumn('status', function ($Duns) {
            return ($Duns->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findDunsByParliamentId($parliamentId)
   {
       return Dun::activeOnly()->where('parliament_id', $parliamentId)->get();
   }

    public static function findDunById($id)
    {
        return Dun::find($id);
    }
}
