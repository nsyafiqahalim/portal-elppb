<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\ApprovalLetterType;

class ApprovalLetterTypeDataObject
{
    public static function findApprovalLetterTypeByCode($code)
    {
        return ApprovalLetterType::where('code', $code)->first();
    }
}
