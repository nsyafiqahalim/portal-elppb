<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\GenerationType;

class GenerationTypeDataObject
{
    public static function findAllGenerationTypes()
    {
        $generationType = GenerationType::activeOnly()->get();

        return $generationType;
    }

    public static function findGenerationTypeByDomainAndType($domain, $type)
    {
        return GenerationType::where('domain', $domain)->where('type', $type)->first();
    }

    public static function findAllGenerationTypesUsingDatatableFormat($param)
    {
        $generationTypes = GenerationType::distinct()->orderby('id', 'asc')
                        ->when(isset($param['domain']) && $param['domain'], function ($query) use ($param) {
                            $query->where('domain', 'like', $param['domain']);
                        })
                        ->when(isset($param['type']) && $param['type'], function ($query) use ($param) {
                            $query->where('type', $param['type']);
                        });
                        
        return datatables()->of($generationTypes)
        ->addIndexColumn()
        ->addColumn('action', function ($generationType) use ($param) {
            $decryptedID = (isset($param['license_application_license_generation_type_id']))? decrypt($param['license_application_license_generation_type_id']) : null;
            $checked = ($decryptedID == $generationType->id)? 'checked': null;

            if (isset($param['type'])) {
                $checked = 'checked';
            }

            $generatedLicense  = config('includelicense.'.$generationType->domain.'.'.$generationType->code);
            //$checked = isset($param['is_single_license_only']) ? 'checked' : $checked;9c89e4a
            return view('license-application.component.generation-type.partials.datatable-button', [
                'id'    =>  encrypt($generationType->id),
                'key'    =>  md5($generationType->id),
                'checked'   =>  $checked,
                'generatedLicense'  =>  json_encode($generatedLicense)
            ])->render();
        })
        ->addColumn('status', function ($generationType) {
            return ($generationType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findGenerationTypeById($id)
    {
        return GenerationType::find($id);
    }
}
