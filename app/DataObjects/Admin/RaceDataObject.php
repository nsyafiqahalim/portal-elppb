<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\Race;

class RaceDataObject
{
    public static function findAllActiveRaces()
    {
        $Races = Race::activeOnly()->get();

        return $Races;
    }

     public static function findRacesByRaceTypeId($raceTypeID)
  {
      $races = Race::activeOnly()->where('race_type_id', $raceTypeID)->get();
      return $races;
  }

    public static function findAllRaceUsingDatatableFormat()
    {
        return datatables()->of(Race::query())
        ->addColumn('action', function ($Race) {
            return view('admin.company-type.partials.datatable-button', compact('Race'))->render();
        })
        ->addColumn('status', function ($Race) {
            return ($Race->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findRaceById($id)
    {
        return Race::find($id);
    }
}
