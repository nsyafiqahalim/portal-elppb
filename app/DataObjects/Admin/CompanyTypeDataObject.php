<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\CompanyType;

class CompanyTypeDataObject
{
    public static function findAllActiveCompanyTypes()
    {
        $companyTypes = CompanyType::activeOnly()->get();

        return $companyTypes;
    }

    public static function findAllCompanyTypeUsingDatatableFormat()
    {
        return datatables()->of(CompanyType::query())
        ->addColumn('action', function ($companyType) {
            return view('admin.company-type.partials.datatable-button', compact('companyType'))->render();
        })
        ->addColumn('status', function ($companyType) {
            return ($companyType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findCompanyTypeById($id)
    {
        return CompanyType::find($id);
    }
}
