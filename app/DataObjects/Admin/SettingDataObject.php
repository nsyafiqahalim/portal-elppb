<?php

namespace App\DataObjects\Admin;

use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Support\Facades\Hash;

use App\User;

use Illuminate\Support\Facades\Cache;

use App\Models\Admin\Setting;

class SettingDataObject
{
    public static function findSettingByDomain($domain)
    {
        if (Cache::get($domain) !== null) {
            return Cache::get($domain);
        }
        //if not cache instances
        else {
            $settings = Cache::rememberForever($domain, function () use ($domain) {
                return  Setting::where('domain', $domain)->get();
            });
            return $settings;
        }
    }

    public static function findSettingByDomainAndCode($domain,$code)
    {
        if (Cache::get($domain.$code) !== null) {
            return Cache::get($domain.$code);
        }
        //if not cache instances
        else {
            $settings = Cache::rememberForever($domain.$code, function () use ($domain,$code) {
                return  Setting::where('domain', $domain)->where('code', $code)->first();
            });
            return $settings;
        }
    }

    public function updateSetting($request, $domain)
    {
        $settings = null;
        DB::transaction(function () use ($request,$domain, &$settings) {
            $settings  = Setting::where('domain', $domain)->where('configurable', 1)->get();
            foreach ($settings as $setting) {
                Setting::where('code', $setting->code)->update([
                    'value' => $request->input($setting->code)
                ]);
            }

            Cache::forget($domain);
            $settings = self::findSettingByDomain($domain);
        });

        return $settings;
    }
}
