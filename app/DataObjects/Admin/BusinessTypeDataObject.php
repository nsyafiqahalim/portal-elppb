<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\BusinessType;

class BusinessTypeDataObject
{
    public static function findAllActiveBusinessTypes()
    {
        $businessTypes = BusinessType::activeOnly()->get();

        return $businessTypes;
    }

    public static function findAllBusinessTypeUsingDatatableFormat()
    {
        return datatables()->of(BusinessType::query())
        ->addColumn('action', function ($busienssType) {
            return view('admin.company-type.partials.datatable-button', compact('busienssType'))->render();
        })
        ->addColumn('status', function ($busienssType) {
            return ($busienssType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findBusinessTypeById($id)
    {
        return BusinessType::find($id);
    }
}
