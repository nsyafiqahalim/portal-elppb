<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\RaceType;

class RaceTypeDataObject
{
    public static function findAllActiveRaceTypes()
    {
        $raceTypes = RaceType::activeOnly()->get();

        return $raceTypes;
    }

    public static function findAllRaceTypeUsingDatatableFormat()
    {
        return datatables()->of(RaceType::query())
        ->addColumn('action', function ($raceType) {
            return view('admin.company-type.partials.datatable-button', compact('raceType'))->render();
        })
        ->addColumn('status', function ($raceType) {
            return ($raceType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findRaceTypeById($id)
    {
        return Race::find($id);
    }
}
