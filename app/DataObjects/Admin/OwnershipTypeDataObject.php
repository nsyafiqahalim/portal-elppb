<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\OwnershipType;

class OwnershipTypeDataObject
{
    public static function findAllActiveOwnershipTypes()
    {
        $OwnershipTypes = OwnershipType::activeOnly()->get();

        return $OwnershipTypes;
    }

    public static function findAllOwnershipTypeUsingDatatableFormat()
    {
        return datatables()->of(OwnershipType::query())
        ->addColumn('action', function ($OwnershipType) {
            return view('admin.company-type.partials.datatable-button', compact('OwnershipType'))->render();
        })
        ->addColumn('status', function ($OwnershipType) {
            return ($OwnershipType->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findOwnershipTypeById($id)
    {
        return OwnershipType::find($id);
    }
}
