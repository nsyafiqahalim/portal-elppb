<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\RiceType;

class RiceTypeDataObject
{
    public static function findAllActiveRiceTypes()
    {
        return RiceType::activeOnly()->get();
    }

    public static function findRiceTypeById($id)
    {
        return RiceType::find($id);
    }
}
