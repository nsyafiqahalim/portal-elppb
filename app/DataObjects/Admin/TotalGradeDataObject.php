<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\TotalGrade;

class TotalGradeDataObject
{
    public static function findAllActiveTotalGrades()
    {
        return TotalGrade::activeOnly()->get();
    }

    public static function findTotalGradeById($id)
    {
        return TotalGrade::find($id);
    }
}
