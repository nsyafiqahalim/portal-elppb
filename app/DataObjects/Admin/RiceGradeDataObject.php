<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\RiceGrade;

class RiceGradeDataObject
{
    public static function findAllActiveRiceGrades()
    {
        return RiceGrade::activeOnly()->get();
    }

    public static function findRiceGradeById($id)
    {
        return RiceGrade::find($id);
    }
}
