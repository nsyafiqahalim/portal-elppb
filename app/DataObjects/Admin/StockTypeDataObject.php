<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\StockType;

class StockTypeDataObject
{
    public static function findAllActiveStockTypes()
    {
        return StockType::activeOnly()->get();
    }

    public static function findStockTypeById($id)
    {
        return StockType::find($id);
    }
}
