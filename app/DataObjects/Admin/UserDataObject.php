<?php

namespace App\DataObjects\Admin;

use DB;

use App\User;

class UserDataObject
{
    public static function findUserBasedOnPermission($permission, $branchid)
    {
        $states = User::permission($permission)
        ->whereHas('profile', function($profile) use($branchid){
            $profile->where('branch_id',$branchid);
        })->first();

        return $states;
    }
    
    public static function findUserWithLowestLicenseApplicationTasksBasedOnPermission($permission, $branchid)
    {
        $states = User::permission($permission)
        ->whereHas('profile', function($profile) use($branchid){
            $profile->where('branch_id',$branchid);
        })
        ->withCount('moaLicenseApplications')
        ->orderby('moa_license_applications_count','asc')->first();

        return $states;
    }

    public static function findUserById($id){
        return User::find($id);
    }
}
