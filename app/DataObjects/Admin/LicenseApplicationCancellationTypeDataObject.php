<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\LicenseApplicationCancellationType;

class LicenseApplicationCancellationTypeDataObject
{
    public static function findLicenseApplicationCancellationTypeById($code)
    {
        return LicenseApplicationCancellationType::where('code', $code)->first();
    }

    public static function findAllActiveLicenseApplicationCancellationTypes()
    {
        $Duns = LicenseApplicationCancellationType::activeOnly()->get();

        return $Duns;
    }
}
