<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\State;

class StateDataObject
{
    public static function findAllActiveStates()
    {
        $states = State::activeOnly()->get();

        return $states;
    }

    public static function findStateById($id)
    {
        return State::find($id);
    }
}
