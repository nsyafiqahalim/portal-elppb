<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\ApprovalLetterApplicationType;

class ApprovalLetterApplicationTypeDataObject
{
    public static function findApprovalLetterApplicationTypeByCode($code)
    {
        return ApprovalLetterApplicationType::where('code', $code)->first();
    }
}
