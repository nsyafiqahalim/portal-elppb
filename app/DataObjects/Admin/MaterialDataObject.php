<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\Material;
use App\DataObjects\LicenseApplication\AttachmentDataObject;

class MaterialDataObject
{
    public static function findAllMaterials()
    {
        $material = Material::activeOnly()->get();

        return $material;
    }

    public static function findMaterialByDomainAndType($domain, $type)
    {
        return Material::where('domain', $domain)->where('type', $type)->first();
    }

    public static function findAllMaterialsUsingDatatableFormat($param)
    {
        $materials = Material::distinct()->orderby('id', 'asc')
                        ->when(isset($param['domain']) && $param['domain'], function ($query) use ($param) {
                            $query->where('domain', 'like', $param['domain']);
                        })
                        ->when(isset($param['type']) && $param['type'], function ($query) use ($param) {
                            $query->where('type', $param['type']);
                        });
                        
        return datatables()->of($materials)
        ->addIndexColumn()
        ->addColumn('action', function ($material) use ($param) {
            //$checked = isset($param['is_single_license_only']) ? 'checked' : $checked;9c89e4a
            return view('license-application.component.material.partials.upload-file', [
                'code'  =>  $material->code
            ])->render();
        })
        ->addColumn('status', function ($material) use($param) {
            $status = 'NOT_UPLOADED';
            if(isset($param['license_application_id'])){
                
                $attachment = AttachmentDataObject::findAttachmentByCodeAndLicenseApplicationID($material->code,$param['license_application_id']);
                if($attachment != null){
                    $status = 'UPLOADED';
                }
            }

            return view('license-application.component.material.status', [
                'status'  =>  $status
            ])->render();
        })
        ->rawColumns(['label','status','action'])
        ->make(true);
    }

    public static function findMaterialById($id)
    {
        return Material::find($id);
    }
}
