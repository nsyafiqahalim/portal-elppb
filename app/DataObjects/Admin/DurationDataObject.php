<?php

namespace App\DataObjects\Admin;

use DB;

use App\Models\Admin\Duration;

class DurationDataObject
{
    public static function findAllDurations()
    {
        $Duns = Duration::activeOnly()->get();

        return $Duns;
    }

    public static function findAllDunUsingDatatableFormat()
    {
        return datatables()->of(Dun::query())
        ->addColumn('action', function ($Duns) {
            return view('admin.company-type.partials.datatable-button', compact('Duns'))->render();
        })
        ->addColumn('status', function ($Duns) {
            return ($Duns->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findDurationById($id)
    {
        return Duration::find($id);
    }
}
