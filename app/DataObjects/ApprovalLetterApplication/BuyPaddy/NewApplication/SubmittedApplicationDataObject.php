<?php

namespace App\DataObjects\ApprovalLetterApplication\BuyPaddy\NewApplication;

use DB;
use Carbon\Carbon;

use App\Models\ApprovalLetterApplication\Temporary;
use App\Models\ApprovalLetterApplication\Attachment;
use App\Models\ApprovalLetterApplication\BuyPaddy;
use App\DataObjects\ApprovalLetterApplicationDataObject;
use App\DataObjects\ApprovalLetterApplication\AttachmentDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyAddressDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyPartnerDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyPremiseDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyStoreDataObject;

use App\DataObjects\LicenseApplication\SerialNumberDataObject;

class SubmittedApplicationDataObject
{
    public static function store($param)
    {
        $approvalLetterApplicationparam = [
              'reference_number'          =>  $param['approval_letter_type_code'].Carbon::now()->format('Ymd').rand(0, 100),
              'status_id'                 =>  $param['status_id'],
              'approval_letter_application_type_id'   =>  $param['approval_letter_application_type_id'],
              'approval_letter_type_id'               =>  $param['approval_letter_type_id'],
              'user_id'                       =>  $param['user_id'],
              'company_name'                  =>  $param['company_name'],
          ];
        $approvalLetterApplication = ApprovalLetterApplicationDataObject::createNewApprovalLetterApplication($approvalLetterApplicationparam);
      
        $companyAddressParam                            = $param['original_address']->toArray();
        $companyAddressParam['state_name']                   = $param['original_address']->state->name;
        $companyAddressParam['approval_letter_application_id']  = $approvalLetterApplication->id;
        $companyAddressParam['company_id']              = $param['original_company']->id;
        unset($param['original_address']['id']);

        $companyDAOParam                                = $param['original_company']->toArray();
        $companyDAOParam['company_type_name']           = $param['original_company']->companyType->name;
        $companyDAOParam['company_id']                  = $param['original_company']->id;
        $companyDAOParam['approval_letter_application_id']      = $approvalLetterApplication->id;
        unset($companyDAOParam['id']);

        $companyPremiseDAOParam                         = $param['original_premise']->toArray();
        $companyPremiseDAOParam['business_type_name']        = $param['original_premise']->businessType->name;
        $companyPremiseDAOParam['building_type_name']        = $param['original_premise']->buildingType->name;
        $companyPremiseDAOParam['state_name']                = $param['original_premise']->state->name;
        $companyPremiseDAOParam['parliament_name']           = $param['original_premise']->parliament->name;
        $companyPremiseDAOParam['dun_name']                  = $param['original_premise']->dun->name;
        $companyPremiseDAOParam['district_name']            = $param['original_premise']->district->name;
        $companyPremiseDAOParam['company_premise_id']       = $companyPremiseDAOParam['id'];

        $companyPremiseDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
        unset($companyPremiseDAOParam['id']);

        $companyStoreDAOParam = $param['original_store']->toArray();
        $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
        $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
        $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
        $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
        $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;

        $companyStoreDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
        unset($companyStoreDAOParam['id']);

        $store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);
        $company = CompanyDataObject::createNewCompany($companyDAOParam);
        $addess = CompanyAddressDataObject::createNewCompanyAddress($companyAddressParam);
        $premise = CompanyPremiseDataObject::addNewCompanyPremise($companyPremiseDAOParam);

        foreach ($param['original_partners'] as $item) {
            $companyPartnerDAOParam = $item->toArray();
            //$companyPartnerDAOParam['ownership_type'] = $item->ownershipType->name;
             $companyPartnerDAOParam['race_name'] = (isset($item->race))?$item->race->name:null;
            $companyPartnerDAOParam['race_type_name'] = (isset($item->raceType))?$item->raceType->name:null;
            $companyPartnerDAOParam['citizen_name'] = ($item->is_citizen  == 1)? 'Warganegara' : 'Bukan Warganegara';
            $companyPartnerDAOParam['state_name'] = $item->state->name;

            $companyPartnerDAOParam['company_id'] = $param['original_company']->id;
            $companyPartnerDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
            unset($companyPartnerDAOParam['id']);

            CompanyPartnerDataObject::addNewCompanyPartner($companyPartnerDAOParam);
        }

        $param['approval_letter_application_id']    =   $approvalLetterApplication->id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


        if (isset($param['license_application_buy_paddy_condition_id'])) {
            BuyPaddy::updateOrCreate([
              'approval_letter_application_id'    =>$approvalLetterApplication->id
          ], [
              'license_application_buy_paddy_condition_id'    =>  $param['license_application_buy_paddy_condition_id']
          ]);
        }
    }

    public static function update($param, $id)
    {
        $approvalLetterApplicationparam = [
              'reference_number'          =>  $param['approval_letter_type_code'].Carbon::now()->format('Ymd').rand(0, 100),
              'status_id'                 =>  $param['status_id'],
              'approval_letter_application_type_id'   =>  $param['approval_letter_application_type_id'],
              'approval_letter_type_id'               =>  $param['approval_letter_type_id'],
              'user_id'                       =>  $param['user_id'],
              'company_name'                  =>  $param['company_name'],
          ];
      
        $approvalLetterApplication = ApprovalLetterApplicationDataObject::updateApprovalLetterApplication($approvalLetterApplicationparam, $id);
        Temporary::where('approval_letter_application_id', $id)->first()->delete();

        $companyAddressParam = $param['original_address']->toArray();
        $companyAddressParam['state_name'] = $param['original_address']->state->name;
        $companyAddressParam['approval_letter_application_id'] = $id;
        $companyAddressParam['company_id'] = $param['original_company']->id;
        unset($param['original_address']['id']);

        $companyDAOParam = $param['original_company']->toArray();
        $companyDAOParam['company_type_name'] = $param['original_company']->companyType->name;
        $companyDAOParam['company_id'] = $param['original_company']->id;
        $companyDAOParam['approval_letter_application_id'] = $id;
        unset($companyDAOParam['id']);

        $companyPremiseDAOParam = $param['original_premise']->toArray();
        $companyPremiseDAOParam['business_type_name'] = $param['original_premise']->businessType->name;
        $companyPremiseDAOParam['building_type_name'] = $param['original_premise']->buildingType->name;
        $companyPremiseDAOParam['state_name'] = $param['original_premise']->state->name;
        $companyPremiseDAOParam['parliament_name'] = $param['original_premise']->parliament->name;
        $companyPremiseDAOParam['dun_name'] = $param['original_premise']->dun->name;
        $companyPremiseDAOParam['company_id'] = $param['original_company']->id;
        $companyPremiseDAOParam['district_name'] = $param['original_premise']->district->name;
        $companyPremiseDAOParam['company_premise_id'] =$companyPremiseDAOParam['id'];

        $companyPremiseDAOParam['approval_letter_application_id'] = $id;
        unset($companyPremiseDAOParam['id']);

        $companyStoreDAOParam = $param['original_store']->toArray();
        $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
        $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
        $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
        $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
        $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;

        $companyStoreDAOParam['approval_letter_application_id'] = $id;
        unset($companyStoreDAOParam['id']);

        $company = CompanyDataObject::createNewCompany($companyDAOParam);
        $addess = CompanyAddressDataObject::createNewCompanyAddress($companyAddressParam);
        $premise = CompanyPremiseDataObject::addNewCompanyPremise($companyPremiseDAOParam);
        $store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);

        foreach ($param['original_partners'] as $item) {
            $companyPartnerDAOParam = $item->toArray();
            //$companyPartnerDAOParam['ownership_type'] = $item->ownershipType->name;
             $companyPartnerDAOParam['race_name'] = (isset($item->race))?$item->race->name:null;
            $companyPartnerDAOParam['race_type_name'] = (isset($item->raceType))?$item->raceType->name:null;
            $companyPartnerDAOParam['citizen_name'] = ($item->is_citizen  == 1)? 'Warganegara' : 'Bukan Warganegara';
            $companyPartnerDAOParam['state_name'] = $item->state->name;

            $companyPartnerDAOParam['company_id'] = $param['original_company']->id;
            $companyPartnerDAOParam['approval_letter_application_id'] = $id;
            unset($companyPartnerDAOParam['id']);

            CompanyPartnerDataObject::addNewCompanyPartner($companyPartnerDAOParam);
        }

        $param['approval_letter_application_id']    =   $id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


        if (isset($param['license_application_buy_paddy_condition_id'])) {
            BuyPaddy::updateOrCreate([
              'approval_letter_application_id'    =>$id
          ], [
              'license_application_buy_paddy_condition_id'    =>  $param['license_application_buy_paddy_condition_id']
          ]);
        }
    }
}
