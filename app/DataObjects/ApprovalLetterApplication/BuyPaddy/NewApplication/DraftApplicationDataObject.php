<?php

namespace App\DataObjects\ApprovalLetterApplication\BuyPaddy\NewApplication;

use DB;

use App\Models\ApprovalLetterApplication\Temporary;
use App\Models\ApprovalLetterApplication\Attachment;
use App\Models\ApprovalLetterApplication\ReferenceLicense;
use App\Models\ApprovalLetterApplication\BuyPaddy;

use App\DataObjects\ApprovalLetterApplicationDataObject;
use App\DataObjects\ApprovalLetterApplication\AttachmentDataObject;

class DraftApplicationDataObject
{
    public static function store($param)
    {
        $approvalLetterApplicationParam = [
               'status_id'                     =>  $param['status_id'],
               'user_id'                       =>  $param['user_id'],
               'company_name'                  =>  $param['company_name'],
               'approval_letter_application_type_id'   =>  $param['approval_letter_application_type_id'],
               'approval_letter_type_id'               =>  $param['approval_letter_type_id']
       ];

        $approvalLetterApplication = ApprovalLetterApplicationDataObject::createDraftApprovalLetterApplication($approvalLetterApplicationParam);
       
        $param['approval_letter_application_id']    =   $approvalLetterApplication->id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);

        
        if (isset($param['license_application_buy_paddy_condition_id'])) {
            BuyPaddy::updateOrCreate([
               'approval_letter_application_id'    =>$approvalLetterApplication->id
           ], [
               'license_application_buy_paddy_condition_id'    =>  $param['license_application_buy_paddy_condition_id']
           ]);
        }

        return Temporary::create([
               'company_premise_id'            =>  $param['premise_id'],
               'company_id'                    =>  $param['company_id'],
               'company_store_id'              =>  $param['store_id'],
               'approval_letter_application_id'        =>  $approvalLetterApplication->id,
       ]);
    }

    public static function update($param, $id)
    {
        $approvalLetterApplicationParam = [
               'status_id'                     =>  $param['status_id'],
               'user_id'                       =>  $param['user_id'],
               'reference_number'              =>  null,
               'company_name'                  =>  $param['company_name'],
               'approval_letter_application_type_id'   =>  $param['approval_letter_application_type_id'],
               'approval_letter_type_id'               =>  $param['approval_letter_type_id']
       ];

        $approvalLetterApplication = ApprovalLetterApplicationDataObject::updateApprovalLetterApplication($approvalLetterApplicationParam, $id);
       

        $param['approval_letter_application_id']    =   $id;
        AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($param['material_domain'],null,$param);


        if (isset($param['license_application_buy_paddy_condition_id'])) {
            BuyPaddy::updateOrCreate([
               'approval_letter_application_id'    =>$id
           ], [
               'license_application_buy_paddy_condition_id'    =>  $param['license_application_buy_paddy_condition_id']
           ]);
        }

        return Temporary::where('approval_letter_application_id', $id)->first()->update([
               'company_premise_id'            =>  $param['premise_id'],
               'company_id'                    =>  $param['company_id'],
               'company_store_id'              =>  $param['store_id'],
       ]);
    }
}
