<?php

namespace App\DataObjects\ApprovalLetterApplication;

use DB;
use Illuminate\Support\Facades\Storage;

use App\Models\ApprovalLetterApplication\Attachment;
use App\Models\ApprovalLetterApplication\Material;

class AttachmentDataObject
{
    public static function addNewAttachment($param)
    {
        $path = Storage::disk('fileURL')->put('approval_letter_attachment_attachment/'.$param['approval_letter_application_id'], $param['file']);

        Attachment::create([
            'file_path'                 =>  $path,
            'code'                      =>  $param['code'],
            'file_original_name'        =>  $param['file_original_name'],
            'approval_letter_application_id'    =>  $param['approval_letter_application_id']
        ]);
    }


    public static function findAttachmentByCodeAndApprovalLetterApplicationID($code, $approvalLetterApplicationId)
    {
        return Attachment::where('code', $code)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    // public static function addAttachmentsByMaterialDomain($domain,$param){
    //         $materials  = Material::where('domain', $domain)->get();
    //         foreach ($materials as $material) {
    //             if (isset($param[$material->code])) {
    //                 $storedParameter = [
    //                     'file'                                  =>  $param[$material->code],
    //                     'file_original_name'                    =>  $param[$material->code]->getClientOriginalName(),
    //                     'code'                                  =>  $material->code,
    //                     'approval_letter_application_id'        =>  $param['approval_letter_application_id'],
    //                 ];

    //                 self::addNewAttachment($storedParameter);
    //             }
    //         }
    // }

    public static function addAttachmentsByMaterialDomain($domain,$param){
            $materials  = Material::where('domain', $domain)->get();
            
            foreach ($materials as $material) {
                if (isset($param[$material->code])) {

                    $attachment = self::findAttachmentByCodeAndApprovalLetterApplicationID($material->code, $param['approval_letter_application_id']);
                    if (isset($attachment)) {
                        unlink(public_path().'//file/'.$attachment->file_path);
                
                        $attachment->delete();
                    }

                    $storedParameter = [
                        'file'                                  =>  $param[$material->code],
                        'file_original_name'                    =>  $param[$material->code]->getClientOriginalName(),
                        'code'                                  =>  $material->code,
                        'approval_letter_application_id'        =>  $param['approval_letter_application_id'],
                    ];

                    self::addNewAttachment($storedParameter);
                }
            }
    }

    public static function validateAttachments($validator,$request, $domain, $id){
        $materials  = Material::where('domain', $domain)->get();
        foreach ($materials as $material) {
            $attachment = AttachmentDataObject::findAttachmentByCodeAndApprovalLetterApplicationID($material->code, $id);
    
            if ($attachment == null && $request->file($material->code) == null) {
                    $validator->errors()->add($material->code, '"'.$material->label.'" diperlukan');
            }
        }

        return $validator;
    }

    public static function addValidateAttachments($domain){
        $rules = [];
        $messages = [];
        $materials  = Material::where('domain', $domain)->get();
        foreach ($materials as $material) {
            $rules[$material->code]  =    ['required', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'];
            $messages[$material->code.'.required']  =    ['"'.$material->label.'" diperlukan'];
            $messages[$material->code.'.max']  =    ['"'.$material->label.'" perlu 20MB dan kebawah'];
            $messages[$material->code.'.mimes']  =    ['"'.$material->label.'" adalah jpeg,png,jpg,pdf'];
        }
        
        return [
            'rules'     =>  $rules,
            'messages'  =>  $messages
        ];
    }

    public static function updateValidateAttachments($domain){
        $rules = [];
        $messages = [];
        $materials  = Material::where('domain', $domain)->get();
        foreach ($materials as $material) {
            $rules[$material->code]  =    ['nullable', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'];
            $messages[$material->code.'.max']  =    ['"'.$material->label.'" perlu 20MB dan kebawah'];
            $messages[$material->code.'.mimes']  =    ['"'.$material->label.'" adalah jpeg,png,jpg,pdf'];
        }
        
        return [
            'rules'     =>  $rules,
            'messages'  =>  $messages
        ];
    }
}
