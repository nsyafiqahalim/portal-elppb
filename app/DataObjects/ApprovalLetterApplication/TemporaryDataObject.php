<?php
namespace App\DataObjects\ApprovaletterApplication;

use DB;
use App\Models\ApprovaletterApplication\Temporary;
use App\Models\ApprovaletterApplication\Attachment;
use App\DataObjects\ApprovaletterApplicationDataObject;
use App\DataObjects\ApprovaletterApplication\AttachmentDataObject;

class TemporaryDataObject
{
    public static function saveAsDraft($param)
    {
        $approvalLetterApplicationParam = [
                    'status_id'                     =>  $param['status_id'],
                'user_id'                       =>  $param['user_id'],
                'company_name'                  =>  $param['company_name'],
                'approval_letter_application_type_id'   =>  $param['approval_letter_application_type_id'],
                'approval_letter_type_id'               =>  $param['approval_letter_type_id'],
                'apply_duration'                =>  $param['apply_duration'],
                'apply_load'                    =>  $param['apply_load'],
        ];
        $approvalLetterApplication = ApprovaletterApplicationDataObject::createDraftApprovalLetterApplication($approvalLetterApplicationParam);
        
        return Temporary::create([
                    'company_premise_id'            =>  $param['premise_id'],
                'company_id'                    =>  $param['company_id'],
                'approval_letter_application_id'        =>  $approvalLetterApplication->id,
        ]);
    }
}
