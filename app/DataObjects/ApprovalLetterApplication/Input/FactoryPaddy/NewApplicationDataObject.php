<?php
namespace App\DataObjects\ApprovalLetterApplication\Input\FactoryPaddy;

use DB;
use App\DataObjects\approval_letterA\CompanyDataObject;
use App\DataObjects\ApprovalLetterApplicationDataObject;
use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

class NewApplicationDataObject
{
    public static function newInputParameter()
    {
        $inputParam = [];
        $inputParam['company_types']            =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                   =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']          =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                    =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']               =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                     =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']              =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']           =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']           =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']    =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        
        return $inputParam;
    }
    public static function draftInputParameter($id)
    {
        $inputParam = [];
        $inputParam['approval_letter_application']              =   ApprovalLetterApplicationDataObject::findApprovalLetterApplicationById($id);
        $inputParam['approval_letter_application_temporary']    =   $inputParam['approval_letter_application']->temporary;
        $inputParam['company_types']                    =   CompanyTypeDataObject::findAllActiveCompanyTypes();
        $inputParam['states']                           =   StateDataObject::findAllActiveStates();
        $inputParam['ownership_types']                   =   OwnershipTypeDataObject::findAllActiveOwnershipTypes();
        $inputParam['races']                            =   RaceDataObject::findAllActiveRaces();
        $inputParam['race_types']                       =   RaceTypeDataObject::findAllActiveRaceTypes();
        $inputParam['duns']                             =   DunDataObject::findAllActiveDuns();
        $inputParam['parliaments']                      =   ParliamentDataObject::findAllActiveParliaments();
        $inputParam['building_types']                   =   BuildingTypeDataObject::findAllActiveBuildingTypes();
        $inputParam['business_types']                   =   BusinessTypeDataObject::findAllActiveBusinessTypes();
        $inputParam['districts']                        =   DistrictDataObject::findAllActiveDistricts();
        $inputParam['store_ownership_types']            =   StoreOwnershipTypeDataObject::findAllActiveStoreOwnershipTypes();
        $inputParam['current_approval_letter_application_id']    =   encrypt($inputParam['approval_letter_application']->id);
        return $inputParam;
    }
}
