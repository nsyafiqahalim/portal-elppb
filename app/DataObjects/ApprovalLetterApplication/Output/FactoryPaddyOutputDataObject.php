<?php
namespace App\DataObjects\ApprovalLetterApplication\Output;

use DB;
use App\DataObjects\ApprovalLetterApplicationDataObject;
use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as OriginalCompanyStoreDataObject;
use App\DataObjects\Admin\ApprovalLetterApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\ApprovalLetterApplication\AttachmentDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyPremiseDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyPartnerDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyStoreDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyAddressDataObject;

class FactoryPaddyOutputDataObject
{
    public static function newOutputParameter($param, $approvalLetterApplicationDataSource)
    {
        $outputParam = $param;
        if ($approvalLetterApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['original_company']              = OriginalCompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = OriginalCompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = OriginalCompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                = OriginalCompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
        } elseif ($approvalLetterApplicationDataSource == 'APPROVAL_LETTER_APPLICATION_DATA') {
            $outputParam['original_company']              = CompanyDataObject::findCompanyById($outputParam['company_id']);
            $outputParam['original_premise']              = CompanyPremiseDataObject::findCompanyPremiseById($outputParam['premise_id']);
            $outputParam['original_partners']             = CompanyPartnerDataObject::findAllActiveCompanyPartners();
            $outputParam['original_store']                = CompanyStoreDataObject::findCompanyStoreById($outputParam['store_id']);
            $outputParam['company_name']                  = $outputParam['original_company']->name;
            $outputParam['original_address']              = $outputParam['original_company']->address;
        }
        return $outputParam;
    }
    public static function draftOutputParameter($param, $approvalLetterApplicationDataSource)
    {
        $outputParam = $param;
        if ($approvalLetterApplicationDataSource == 'REFERENCE_DATA') {
            $outputParam['company_name'] = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
        } elseif ($approvalLetterApplicationDataSource == 'APPROVAL_LETTER_APPLICATION_DATA') {
            $outputParam['company_name'] = OriginalCompanyDataObject::findCompanyById($param['company_id'])->name ?? null;
        }
        return $outputParam;
    }
}
