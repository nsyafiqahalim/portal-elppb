<?php

namespace App\DataObjects\ApprovalLetterApplication;

use DB;
use Illuminate\Support\Facades\Storage;

use App\Models\ApprovalLetterApplication\Attachment;

class AttachmentDataObject
{
    public static function addNewAttachment($param)
    {
        $path = Storage::disk('fileURL')->put('approval_letter_attachment_attachment/'.$param['approval_letter_application_id'], $param['file']);

        Attachment::create([
            'file_path'                 =>  $path,
            'code'                      =>  $param['code'],
            'file_original_name'        =>  $param['file_original_name'],
            'approval_letter_application_id'    =>  $param['approval_letter_application_id']
        ]);
    }

    public static function findSSMAttachmentByApprovalLetterApplicationID($approvalLetterApplicationId)
    {
        return Attachment::where('code', Attachment::SSM)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    public static function findMinutMeetingAttachmentByApprovalLetterApplicationID($approvalLetterApplicationId)
    {
        return Attachment::where('code', Attachment::MINUTE_MEETING)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    public static function findByCodeAndApprovalLetterApplicationId($code, $approvalLetterApplicationId)
    {
        return Attachment::where('code', $code)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    public static function findOriginalAttachmentAttachmentByApprovalLetterApplicationID($approvalLetterApplicationId)
    {
        return Attachment::where('code', Attachment::ORIGINAL_ATTACHMENT)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    public static function findSaleAgreementAttachmentAttachmentByApprovalLetterApplicationID($approvalLetterApplicationId)
    {
        return Attachment::where('code', Attachment::SALE_AGREEMENT)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    public static function findAccountStatementAttachmentAttachmentByApprovalLetterApplicationID($approvalLetterApplicationId)
    {
        return Attachment::where('code', Attachment::ACCOUNT_STATEMENT)->where('approval_letter_application_id', $approvalLetterApplicationId)->first();
    }

    public static function storeSSM($param,$approvalLetterApplicationId){
        if (isset($param['ssm'])) {
            $storedParameter = [
            'file'                          =>  $param['ssm'],
            'file_original_name'            =>  $param['ssm']->getClientOriginalName(),
            'code'                          =>  Attachment::SSM,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            self::addNewAttachment($storedParameter);
        }
    }
    
    public static function storeMinuteMeeting($param,$approvalLetterApplicationId){
        if (isset($param['minute_meeting'])) {
            $storedParameter = [
            'file'                          =>  $param['minute_meeting'],
            'file_original_name'            =>  $param['minute_meeting']->getClientOriginalName(),
            'code'                          =>  Attachment::MINUTE_MEETING,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            self::addNewAttachment($storedParameter);
        }
    }

    public static function storeOriginalAttachment($param,$approvalLetterApplicationId){
        if (isset($param['minute_meeting'])) {
            $storedParameter = [
            'file'                          =>  $param['minute_meeting'],
            'file_original_name'            =>  $param['minute_meeting']->getClientOriginalName(),
            'code'                          =>  Attachment::ORIGINAL_ATTACHMENT,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            self::addNewAttachment($storedParameter);
        }
    }

    public static function storeSaleAgreement($param,$approvalLetterApplicationId){
        if (isset($param['sale_agreement'])) {
            $storedParameter = [
            'file'                          =>  $param['sale_agreement'],
            'file_original_name'            =>  $param['sale_agreement']->getClientOriginalName(),
            'code'                          =>  Attachment::SALE_AGREEMENT,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            self::addNewAttachment($storedParameter);
        }
    }

    public static function storeAccountStatement($param,$approvalLetterApplicationId){
        if (isset($param['account_statement'])) {
            $storedParameter = [
            'file'                          =>  $param['account_statement'],
            'file_original_name'            =>  $param['account_statement']->getClientOriginalName(),
            'code'                          =>  Attachment::ACCOUNT_STATEMENT,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            self::addNewAttachment($storedParameter);
        }
    }

    public static function updateSSM($param,$approvalLetterApplicationId){
        if (isset($param['ssm'])) {
            $attachment = AttachmentDataObject::findByCodeAndApprovalLetterApplicationId(Attachment::SSM, $approvalLetterApplicationId);
            if (isset($attachment)) {
                unlink(public_path().'//file/'.$attachment->file_path);
        
                $attachment->delete();
            }

            $ssmParam = [
            'file'                          =>  $param['ssm'],
            'file_original_name'            =>  $param['ssm']->getClientOriginalName(),
            'code'                          =>  Attachment::SSM,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            AttachmentDataObject::addNewAttachment($ssmParam);
        }
    }

    public static function updateMinuteMeeting($param,$approvalLetterApplicationId){
        if (isset($param['minute_meeting'])) {
                $attachment = AttachmentDataObject::findByCodeAndApprovalLetterApplicationId(Attachment::MINUTE_MEETING, $approvalLetterApplicationId);
            if (isset($attachment)) {
                unlink(public_path().'//file/'.$attachment->file_path);
        
                $attachment->delete();
            }

            $updateParam = [
            'file'                          =>  $param['minute_meeting'],
            'file_original_name'            =>  $param['minute_meeting']->getClientOriginalName(),
            'code'                          =>  Attachment::MINUTE_MEETING,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            AttachmentDataObject::addNewAttachment($updateParam);
        }
    }

    public static function updateOriginalAttachment($param,$approvalLetterApplicationId){
        if (isset($param['original_attachment'])) {
                $attachment = AttachmentDataObject::findByCodeAndApprovalLetterApplicationId(Attachment::ORIGINAL_ATTACHMENT, $approvalLetterApplicationId);
            if (isset($attachment)) {
                unlink(public_path().'//file/'.$attachment->file_path);
        
                $attachment->delete();
            }

            $updateParam = [
            'file'                          =>  $param['original_attachment'],
            'file_original_name'            =>  $param['original_attachment']->getClientOriginalName(),
            'code'                          =>  Attachment::ORIGINAL_ATTACHMENT,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            AttachmentDataObject::addNewAttachment($updateParam);
        }
    }

    public static function updateAccountStatement($param,$approvalLetterApplicationId){
        if (isset($param['account_statement'])) {
                $attachment = AttachmentDataObject::findByCodeAndApprovalLetterApplicationId(Attachment::ACCOUNT_STATEMENT, $approvalLetterApplicationId);
            if (isset($attachment)) {
                unlink(public_path().'//file/'.$attachment->file_path);
        
                $attachment->delete();
            }

            $updateParam = [
            'file'                          =>  $param['account_statement'],
            'file_original_name'            =>  $param['account_statement']->getClientOriginalName(),
            'code'                          =>  Attachment::ACCOUNT_STATEMENT,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            AttachmentDataObject::addNewAttachment($updateParam);
        }
    }

    public static function updateSaleAgreement($param,$approvalLetterApplicationId){
        if (isset($param['sale_agreement'])) {
                $attachment = AttachmentDataObject::findByCodeAndApprovalLetterApplicationId(Attachment::SALE_AGREEMENT, $approvalLetterApplicationId);
            if (isset($attachment)) {
                unlink(public_path().'//file/'.$attachment->file_path);
        
                $attachment->delete();
            }

            $updateParam = [
            'file'                          =>  $param['sale_agreement'],
            'file_original_name'            =>  $param['sale_agreement']->getClientOriginalName(),
            'code'                          =>  Attachment::SALE_AGREEMENT,
            'approval_letter_application_id'        =>  $approvalLetterApplicationId,
            ];

            AttachmentDataObject::addNewAttachment($updateParam);
        }
    }
}
