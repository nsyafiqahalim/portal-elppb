<?php

namespace App\DataObjects;

use DB;
use Carbon\Carbon;
use App\Models\License;

class LicenseDataObject
{
    public static function findLicenseById($id)
    {
        return License::find($id);
    }

    public static function findActiveLicenseExistByCompanyIdAndLicenseTypeId($param)
    {
        return License::
        where('company_id', $param['company_id'])
        ->where('license_type_id', $param['license_type_id'])
        ->where('status_id', $param['status_id'])
        ->first();
    }

    public static function createDraftLicense($param)
    {
        return License::create([
            'status_id'                     =>  $param['status_id'],
            'user_id'                       =>  $param['user_id'],
            'company_name'                  =>  $param['company_name'] ?? '-',
            'license_type_id'               =>  $param['license_type_id'],
            'license_application_type_id'   =>  $param['license_application_type_id'],
            'apply_duration'                =>  $param['apply_duration'],
            'apply_load'                    =>  $param['apply_load']
        ]);
    }

    public static function findActiveWholesaleLicenseAndFactoryLicenseByCompanyId($companyId)
    {
        return License::distinct()
        ->where('company_id',$companyId)
        ->whereHas('licenseType',function($licenseType){
            $licenseType->where('code','C')
            ->orWhere('code','F');
        })
        ->whereHas('status',function($status){
            $status->where('code','LESEN_AKTIF');
        })
        ->get();
    }

    public static function findMyLicenseUsingDatatableFormat($param)
    {
        $licenses =  License::distinct()
                    ->when(isset($param['user_id']), function ($License) use ($param) {
                        $License->where('user_id', $param['user_id']);
                    })->orderby('id', 'desc');
                
        return datatables()->of($licenses)
        ->addIndexColumn()
        ->addColumn('license_type', function ($license) {
            return $license->licenseType->name ?? '-';
        })->addColumn('license_application_type', function ($license) {
            return $license->licenseApplicationType->name ?? '-';
        })->addColumn('status', function ($license) {
            return 'Aktif';
        })
        ->addColumn('company', function ($license) {
            return $license->company->name?? '-';
        })
        ->addColumn('license_number', function ($license) {
            return $license->license_number ?? '-';
        })
        ->addColumn('action', function ($license) {
            $licenseTypeName = $license->licenseType->name;

            if ($licenseTypeName == 'Runcit') {
                $licenseType = 'retail';
            } elseif ($licenseTypeName == 'Borong') {
                $licenseType = 'wholesale';
            } elseif ($licenseTypeName == 'Import') {
                $licenseType = 'import';
            } elseif ($licenseTypeName == 'Eksport') {
                $licenseType = 'export';
            } elseif ($licenseTypeName == 'Membeli Padi') {
                $licenseType = 'buy_paddy';
            } elseif ($licenseTypeName == 'Kilang Padi Komersil') {
                $licenseType = 'factory_paddy';
            }

            return view(
                'license-management.partials.datatable-button',
                [
                    'id'    =>  encrypt($license->id),
                    'licenseType'   =>  $licenseType
                ]
            )->render();
        })
        ->rawColumns(['status','action'])
    ->make(true);
    }
}
