<?php

namespace App\DataObjects;

use DB;
use Carbon\Carbon;
use App\Models\LicenseApplication;

class LicenseApplicationDataObject
{
    public static function createNewLicenseApplication($param)
    {
        return LicenseApplication::create([
            'reference_number'                  =>  $param['reference_number'],
            'status_id'                         =>  $param['status_id'],
            'company_name'                      =>  $param['company_name'],
            'license_type_id'                   =>  $param['license_type_id'],
            'license_application_type_id'       =>  $param['license_application_type_id'],
            'license_application_category_id'   =>  $param['license_application_category_id']?? null,
            'license_application_license_generation_type_id'   =>  $param['license_application_license_generation_type_id'] ?? null,
            'branch_id'                         =>  $param['branch_id'],
            'applicant_id'                      =>  $param['applicant_id'],
            'user_id'                           =>  $param['user_id'] ?? null,
            'duration_id'                       =>  $param['duration_id'] ?? null,
            'material_domain'                   =>  $param['material_domain'] ?? null,
            'apply_duration'                    =>  $param['apply_duration'],
            'apply_date'                        =>  Carbon::now()->format('Y-m-d')
        ]);
    }

    public static function updateLicenseApplicationStatusById($param, $id)
    {
        $licenseApplication = LicenseApplication::find($id);
        $licenseApplication->update([
            'reference_number'          =>  $param['reference_number'],
            'status_id'                 =>  $param['status_id'],
            //'company_name'              =>  $param['company_name'],
            'apply_date'                =>  Carbon::now()->format('Y-m-d')
        ]);

        return $licenseApplication;
    }

    public static function updateLicenseApplication($param, $id)
    {
        return LicenseApplication::find($id)->update([
            'reference_number'          =>  $param['reference_number'],
            'status_id'                 =>  $param['status_id'],
            'company_name'                 =>  $param['company_name'],
            //'license_type_id'           =>  $param['license_type_id'],
            //'license_application_type_id'=>  $param['license_application_type_id'],
            'license_application_category_id'=>  $param['license_application_category_id']?? null,
            'license_application_license_generation_type_id'   =>  $param['license_application_license_generation_type_id'] ?? null,
            'branch_id'                 =>  $param['branch_id'],
            'applicant_id'                 =>  $param['applicant_id'],
            'user_id'                 =>  $param['user_id'] ?? null,
            'duration_id'               =>  $param['duration_id'] ?? null,
            'material_domain'                 =>  $param['material_domain'] ?? null,
            'apply_duration'            =>  $param['apply_duration'],
            'apply_date'                =>  Carbon::now()->format('Y-m-d')
        ]);
    }

    public static function findLicenseApplicationById($id)
    {
        return LicenseApplication::find($id);
    }

    public static function findLicenseApplicationByPreviouseLicenseApplicationIncludeLicenseId($includeLicenseId){
         return LicenseApplication::whereHas('includeLicense', function ($license) use ($includeLicenseId) {
            return $license->where('id', $includeLicenseId);
        })->first();
    }
    
    public static function findLicenseApplicationByLicenseId($licenseId)
    {
        return LicenseApplication::whereHas('licenses', function ($license) use ($licenseId) {
            return $license->where('id', $licenseId);
        })->first();
    }

    public static function createDraftLicenseApplication($param){
        return LicenseApplication::create([
            'status_id'                         =>  $param['status_id'],
            'applicant_id'                      =>  $param['applicant_id'],
            'license_type_id'                   =>  $param['license_type_id'],
            'license_application_type_id'       =>  $param['license_application_type_id'],
            'license_application_category_id'   =>  $param['license_application_category_id']?? null,
            'company_name'   =>  $param['company_name']?? null
        ]);
    }

    // public static function createDraftLicenseApplication($param)
    // {
    //     return LicenseApplication::create([
    //         'status_id'                 =>  $param['status_id'],
    //         'applicant_id'                 =>  $param['applicant_id'],
    //         'user_id'                 =>    $param['user_id'] ?? null,
    //         'duration_id'               =>  $param['duration_id'] ?? null,
    //         'company_name'                 =>  $param['company_name'],
    //         'license_type_id'           =>  $param['license_type_id'],
    //         'license_application_type_id'=>  $param['license_application_type_id'],
    //         'license_application_category_id'=>  $param['license_application_category_id']?? null,
    //         'material_domain'                 =>  $param['material_domain'] ?? null,
    //         'license_application_license_generation_type_id'   =>  $param['license_application_license_generation_type_id'] ?? null,
    //         'apply_duration'            =>  $param['apply_duration'],
    //         'apply_load'                =>  $param['apply_load']
    //     ]);
    // }

    public static function findMyLicenseApplicationUsingDatatableFormat($param)
    {
        $licenseApplications =  LicenseApplication::distinct()
                    ->when(isset($param['user_id']), function ($licenseApplication) use ($param) {
                        $licenseApplication->where('applicant_id', $param['user_id']);
                    })->orderby('id', 'desc');
                
        return datatables()->of($licenseApplications)
        ->addIndexColumn()
        ->addColumn('license_type', function ($licenseApplication) {
            return $licenseApplication->licenseType->name ?? '-';
        })->addColumn('license_application_type', function ($licenseApplication) {
            if(isset($licenseApplication->change)){
                $changeTypeCode = $licenseApplication->change->changeType->code;
                if($changeTypeCode == 'COMPANY_INFORMATION'){
                    $status = 'PERUBAHAN NAMA SYARIKAT';
                }
                elseif($changeTypeCode == 'PREMISE_INFORMATION'){
                    $status = 'PERUBAHAN ALAMAT PREMIS';
                }
                elseif($changeTypeCode == 'STORE_INFORMATION'){
                    $status = 'PERUBAHAN ALAMAT STOR';
                }
                elseif($changeTypeCode == 'LICENSE_APPLICATION_INFORMATION'){
                    $status = 'PERUBAHAN HAD MUATAN';
                }else{
                    $status = 'PEMBAHARUAN DAN PERUBAHAN';
                }
                return $status;
            }
            else{
                return $licenseApplication->licenseApplicationType->name ?? '-';
                
            }
        })->addColumn('status', function ($licenseApplication) {
            return $licenseApplication->status->name ?? '-';
        })
        ->addColumn('company', function ($licenseApplication) {
            return $licenseApplication->company_name?? '-';
        })
        ->addColumn('reference_number', function ($licenseApplication) {
            return $licenseApplication->reference_number ?? '-';
        })
        ->addColumn('action', function ($licenseApplication) {
            $licenseTypeName = $licenseApplication->licenseType->name;
            $licenseApplicationTypeCode = $licenseApplication->licenseApplicationType->code;
            $status = (isset($licenseApplication->reference_number))?'SUBMITTED':'NOT_SUBMITTED';
            $licenseApplicationType = null;

            if ($licenseTypeName == 'Runcit') {
                $licenseType = 'retail';
            } elseif ($licenseTypeName == 'Borong') {
                $licenseType = 'wholesale';
            } elseif ($licenseTypeName == 'Import') {
                $licenseType = 'import';
            } elseif ($licenseTypeName == 'Eksport') {
                $licenseType = 'export';
            } elseif ($licenseTypeName == 'Membeli Padi') {
                $licenseType = 'buy_paddy';
            } elseif ($licenseTypeName == 'Kilang Padi Komersil') {
                $licenseType = 'factory_paddy';
            }

            if ($licenseApplicationTypeCode == 'BAHARU') {
                $licenseApplicationType = 'new';
            } elseif ($licenseApplicationTypeCode == 'PEMBAHARUAN') {
                $licenseApplicationType = 'renew';
            } elseif ($licenseApplicationTypeCode == 'PEMBATALAN') {
                $licenseApplicationType = 'cancellation';
            } elseif ($licenseApplicationTypeCode == 'PENGGANTIAN') {
                $licenseApplicationType = 'replacement';
            }
            elseif ($licenseApplicationTypeCode == 'PERUBAHAN_MAKLUMAT_SYARIKAT') {
                 $licenseApplicationType = 'change_company';
            }
            elseif ($licenseApplicationTypeCode == 'PERUBAHAN_MAKLUMAT_PREMIS') {
                 $licenseApplicationType = 'change_premise';
            }
            elseif ($licenseApplicationTypeCode == 'PERUBAHAN_MAKLUMAT_STOR') {
                $licenseApplicationType = 'change_store';
            }
            elseif ($licenseApplicationTypeCode == 'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN') {
                $licenseApplicationType = 'change_license_application';
            }
            elseif ($licenseApplicationTypeCode == 'PEMBAHARUAN_DAN_PERUBAHAN') {
                $licenseApplicationType = 'change_and_renew';
            }

            return view(
                'license-application.partials.datatable-button',
                [
                    'id'                        =>  encrypt($licenseApplication->id),
                    'licenseType'               =>  $licenseType,
                    'status'                    =>  $status,
                    'licenseApplicationType'    =>  $licenseApplicationType
                ]
            )->render();
        })
    ->make(true);
    }
}
