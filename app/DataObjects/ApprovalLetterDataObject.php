<?php
namespace App\DataObjects;

use DB;
use Carbon\Carbon;
use App\Models\ApprovalLetter;

class ApprovalLetterDataObject
{
    public static function findApprovalLetterById($id)
    {
        return ApprovalLetter::find($id);
    }

    public static function createDraftApprovalLetter($param)
    {
        return ApprovalLetter::create([
            'status_id'                     =>  $param['status_id'],
            'user_id'                       =>  $param['user_id'],
            'company_name'                  =>  $param['company_name'] ?? '-',
            'approval_letter_type_id'               =>  $param['approval_letter_type_id'],
            'approval_letter_application_type_id'   =>  $param['approval_letter_application_type_id'],
            'apply_duration'                =>  $param['apply_duration'],
            'apply_load'                    =>  $param['apply_load']
        ]);
    }
    public static function findActiveApprovalLetterExistByCompanyIdAndapprovalLetterTypeId($param)
    {
        return ApprovalLetter::
        where('company_id', $param['company_id'])
        ->where('approval_letter_type_id', $param['approval_letter_type_id'])
        ->where('status_id', $param['status_id'])
        ->first();
    }
    public static function findMyApprovalLetterUsingDatatableFormat($param)
    {
        $approvalLetters =  ApprovalLetter::distinct()
                    ->when(isset($param['user_id']), function ($approvalLetter) use ($param) {
                        $approvalLetter->where('user_id', $param['user_id']);
                    })->orderby('id', 'desc');
                
        return datatables()->of($approvalLetters)
        ->addIndexColumn()
        ->addColumn('approval_letter_type', function ($approvalLetter) {
            return $approvalLetter->approval_letterType->name ?? '-';
        })->addColumn('approval_letter_application_type', function ($approvalLetter) {
            return $approvalLetter->approval_letterApplicationType->name ?? '-';
        })->addColumn('status', function ($approvalLetter) {
            return 'Aktif';
        })
        ->addColumn('company', function ($approvalLetter) {
            return $approvalLetter->company->name?? '-';
        })
        ->addColumn('approval_letter_number', function ($approvalLetter) {
            return $approvalLetter->approval_letter_number ?? '-';
        })
        ->addColumn('action', function ($approvalLetter) {
            $approvalLetterTypeName = $approvalLetter->approvalLetterType->name;
            if ($approvalLetterTypeName == 'Membeli Padi') {
                $approvalLetterType = 'buy_paddy';
            } elseif ($approvalLetterTypeName == 'Kilang Padi Komersil') {
                $approvalLetterType = 'factory_paddy';
            }

            return view(
                'approval-letter-management.partials.datatable-button',
                [
                    'id'    =>  encrypt($approvalLetter->id),
                    'approvalLetterType'   =>  $approvalLetterType
                ]
            )->render();
        })
        ->rawColumns(['status','action'])
    ->make(true);
    }
}
