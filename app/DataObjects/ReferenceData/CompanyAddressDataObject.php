<?php

namespace App\DataObjects\ReferenceData;

use DB;

use App\Models\ReferenceData\CompanyAddress;

class CompanyAddressDataObject
{
    public static function createNewCompanyAddress($param)
    {
        return CompanyAddress::create([
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'phone_number'              =>  $param['phone_number'],
            'fax_number'                =>  $param['fax_number'],
            'email'                     =>  $param['email'],
            'company_id'                =>  $param['company_id'],
        ]);
    }

    public static function updateCompanyAddress($param, $companyId)
    {
        $companyAddress = CompanyAddress::UpdateOrCreate(
        [
            'company_id'                =>  $companyId
        ],
        [
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'phone_number'              =>  $param['phone_number'],
            'fax_number'              =>  $param['fax_number'],
            'email'                     =>  $param['email'],
        ]);
    }

    public static function findCompanyAddressByCompanyId($companyId){
        return CompanyAddress::with(['state'])->where('company_id',$companyId)->first();
    }
}
