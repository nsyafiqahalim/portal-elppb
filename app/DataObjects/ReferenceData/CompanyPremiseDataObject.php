<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\CompanyPremise;

class CompanyPremiseDataObject
{
    public static function addNewCompanyPremise($param)
    {
        return CompanyPremise::create([
            //'name'                  =>  $param['name'],
            'business_type_id'       =>  $param['business_type_id'],
            'building_type_id'       =>  $param['building_type_id'],
            'dun_id'                =>  $param['dun_id'],
            'district_id'                =>  $param['district_id'],
            'parliament_id'         =>  $param['parliament_id'],
            'phone_number'              =>  $param['phone_number'],
            'fax_number'                =>  $param['fax_number'],
            'email'                     =>  $param['email'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'company_id'               =>  $param['company_id'],
        ]);
    }

    public static function updateCompanyPremise($param, $id)
    {
        return CompanyPremise::find($id)->update([
            //'name'                  =>  $param['name'],
            'business_type_id'       =>  $param['business_type_id'],
            'building_type_id'       =>  $param['building_type_id'],
            'dun_id'                =>  $param['dun_id'],
            'district_id'                =>  $param['district_id'],
            'parliament_id'         =>  $param['parliament_id'],
            'phone_number'              =>  $param['phone_number'],
            'fax_number'                =>  $param['fax_number'],
            'email'                     =>  $param['email'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'company_id'               =>  $param['company_id'],
        ]);
    }

    public static function findAllActiveCompanyPremises()
    {
        $Companys = CompanyPremise::activeOnly()->get();

        return $Companys;
    }

    public static function findAllCompanyPremisesUsingDatatableFormat($param)
    {
        $companies =  CompanyPremise::distinct()
                    ->when(isset($param['company_id']), function ($company) use ($param) {
                        $company->where('company_id', $param['company_id']);
                    })
                    ->when(isset($param['id']), function ($company) use ($param) {
                        $company->where('id', $param['id']);
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('status', function ($companyPremise) {
            return ($companyPremise->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($companyPremise) {
            return $companyPremise->address_1.','
                    .$companyPremise->address_2.','
                    .$companyPremise->address_3.','
                    .$companyPremise->postcode.','
                    .$companyPremise->state->name.',';
        })->addColumn('business_type', function ($companyPremise) {
            return $companyPremise->businessType->name ?? '-';
        })->addColumn('building_type', function ($companyPremise) {
            return $companyPremise->buildingType->name ?? '-';
        })
        ->addColumn('dun', function ($companyPremise) {
            return $companyPremise->dun->name ?? '-';
        })
        ->addColumn('parliament', function ($companyPremise) {
            return $companyPremise->parliament->name ?? '-';
        })
        ->addColumn('action', function ($companyPremise) use ($param) {
            $param['premise_id']= (isset($param['premise_id']))? $param['premise_id'] : null;
            $checked = ($param['premise_id'] == $companyPremise->id)? 'checked': null;
            
            $arrayedCompanyrPremise =   $companyPremise->toArray();
            $arrayedCompanyrPremise['address']   =   convertAddressIntoString($companyPremise);
            return view(
                'license-application.component.premise.partials.radiobutton',
                [
                'id'            =>  encrypt($companyPremise->id),
                'key'        =>  md5($companyPremise->id),
                'premise'    =>  json_encode($arrayedCompanyrPremise),
                'checked'   =>  $checked
            ]
            )->render();
        })
        ->addColumn('apply_load', function ($companyPremise) use ($param) {
            $param['premise_id']= (isset($param['premise_id']))? $param['premise_id'] : null;
            $disabled = ($param['premise_id'] == $companyPremise->id)? 'readonly' : 'disabled';
            $applyLoad = (isset($param['apply_load']))?$param['apply_load'] :   null;
       
            return view(
                'license-application.component.premise.partials.apply-load',
                [
                'disabled'   =>  $disabled,
                'code'      =>  md5($companyPremise->id),
                'applyLoad'   =>  $applyLoad,
                'premiseId' =>  $param['premise_id']
            ]
            )->render();
        })
        ->addColumn('edit', function ($companyPremise) use($param) {
            $disabled = (isset($param['id']))? 'disabled': null;

            return view(
                'license-application.component.premise.partials.button',
                [
                'id'        =>  encrypt($companyPremise->id),
                'disabled'  =>  $disabled
            ]
            )->render();
        })
        ->addColumn('company_name', function ($companyPremise) {
            return $companyPremise->company->name ?? '-';
        })
        ->rawColumns(['edit','action','apply_load'])
        ->make(true);
    }

    public static function findCompanyPremiseById($id)
    {
        return CompanyPremise::with([
            'businessType',
            'buildingType',
            'parliament',
            'dun',
            'district',
            'state'
        ])->find($id);
        
        //return CompanyPremise::find($id);
    }
}
