<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\CompanyStock;

class CompanyStockDataObject
{
    public static function addNewCompanyStock($param)
    {
        return CompanyStock::create([
            //'name'                    =>  $param['name'],
            'initial_stocks'                    =>  $param['initial_stocks'],
            'total_buy'                         =>  $param['total_buy'],
            'buying_price'                      =>  $param['buying_price'],
            'total_spin'                        =>  $param['total_spin'],
            'stock_balance'                     =>  $param['stock_balance'],
            'spin_result'                       =>  $param['spin_result'],
            'fax_number'                        =>  $param['fax_number'],
            'total_sell'                        =>  $param['total_sell'],
            'selling_price'                     =>  $param['selling_price'],
            'stock_at'                          =>  $param['stock_at'],
            'stock_type_id'                     =>  $param['stock_type_id'],
            'stock_category_id'                 =>  $param['stock_category_id'],
            'stock_statement_spin_type_id'      =>  $param['stock_statement_spin_type_id'],
            'stock_statement_total_grade_id'    =>  $param['stock_statement_total_grade_id'],
        ]);
    }

    public static function updateCompanyStock($param, $id)
    {
        return CompanyStock::find($id)->update([
            'initial_stocks'                    =>  $param['initial_stocks'],
            'total_buy'                         =>  $param['total_buy'],
            'buying_price'                      =>  $param['buying_price'],
            'total_spin'                        =>  $param['total_spin'],
            'stock_balance'                     =>  $param['stock_balance'],
            'spin_result'                       =>  $param['spin_result'],
            'fax_number'                        =>  $param['fax_number'],
            'total_sell'                        =>  $param['total_sell'],
            'selling_price'                     =>  $param['selling_price'],
            'stock_at'                          =>  $param['stock_at'],
            'stock_type_id'                     =>  $param['stock_type_id'],
            'stock_category_id'                 =>  $param['stock_category_id'],
            'stock_statement_spin_type_id'      =>  $param['stock_statement_spin_type_id'],
            'stock_statement_total_grade_id'    =>  $param['stock_statement_total_grade_id'],
        ]);
    }

    public static function findLatest3StockStatement()
    {
        return CompanyStock::orderby('stock_at','desc')->limit(3)->get();
    }

    public static function findAllCompanyStocksUsingDatatableFormat($param)
    {
        $companyStock =  CompanyStock::distinct()
                    ->when(isset($param['company_id']), function ($company) use ($param) {
                        $company->where('company_id', $param['company_id']);
                    })
                    ->when(isset($param['id']), function ($company) use ($param) {
                        $company->where('id', $param['id']);
                    });
                
        return datatables()->of($companyStock)
        ->addIndexColumn()
        ->addColumn('stock_at', function ($companyStock) {
            return $companyStock->stock_at->format('d/m/Y');
        })
        ->addColumn('stock_category', function ($companyStock) {
            return $companyStock->category->name;
        })
        ->addColumn('stock_type', function ($companyStock) {
            return $companyStock->type->name;
        })
        ->addColumn('initial_stock', function ($companyStock) {
            return (isset($companyStock->initial_stock))? number_format($companyStock->initial_stock):null;
        })
         ->addColumn('stock_balance', function ($companyStock) {
            return (isset($companyStock->stock_balance))? number_format($companyStock->stock_balance):null;
        })
        ->addColumn('action', function ($companyStock) use ($param) {
            $param['premise_id']= (isset($param['premise_id']))? $param['premise_id'] : null;
            $checked = ($param['premise_id'] == $companyStock->id)? 'checked': null;
       
            return view(
                'license-application.component.premise.partials.radiobutton',
                [
                'id'    =>  encrypt($companyStock->id),
                'checked'   =>  $checked
            ]
            )->render();
        })
        ->addColumn('edit', function ($companyStock) use($param) {
            $disabled = (isset($param['id']))? 'disabled': null;

            return view(
                'license-application.component.premise.partials.button',
                [
                'id'        =>  encrypt($companyStock->id),
                'disabled'  =>  $disabled
            ]
            )->render();
        })
        ->addColumn('company_name', function ($companyStock) {
            return $companyStock->company->name ?? '-';
        })
        ->rawColumns(['edit','action'])
        ->make(true);
    }

    public static function findCompanyStockById($id)
    {
        return CompanyStock::with([
            'businessType',
            'buildingType',
            'parliament',
            'dun',
            'district',
            'state'
        ])->find($id);
        
        //return CompanyPremise::find($id);
    }
}
