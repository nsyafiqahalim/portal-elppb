<?php

namespace App\DataObjects\ReferenceData;

use DB;

use App\Models\ReferenceData\CompanyStockStatement;

use Carbon\Carbon;

class CompanyStockStatementDataObject
{
    public static function addCompanyStockStatement($param)
    {
        return  CompanyStockStatement::firstOrCreate([
                'license_application_id'                    =>  $param['license_application_id'],
                'license_application_type_id'               =>  $param['license_application_type_id'],
                'status_id'                                 =>  $param['status_id'],
                'license_type_id'                           =>  $param['license_type_id'],
            ]);
    }

    public static function findCompanyStockStatementUsingDatatableFormat($param)
    {
        $companyStockStatement =  CompanyStockStatement::distinct()
        ->with([
            'stockCategory',
            'spinType',
            'totalGrade',
            'riceGrade',
            'stockType'
        ])
        ->when(isset($param['company_id']), function ($company) use($param) {
            $company->where('company_id', $param['company_id']);
        })->orderby('id','desc');
                
        return datatables()->of($companyStockStatement)
        ->addIndexColumn()
       ->addColumn('stock_category', function ($companyStockStatement) {
            return $companyStockStatement->stockCategory->name ?? '-';
        })
        ->addColumn('period', function ($companyStockStatement) {
            return $companyStockStatement->stockStatementPeriod->label ?? '-';
        })
        ->addColumn('total_grade', function ($companyStockStatement) {
            return $companyStockStatement->totalGrade->name ?? '-';
        })
        ->addColumn('rice_grade', function ($companyStockStatement) {
            return $companyStockStatement->riceGrade->name ?? '-';
        })
        ->addColumn('stock_type', function ($companyStockStatement) {
            return $companyStockStatement->stockType->name ?? '-';
        })
        ->addColumn('initial_stock', function ($companyStockStatement) {
            //return $companyStockStatementType->name ?? '-';
            return number_format($companyStockStatement->initial_stock,2) ?? '0.00';
        })
        ->addColumn('balance_stock', function ($companyStockStatement) {
            //return $companyStockStatementType->name ?? '-';
            return number_format($companyStockStatement->balance_stock,2) ?? '0.00';
        })
        ->rawColumns(['status','action','license_information'])
    ->make(true);
    }
}
