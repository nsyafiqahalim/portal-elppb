<?php
namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\CompanyPartner;

class CompanyPartnerDataObject
{
    public static function addNewCompanyPartner($param)
    {
        return CompanyPartner::create([
            'name'                      =>  $param['name'],
            'race_type_id'              =>  $param['race_type_id'],
            'race_id'                   =>  $param['race_id'],
            'is_citizen'                =>  $param['is_citizen'],
            'total_share'               =>  $param['total_share'],
            'share_percentage'          =>  $param['share_percentage'],
            'phone_number'              =>  $param['phone_number'],
            'email'                     =>  $param['email'],
            'nric'                      =>  $param['nric'],
            'passport'                  =>  $param['passport'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'company_id'                =>  $param['company_id'],
        ]);
    }

    public static function updateCompanyPartner($param,$id)
    {
        return CompanyPartner::find($id)->update([
            'name'                      =>  $param['name'],
            'race_type_id'              =>  $param['race_type_id'],
            'race_id'                   =>  $param['race_id'],
            'is_citizen'                =>  $param['is_citizen'],
            'total_share'               =>  $param['total_share'],
            'share_percentage'          =>  $param['share_percentage'],
            'phone_number'              =>  $param['phone_number'],
            'email'                     =>  $param['email'],
            'nric'                      =>  $param['nric'],
            'passport'                  =>  $param['passport'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'company_id'                =>  $param['company_id'],
        ]);
    }

    public static function findNonCitizenTotalShareByCompanyId($companyId, $excludedPartnerId = null)
    {
        return CompanyPartner::where('company_id',$companyId)
        ->when($excludedPartnerId, function ($partner) use ($excludedPartnerId) {
                $partner->where('id','<>', $excludedPartnerId);
        })
        ->get()->sum('share_percentage');
    }

    public static function findAllCompanyPartnersUsingDatatableFormat($param)
    {
        $companyPatners =  CompanyPartner::distinct()
                    ->when(isset($param['company_id']), function ($company) use ($param) {
                        $company->where('company_id', $param['company_id']);
                    });
                
        return datatables()->of($companyPatners)
        ->addIndexColumn()
        ->addColumn('status', function ($companyPartner) {
            return ($companyPartner->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($companyPartner) {
            return convertAddressIntoString($companyPartner);
        })->addColumn('ownership_type', function ($companyPartner) {
            return $companyPartner->ownershipType->name ?? '-';
        })
        ->addColumn('race', function ($companyPartner) {
            return $companyPartner->race->name ?? '-';
        })
        ->addColumn('nric', function ($companyPartner) {
            return $companyPartner->nric ?? '-';
        })
        ->addColumn('citizen', function ($companyPartner) {
            return ($companyPartner->is_citizen == 1) ? 'Warganegara' : 'Bukan Warganegara';
        })
        ->addColumn('action', function ($companyPartner) {
            $encryptedId    = encrypt($companyPartner->id);
            $arrayedCompanyPartner                  =   $companyPartner->toArray();
            $arrayedCompanyPartner['citizen']     =   ($companyPartner->is_citizen == 1) ? 'Warganegara' : 'Bukan Warganegara';
            //$arrayedCompanyPartner['address']   =   convertAddressIntoString($companyPartner);
            
            return view(
                'license-application.component.partner.partials.button',
                [
                'id'    =>  $encryptedId,
                'companyPartner'    =>  json_encode($arrayedCompanyPartner),

            ]
            )->render();
        })->make(true);
    }

    public static function findCompanyPartnerById($id)
    {
        return CompanyPartner::with('raceType','race','state')->find($id);
    }

    public static function delete($id)
    {
        return CompanyPartner::find($id)->delete();
    }

    public static function findAllActiveCompanyPartners()
    {
        $companyPartners = CompanyPartner::with('raceType','race','state')->get();

        return $companyPartners;
    }
}
