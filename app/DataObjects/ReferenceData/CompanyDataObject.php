<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\Company;

class CompanyDataObject
{
    public static function createNewCompany($param)
    {
        return Company::create([
            'name'                  =>  $param['name'],
            'company_type_id'       =>  $param['company_type_id'],
            'registration_number'   =>  $param['registration_number'],
            'old_registration_number'   =>  $param['old_registration_number'],
            'expiry_date'           =>  $param['expiry_date'],
            'paidup_capital'        =>  $param['paidup_capital'],
            'user_id'               =>  $param['user_id'],
        ]);
    }
    
    public static function updateCompany($param,$companyId)
    {
        $company    =    Company::find($companyId);
        $company->update([
            'name'                  =>  $param['name'],
            'company_type_id'       =>  $param['company_type_id'],
            'registration_number'   =>  $param['registration_number'],
            'expiry_date'           =>  $param['expiry_date'],
            'paidup_capital'        =>  $param['paidup_capital']
        ]);

        return $company;
    }

    public static function findAllActiveCompanys()
    {
        return Company::activeOnly()->get();
    }

    public static function findCompanyyById($id)
    {
        return Company::find($id);
    }

    public static function findCompanyByQueries($param)
    {
        return Company::distinct()->select(['id','name'])
                ->where('user_id',$param['user_id'])
                ->where(function($company) use($param){
                    $company->where('registration_number','like' ,'%'.$param['q'].'%')
                    ->orWhere('name','like' ,'%'.$param['q'].'%')
                    ->orWhere('old_registration_number','like' ,'%'.$param['q'].'%');
                })
                ->get();
    }
    public static function findAllCompanyUsingDatatableFormat($param)
    {
        $companies =  Company::with(['address','companyType','approvalLetters'])->distinct()
                    ->when(isset($param['user_id']), function ($company) use ($param) {
                        $company->where('user_id', $param['user_id']);
                    })
                    ->when(isset($param['has_approval_letter']), function ($company) use ($param) {
                        if($param['has_approval_letter'] == 1){
                            $company->whereHas('approvalLetters',function($approvalLetter){
                                $approvalLetter->whereHas('status',function($status){
                                    $status->where('code','SURAT_KELULUSAN_BERSYARAT_AKTIF');
                                });
                            });
                        }
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return convertAddressIntoString($company->address);
            } else {
                return '-';
            }
        })->addColumn('company_type', function ($company) {
            return $company->companyType->name ?? '-';
        })
        ->addColumn('expiry_date', function ($company) {
            return (isset($company->expiry_date)) ? $company->expiry_date->format('d/m/Y') : '-';
        })
        ->addColumn('formatteed_paidup_capital', function ($company) {
            return number_format($company->paidup_capital, 2);
        })
        ->addColumn('edit', function ($company) use ($param) {
            return view(
                'license-application.component.company.partials.button',
                [
                'id'    =>  encrypt($company->id)
            ]
            )->render();
        })
        ->addColumn('action', function ($company) use ($param) {
            $decryptedID = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $checked = ($decryptedID == $company->id)? 'checked': null;
            
            $arrayedCompany  = $company->toArray();
            $arrayedCompany['expiry_date']  =   (isset($company->expiry_date))? $company->expiry_date->format('d-m-Y'): '-';
            $arrayedCompany['old_registration_number']  =   (isset($company->old_registration_number))? $company->old_registration_number: '-';
            $arrayedCompany['paidup_capital']  =   'RM'.(isset($company->paidup_capital))? number_format($company->paidup_capital,2) : '0.00';
            
            $arrayedCompany['address'] = convertAddressIntoString($company->address);
            
            unset($arrayedCompany['id']);
            unset($arrayedCompany['user_id']);
           
            unset($arrayedCompany['company_type']['id']);
            unset($arrayedCompany['company_type']['old_id']);
            unset($arrayedCompany['approvalLetters']['id']);
            unset($arrayedCompany['state']['id']);
            unset($arrayedCompany['company_type_id']);
            unset($arrayedCompany['state_id']);

            $encryptedId = encrypt($company->id);
            return view(
                'license-application.component.company.partials.radiobutton',
                [
                'id'    =>  $encryptedId,
                'key'    =>  md5($encryptedId),
                'checked'   =>  $checked,
                'company'   =>  json_encode($arrayedCompany)
            ]
            )->render();
        })
        ->rawColumns(['edit','action','address'])->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::with([
            'companyType',
            'businessType',
            'address'
            ])->findOrFail($id);
    }
}
