<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;
use Webpatser\Uuid\Uuid;

use App\Models\ReferenceData\CompanyStore;

class CompanyStoreDataObject
{
    public static function addNewCompanyStore($param)
    {
        return CompanyStore::create([
            //'name'                  =>  $param['name'],
            'unique_id_identifier'      =>  Uuid::generate(1),
            'store_ownership_type_id'       =>  $param['store_ownership_type_id'],
            'building_type_id'              =>  $param['building_type_id'],
            'district_id'                   =>  $param['district_id'],
            'address_1'                     =>  $param['address_1'],
            'address_2'                     =>  $param['address_2'],
            'address_3'                     =>  $param['address_3'],
            'postcode'                      =>  $param['postcode'],
            'state_id'                      =>  $param['state_id'],
            'company_id'                    =>  $param['company_id'],
            'email'                         =>  $param['email'],
            'phone_number'                  =>  $param['phone_number'],
            'fax_number'                    =>  $param['fax_number'],
        ]);
    }

    public static function updateCompanyStore($param , $id){
        return CompanyStore::find($id)->update([
            //'name'                  =>  $param['name'],
            'store_ownership_type_id'       =>  $param['store_ownership_type_id'],
            'building_type_id'              =>  $param['building_type_id'],
            'district_id'                   =>  $param['district_id'],
            'address_1'                     =>  $param['address_1'],
            'address_2'                     =>  $param['address_2'],
            'address_3'                     =>  $param['address_3'],
            'postcode'                      =>  $param['postcode'],
            'state_id'                      =>  $param['state_id'],
            'company_id'                    =>  $param['company_id'],
            'email'                         =>  $param['email'],
            'phone_number'                  =>  $param['phone_number'],
            'fax_number'                    =>  $param['fax_number'],
        ]);
    }

    public static function findAllActiveCompanyStores()
    {
        $Companys = CompanyStore::activeOnly()->get();

        return $Companys;
    }

    

    public static function findAllCompanyStoresUsingDatatableFormat($param)
    {
        $companies =  CompanyStore::with(
            'buildingType',
            'storeOwnershipType',
            'state',
            'businessType',
            'district')
        ->distinct()
                    ->when(isset($param['company_id']), function ($company) use ($param) {
                        $company->where('company_id', $param['company_id']);
                    })->when(isset($param['id']), function ($company) use ($param) {
                        $company->where('id', $param['id']);
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('status', function ($companyStore) {
            return ($companyStore->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($companyStore) {
            return $companyStore->address_1.','
                    .$companyStore->address_2.','
                    .$companyStore->address_3.','
                    .$companyStore->postcode.','
                    .$companyStore->state->name.',';
        })->addColumn('store_ownership_type', function ($companyStore) {
            return $companyStore->storeOwnershipType->name ?? '-';
        })->addColumn('building_type', function ($companyStore) {
            return $companyStore->buildingType->name ?? '-';
        })
        ->addColumn('company_name', function ($companyStore) {
            return $companyStore->company->name ?? '-';
        })
        ->addColumn('action', function ($companyStore) use ($param) {
            if(isset($param['store_id'])){
                $checked    =   (in_array($companyStore->id,$param['store_id']))?'checked': null;
            }
            else{
                $checked = null;
            }
            
            $arrayedCompanyStore              =   $companyStore->toArray();
            $arrayedCompanyStore['address']   =   convertAddressIntoString($companyStore);
            $arrayedCompanyStore['load']      =     10;

            return view(
                'license-application.component.store.partials.radiobutton',
                [
                'id'        =>  encrypt($companyStore->id),
                'uuid'      =>  $companyStore->unique_id_identifier,
                'checked'   =>  $checked,
                'store'     =>  json_encode($arrayedCompanyStore)
            ]
            )->render();
        })
        ->addColumn('edit', function ($companyStore) use($param) {
            $disabled = (isset($param['id']))? 'disabled': null;

            return view(
                'license-application.component.store.partials.button',
                [
                'id'        =>  encrypt($companyStore->id),
                'disabled'  =>  $disabled
            ]
            )->render();
        })
         ->rawColumns(['edit','action'])
        ->make(true);
    }

    public static function findCompanyStoreById($id)
    {
        return CompanyStore::find($id);
    }
}
