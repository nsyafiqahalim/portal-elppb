<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;

use Illuminate\Auth\Notifications\VerifyEmail as EmailVerificationNotification;

class EmailVerification extends EmailVerificationNotification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      $verificationUrl = $this->verificationUrl($notifiable);

      if (static::$toMailCallback) {
          return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
      }

      return (new MailMessage)
          ->subject(Lang::get('Pengesahan Akaun - Sistem eLPPB'))
          ->line(Lang::get('Sila klik butang dibawah untuk mengesahkan akaun eLPPB anda.'))
          ->action(Lang::get('Pengesahan Akaun'), $verificationUrl)
          ->line(Lang::get('Jika anda tidak membuat pendaftaran pengguna, tiada tindakan lanjut diperlukan.'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
