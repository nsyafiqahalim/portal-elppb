<?php

namespace App\Traits;

use App\User;

trait BasicModelTrait
{
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function getCreatedDateAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    public function getUpdatedDateAttribute()
    {
        return $this->updated_at->format('d/m/Y');
    }

    public function geEncryptedIdAttribute()
    {
        return encrypt($this->id);
    }
}
