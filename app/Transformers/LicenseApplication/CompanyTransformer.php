<?php
namespace App\Transformers\LicenseApplication;

use App\Models\ReferenceData\Company as ReferenceDataCompany;

class CompanyTransformer{

    public static function transfromReferenceDataCompany(ReferenceDataCompany $company){
        $arrayCompany = $company->toArray();
        return [
            'name'                          =>  $arrayCompany['name'],
            'user_id'                       =>  $arrayCompany['user_id'],
            'company_type_id'               =>  $arrayCompany['company_type_id'],
            'company_id'                    =>  $arrayCompany['id'],
            'company_type_name'             =>  $arrayCompany['company_type']['name'],
            'registration_number'           =>  $arrayCompany['registration_number'],
            'expiry_date'                   =>  $arrayCompany['expiry_date'],
            'paidup_capital'                =>  $arrayCompany['paidup_capital'],
        ];
    }
}