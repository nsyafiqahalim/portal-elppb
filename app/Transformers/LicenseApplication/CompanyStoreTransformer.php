<?php

namespace App\Transformers\LicenseApplication;

use App\Models\ReferenceData\CompanyStore as ReferenceDataCompanyStore;

class CompanyStoreTransformer{

    public static function transfromReferenceDataCompanyStore($companyStores){
       
        foreach($companyStores as $cs){
            $ids[]  =   decrypt($cs);
        }

        $companyStores = ReferenceDataCompanyStore::whereIn('id',$ids)->get();

        return  $companyStores->map(function($companyStore){
            $arrayCompanyStore = $companyStore->toArray();

            return [
                'company_store_id'                  =>  $arrayCompanyStore['id'],
                'store_ownership_type_id'           =>  $arrayCompanyStore['store_ownership_type_id'],
                'store_ownership_type_name'         =>  $arrayCompanyStore['storeOwnershipType']['name']  ?? null,
                'building_type_id'                  =>  $arrayCompanyStore['building_type_id'],
                'building_type_name'                =>  $arrayCompanyStore['buildingType']['name']  ?? null,
                'company_id'                        =>  $arrayCompanyStore['company_id'],    
                'district_id'                       =>  $arrayCompanyStore['district_id'],
                'district_name'                     =>  $arrayCompanyStore['district']['name']  ?? null,
                'address_1'                         =>  $arrayCompanyStore['address_1'],
                'address_2'                         =>  $arrayCompanyStore['address_2'],
                'address_3'                         =>  $arrayCompanyStore['address_3'],
                'postcode'                          =>  $arrayCompanyStore['postcode'],
                'state_id'                          =>  $arrayCompanyStore['state_id'],
                'state_name'                        =>  $arrayCompanyStore['state']['name']  ?? null,
                'phone_number'                      =>  $arrayCompanyStore['phone_number'],
                'fax_number'                        =>  $arrayCompanyStore['fax_number'],
                'email'                             =>  $arrayCompanyStore['email']
            ];
        });

        
    }
}