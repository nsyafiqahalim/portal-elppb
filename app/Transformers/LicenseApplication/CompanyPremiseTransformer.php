<?php
namespace App\Transformers\LicenseApplication;

use App\Models\ReferenceData\CompanyPremise as ReferenceDataCompanyPremise;

class CompanyPremiseTransformer{

    public static function transfromReferenceDataCompanyPremise(ReferenceDataCompanyPremise $companyPremise){
        $arrayCompanyPremise = $companyPremise->toArray();
        return [
            'company_premise_id'            =>  $arrayCompanyPremise['id'],
            'business_type_id'              =>  $arrayCompanyPremise['business_type_id'],
            'business_type_name'            =>  $arrayCompanyPremise['business_type']['name']  ?? null,
            'building_type_id'              =>  $arrayCompanyPremise['building_type_id'],
            'building_type_name'            =>  $arrayCompanyPremise['building_type']['name']  ?? null,
            'dun_id'                        =>  $arrayCompanyPremise['dun_id'],
            'dun_name'                      =>  $arrayCompanyPremise['dun']['name']  ?? null,
            'company_id'                    =>  $arrayCompanyPremise['company_id'],
            'parliament_id'                 =>  $arrayCompanyPremise['parliament_id'],
            'district_id'                   =>  $arrayCompanyPremise['district_id'],
            'district_name'                 =>  $arrayCompanyPremise['distrinct']['name']  ?? null,
            'parliament_name'               =>  $arrayCompanyPremise['parliament']['name']  ?? null,
            'address_1'                     =>  $arrayCompanyPremise['address_1'],
            'address_2'                     =>  $arrayCompanyPremise['address_2'],
            'address_3'                     =>  $arrayCompanyPremise['address_3'],
            'postcode'                      =>  $arrayCompanyPremise['postcode'],
            'state_id'                      =>  $arrayCompanyPremise['state_id'],
            'state_name'                    =>  $arrayCompanyPremise['state']['name']  ?? null,
            'phone_number'                  =>  $arrayCompanyPremise['phone_number'],
            'fax_number'                    =>  $arrayCompanyPremise['fax_number'],
            'email'                         =>  $arrayCompanyPremise['email'], 
        ];
    }
}