<?php

namespace App\Transformers\LicenseApplication;

use App\Models\ReferenceData\CompanyPartner as ReferenceDataCompanyPartner;

class CompanyPartnerTransformer{

    public static function transfromReferenceDataCompanyPartner($companyPartners){

        return  $companyPartners->map(function($parter){
            $arrayedPartner = $parter->toArray();

            return 
            [
                'company_partner_id'            =>  $arrayedPartner['id'],
                'name'                          =>  $arrayedPartner['name'],
                'race_type_id'                  =>  $arrayedPartner['race_type_id'],
                'race_type_name'                =>  $arrayedPartner['race_type']['name'],
                'is_citizen'                    =>  $arrayedPartner['is_citizen'],
                'citizen_name'                  =>  ($arrayedPartner['is_citizen']  == 1)? 'Warganegara' : 'Bukan Warganegara',
                'race_id'                       =>  $arrayedPartner['race_id'],
                'race_name'                     =>  $arrayedPartner['race']['name'],
                'total_share'                   =>  $arrayedPartner['total_share'],
                'share_percentage'              =>  $arrayedPartner['share_percentage'],
                'phone_number'                  =>  $arrayedPartner['phone_number'],
                'email'                         =>  $arrayedPartner['email'],
                'nric'                          =>  $arrayedPartner['nric'],
                'address_1'                     =>  $arrayedPartner['address_1'],
                'address_2'                     =>  $arrayedPartner['address_2'],
                'address_3'                     =>  $arrayedPartner['address_3'],
                'postcode'                      =>  $arrayedPartner['postcode'],
                'state_id'                      =>  $arrayedPartner['state_id'],
                'state_name'                    =>  $arrayedPartner['state']['name'],
            ];
        });
    }
}