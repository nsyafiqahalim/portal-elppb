<?php
namespace App\Transformers\LicenseApplication;

use App\Models\ReferenceData\CompanyAddress as ReferenceDataCompanyAddress;

class CompanyAddressTransformer{

    public static function transfromReferenceDataCompanyAddress(ReferenceDataCompanyAddress $companyAddress){
        $arrayCompanyAddress = $companyAddress->toArray();
        
        return [
            'company_address_id'            =>  $arrayCompanyAddress['id'],
            'company_id'                    =>  $arrayCompanyAddress['company_id'],
            'address_1'                     =>  $arrayCompanyAddress['address_1'],
            'address_2'                     =>  $arrayCompanyAddress['address_2'],
            'address_3'                     =>  $arrayCompanyAddress['address_3'],
            'postcode'                      =>  $arrayCompanyAddress['postcode'],
            'state_id'                      =>  $arrayCompanyAddress['state_id'],
            'state_name'                    =>  $arrayCompanyAddress['state']['name']  ?? null,
            'phone_number'                  =>  $arrayCompanyAddress['phone_number'],
            'fax_number'                    =>  $arrayCompanyAddress['fax_number'],
            'email'                         =>  $arrayCompanyAddress['email'], 
        ];
    }
}