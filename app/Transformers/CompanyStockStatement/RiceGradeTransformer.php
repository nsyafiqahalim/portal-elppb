<?php
namespace App\Transformers\CompanyStockStatement;

class RiceGradeTransformer{

    public static function transformIntoStockStatement($param){
        
        return [
            'stock_category_id'             =>  $param['stock_category_id'],
            'company_id'                    =>  $param['company_id'],
            'license_type_id'               =>  $param['license_type_id'],
            'rice_grade_id'                 =>  $param['grade_type_id'],
            'initial_stock'                 =>  $param['initial_stock'],
            'total_buy'                     =>  $param['total_buy'],
            'total_sell'                    =>  $param['total_sell'],
            'buying_price'                  =>  $param['buying_price'],
            'stock_balance'                 =>  $param['stock_balance'], 
            'selling_price'                 =>  $param['selling_price'],
            'stock_statement_period_id'     =>  $param['stock_statement_period_id']
        ];
    }
}