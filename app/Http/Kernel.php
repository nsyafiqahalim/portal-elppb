<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,

        'StoreCompanyValidationMiddleware'  =>\App\Http\Middleware\ReferenceData\StoreCompanyValidationMiddleware::class,
        'StoreCompanyPartnerValidationMiddleware'  =>\App\Http\Middleware\ReferenceData\StoreCompanyPartnerValidationMiddleware::class,
        'StoreRetailCompanyPartnerValidationMiddleware'  =>\App\Http\Middleware\ReferenceData\StoreRetailCompanyPartnerValidationMiddleware::class,
        'StoreCompanyPremiseValidationMiddleware'  =>\App\Http\Middleware\StoreCompanyPremiseValidationMiddleware::class,
        'StoreCompanyStoreValidationMiddleware'  =>\App\Http\Middleware\StoreCompanyStoreValidationMiddleware::class,
        
        'StoreCompanyStockValidationMiddleware'  =>\App\Http\Middleware\ReferenceData\CompanyStockStatement\StoreValidationMiddleware::class,
        'UpdateCompanyStockValidationMiddleware'  =>\App\Http\Middleware\ReferenceData\CompanyStockStatement\UpdateValidationMiddleware::class,
        
        /**
         * Lesen Runcit
         */
        'StoreRetailNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\NewApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\NewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\NewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\NewApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreNewRetailRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftNewRetailRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftNewRetailRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftNewRetailRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreRetailCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\CancellationApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\CancellationApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\CancellationApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\CancellationApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreRetailReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ReplacementApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ReplacementApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ReplacementApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ReplacementApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreRetailCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\CompanyApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\CompanyApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\CompanyApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\CompanyApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreRetailLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\LicenseApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\LicenseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\LicenseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\LicenseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreRetailPremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\PremiseApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailPremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\PremiseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailPremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\PremiseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailPremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\PremiseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreRetailRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftRetailRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftRetailRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftRetailRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        /**
         * Lesen Runcit
         */

         /**
         * Lesen Borong
         */
        'StoreWholesaleNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\NewApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\NewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\NewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\NewApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreNewWholesaleRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftNewWholesaleRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftNewWholesaleRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftNewWholesaleRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreWholesaleCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\CancellationApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\CancellationApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\CancellationApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\CancellationApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreWholesaleReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ReplacementApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ReplacementApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ReplacementApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ReplacementApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreWholesaleCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\CompanyApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\CompanyApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\CompanyApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\CompanyApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreWholesaleLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\LicenseApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\LicenseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\LicenseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\LicenseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreWholesalePremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\PremiseApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesalePremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\PremiseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesalePremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\PremiseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesalePremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\PremiseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreWholesaleStoreApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\StoreApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleStoreApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\StoreApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleStoreApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\StoreApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleStoreApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\StoreApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreWholesaleRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftWholesaleRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftWholesaleRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftWholesaleRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        
        /**
         * Lesen Borong
         */

         /**
         * Lesen Import
         */
        'StoreImportNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\NewApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\NewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\NewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\NewApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreNewImportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftNewImportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftNewImportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftNewImportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreImportCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\CancellationApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\CancellationApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\CancellationApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\CancellationApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreImportReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ReplacementApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ReplacementApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ReplacementApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ReplacementApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreImportCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\CompanyApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\CompanyApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\CompanyApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\CompanyApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreImportLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\LicenseApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\LicenseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\LicenseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\LicenseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreImportPremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\PremiseApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportPremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\PremiseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportPremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\PremiseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportPremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\PremiseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreImportStoreApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\StoreApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportStoreApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\StoreApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportStoreApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\StoreApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportStoreApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\StoreApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreImportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftImportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftImportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftImportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Import\ChangeApplication\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        /**
         * Lesen Import
         */

         /**
         * Lesen Export
         */
        'StoreExportNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\NewApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportNewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\NewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\NewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportNewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\NewApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreNewExportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftNewExportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftNewExportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftNewExportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreExportCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\CancellationApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportCancellationApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\CancellationApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\CancellationApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportCancellationApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\CancellationApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        'StoreExportReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ReplacementApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportReplacementApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ReplacementApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ReplacementApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportReplacementApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ReplacementApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreExportCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\CompanyApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportCompanyApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\CompanyApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\CompanyApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportCompanyApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\CompanyApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreExportLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\LicenseApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportLicenseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\LicenseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\LicenseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportLicenseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\LicenseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreExportPremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\PremiseApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportPremiseApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\PremiseApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportPremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\PremiseApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportPremiseApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\PremiseApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreExportStoreApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\StoreApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportStoreApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\StoreApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportStoreApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\StoreApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportStoreApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\StoreApplication\UpdateDraftApplicationValidationMiddleware::class,

        'StoreExportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\RenewApplication\StoreApplicationValidationMiddleware::class,
        'DraftExportRenewApplicationValidationMiddleware'             =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\RenewApplication\DraftApplicationValidationMiddleware::class,
        'SubmitDraftExportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\RenewApplication\SubmitDraftApplicationValidationMiddleware::class,
        'UpdateDraftExportRenewApplicationValidationMiddleware'       =>\App\Http\Middleware\LicenseApplication\Export\ChangeApplication\RenewApplication\UpdateDraftApplicationValidationMiddleware::class,
        
        /**
         * Lesen Export
         */


         /**
         * Lesen Beli Padi
         */
        'StoreNewBuyPaddyNewApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\StoreNewApplicationValidationMiddleware::class,
        'StoreDraftBuyPaddyNewApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\UpdateNewApplicationValidationMiddleware::class,
        'StoreNewBuyPaddyRenewApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\StoreRenewApplicationValidationMiddleware::class,
        'StoreDraftBuyPaddyRenewApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\UpdateRenewApplicationValidationMiddleware::class,
        'StoreNewBuyPaddyChangeApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\StoreChangeApplicationValidationMiddleware::class,
        'StoreDraftBuyPaddyChangeApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\UpdateChangeApplicationValidationMiddleware::class,
        'StoreCancellationBuyPaddyApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\StoreCancellationApplicationValidationMiddleware::class,
        'StoreDraftBuyPaddyCancellationApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\UpdateCancellationApplicationValidationMiddleware::class,
        'StoreReplacementBuyPaddyApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\StoreReplacementApplicationValidationMiddleware::class,
        'StoreDraftBuyPaddyReplacementApplicationValidationMiddleware'  =>\App\Http\Middleware\LicenseApplication\BuyPaddy\UpdateReplacementApplicationValidationMiddleware::class,
        
        /**
         * Lesen Beli Padi
         */


         /**
         * Surat Kelulusan Membeli Padi
         */
        'StoreNewBuyPaddyApprovalLetterApplicationValidationMiddleware'  =>\App\Http\Middleware\ApprovalLetterApplication\BuyPaddy\StoreNewApplicationValidationMiddleware::class,
        'UpdateNewBuyPaddyApprovalLetterApplicationValidationMiddleware'  =>\App\Http\Middleware\ApprovalLetterApplication\BuyPaddy\UpdateNewApplicationValidationMiddleware::class,
         /**
         * Surat Kelulusan Membeli Padi
         */

          /**
         * Surat Kelulusan Kilang Padi
         */
        'StoreNewFactoryPaddyApprovalLetterApplicationValidationMiddleware'  =>\App\Http\Middleware\ApprovalLetterApplication\FactoryPaddy\StoreNewApplicationValidationMiddleware::class,
        'UpdateNewFactoryPaddyApprovalLetterApplicationValidationMiddleware'  =>\App\Http\Middleware\ApprovalLetterApplication\FactoryPaddy\UpdateNewApplicationValidationMiddleware::class,
         /**
         * Surat Kelulusan Kilang Padi
         */
];

    /**
         * The priority-sorted list of middleware.
         *
         * This forces non-global middleware to always be in the given order.
         *
         * @var array
         */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Routing\Middleware\ThrottleRequests::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
