<?php

namespace App\Http\Middleware;

use Closure;

class StoreCompanyPremiseValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'premise_business_type_id' => ['required', 'string' , 'max:255'],
            'premise_building_type_id' => ['required', 'string' , 'max:255'],
            'premise_dun_id' => ['required', 'string' , 'max:255'],
            'premise_parliament_id' => ['required', 'string' , 'max:255'],
            'premise_district_id' => ['required', 'string' , 'max:255'],
            'premise_address_1' => ['required', 'string' , 'max:255'],
            'premise_address_2' => ['nullable', 'string' , 'max:255'],
            'premise_address_3' => ['nullable', 'string' , 'max:255'],
            'premise_postcode' => ['required', 'string' , 'digits:5'],
            'premise_state_id' => ['required', 'string' , 'max:255'],
            'premise_phone_number' => ['required', 'alpha_dash' ,'max:12'],
            'premise_fax_number' => ['nullable', 'string' ,'max:12'],
            'premise_email' => ['required', 'email' , 'max:255'],
        ],[
            'premise_dun_id.required'  =>  'Dun diperlukan. Ruangan ini wajib diisi',
            'premise_parliament_id.required'  =>  'Parlimen diperlukan. Ruangan ini wajib diisi',
            'premise_building_type_id.required'  =>  'Jenis Bangunan diperlukan. Ruangan ini wajib diisi',
            'premise_business_type_id.required'  =>  'Jenis Perniagaan Pengenalan diperlukan. Ruangan ini wajib diisi',

            'premise_address_1.required'  =>  'Alamat 1 diperlukan. Ruangan ini wajib diisi',
            'premise_postcode.required'  =>  'Poskod diperlukan. Ruangan ini wajib diisi',
            'premise_state_id.required'  =>  'Negeri diperlukan. Ruangan ini wajib diisi',
            'premise_district_id.required'  =>  'Daerah diperlukan. Ruangan ini wajib diisi',
            'premise_phone_number.required'  =>  'Nombor Telefon diperlukan. Ruangan ini wajib diisi',
            'premise_email.required'  =>  'Emel Telefon diperlukan. Ruangan ini wajib diisi',
            'premise_phone_number' => ['required', 'alpha_dash' ,'max:12'],
            'premise_fax_number' => ['nullable', 'string' ,'max:12'],
            'premise_email' => ['required', 'email' , 'max:255'],
        ]);

        return $next($request);
    }
}
