<?php

namespace App\Http\Middleware\LicenseApplication\Retail\ChangeApplication\CompanyApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class UpdateDraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);
        
        $attachmentValidations = AttachmentDataObject::addDraftValidateAttachments($param);

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'company_name' =>['required', 'string' , 'max:255'],
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'company_name.required'   =>  ['Nama Syarikat diperlukan'],
            'company_name.max'   =>  ['Nama Syarikat hanya membenarkan 255 aksara'],
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);

        $validator->validate();
        

        return $next($request);
    }
}
