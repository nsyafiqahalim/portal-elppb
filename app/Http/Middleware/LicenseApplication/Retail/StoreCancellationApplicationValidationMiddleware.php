<?php

namespace App\Http\Middleware\LicenseApplication\Retail;

use Closure;
use  App\Models\Admin\Material;

class StoreCancellationApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'remarks' => ['required'],
            'license_application_cancellation_type_id' => ['required'],
            'cancellation' => ['required', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'],

        ], [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'remarks.required'   =>  ['Ulasan diperlukan'],
            'license_application_cancellation_type_id.required'   =>  ['Tujuan pembatalan diperlukan'],
            'cancellation.required'   =>  ['Surat persetujuan pembatalan dari rakan kongsi syarikat diperlukan'],
            'cancellation.max'   =>  ['Saiz Surat persetujuan pembatalan dari rakan kongsi syarikat perlu 20MB dan kebawah'],
            'cancellation.mimes'   =>  ['Format yang dibenarkan untuk Surat persetujuan pembatalan dari rakan kongsi syarikat adalah jpeg,png,jpg,pdf'],
        ]);

        return $next($request);
    }
}
