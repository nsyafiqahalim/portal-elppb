<?php

namespace App\Http\Middleware\LicenseApplication\Retail\NewApplication;

use Closure;

use App\DataObjects\LicenseApplication\AttachmentDataObject;

class StoreApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
        ];
        
        $attachmentValidations = AttachmentDataObject::addValidateAttachments($param);
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        $request->validate($rules,$messages);

        return $next($request);
    }
}
