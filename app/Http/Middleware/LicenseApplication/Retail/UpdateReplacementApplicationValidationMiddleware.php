<?php

namespace App\Http\Middleware\LicenseApplication\Retail;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;
use  App\Models\Admin\Material;

class UpdateReplacementApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);
        $domain =  decrypt($request->material_domain);
        
        $validator = \Validator::make($param,[
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'remarks' => ['required'],
            'license_application_replacement_type_id' => ['required'],
            'original_attachment' => ['nullable', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'],
        ], [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'remarks.required'   =>  ['Ulasan diperlukan'],
            'license_application_replacement_type_id.required'   =>  ['Tujuan penggantian diperlukan'],
            'original_attachment.max'   =>  ['Saiz salinan lampiran asal perlu 20MB dan kebawah'],
            'original_attachment.mimes'   =>  ['Format yang dibenarkan untuk salinan lampiran asal adalah jpeg,png,jpg,pdf'],
        ]);
        
        $validator->after(function ($validator) use($request,$id,$domain ){

            $materials  = Material::where('domain', $domain)->get();
            foreach ($materials as $material) {
                $attachment = AttachmentDataObject::findAttachmentByCodeAndLicenseApplicationID($material->code, $id);
                //dd($request->file($material->code));
                if ($attachment == null && $request->file($material->code) == null) {
                     $validator->errors()->add($material->code, $material->label.' diperlukan');
                }

            }
        });

        $validator->validate();

        return $next($request);
    }
}
