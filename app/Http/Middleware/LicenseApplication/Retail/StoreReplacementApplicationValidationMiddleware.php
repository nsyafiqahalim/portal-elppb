<?php

namespace App\Http\Middleware\LicenseApplication\Retail;

use Closure;

class StoreReplacementApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'remarks' => ['required'],
            'license_application_replacement_type_id' => ['required'],
            'ssm' => ['required', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'],

        ], [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'remarks.required'   =>  ['Ulasan diperlukan'],
            'license_application_replacement_type_id.required'   =>  ['Tujuan penggantian diperlukan'],
            'ssm.required'   =>  ['Salinan Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan diperlukan'],
            'ssm.max'   =>  ['Saiz salinan Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan perlu 20MB dan kebawah'],
            'ssm.mimes'   =>  ['Format yang dibenarkan untuk salinan Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan adalah jpeg,png,jpg,pdf'],
        ]);

        return $next($request);
    }
}
