<?php

namespace App\Http\Middleware\LicenseApplication\Retail\CancellationApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class DraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $attachmentValidations = AttachmentDataObject::addDraftValidateAttachments($param);

         $rules = [
            'company_id' => ['nullable'],
            'premise_id' => ['nullable'],
            'remarks' => ['nullable'],
            'license_application_cancellation_type_id' => ['nullable'],
        ];
        $messages = [
           'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'remarks.required'   =>  ['Ulasan diperlukan'],
            'license_application_cancellation_type_id.required'   =>  ['Tujuan pembatalan diperlukan'],
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $request->validate($rules,$messages);

        return $next($request);
    }
}
