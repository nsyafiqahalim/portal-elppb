<?php

namespace App\Http\Middleware\LicenseApplication\Retail;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;
use  App\Models\Admin\Material;

class UpdateRenewApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);;
        $domain =  decrypt($request->material_domain);
        $validator = \Validator::make($param,[
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'apply_load' => ['required','alpha_num'],
            'apply_duration' => ['required'],
            'ssm' => ['nullable', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'],
            'minute_meeting' => ['nullable', 'max:20000', 'mimes:jpeg,png,jpg,pdf'],
        ], [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'apply_duration.required'   =>  ['Tempoh permohonan diperlukan'],
            'apply_load.required'   =>  ['Had Muatan diperlukan'],
            'apply_load.alpha_num'   =>  ['Hanya format nombor dibenarkan'],
            'ssm.required'   =>  ['Salinan SSM / Salinan lesen perniagaan (Sabah/Sarawak) / I.R.D No 7Hantar diperlukan'],
            'ssm.max'   =>  ['Saiz salinan SSM perlu 20MB dan kebawah'],
            'ssm.mimes'   =>  ['Format yang dibenarkan untuk salinan SSM adalah jpeg,png,jpg,pdf'],
            'minute_meeting.required'   =>  ['Salinan sijil pendaftaran/cabutan minit mesyuarat diperlukan'],
            'minute_meeting.max'   =>  ['Saiz salinan sijil pendaftaran/cabutan minit mesyuarat perlu 20MB dan kebawah'],
            'minute_meeting.mimes'   =>  ['Format yang dibenarkan untuk salinan sijil pendaftaran/cabutan minit mesyuarat adalah jpeg,png,jpg,pdf'],
        ]);
        
        $validator->after(function ($validator) use($request,$id,$domain ){

            $materials  = Material::where('domain', $domain)->get();
            foreach ($materials as $material) {
                $attachment = AttachmentDataObject::findAttachmentByCodeAndLicenseApplicationID($material->code, $id);
                //dd($request->file($material->code));
                if ($attachment == null && $request->file($material->code) == null) {
                     $validator->errors()->add($material->code, $material->label.' diperlukan');
                }

            }
        });

        $validator->validate();

        return $next($request);
    }
}
