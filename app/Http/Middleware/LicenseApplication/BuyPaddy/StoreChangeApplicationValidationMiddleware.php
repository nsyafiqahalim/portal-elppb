<?php

namespace App\Http\Middleware\LicenseApplication\BuyPaddy;

use Closure;

class StoreChangeApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'store_id' => ['required'],
            'ssm' => ['required', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'],
            'minute_meeting' => ['required', 'max:20000', 'mimes:jpeg,png,jpg,pdf'],
            'sale_agreement' => ['required', 'max:20000', 'mimes:jpeg,bmp,png,jpg,pdf'],
            'account_statement' => ['required', 'max:20000', 'mimes:jpeg,png,jpg,pdf'],
        ], [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'store_id.required'   =>  ['Stor masih belum dipilih'],
            'ssm.required'   =>  ['Salinan SSM / Salinan lesen perniagaan (Sabah/Sarawak) / I.R.D No 7Hantar diperlukan'],
            'ssm.max'   =>  ['Saiz salinan SSM perlu 20MB dan kebawah'],
            'ssm.mimes'   =>  ['Format yang dibenarkan untuk salinan SSM adalah jpeg,png,jpg,pdf'],
            'minute_meeting.required'   =>  ['Salinan sijil pendaftaran/cabutan minit mesyuarat diperlukan'],
            'minute_meeting.max'   =>  ['Saiz salinan sijil pendaftaran/cabutan minit mesyuarat perlu 20MB dan kebawah'],
            'minute_meeting.mimes'   =>  ['Format yang dibenarkan untuk salinan sijil pendaftaran/cabutan minit mesyuarat adalah jpeg,png,jpg,pdf'],
            'sale_agreement.required'   =>  ['Salinan perjanjian jual beli diperlukan'],
            'sale_agreement.max'   =>  ['Saiz salinan perjanjian jual beli perlu 20MB dan kebawah'],
            'sale_agreement.mimes'   =>  ['Format yang dibenarkan untuk salinan perjanjian jual beli adalah jpeg,png,jpg,pdf'],
            'account_statement.required'   =>  ['Salinan penyata akaun 3 bulan diperlukan'],
            'account_statement.max'   =>  ['Saiz salinan penyata akaun 3 bulan perlu 20MB dan kebawah'],
            'account_statement.mimes'   =>  ['Format yang dibenarkan untuk salinan penyata akaun 3 bulan adalah jpeg,png,jpg,pdf'],
        ]);

         $request->validate($rules,$messages);
         
        return $next($request);
    }
}
