<?php

namespace App\Http\Middleware\LicenseApplication\BuyPaddy\ChangeApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class StoreRenewApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $domain = decrypt($request->material_domain);
        $attachmentValidations = AttachmentDataObject::addValidateAttachments($domain);

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'store_id' => ['required'],
            'premise_business_type_id' => ['required', 'string' , 'max:255'],
            'premise_building_type_id' => ['required', 'string' , 'max:255'],
            'premise_dun_id' => ['required', 'string' , 'max:255'],
            'premise_parliament_id' => ['required', 'string' , 'max:255'],
            'premise_district_id' => ['required', 'string' , 'max:255'],
            'premise_address_1' => ['required', 'string' , 'max:255'],
            'premise_address_2' => ['nullable', 'string' , 'max:255'],
            'premise_address_3' => ['nullable', 'string' , 'max:255'],
            'premise_postcode' => ['required', 'string' , 'digits:5'],
            'premise_state_id' => ['required', 'string' , 'max:255'],
            'premise_phone_number' => ['required', 'alpha_dash' ,'max:12'],
            'premise_fax_number' => ['nullable', 'string' ,'max:12'],
            'premise_email' => ['required', 'email' , 'max:255'],
            'apply_load' => ['required','alpha_num'],
            'company_name' =>['required', 'string' , 'max:255'],

            'store_building_type_id' => ['required', 'string' , 'max:255'],
            'store_district_id' => ['required', 'string' , 'max:255'],
            'store_address_1' => ['required', 'string' , 'max:255'],
            'store_address_2' => ['nullable', 'string' , 'max:255'],
            'store_address_3' => ['nullable', 'string' , 'max:255'],
            'store_postcode' => ['required', 'string' , 'digits:5'],
            'store_state_id' => ['required', 'string' , 'max:255'],
            'store_phone_number' => ['required', 'alpha_dash' ,'max:12'],
            'store_fax_number' => ['nullable', 'string' ,'max:12'],
            'store_email' => ['required', 'email' , 'max:255'],
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'premise_dun_id.required'  =>  'Dun premis diperlukan',
            'premise_parliament_id.required'  =>  'Parlimen premis diperlukan',
            'premise_building_type_id.required'  =>  'Jenis Bangunan premis diperlukan',
            'premise_business_type_id.required'  =>  'Jenis Perniagaan Pengenalan premis diperlukan',
            'premise_address_1.required'  =>  'Alamat 1 premis diperlukan',
            'premise_postcode.required'  =>  'Poskod premis diperlukan',
            'premise_state_id.required'  =>  'Negeri premis diperlukan',
            'premise_district_id.required'  =>  'Daerah premis diperlukan',
            'premise_phone_number.required'  =>  'Nombor Telefon premis diperlukan',
            'premise_email.required'  =>  'Emel Telefon premis diperlukan',
            'premise_phone_number' => ['required', 'alpha_dash' ,'max:12'],
            'premise_fax_number' => ['nullable', 'string' ,'max:12'],
            'premise_email' => ['required', 'email' , 'max:255'],
            'apply_load.alpha_num'   =>  ['Had Muatan Hanya membenarkan format nombor'],
            'company_name.required'   =>  ['Nama Syarikat diperlukan'],
            'company_name.max'   =>  ['Nama Syarikat hanya membenarkan 255 aksara'],

            'store_building_type_id.required'  =>  'Jenis Bangunan stor diperlukan',
            'store_address_1.required'  =>  'Alamat 1 stor diperlukan',
            'store_postcode.required'  =>  'Poskod stor diperlukan',
            'store_state_id.required'  =>  'Negeri stor diperlukan',
            'store_district_id.required'  =>  'Daerah stor diperlukan',
            'store_phone_number.required'  =>  'Nombor Telefon stor diperlukan',
            'store_email.required'  =>  'Emel stor diperlukan',
            'store_phone_number.required' => 'Nombor Telefon  stor diperlukan',
            'store_fax_number.required' => 'Nombor Faks stor hanya antara 10 hingga ke 11 digit',
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $request->validate($rules,$messages);

        return $next($request);
    }
}
