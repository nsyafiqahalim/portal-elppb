<?php

namespace App\Http\Middleware\LicenseApplication\Export\NewApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class DraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $attachmentValidations = AttachmentDataObject::addDraftValidateAttachments($param);

        $rules = [];
        $messages = [];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $request->validate($rules,$messages);

        return $next($request);
    }
}
