<?php

namespace App\Http\Middleware\LicenseApplication\Export\ChangeApplication\StoreApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class UpdateDraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);
        
        $attachmentValidations = AttachmentDataObject::addDraftValidateAttachments($param);

        $rules = [
            'store_building_type_id' => ['nullable', 'string' , 'max:255'],
            'store_district_id' => ['nullable', 'string' , 'max:255'],
            'store_address_1' => ['nullable', 'string' , 'max:255'],
            'store_address_2' => ['nullable', 'string' , 'max:255'],
            'store_address_3' => ['nullable', 'string' , 'max:255'],
            'store_postcode' => ['nullable', 'string' , 'digits:5'],
            'store_state_id' => ['nullable', 'string' , 'max:255'],
            'store_phone_number' => ['nullable', 'alpha_dash' ,'max:12'],
            'store_fax_number' => ['nullable', 'string' ,'max:12'],
            'store_email' => ['nullable', 'email' , 'max:255'],
        ];

        $messages = [
            'store_building_type_id.required'  =>  'Jenis Bangunan stor diperlukan',
            'store_address_1.required'  =>  'Alamat 1 stor diperlukan',
            'store_postcode.required'  =>  'Poskod stor diperlukan',
            'store_state_id.required'  =>  'Negeri stor diperlukan',
            'store_district_id.required'  =>  'Daerah stor diperlukan',
            'store_phone_number.required'  =>  'Nombor Telefon stor diperlukan',
            'store_email.required'  =>  'Emel stor diperlukan',
            'store_phone_number.required' => 'Nombor Telefon  stor diperlukan',
            'store_fax_number.required' => 'Nombor Faks stor hanya antara 10 hingga ke 11 digit',
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            $validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
        });

        $validator->validate();
        

        return $next($request);
    }
}
