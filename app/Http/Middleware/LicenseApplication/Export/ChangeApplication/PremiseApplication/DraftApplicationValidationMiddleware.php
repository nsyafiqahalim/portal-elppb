<?php

namespace App\Http\Middleware\LicenseApplication\Export\ChangeApplication\PremiseApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class DraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $attachmentValidations = AttachmentDataObject::addDraftValidateAttachments($param);

        $rules = [
            'premise_business_type_id' => ['nullable', 'string' , 'max:255'],
            'premise_building_type_id' => ['nullable', 'string' , 'max:255'],
            'premise_dun_id' => ['nullable', 'string' , 'max:255'],
            'premise_parliament_id' => ['nullable', 'string' , 'max:255'],
            'premise_district_id' => ['nullable', 'string' , 'max:255'],
            'premise_address_1' => ['nullable', 'string' , 'max:255'],
            'premise_address_2' => ['nullable', 'string' , 'max:255'],
            'premise_address_3' => ['nullable', 'string' , 'max:255'],
            'premise_postcode' => ['nullable', 'string' , 'digits:5'],
            'premise_state_id' => ['nullable', 'string' , 'max:255'],
            'premise_phone_number' => ['nullable', 'alpha_dash' ,'max:12'],
            'premise_fax_number' => ['nullable', 'string' ,'max:12'],
            'premise_email' => ['nullable', 'email' , 'max:255'],
        ];

        $messages = [
            'premise_dun_id.required'  =>  'Dun premis diperlukan',
            'premise_parliament_id.required'  =>  'Parlimen premis diperlukan',
            'premise_building_type_id.required'  =>  'Jenis Bangunan premis diperlukan',
            'premise_business_type_id.required'  =>  'Jenis Perniagaan Pengenalan premis diperlukan',
            'premise_address_1.required'  =>  'Alamat 1 premis diperlukan',
            'premise_postcode.required'  =>  'Poskod premis diperlukan',
            'premise_state_id.required'  =>  'Negeri premis diperlukan',
            'premise_district_id.required'  =>  'Daerah premis diperlukan',
            'premise_phone_number.required'  =>  'Nombor Telefon premis diperlukan',
            'premise_email.required'  =>  'Emel Telefon premis diperlukan',
            'premise_phone_number' => ['required', 'alpha_dash' ,'max:12'],
            'premise_fax_number' => ['nullable', 'string' ,'max:12'],
            'premise_email' => ['required', 'email' , 'max:255'],
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $request->validate($rules,$messages);

        return $next($request);
    }
}
