<?php

namespace App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class UpdateCompanyApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $domain = decrypt($request->material_domain);
        $id = decrypt($request->id);
        $attachmentValidations = AttachmentDataObject::updateValidateAttachments($domain);

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'company_name' =>['required', 'string' , 'max:255'],
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'company_name.required'   =>  ['Nama Syarikat diperlukan'],
            'company_name.max'   =>  ['Nama Syarikat hanya membenarkan 255 aksara'],
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            $validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
        });

        $validator->validate();


        return $next($request);
    }
}
