<?php

namespace App\Http\Middleware\LicenseApplication\Export\ReplacementApplication;

use Closure;

use App\DataObjects\LicenseApplication\AttachmentDataObject;

class StoreApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'store_id' => ['required'],
            'remarks' => ['required'],
            'license_application_replacement_type_id' => ['required'],
        ];
        $messages = [
           'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'store_id.required'   =>  ['Stor masih belum dipilih'],
            'remarks.required'   =>  ['Ulasan diperlukan'],
            'license_application_replacement_type_id.required'   =>  ['Tujuan penggantian diperlukan'],
        ];
        
        $attachmentValidations = AttachmentDataObject::addValidateAttachments($param);

        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        $request->validate($rules,$messages);

        return $next($request);
    }
}
