<?php

namespace App\Http\Middleware\LicenseApplication\Export\CancellationApplication;

use Closure;

use App\DataObjects\LicenseApplication\AttachmentDataObject;

class SubmitDraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);
        
        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'remarks' => ['required'],
            'store_id' => ['required'],
            'license_application_cancellation_type_id' => ['required'],
        ];
        $messages = [
           'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'store_id.required'   =>  ['Stor masih belum dipilih'],
            'remarks.required'   =>  ['Ulasan diperlukan'],
            'license_application_cancellation_type_id.required'   =>  ['Tujuan pembatalan diperlukan'],
        ];
        
        $attachmentValidations = AttachmentDataObject::updateValidateAttachments($param);
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            $validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
        });

        $validator->validate();

        return $next($request);
    }
}
