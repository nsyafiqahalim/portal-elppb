<?php

namespace App\Http\Middleware\LicenseApplication\Wholesale\NewApplication;

use Closure;
use App\DataObjects\LicenseApplication\AttachmentDataObject;

class UpdateDraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);

        $attachmentValidations = AttachmentDataObject::updateValidateAttachments($param);
    
        $rules = [];
        $messages = [];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            //$validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
            //print_r($validator);
        });
        
        $validator->validate();

        return $next($request);
    }
}
