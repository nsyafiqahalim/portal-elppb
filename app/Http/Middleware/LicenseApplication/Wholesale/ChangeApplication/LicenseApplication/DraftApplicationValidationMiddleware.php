<?php

namespace App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\LicenseApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class DraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $attachmentValidations = AttachmentDataObject::addDraftValidateAttachments($param);

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'apply_load' => ['nullable','alpha_num'],
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'apply_load.required'   =>  ['Had Muatan Syarikat diperlukan'],
            'apply_load.alpha_num'   =>  ['Had Muatan Hanya membenarkan format nombor'],
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $request->validate($rules,$messages);

        return $next($request);
    }
}
