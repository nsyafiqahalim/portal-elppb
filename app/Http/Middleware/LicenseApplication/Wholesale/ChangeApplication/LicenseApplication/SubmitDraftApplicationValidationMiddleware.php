<?php

namespace App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\LicenseApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class SubmitDraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);

        $attachmentValidations = AttachmentDataObject::updateValidateAttachments($param);

        $rules = [
            'company_id' => ['required'],

        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],

        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            //$validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
        });

        $validator->validate();
        

        return $next($request);
    }
}
