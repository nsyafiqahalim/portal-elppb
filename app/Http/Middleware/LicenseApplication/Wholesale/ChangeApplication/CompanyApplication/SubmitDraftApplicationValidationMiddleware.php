<?php

namespace App\Http\Middleware\LicenseApplication\Wholesale\ChangeApplication\CompanyApplication;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class SubmitDraftApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);

        $attachmentValidations = AttachmentDataObject::updateValidateAttachments($param);

        $rules = [
            'company_name' =>['required', 'string' , 'max:255'],
        ];

        $messages = [
            'company_name.required'   =>  ['Nama Syarikat diperlukan'],
            'company_name.max'   =>  ['Nama Syarikat hanya membenarkan 255 aksara'],
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            $validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
        });

        $validator->validate();
        

        return $next($request);
    }
}
