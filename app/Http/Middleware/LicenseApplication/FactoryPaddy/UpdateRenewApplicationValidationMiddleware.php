<?php

namespace App\Http\Middleware\LicenseApplication\FactoryPaddy;

use Closure;

use  App\DataObjects\LicenseApplication\AttachmentDataObject;

class UpdateRenewApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $id = decrypt($request->id);
        $domain = decrypt($request->material_domain);

        $attachmentValidations = AttachmentDataObject::updateValidateAttachments($domain);

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'store_id' => ['required'],
            'apply_load' => ['required'],
            'apply_duration' => ['required'],
            'license_application_license_generation_type_id' => ['required'],
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'store_id.required'   =>  ['Stor masih belum dipilih'],
            'apply_load.required'   =>  ['Had Muatan belum diperlukan'],
            'apply_duration.required'   =>  ['Tempoh Permohonan masih belum dipilih'],
            'license_application_license_generation_type_id.required'   =>  ['Lesen Yang Mahu Dijana masih belum dipilih'],
        ];

        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $validator = \Validator::make($param, $rules,$messages);
        $validator->after(function ($validator) use ($request, $id) {
            $validator = AttachmentDataObject::validateAttachments($validator,$request, $id);
        });

        $validator->validate();

        return $next($request);
    }
}
