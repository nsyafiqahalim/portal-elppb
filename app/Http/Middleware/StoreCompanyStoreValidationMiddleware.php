<?php

namespace App\Http\Middleware;

use Closure;

class StoreCompanyStoreValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'store_store_ownership_type_id' => ['required', 'string' , 'max:255'],
            'store_building_type_id' => ['required', 'string' , 'max:255'],
            'store_district_id' => ['required', 'string' , 'max:255'],
            'store_address_1' => ['required', 'string' , 'max:255'],
            'store_address_2' => ['nullable', 'string' , 'max:255'],
            'store_address_3' => ['nullable', 'string' , 'max:255'],
            'store_postcode' => ['required', 'string' , 'digits:5'],
            'store_state_id' => ['required', 'string' , 'max:255'],
            'store_email' => ['required', 'email' , 'max:255'],
            'store_phone_number' => ['required', 'alpha_dash'],
            'store_faks_number' => ['nullable', 'alpha_dash'],

        ]);

        return $next($request);
    }
}
