<?php

namespace App\Http\Middleware;

use Closure;
use App\DataObjects\Admin\CompanyTypeDataObject;

class StoreCompanyValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param = $request->all();

        $expiryDateStatus = 'required';
        $paidupCapitalStatus = 'required';
        $companyRegistrationNumber = 'digits:12';
        
        if (isset($param['company_company_type_id'])) {
            $decryptedCompanyTypeId = decrypt($param['company_company_type_id']);
            $companyType = CompanyTypeDataObject::findCompanyTypeById($decryptedCompanyTypeId);
            
            if ($companyType->has_paidup_capital == 0) {
                $paidupCapitalStatus = 'nullable';
            }

            if ($companyType->has_expiry_date == 0) {
                $expiryDateStatus = 'nullable';
            }
            
            if ($companyType->code ==  'PERTUBUHAN_PELADANG') {
                $companyRegistrationNumber = 'digits:12';
            }
        }
        // Execute Fixed Business Rule validation first
        $request->validate([
            'company_company_type_id' => ['required', 'string' , 'max:255'],
            'company_name' => ['required', 'string' , 'max:255'],
            'company_mycoid' => ['required', 'string' , $companyRegistrationNumber],
            'company_paidup_capital' => [$paidupCapitalStatus, 'string' , 'regex:/^\d+(\.\d{1,2})?$/'],
            'company_expiry_date' => [$expiryDateStatus, 'date_format:d-m-Y', 'max:255'],
            'company_address_1' => ['required', 'string' , 'max:255'],
            'company_address_2' => ['nullable', 'string' , 'max:255'],
            'company_address_3' => ['nullable', 'string' , 'max:255'],
            'company_postcode' => ['required', 'string' , 'digits:5'],
            'company_state_id' => ['required', 'string' , 'max:255'],
            'company_phone_number' => ['required', 'alpha_dash' ,'max:12', 'regex:[0-9.-]*$'],
            'company_fax_number' => ['nullable', 'string' ,'max:12', 'regex:[0-9.-]*$'],
            'company_email' => ['required', 'email' , 'max:255'],
        ]);
        
        return $next($request);
    }
}
