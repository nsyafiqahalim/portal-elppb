<?php

namespace App\Http\Middleware\ApprovalLetterApplication\BuyPaddy;

use Closure;
use  App\DataObjects\ApprovalLetterApplication\AttachmentDataObject;

class StoreNewApplicationValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param  = $request->all();
        $domain = decrypt($request->material_domain);
        $attachmentValidations = AttachmentDataObject::addValidateAttachments($domain);

        $rules = [
            'company_id' => ['required'],
            'premise_id' => ['required'],
            'store_id' => ['required'],
            'license_application_buy_paddy_condition_id' => ['required']
        ];

        $messages = [
            'company_id.required'   =>  ['Syarikat masih belum dipilih'],
            'premise_id.required'   =>  ['Premis masih belum dipilih'],
            'store_id.required'   =>  ['Stor masih belum dipilih'],
            'license_application_buy_paddy_condition_id.required'   =>  ['Syarat membeli padi diperlukan']
        ];
        
        $rules = array_merge($rules,$attachmentValidations['rules']);
        $messages = array_merge($messages,$attachmentValidations['messages']);

        $request->validate($rules,$messages);

        return $next($request);

        return $next($request);
    }
}
