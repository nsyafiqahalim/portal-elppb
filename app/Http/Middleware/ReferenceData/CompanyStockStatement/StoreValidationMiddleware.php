<?php

namespace App\Http\Middleware\ReferenceData\CompanyStockStatement;

use Closure;

use App\DataObjects\ReferenceData\CompanyPartnerDataObject;
use App\DataObjects\LicenseDataObject;
use App\DataObjects\Admin\RiceGradeDataObject;
use App\DataObjects\Admin\TotalGradeDataObject;
use App\DataObjects\Admin\SpinTypeDataObject;
use App\DataObjects\Admin\StockTypeDataObject;

class StoreValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $param  = $request->all();

        $rules = [
            'company_id' => ['required'],
            'license_id' => ['required']
        ];

        $messages = [
            'company_id.required'   =>  ['Ruangan ini wajib diisi'],
            'license_id.required'   =>  ['Ruangan ini wajib diisi']
        ];
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->validate();

        $licenseId  =   decrypt($request['license_id']);
        if($licenseId == 'C'){
            $rules = [
                'grade_type_id' => ['required'],
                'initial_stock' => ['required', 'numeric'],
                //'selling' => ['required'],
                //'buying' => ['required'],
                'total_buy' => ['required'],
                'total_sell' => ['required'],
                'selling_price' => ['required'],
                'buying_price' => ['required'],
                'stock_balance' => ['required', 'numeric'],
                'stock_at' => ['required', 'date_format:d-m-Y']
            ];

            $messages = [
                'grade_type_id.required'   =>  ['Ruangan ini wajib diisi'],
                'initial_stock.required'   =>  ['Ruangan ini wajib diisi'],
                //'selling.required'   =>  ['Ruangan ini wajib diisi'],
                //'buying.required'   =>  ['Ruangan ini wajib diisi'],
                'total_buy.required'   =>  ['Ruangan ini wajib diisi'],
                'total_sell.required'   =>  ['Ruangan ini wajib diisi'],
                'selling_price.required'   =>  ['Ruangan ini wajib diisi'],
                'buying_price.required'   =>  ['Ruangan ini wajib diisi'],
                'stock_balance.required'   =>  ['Ruangan ini wajib diisi'],
                'stock_at.required'   =>  ['Ruangan ini wajib diisi'],
                'stock_balance.required'   =>  ['Ruangan ini wajib diisi'],
                'stock_balance.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
                'initial_stock.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
            ];
        }
        else{
            $rules = [
                    'stock_type_id' => ['required'],
                ];

                $messages = [
                    'stock_type_id.required'   =>  ['Ruangan ini wajib diisi'],
                ];
            
            $validator = \Validator::make($param, $rules,$messages);
            $validator->validate();

            $stockTypeId    =   decrypt($request['stock_type_id']);
            $stockType      =   StockTypeDataObject::findStockTypeById($stockTypeId);
            if($stockType->code == 'JENIS_PADI'){
                
                $rules = [
                    'initial_stock' => ['required', 'numeric'],
                    'total_buy' => ['required'],
                    'total_spin' => ['required'],
                    'buying_price' => ['required', 'numeric'],
                    'spin_result' => ['required'],
                    'stock_balance' => ['required', 'numeric'],
                    'stock_at' => ['required', 'date_format:d-m-Y']
                    
                ];

                $messages = [
                    'grade_type_id.required'   =>  ['Ruangan ini wajib diisi'],
                    'initial_stock.required'   =>  ['Ruangan ini wajib diisi'],
                    'total_buy.required'   =>  ['Ruangan ini wajib diisi'],
                    'total_spin.required'   =>  ['Ruangan ini wajib diisi'],
                    'spin_result.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_balance.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_at.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_balance.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_balance.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
                    'initial_stock.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
                    'buying_price.required'   =>  ['Ruangan ini wajib diisi'],
                    'buying_price.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
                ];
            }else{
                $rules = [
                    'initial_stock' => ['required', 'numeric'],
                    'total_grade' => ['required'],
                    'selling_price' => ['required'],
                    'total_sell' => ['required'],
                    'stock_balance' => ['required', 'numeric'],
                    'stock_at' => ['required', 'date_format:d-m-Y']
                ];

                $messages = [
                    'grade_type_id.required'   =>  ['Ruangan ini wajib diisi'],
                    'initial_stock.required'   =>  ['Ruangan ini wajib diisi'],
                    'total_grade.required'   =>  ['Ruangan ini wajib diisi'],
                    'selling_price.required'   =>  ['Ruangan ini wajib diisi'],
                    'total_sell.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_balance.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_at.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_balance.required'   =>  ['Ruangan ini wajib diisi'],
                    'stock_balance.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
                    'initial_stock.regex' => ['Ruangan ini hanya membenarkan nombor dan 2 titik perpuluhan'],
                ];
            }
        }

        $validator = \Validator::make($param, $rules,$messages);
        $validator->validate();

        return $next($request);
    }
}
