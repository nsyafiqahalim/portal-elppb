<?php

namespace App\Http\Middleware\ReferenceData\CompanyStockStatement;

use Closure;

use App\DataObjects\ReferenceData\CompanyPartnerDataObject;

class StoreValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $param  = $request->all();

        $rules = [
            'company_id' => ['required'],
            'license_id' => ['required']
        ];

        $messages = [
            'company_id.required'   =>  ['Ruangan ini wajib diisi'],
            'license_id.required'   =>  ['Ruangan ini wajib diisi']
        ];
        
        $validator = \Validator::make($param, $rules,$messages);
        $validator->validate();

        /*
        $validator->after(function ($validator) use($request,$decryptedCitizen){
            if(isset($decryptedCitizen)&& $decryptedCitizen != null){
                if($decryptedCitizen == 0){
                    $decryptedCompanyId = decrypt($request->company_id);
                    $partnerId = (isset($request->id))?decrypt($request->id):null;
                    $nonCitizenSharePercentage = CompanyPartnerDataObject::findNonCitizenTotalShareByCompanyId($decryptedCompanyId,$partnerId);
                    $totalNonCitizenShare = $nonCitizenSharePercentage + $request->partner_share_percentage;
                    $allowedShare = 30 - $nonCitizenSharePercentage;
                    
                    $totalNonCitizenShare = number_format($totalNonCitizenShare,2);
                    $allowedShare = number_format($allowedShare,2);
                    if($totalNonCitizenShare > 30){
                        $validator->errors()->add('partner_share_percentage', '
                        <ul>
                        <li>Jumlah peratus syer untuk BUKAN WARGANEGARA tidak boleh melebihi 30% untuk sesebuah syarikat.</li>
                        <li>Syer terkini direkodkan: '.$nonCitizenSharePercentage.'%.</li>
                        <li> Baki Syer dibenarkan: '.$allowedShare.'%</li>
                        </ul>');
                    }
                }
            }
        });
        */
        

        return $next($request);
    }
}
