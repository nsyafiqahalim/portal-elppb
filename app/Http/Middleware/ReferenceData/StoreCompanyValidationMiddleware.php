<?php

namespace App\Http\Middleware\ReferenceData;

use Closure;
use App\DataObjects\Admin\CompanyTypeDataObject;
use Carbon\Carbon;

class StoreCompanyValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {
        $param = $request->all();

        $expiryDateStatus = 'required';
        $paidupCapitalStatus = 'required';
        $companyRegistrationNumber = 'digits:12';
        $checkRegistrationNumber = 'unique:companies,registration_number';
        $checkOldRegistrationNumber = 'unique:companies,old_registration_number';

        if (isset($param['company_company_type_id'])) {
            $decryptedCompanyTypeId = decrypt($param['company_company_type_id']);
            $companyType = CompanyTypeDataObject::findCompanyTypeById($decryptedCompanyTypeId);
            
            if ($companyType->has_paidup_capital == 0) {
                $paidupCapitalStatus = 'nullable';
            }

            if ($companyType->has_expiry_date == 0) {
                $expiryDateStatus = 'nullable';
            }
            
            if ($companyType->code ==  'PERTUBUHAN_PELADANG') {
                $companyRegistrationNumber = 'digits:12';
            }
        }

        if(isset($request->id)){
            $decryptedId = decrypt($request->id);
            $checkRegistrationNumber = 'unique:companies,registration_number,'.$decryptedId;
            $checkOldRegistrationNumber = 'unique:companies,old_registration_number,'.$decryptedId;
        }

        // Execute Fixed Business Rule validation first
        $request->validate([
            'company_company_type_id' => ['required', 'string' , 'max:255'],
            'company_name' => ['required', 'string' , 'max:255'],
            'company_mycoid' => ['required', 'string' , $checkRegistrationNumber ],
            'company_old_mycoid' => ['nullable', 'string' , 'max:10',$checkOldRegistrationNumber],
            'company_paidup_capital' => [$paidupCapitalStatus, 'string' , 'regex:/^\d+(\.\d{1,2})?$/'],
            'company_expiry_date' => [$expiryDateStatus, 'date_format:d-m-Y', 'max:255','after:today'],
            'company_address_1' => ['required', 'string' , 'max:255'],
            'company_address_2' => ['nullable', 'string' , 'max:255'],
            'company_address_3' => ['nullable', 'string' , 'max:255'],
            'company_postcode' => ['required', 'string' , 'digits:5'],
            'company_state_id' => ['required', 'string' , 'max:255'],
            'company_phone_number' => ['required', 'string' , 'regex:/^\d{1,4}(-\d{1,7})?$/','not_regex:/[a-z]/','max:12'],
            'company_fax_number' => ['nullable', 'string' , 'regex:/^\d{1,4}(-\d{1,7})?$/','not_regex:/[a-z]/','max:12'],
            'company_email' => ['required', 'email' , 'max:255'],
        ],[
            'company_company_type_id.required'  =>  'Jenis Syarikat diperlukan. Ruangan ini wajib diisi',
            'company_name.required'  =>  'Nama Syarikat diperlukan. Ruangan ini wajib diisi',
            'company_mycoid.required'  =>  'Nombor pendaftaran syarikat diperlukan. Ruangan ini wajib diisi',
            'company_old_mycoid.required'  =>  'Nombor pendaftaran syarikat lama diperlukan. Ruangan ini wajib diisi',
            'company_paidup_capital.required'  =>  'Modal Berbayar diperlukan. Ruangan ini wajib diisi',
            'company_expiry_date.required'  =>  'Tarikh Mansuh SSM/Trading License/IRD diperlukan. Ruangan ini wajib diisi',
            'company_address_1.required'  =>  'Alamat 1 diperlukan. Ruangan ini wajib diisi',
            'company_postcode.required'  =>  'Poskod diperlukan. Ruangan ini wajib diisi',
            'company_state_id.required'  =>  'Negeri diperlukan. Ruangan ini wajib diisi',
            'company_phone_number.required'  =>  'Nombor Telefon diperlukan. Ruangan ini wajib diisi',
            'company_email.required'  =>  'Emel diperlukan. Ruangan ini wajib diisi',
            'company_email.email'  =>  'Emel Syarikat mesti alamat e-mel yang sah. contoh@contoh.com.',
            'company_company_type_id.max'  =>  'Jenis Syarikat hanya membenarkan 255 aksara',
            'company_name.max'  =>  'Nama Syarikat hanya membenarkan 255 aksara',
            'company_mycoid.digit'  =>  'Nombor pendaftaran syarikat hanya membenarkan 12 digit',
            'company_old_mycoid.max'  =>  'Nombor pendaftaran syarikat hanya membenarkan 10 aksara',
            'company_paidup_capital.max'  =>  'Modal Berbayar hanya membenarkan 255 aksara',
            'company_expiry_date.max'  =>  'Tarikh Mansuh SSM/Trading License/IRD hanya membenarkan 255 aksara',
            'company_address_1.max'  =>  'Alamat 1 hanya membenarkan 255 aksara',
            'company_address_2.max'  =>  'Alamat 2 hanya membenarkan 255 aksara',
            'company_address_2.max'  =>  'Alamat 3 hanya membenarkan 255 aksara',
            'company_postcode.digits'  =>  'Poskod hanya membenarkan 5 digit',
            'company_state_id.max'  =>  'Negeri hanya membenarkan 255 aksara',
            'company_phone_number.regex'  =>  "Nombor Telefon hanya membenarkan nombor dan  satu aksara '-'",
            //'company_phone_number.not_regex'  =>   "Aksara lain tidak dibenarkan kecuali aksara '-'",
            'company_fax_number.regex'  =>  "Nombor Faks hanya membenarkan nombor dan satu aksara '-'",
            //'company_fax_number.not_regex'  =>  "Aksara lain tidak dibenarkan kecuali aksara '-'",
            'company_phone_number.max'  =>  "Nombor Telefon hanya membenarkan 12 aksara sahaja",
            'company_fax_number.max'  =>  "Nombor Faks hanya membenarkan 12 aksara sahaja",
            'company_email.max'  =>  'Emel hanya membenarkan 255 aksara',
            'company_mycoid.unique' =>  'Nombor Pendaftaran telah digunakan. Sila cuba nombor pendaftaran yang berlainan',
            'company_old_mycoid.unique' =>  'Nombor Pendaftaran Lama telah digunakan. Sila cuba nombor pendaftaran yang berlainan',
            'company_expiry_date.date_format'  =>  "Tarikh Mansuh SSM/Trading License/IRD hanya membenarkan format 'd-m-Y'. Sebagai Contoh 31-08-1957",
            'company_expiry_date.after'  =>  "Tarikh Mansuh SSM/Trading License/IRD mesti selepas hari ini. Tarikh hari ini:".Carbon::now()->format('d-m-Y'),
        ]);
        
        return $next($request);
    }
}

