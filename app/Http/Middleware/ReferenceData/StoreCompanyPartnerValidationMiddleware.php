<?php

namespace App\Http\Middleware\ReferenceData;

use Closure;

use App\DataObjects\ReferenceData\CompanyPartnerDataObject;

class StoreCompanyPartnerValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $param = $request->all();
        $icRequired = 'required';
        $passportRequired = 'nullable';
        $raceType = 'nullable';
        $race = 'nullable';
        $decryptedCitizen = null;

        if(isset($param['partner_is_citizen'])){
            $decryptedCitizen = decrypt($param['partner_is_citizen']);
            if($decryptedCitizen == 0){
                $icRequired = 'nullable';
                //$passportRequired = 'required';
            }
            else{
                $raceType = 'required';
                $race = 'required';
            }
        }

        $param  = $request->all();
        $validator = \Validator::make($param, [
            'partner_race_type_id' => [$raceType, 'string' , 'max:255'],
            'partner_race_id' => [$race, 'string' , 'max:255'],
            'partner_name' => ['required', 'string' , 'max:255'],
            'partner_ic' => [$icRequired, 'string' , 'digits:12'],
            'partner_passport' => [$passportRequired, 'alpha_num' , 'max:8'],
            'partner_is_citizen' => ['required', 'string' , 'max:255'],
            'partner_total_share' => ['required', 'numeric' ,'gt:0'],
            'partner_share_percentage' => ['required', 'numeric' , 'lte:100','gt:0'],
            'partner_address_1' => ['required', 'string' , 'max:255'],
            'partner_address_2' => ['nullable', 'string' , 'max:255'],
            'partner_address_3' => ['nullable', 'string' , 'max:255'],
            'partner_postcode' => ['required', 'string' , 'digits:5'],
            'partner_state_id' => ['required', 'string' , 'max:255'],
            'partner_phone_number' => ['required', 'string' , 'regex:/^\d{1,4}(-\d{1,7})?$/','not_regex:/[a-z]/','max:12'],
            'partner_email' => ['required', 'email' , 'max:255'],
        ],[
            'partner_race_type_id.required'  =>  'Jenis Bangsa diperlukan. Ruangan ini wajib diisi',
            'partner_race_id.required'  =>  'Bangsa diperlukan. Ruangan ini wajib diisi',
            'partner_name.required'  =>  'Nama diperlukan. Ruangan ini wajib diisi',
            'partner_ic.required'  =>  'Nombor Kad Pengenalan diperlukan. Ruangan ini wajib diisi',
            'partner_passport.required'  =>  'Passport diperlukan. Ruangan ini wajib diisi',
            'partner_is_citizen.required'  =>  'Warganegara diperlukan. Ruangan ini wajib diisi',
            'partner_total_share.required'  =>  'Jumlah Syer diperlukan. Ruangan ini wajib diisi',
            'partner_share_percentage.required'  =>  'Peratus Syer diperlukan. Ruangan ini wajib diisi',

            'partner_address_1.required'  =>  'Alamat 1 diperlukan. Ruangan ini wajib diisi',
            'partner_postcode.required'  =>  'Poskod diperlukan. Ruangan ini wajib diisi',
            'partner_state_id.required'  =>  'Negeri diperlukan. Ruangan ini wajib diisi',
            'partner_phone_number.required'  =>  'Nombor Telefon diperlukan. Ruangan ini wajib diisi',
            'partner_email.required'  =>  'Emel Telefon diperlukan. Ruangan ini wajib diisi',

            'partner_partner_type_id.max'  =>  'Jenis Syarikat hanya membenarkan 255 aksara',
            'partner_name.max'  =>  'Nama Syarikat hanya membenarkan 255 aksara',
            'partner_mycoid.digits'  =>  'Nombor pendaftaran syarikat hanya membenarkan 12 digit',
            'partner_passport.max'  =>  'Nombor pendaftaran syarikat hanya membenarkan 8 aksara',
            'partner_paidup_capital.max'  =>  'Modal Berbayar hanya membenarkan 255 aksara',
            'partner_expiry_date.max'  =>  'Tarikh Mansuh SSM/Trading License/IRD hanya membenarkan 255 aksara',
            'partner_ic.digits'  =>  'Kad Pengenalan hanya membenarkan 12 digit',
            'partner_address_2.max'  =>  'Alamat 2 hanya membenarkan 255 aksara',
            'partner_address_2.max'  =>  'Alamat 3 hanya membenarkan 255 aksara',
            'partner_postcode.digits'  =>  'Poskod hanya membenarkan 5 digit',
            'partner_state_id.max'  =>  'Negeri hanya membenarkan 255 aksara',
            'partner_phone_number.max'  =>  "Nombor Telefon hanya membenarkan 12 aksara sahaja",
            'partner_phone_number.max'  =>  "Nombor Telefon hanya membenarkan 12 aksara sahaja",
            'partner_email.max'  =>  'Emel hanya membenarkan 255 aksara',
            'partner_email.email'  =>  'Emel mesti alamat e-mel yang sah. contoh@contoh.com.',
            'partner_share_percentage.lte'  =>  'Peratus Syer mesti kurang atau sama dengan 100.',
            'partner_share_percentage.gt'  =>  'Peratus Syer mesti  lebih dari 0',

            'partner_expiry_date.date_format'  =>  "Tarikh Mansuh SSM/Trading License/IRD hanya membenarkan format 'd-m-Y'. Sebagai Contoh 31-08-1957",
        ]);
        
        /*
        $validator->after(function ($validator) use($request,$decryptedCitizen){
            if(isset($decryptedCitizen)&& $decryptedCitizen != null){
                if($decryptedCitizen == 0){
                    $decryptedCompanyId = decrypt($request->company_id);
                    $partnerId = (isset($request->id))?decrypt($request->id):null;
                    $nonCitizenSharePercentage = CompanyPartnerDataObject::findNonCitizenTotalShareByCompanyId($decryptedCompanyId,$partnerId);
                    $totalNonCitizenShare = $nonCitizenSharePercentage + $request->partner_share_percentage;
                    $allowedShare = 30 - $nonCitizenSharePercentage;
                    
                    $totalNonCitizenShare = number_format($totalNonCitizenShare,2);
                    $allowedShare = number_format($allowedShare,2);
                    if($totalNonCitizenShare > 30){
                        $validator->errors()->add('partner_share_percentage', '
                        <ul>
                        <li>Jumlah peratus syer untuk BUKAN WARGANEGARA tidak boleh melebihi 30% untuk sesebuah syarikat.</li>
                        <li>Syer terkini direkodkan: '.$nonCitizenSharePercentage.'%.</li>
                        <li> Baki Syer dibenarkan: '.$allowedShare.'%</li>
                        </ul>');
                    }
                }
            }
        });
        */
        $validator->validate();

        return $next($request);
    }
}
