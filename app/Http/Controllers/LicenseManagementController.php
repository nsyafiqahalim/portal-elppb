<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LicenseManagementController extends Controller
{
    public function index()
    {
        return view('license-management.index');
    }

    public function view()
    {
        return view('license-management.view');
    }

    public function edit()
    {
        return view('license-management.edit');
    }

    public function add()
    {
        return view('license-management.add');
    }
}
