<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    // view Profile
    public function updateProfile()
    {
      return view('profile.profile-view');
    }

    public function updatePassword()
    {
      return view('profile.update-password');
    }
}
