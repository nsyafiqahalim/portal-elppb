<?php

namespace App\Http\Controllers\API\V1\ApprovalLetterApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\Admin\GenerationTypeDataObject;

class GenerationTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['domain'])) {
            $param['domain']  = decrypt($param['domain']);
        }

        return GenerationTypeDataObject::findAllGenerationTypesUsingDatatableFormat($param);
    }
}
