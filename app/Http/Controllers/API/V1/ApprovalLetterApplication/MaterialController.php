<?php

namespace App\Http\Controllers\API\V1\ApprovalLetterApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\ApprovalLetterApplication\MaterialDataObject;

class MaterialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['material_domain'])) {
            $param['domain']  = decrypt($param['material_domain']);
        }

         if (isset($param['approval_letter_application_id'])) {
            $param['approval_letter_application_id']  = decrypt($param['approval_letter_application_id']);
        }
        
        return MaterialDataObject::findAllMaterialsUsingDatatableFormat($param);
    }
}
