<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\ApprovalLetterApplicationDataObject;

class ApprovalLetterApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['user_id'])) {
            $param['user_id']  = decrypt($param['user_id']);
        }
        
        return ApprovalLetterApplicationDataObject::findMyApprovalLetterApplicationUsingDatatableFormat($param);
    }
}
