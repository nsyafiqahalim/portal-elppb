<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\LicenseApplication\IncludeLicenseDataObject;

use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\GenerationTypeDataObject;

class LicenseManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return IncludeLicenseDataObject::findMyIncludeLicenseUsingDatatableFormat($param);
    }

    public function isWholesaleLicenseExist(Request $request)
    {
        $param = $request->all();
        $param['company_id'] =  decrypt($param['company_id']);
        $param['domain'] =  decrypt($param['domain']);
        $param['status_id'] =   StatusDataObject::findStatusByCode('LESEN_AKTIF')->id;
        $param['license_type_id']   =   LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
        $license =  LicenseDataObject::findActiveLicenseExistByCompanyIdAndLicenseTypeId($param);
        
        if (isset($license)) {
            $status = 'EXIST';
            $liceseId = encrypt($license->id);
            $licenseNumber = $license->license_number;
            $expiryDate = $license->expiry_date;
            $premiseId = $license->licenseApplication->companyPremise->company_premise_id;
            $storeId = $license->licenseApplication->companyStore->company_store_id;
            $premiseId = $license->licenseApplication->companyPremise->company_premise_id;
            $storeId = $license->licenseApplication->companyStore->company_store_id;

            if ($param['domain'] == 'IMPORT') {
                $licenseApplicationLicenseGenerationTypeId = GenerationTypeDataObject::findGenerationTypeByDomainAndType('IMPORT', 'SINGLE_LICENSE')->id;
            } elseif ($param['domain'] == 'EXPORT') {
                $licenseApplicationLicenseGenerationTypeId = GenerationTypeDataObject::findGenerationTypeByDomainAndType('EXPORT', 'SINGLE_LICENSE')->id;
            }
        } else {
            $status = 'NOT_EXIST';
            $licenseNumber = null;
            $liceseId = null;
            $expiryDate = null;
            $premiseId = null;
            $storeId = null;
            if ($param['domain'] == 'IMPORT') {
                $licenseApplicationLicenseGenerationTypeId = GenerationTypeDataObject::findGenerationTypeByDomainAndType('IMPORT', 'WITH_PARENT_LICENSE')->id;
            } elseif ($param['domain'] == 'EXPORT') {
                $licenseApplicationLicenseGenerationTypeId = GenerationTypeDataObject::findGenerationTypeByDomainAndType('EXPORT', 'WITH_PARENT_LICENSE')->id;
            }
        }
        return response()->json([
            'status'   =>  $status,
            'license_id'    =>  $liceseId,
            'license_number'  =>  $licenseNumber,
            'expiry_date'  =>  $expiryDate,
            'premise_id'  =>  encrypt($premiseId),
            'store_id'  =>  encrypt($storeId),
            'license_application_license_generation_type_id'    =>  encrypt($licenseApplicationLicenseGenerationTypeId)
        ], 200);
    }
}
