<?php
namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\DataObjects\Admin\RaceDataObject;

class RaceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function findRacesByRaceTypeId($raceTypeId)
    {
        $decryptedRaceTypeId = decrypt($raceTypeId);

        $races = RaceDataObject::findRacesByRaceTypeId($decryptedRaceTypeId)->map(function ($race) {
            return [
                'id'    =>  encrypt($race->id),
                'label' =>  $race->name,
                'code' =>  $race->code
            ];
        });

        return response()->json($races);
    }
}
