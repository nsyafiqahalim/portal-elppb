<?php
namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\DataObjects\Admin\DunDataObject;

class DunController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function findDunsByParliamentId($parliamentId)
    {
        $decryptedParliamentId = decrypt($parliamentId);

        $duns = DunDataObject::findDunsByParliamentId($decryptedParliamentId)->map(function ($dun) {
            return [
                'id'    =>  encrypt($dun->id),
                'label' =>  $dun->name,
                'code'  =>  $dun->code
            ];
        });

        return response()->json($duns);
    }
}
