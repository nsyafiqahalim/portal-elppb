<?php
namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\ParliamentDataObject;

class StateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function findDistrictsAndParliamentsByStateId($stateId)
    {
        $decryptedStateId = decrypt($stateId);

        $districts = DistrictDataObject::findDistrictByStateId($decryptedStateId)->map(function ($race) {
            return [
                'id'    =>  encrypt($race->id),
                'label' =>  $race->name,
                'code' =>  $race->code
            ];
        });

        $parliaments = ParliamentDataObject::findParliamentsByStateId($decryptedStateId)->map(function ($race) {
            return [
                'id'    =>  encrypt($race->id),
                'label' =>  $race->name,
                'code' =>  $race->code
            ];
        });


        return response()->json([
            'districts' =>  $districts,
            'parliaments'   =>  $parliaments
        ]);
    }
}
