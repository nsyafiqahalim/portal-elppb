<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\Wholesale\ChangeApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\Wholesale\ChangeApplication\PremiseApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\Wholesale\ChangeApplication\PremiseApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\ChangeApplication\Wholesale\CompanyOutputDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationChangeTypeDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\UserDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;

use App\DataObjects\Admin\DurationDataObject;
use App\DataObjects\Admin\CompanyTypeDataObject;
use App\DataObjects\Admin\StateDataObject;
use App\DataObjects\Admin\OwnershipTypeDataObject;
use App\DataObjects\Admin\RaceDataObject;
use App\DataObjects\Admin\RaceTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\BusinessTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StoreOwnershipTypeDataObject;

class PremiseApplicationController extends Controller
{
    private const LICENSE_APPLICATION_DATA_SOURCE = 'REFERENCE_DATA';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['original_license_application_decrypted_id']         = decrypt($param['original_license_application_id']);
            $param['premise_id']                                        = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['store_id']                                        = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            
            $param['company_id']                                        = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['include_license_id']                                        = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['material_domain']                                   = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $param['applicant_id']                                           = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['change_type_code']                                  = (isset($param['change_type_code']))? decrypt($param['change_type_code']) : null;
            
            //setup custom parameter and replacing original parameter with other parameter
            $param['license_application_change_type']                   = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode($param['change_type_code']);
            $originalLicenseApplication                                 = LicenseApplicationDataObject::findLicenseApplicationById($param['original_license_application_decrypted_id']);
            $licenseType                                                = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $param['license_application_type_id']                       = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode('PERUBAHAN_MAKLUMAT_SYARIKAT')->id;
            $param['license_type_id']                                   = $licenseType->id;
            $param['status_id']                                         = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_PERUBAHAN')->id;
            $param['license_type_id']                                   = $licenseType->id;
            $param['license_type_code']                                 = $licenseType->code;
            $param['original_load']                                     = $originalLicenseApplication->apply_load;
            $param['apply_load']                                        = $originalLicenseApplication->apply_load;
            $param['apply_duration']                                    = $originalLicenseApplication->apply_duration;
            $param['duration_id']                                       = $originalLicenseApplication->duration_id;

            $outputParam = CompanyOutputDataObject::newOutputParameter($param, self::LICENSE_APPLICATION_DATA_SOURCE);
            $outputParam['original_company']->name                     = $param['company_name'];
            
            $outputParam['company_name']                               = $param['company_name'];
            $outputParam['user_id']                    =   UserDataObject::findUserWithLowestLicenseApplicationTasksBasedOnPermission('Permohonan Baharu Lesen Borong (Cawangan)', $outputParam['branch']->id)->id;

            
            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();

            $param['original_license_application_decrypted_id'] = decrypt($param['original_license_application_id']);
            $param['include_license_id']  = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['store_id']                                        = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $param['applicant_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BORONG_PERUBAHAN')->id;
            $param['change_type_code']                                  = (isset($param['change_type_code']))? decrypt($param['change_type_code']) : null;
            
            //setup custom parameter and replacing original parameter with other parameter
            $param['license_application_change_type']                   = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode($param['change_type_code']);
            $param['license_application_type_id']                   = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode('PERUBAHAN_MAKLUMAT_SYARIKAT')->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $originalLicenseApplication             =   LicenseApplicationDataObject::findLicenseApplicationById($param['original_license_application_decrypted_id']);
            $param['original_load']                                     = $originalLicenseApplication->apply_load;
            $param['apply_load']                                        = $originalLicenseApplication->apply_load;
            $param['apply_duration']                                    = $originalLicenseApplication->apply_duration;
        
            $outputParam = CompanyOutputDataObject::draftOutputParameter($param, self::LICENSE_APPLICATION_DATA_SOURCE);
            $outputParam['original_company']->name                    = $param['company_name'];
            $outputParam['company_name']                    = $param['company_name'];
            

            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            
            $outputParam                                    =   $param;
            $outputParam['material_domain']                 = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $outputParam['status_id']                       = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_PERUBAHAN')->id;
            $outputParam['license_application_id']          = $decryptedID;
            $outputParam['district_id']                     = (isset($param['district_id']))? decrypt($param['district_id']) : null;
            $outputParam['premise_business_type_id']        = (isset($outputParam['premise_business_type_id']))?decrypt($param['premise_business_type_id']):null;
            $outputParam['premise_building_type_id']        = (isset($outputParam['premise_building_type_id']))?decrypt($param['premise_building_type_id']):null;
            $outputParam['premise_dun_id']                  = (isset($outputParam['premise_dun_id']))?decrypt($param['premise_dun_id']):null;
            $outputParam['premise_parliament_id']           = (isset($outputParam['premise_parliament_id']))?decrypt($param['premise_parliament_id']):null;
            $outputParam['premise_district_id']             = (isset($outputParam['premise_district_id']))?decrypt($param['premise_district_id']):null;
            $outputParam['premise_state_id']                = (isset($outputParam['premise_state_id']))?decrypt($param['premise_state_id']):null;
            $outputParam['company_id']                = (isset($outputParam['company_id']))?decrypt($param['company_id']):null;
            $outputParam['premise_id']                = (isset($outputParam['premise_id']))?decrypt($param['premise_id']):null;
            $licenseType                                    = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $outputParam['license_type_code']               = $licenseType->code;
            $outputParam['branch']                          = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['district_id'])->branch;
            $outputParam['user_id']                         = UserDataObject::findUserWithLowestLicenseApplicationTasksBasedOnPermission('Permohonan Baharu Lesen Borong (Cawangan)', $outputParam['branch']->id)->id;
            
            $outputParam['updated_premise']                 = [
                                                            'business_type_id'          =>  $outputParam['premise_business_type_id'],
                                                            'business_type_name'        =>  (isset($outputParam['premise_business_type_id']))?BusinessTypeDataObject::findBusinessTypeById($outputParam['premise_business_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['premise_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['premise_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['premise_building_type_id'])->name:null,
                                                            'dun_id'                    =>  $outputParam['premise_dun_id'],
                                                            'dun_name'                  =>  (isset($outputParam['premise_dun_id']))?DunDataObject::findDunById($outputParam['premise_dun_id'])->name:null,
                                                            'parliament_id'             =>  $outputParam['premise_parliament_id'],
                                                            'district_id'               =>  $outputParam['premise_district_id'],
                                                            'district_name'             =>   (isset($outputParam['premise_district_id']))?DistrictDataObject::findDistrictById($outputParam['premise_district_id'])->name:null,
                                                            'parliament_name'           =>   (isset($outputParam['premise_parliament_id']))?ParliamentDataObject::findParliamentById($outputParam['premise_parliament_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['premise_address_1'],
                                                            'address_2'                 =>  $outputParam['premise_address_2'],
                                                            'address_3'                 =>  $outputParam['premise_address_3'],
                                                            'postcode'                  =>  $outputParam['premise_postcode'],
                                                            'state_id'                  =>  $outputParam['premise_state_id'],
                                                            'state_name'                =>   (isset($outputParam['premise_state_id']))?StateDataObject::findStateById($outputParam['premise_state_id'])->name:null,
                                                            'company_premise_id'        =>  $outputParam['premise_id'],
                                                            'company_id'                =>  $outputParam['company_id'],
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['premise_phone_number'],
                                                            'fax_number'                =>  $outputParam['premise_fax_number'],
                                                            'email'                     =>  $outputParam['premise_email'],
                                                            ];

            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            $outputParam                                          = $param;
            $outputParam['material_domain']                       = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $outputParam['premise_business_type_id']        = (isset($outputParam['premise_business_type_id']))?decrypt($param['premise_business_type_id']):null;
            $outputParam['premise_building_type_id']        = (isset($outputParam['premise_building_type_id']))?decrypt($param['premise_building_type_id']):null;
            $outputParam['premise_dun_id']                  = (isset($outputParam['premise_dun_id']))?decrypt($param['premise_dun_id']):null;
            $outputParam['premise_parliament_id']           = (isset($outputParam['premise_parliament_id']))?decrypt($param['premise_parliament_id']):null;
            $outputParam['premise_district_id']             = (isset($outputParam['premise_district_id']))?decrypt($param['premise_district_id']):null;
            $outputParam['premise_state_id']                = (isset($outputParam['premise_state_id']))?decrypt($param['premise_state_id']):null;
            $outputParam['company_id']                = (isset($outputParam['company_id']))?decrypt($param['company_id']):null;
            $outputParam['premise_id']                = (isset($outputParam['premise_id']))?decrypt($param['premise_id']):null;
            $outputParam['license_application_id']                = $decryptedID;

            $outputParam['updated_premise']                 = [
                                                            'business_type_id'          =>  $outputParam['premise_business_type_id'],
                                                            'business_type_name'        =>  (isset($outputParam['premise_business_type_id']))?BusinessTypeDataObject::findBusinessTypeById($outputParam['premise_business_type_id'])->name:null,
                                                            'building_type_id'          =>  $outputParam['premise_building_type_id'],
                                                            'building_type_name'        =>  (isset($outputParam['premise_building_type_id']))?BuildingTypeDataObject::findBuildingTypeById($outputParam['premise_building_type_id'])->name:null,
                                                            'dun_id'                    =>  $outputParam['premise_dun_id'],
                                                            'dun_name'                  =>  (isset($outputParam['premise_dun_id']))?DunDataObject::findDunById($outputParam['premise_dun_id'])->name:null,
                                                            'parliament_id'             =>  $outputParam['premise_parliament_id'],
                                                            'district_id'               =>  $outputParam['premise_district_id'],
                                                            'district_name'             =>   (isset($outputParam['premise_district_id']))?DistrictDataObject::findDistrictById($outputParam['premise_district_id'])->name:null,
                                                            'parliament_name'           =>   (isset($outputParam['premise_parliament_id']))?ParliamentDataObject::findParliamentById($outputParam['premise_parliament_id'])->name:null,
                                                            'address_1'                 =>  $outputParam['premise_address_1'],
                                                            'address_2'                 =>  $outputParam['premise_address_2'],
                                                            'address_3'                 =>  $outputParam['premise_address_3'],
                                                            'postcode'                  =>  $outputParam['premise_postcode'],
                                                            'state_id'                  =>  $outputParam['premise_state_id'],
                                                            'state_name'                =>   (isset($outputParam['premise_state_id']))?StateDataObject::findStateById($outputParam['premise_state_id'])->name:null,
                                                            'company_premise_id'        =>  $outputParam['premise_id'],
                                                            'company_id'                =>  $outputParam['company_id'],
                                                            //'license_application_id'    =>  $param['premise_license_application_id'],
                                                            'phone_number'              =>  $outputParam['premise_phone_number'],
                                                            'fax_number'                =>  $outputParam['premise_fax_number'],
                                                            'email'                     =>  $outputParam['premise_email'],
                                                            ];

            DraftApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
