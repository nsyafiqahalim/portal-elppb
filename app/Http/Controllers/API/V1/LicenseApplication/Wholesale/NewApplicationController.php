<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\Wholesale;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\LicenseApplication\Wholesale\NewApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\Wholesale\NewApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\Wholesale\NewApplicationOutputDataObject;

use App\DataObjects\LicenseApplication\TemporaryDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\LicenseApplication\AttachmentDataObject;

class NewApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $outputParam = NewApplicationOutputDataObject::submitApplication($param);

            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            
            $outputParam = NewApplicationOutputDataObject::draftApplication($param);
            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();

            $outputParam = NewApplicationOutputDataObject::submitApplication($param);
            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            
            $outputParam = NewApplicationOutputDataObject::draftApplication($param);
            //DraftApplicationDataObject::update($outputParam, $decryptedID);

            $currentLiceseApplication                   =   LicenseApplicationDataObject::findLicenseApplicationById($decryptedID);
            
            $arrayedCurrentLicenseApplication           =   $currentLiceseApplication->toArray();
            $licenseApplication                         =   LicenseApplicationDataObject::updateLicenseApplication($outputParam['license_application'], $decryptedID);
            $outputParam['license_application_id']      =   $decryptedID;
            
            AttachmentDataObject::addOrUpdateAttachmentsByMaterialDomain($outputParam['material_domain'],$arrayedCurrentLicenseApplication['material_domain'],$outputParam);

            TemporaryDataObject::updateLicenseTemporaryWithStore($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
    
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
