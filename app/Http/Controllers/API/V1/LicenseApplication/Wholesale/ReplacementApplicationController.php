<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\Wholesale;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\Wholesale\ReplacementApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\Wholesale\ReplacementApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\Wholesale\ReplacementApplicationOutputDataObject;

use App\DataObjects\Admin\StatusDataObject;
use App\DataObjects\Admin\UserDataObject;
use App\DataObjects\Admin\AreaCoverageDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;

class ReplacementApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $outputParam = ReplacementApplicationOutputDataObject::submitApplication($param);

            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            
            $outputParam = ReplacementApplicationOutputDataObject::draftApplication($param);
            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();

            $outputParam    =   $param;
            $outputParam['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $outputParam['license_application_replacement_type_id']  = (isset($outputParam['license_application_replacement_type_id']))? decrypt($outputParam['license_application_replacement_type_id']) : null;
            $outputParam['license_application_id']                     = $decryptedID;

            $outputParam['status_id']                       = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_PEMBATALAN')->id;
            $outputParam['license_application_id']          = $decryptedID;
            $outputParam['district_id']                     = (isset($param['district_id']))? decrypt($param['district_id']) : null;
            $licenseType                                    = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $outputParam['license_type_code']               = $licenseType->code;
            $outputParam['branch']                          = AreaCoverageDataObject::findAreaCoverageByDistrictId($outputParam['district_id'])->branch;
            $outputParam['user_id']                         = UserDataObject::findUserWithLowestLicenseApplicationTasksBasedOnPermission('Permohonan Baharu Lesen Borong (Cawangan)', $outputParam['branch']->id)->id;

            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();

            $outputParam    =   $param;
            $outputParam['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $outputParam['license_application_replacement_type_id']  = (isset($outputParam['license_application_replacement_type_id']))? decrypt($outputParam['license_application_replacement_type_id']) : null;
            $outputParam['license_application_id']                     = $decryptedID;
            //$outputParam = ReplacementApplicationOutputDataObject::draftApplication($param);
            DraftApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
    
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
