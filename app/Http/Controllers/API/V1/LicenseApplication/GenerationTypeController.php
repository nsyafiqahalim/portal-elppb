<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\Admin\GenerationTypeDataObject;
use LicenseApplicationGenerationTypeFacade;
class GenerationTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        $companyId                      =   decrypt($param['company_id']);
        $licenseType                    =   decrypt($param['license_type']);
        $licenseApplicationType         =   decrypt($param['license_application_type']);
        $includeLicenseId               =   (isset($param['include_license_id']))?decrypt($param['include_license_id']):null;
        $param['domain']                =   LicenseApplicationGenerationTypeFacade::findGenerationTypeDomainByCompanyId($companyId, $licenseType, $licenseApplicationType,$includeLicenseId);

        $data =  GenerationTypeDataObject::findAllGenerationTypesUsingDatatableFormat($param);
        return $data;
    }
}
