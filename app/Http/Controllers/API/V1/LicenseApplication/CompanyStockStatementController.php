<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\DataObjects\ReferenceData\CompanyStockStatementDataObject;
use App\DataObjects\Admin\StockStatmentPeriodDataObject;


class CompanyStockStatementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function store(Request $request){
        $param  =   $request->all();
        $deryptedId =   decrypt($param['id']);

        $currentStockStatementPeriod = StockStatmentPeriodDataObject::findCurrentStockStementPeriod();

        return response()->json([
            'status'   =>  1,
        ], 200);
    }

    public function datatable(Request $request){
        $param = $request->all();
        $param['company_id']    =   decrypt($param['company_id']);
        
        return CompanyStockStatementDataObject::findCompanyStockStatementUsingDatatableFormat($param);
    }
}
