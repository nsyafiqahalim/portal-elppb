<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\ReferenceData\CompanyPartnerDataObject;

class CompanyPartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['partner_race_id']       = (isset($param['partner_race_id']))?decrypt($param['partner_race_id']): null;
            $param['partner_race_type_id']  = (isset($param['partner_race_type_id']))?decrypt($param['partner_race_type_id']): null;
            $param['partner_is_citizen']  = decrypt($param['partner_is_citizen']);

            $param['partner_state_id']  = decrypt($param['partner_state_id']);
            $param['company_id']  = decrypt($param['company_id']);
            
            $formattedParam = [
                'name'                  =>  $param['partner_name'],
                'race_type_id'          =>  $param['partner_race_type_id'],
                'race_id'               =>  $param['partner_race_id'],
                'is_citizen'            =>  $param['partner_is_citizen'],
                'total_share'           =>  $param['partner_total_share'],
                'share_percentage'      =>  $param['partner_share_percentage'],
                'phone_number'          =>  $param['partner_phone_number'],
                'email'                 =>  $param['partner_email'],
                'nric'                  =>  $param['partner_ic'],
                'passport'              =>  $param['partner_passport'] ?? null,
                'address_1'             =>  $param['partner_address_1'],
                'address_2'             =>  $param['partner_address_2'],
                'address_3'             =>  $param['partner_address_3'],
                'postcode'              =>  $param['partner_postcode'],
                'state_id'              =>  $param['partner_state_id'],
                'company_id'            =>  $param['company_id'],
            ];
            $companyPartnerDAO = CompanyPartnerDataObject::addNewCompanyPartner($formattedParam);
        });

        return response()->json([
            'message'   =>  'Maklumat rakan kongsi berjaya ditambah',
            'success'   =>  true,
            'datatable' =>  'company-partner-datatable',
            'loadDatatableConfig'   =>  'loadCompanyPartnerConfig()',
            'route'     => null
        ], 200);
    }


    public function update(Request $request,$id )
    {
        DB::transaction(function () use ($request, $id) {
            $param = $request->all();
            $param['decryptedID']           = decrypt($id);
            $param['partner_race_id']       = (isset($param['partner_race_id']))?decrypt($param['partner_race_id']): null;
            $param['partner_race_type_id']  = (isset($param['partner_race_type_id']))?decrypt($param['partner_race_type_id']): null;
            $param['partner_is_citizen']    = decrypt($param['partner_is_citizen']);

            $param['partner_state_id']  = decrypt($param['partner_state_id']);
            $param['company_id']  = decrypt($param['company_id']);
            
            $formattedParam = [
                'name'                  =>  $param['partner_name'],
                'race_type_id'          =>  $param['partner_race_type_id'],
                'race_id'               =>  $param['partner_race_id'],
                'is_citizen'            =>  $param['partner_is_citizen'],
                'total_share'           =>  $param['partner_total_share'],
                'share_percentage'      =>  $param['partner_share_percentage'],
                'phone_number'          =>  $param['partner_phone_number'],
                'email'                 =>  $param['partner_email'],
                'nric'                  =>  $param['partner_ic'],
                'passport'              =>  $param['partner_passport'] ?? null,
                'address_1'             =>  $param['partner_address_1'],
                'address_2'             =>  $param['partner_address_2'],
                'address_3'             =>  $param['partner_address_3'],
                'postcode'              =>  $param['partner_postcode'],
                'state_id'              =>  $param['partner_state_id'],
                'company_id'            =>  $param['company_id'],
            ];
            $companyPartnerDAO = CompanyPartnerDataObject::updateCompanyPartner($formattedParam,$param['decryptedID']);
        });

        return response()->json([
            'message'   =>  'Maklumat rakan kongsi berjaya dikemaskini',
            'success'   =>  true,
            'datatable' =>  'company-partner-datatable',
            'loadDatatableConfig'   =>  'loadCompanyPartnerConfig()',
            'route'     => null
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        if (isset($param['company_id'])) {
            $param['company_id']  = decrypt($param['company_id']);
        }
        
        return CompanyPartnerDataObject::findAllCompanyPartnersUsingDatatableFormat($param);
    }

    public function delete($id)
    {
        $decryptedID = decrypt($id);
        
        CompanyPartnerDataObject::delete($decryptedID);

        return response()->json([
            'message'   =>  'Maklumat rakan kongsi berjaya dipadam',
            'success'   =>  true,
            'datatable' =>  'company-partner-datatable',
            'loadDatatableConfig'   =>  'loadCompanyPartnerConfig()',
            'route'     => null
        ], 200);
    }

    public function findCompanyPartnerDetailsByCompanyId($id)
    {
        $decryptedId                    =   decrypt($id);
        $partner                        =   CompanyPartnerDataObject::findCompanyPartnerById($decryptedId);
        $partner->race_type_code        =   (isset($partner->raceType))?$partner->raceType->code:null;
        $partner->race_code             =   (isset($partner->race))?$partner->race->code:null;;
        $partner->state_code            =   $partner->state->code;
        $partner->race_type_id          =   (isset($partner->raceType))?encrypt($partner->race_type_id):null;

        if($partner->is_citizen == 1){
            $partner->citizen   =   'WARGANEGARA';
        }
        else{
            $partner->citizen   =   'BUKAN_WARGANEGARA';
        }
        $outputParam                    =   $partner->toArray();

        unset($outputParam['race_type']);
        unset($outputParam['race']);
        unset($outputParam['id']);
        unset($outputParam['company_id']);
        unset($outputParam['state']);

        return response()->json($outputParam, 200);
    }
}
