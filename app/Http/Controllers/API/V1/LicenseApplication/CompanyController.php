<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\ReferenceData\CompanyDataObject;
use App\DataObjects\ReferenceData\CompanyAddressDataObject;
use App\DataObjects\LicenseDataObject;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['company_state_id']  = decrypt($param['company_state_id']);
            $param['user_id']  = decrypt($param['user_id']);
            $param['company_company_type_id']  = decrypt($param['company_company_type_id']);
            $param['company_expiry_date']   =   (isset($param['company_expiry_date'])) ? Carbon::parse($param['company_expiry_date'])->format('Y-m-d'): null;

            $formattedCompanyParam = [
                'name'                  =>  $param['company_name'],
                'company_type_id'       =>  $param['company_company_type_id'],
                'old_registration_number'   =>  $param['company_old_mycoid'],
                'registration_number'   =>  $param['company_mycoid'],
                'expiry_date'           =>  $param['company_expiry_date'],
                'paidup_capital'        =>  $param['company_paidup_capital'],
                'user_id'               =>  $param['user_id'],
            ];

            $companyDAO = CompanyDataObject::createNewCompany($formattedCompanyParam);

            $formattedCompanyAddressParam = [
                'address_1'                 =>  $param['company_address_1'],
                'address_2'                 =>  $param['company_address_2'],
                'address_3'                 =>  $param['company_address_3'],
                'postcode'                  =>  $param['company_postcode'],
                'state_id'                  =>  $param['company_state_id'],
                'phone_number'              =>  $param['company_phone_number'],
                'fax_number'                =>  $param['company_fax_number'],
                'email'                     =>  $param['company_email'],
                'company_id'                => $companyDAO->id
            ];

            $companyAddressDAO = CompanyAddressDataObject::createNewCompanyAddress($formattedCompanyAddressParam);
        });

        return response()->json([
            'message'   =>  'Maklumat syarikat berjaya ditambah',
            'success'   =>  true,
            'datatable' =>  'company-datatable',
            'loadDatatableConfig'   =>  'loadCompanyConfig()',
            'route'     => null
        ], 200);
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request,$id) {
            $param = $request->all();

            $param['company_id']                =   decrypt($id);
            $param['company_state_id']          =   decrypt($param['company_state_id']);
            $param['user_id']                   =   decrypt($param['user_id']);
            $param['company_company_type_id']   =   decrypt($param['company_company_type_id']);
            $param['company_expiry_date']       =   (isset($param['company_expiry_date'])) ? Carbon::parse($param['company_expiry_date'])->format('Y-m-d'): null;

            $formattedCompanyParam = [
                'name'                      =>  $param['company_name'],
                'company_type_id'           =>  $param['company_company_type_id'],
                'registration_number'       =>  $param['company_mycoid'],
                'old_registration_number'   =>  $param['company_old_mycoid'],
                'expiry_date'               =>  $param['company_expiry_date'],
                'paidup_capital'            =>  $param['company_paidup_capital']
            ];

            $companyDAO = CompanyDataObject::updateCompany($formattedCompanyParam,$param['company_id']);

            $formattedCompanyAddressParam = [
                'address_1'                 =>  $param['company_address_1'],
                'address_2'                 =>  $param['company_address_2'],
                'address_3'                 =>  $param['company_address_3'],
                'postcode'                  =>  $param['company_postcode'],
                'state_id'                  =>  $param['company_state_id'],
                'phone_number'              =>  $param['company_phone_number'],
                'fax_number'                =>  $param['company_fax_number'],
                'email'                     =>  $param['company_email'],
            ];

            $companyAddressDAO = CompanyAddressDataObject::updateCompanyAddress($formattedCompanyAddressParam, $param['company_id']);
        });

        return response()->json([
            'message'   =>  'Maklumat syarikat berjaya dikemaskini',
            'success'   =>  true,
            'datatable' =>  'company-datatable',
            'loadDatatableConfig'   =>  'loadCompanyConfig()',
            'route'     => null
        ], 200);
    }

    public function findCompanyDetailAndActiveLicenseByCompanyId($id){
        $decryptedId                    =    decrypt($id);
        $company                        =   CompanyDataObject::findCompanyById($decryptedId);
        $companyType                    =   $company->companyType;
        $company->company_type_code     =   $companyType->code;
        $company->formatted_expiry_date =   (isset($company->expiry_date))?$company->expiry_date->format('d-m-Y'):null;
        $company->paidup_capital        =   (isset($company->paidup_capital))?number_format($company->paidup_capital,2):null;
        $activeLicenses                 =   LicenseDataObject::findActiveWholesaleLicenseAndFactoryLicenseByCompanyId($company->id);

        if($companyType->has_expiry_date == 0){
           $company->has_expiry_date = true;
        }
        else{
            $company->has_expiry_date = false;
        }

        if($companyType->has_paidup_capital == 0){
            $company->has_paidup_capital = true;
        }
        else{
            $company->has_paidup_capital = false;
        }

        $outputParam                            =   $company->toArray();
        $outputParam['company_type_name']       =   $companyType->name;
        $outputParam['licenses']                =   ($activeLicenses != null)?$activeLicenses->map(function($license){
                                                        return[
                                                            'id'                =>  encrypt($license->id),
                                                            'license_type'      =>  $license->licenseType->name,
                                                            'license_type_code' =>  $license->licenseType->code,
                                                            'label'             =>  $license->license_number,
                                                        ];
                                                    }): [];

        unset($outputParam['id']);
        unset($outputParam['state_id']);
        unset($outputParam['company_type_id']);
        unset($outputParam['state']);
        unset($outputParam['company_type']);
        unset($outputParam['user_id']);
        unset($outputParam['company_id']);

        return response()->json($outputParam, 200);
    }

    public function findCompanyDetailsByCompanyId($id)
    {
        $decryptedId                    =    decrypt($id);
        $company                        =   CompanyDataObject::findCompanyById($decryptedId);
        $companyAddresses               =   CompanyAddressDataObject::findCompanyAddressByCompanyId($decryptedId);
        $companyType                    =   $company->companyType;
        $company->company_type_code     =   $companyType->code;
        $company->formatted_expiry_date =   (isset($company->expiry_date))?$company->expiry_date->format('d-m-Y'):null;
        $companyAddresses->state_code   =   $companyAddresses->state->code;

        
        if($companyType->has_expiry_date == 0){
           $company->has_expiry_date = true;
        }
        else{
            $company->has_expiry_date = false;
        }

        if($companyType->has_paidup_capital == 0){
            $company->has_paidup_capital = true;
        }
        else{
            $company->has_paidup_capital = false;
        }

        $arrayedCompany                 =   $company->toArray();
        $arrayedCompanyAddress          =   $companyAddresses->toArray();
        $outputParam                    =   array_merge($arrayedCompany,$arrayedCompanyAddress);
        $outputParam['company_type_name']    =   $companyType->name;

        unset($outputParam['id']);
        unset($outputParam['state_id']);
        unset($outputParam['company_type_id']);
        unset($outputParam['address']['id']);
        unset($outputParam['address']['state']['id']);
        unset($outputParam['address']['state_id']);
        unset($outputParam['address']['company_id']);
        unset($outputParam['state']);
        unset($outputParam['company_type']);
        unset($outputParam['user_id']);
        unset($outputParam['company_id']);

        return response()->json($outputParam, 200);
    }

    public function findCompanyByQuery(Request $request)
    {   
        $param              =   $request->all();
        $param['user_id']   =   decrypt($param['user_id']);
        $companies          =   CompanyDataObject::findCompanyByQueries($param);

        $companies = $companies->map(function($company){
            return [
                'id' => encrypt($company->id),
                'text' => $company->name
            ];
        });

        return response()->json($companies, 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        $param['user_id']   =    decrypt($param['user_id']);
        $param['has_approval_letter']   =    (isset($param['has_approval_letter']))?decrypt($param['has_approval_letter']):null;
        //$param['has_approval_letter']   =    (isset($param['has_approval_letter']))?decrypt($param['has_approval_letter']):null;
        
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
