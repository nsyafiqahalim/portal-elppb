<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\Admin\MaterialDataObject;
use LicenseApplicationMaterialFacade;

class MaterialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        $companyId                      =   decrypt($param['company_id']);
        $licenseType                    =   decrypt($param['license_type']);
        $licenseApplicationType         =   decrypt($param['license_application_type']);

        $param['domain']  = LicenseApplicationMaterialFacade::findMaterialDomainByCompanyId($companyId, $licenseType, $licenseApplicationType);
        
        if (isset($param['license_application_id'])) {
            $param['license_application_id']  = decrypt($param['license_application_id']);
        }
        return MaterialDataObject::findAllMaterialsUsingDatatableFormat($param);
    }
}
