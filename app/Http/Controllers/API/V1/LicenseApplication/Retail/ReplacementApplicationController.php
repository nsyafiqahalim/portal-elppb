<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\Retail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\LicenseApplication\Retail\ReplacementApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\Retail\ReplacementApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\Retail\ReplacementApplicationOutputDataObject;


class ReplacementApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $outputParam = ReplacementApplicationOutputDataObject::submitApplication($param);

            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            
            $outputParam = ReplacementApplicationOutputDataObject::draftApplication($param);
            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();

            $outputParam = ReplacementApplicationOutputDataObject::submitApplication($param);
            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            
            $outputParam = ReplacementApplicationOutputDataObject::draftApplication($param);
            DraftApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
    
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
