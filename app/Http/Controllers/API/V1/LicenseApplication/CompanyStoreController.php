<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\ReferenceData\CompanyStoreDataObject;
use App\DataObjects\ReferenceData\CompanyStoreDataObject as LicenseApplicationCompanyStoreDataObject;

use App\DataObjects\Admin\StoreOwnershipTypeDataObject;
use App\DataObjects\Admin\BuildingTypeDataObject;
use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StateDataObject;

class CompanyStoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['store_store_ownership_type_id']  = decrypt($param['store_store_ownership_type_id']);
            $param['store_building_type_id']  = decrypt($param['store_building_type_id']);
            $param['store_district_id']  = decrypt($param['store_district_id']);
            $param['store_state_id']  = decrypt($param['store_state_id']);
            $param['company_id']  = decrypt($param['company_id']);

            $formattedParam = [
                'store_ownership_type_id'       =>  $param['store_store_ownership_type_id'],
                'building_type_id'              =>  $param['store_building_type_id'],
                'district_id'                   =>  $param['store_district_id'],
                'address_1'                     =>  $param['store_address_1'],
                'address_2'                     =>  $param['store_address_2'],
                'address_3'                     =>  $param['store_address_3'],
                'postcode'                      =>  $param['store_postcode'],
                'state_id'                      =>  $param['store_state_id'],
                'company_id'                    =>  $param['company_id'],
                'email'                         =>  $param['store_email'],
                'phone_number'                  =>  $param['store_phone_number'],
                'fax_number'                    =>  $param['store_faks_number'],
            ];
            
            $companyStoreDAO = CompanyStoreDataObject::addNewCompanyStore($formattedParam);
        });

        return response()->json([
            'message'   =>  'Maklumat stor syarikat berjaya ditambah',
            'success'   =>  true,
            'datatable' =>  'company-store-datatable',
            'loadDatatableConfig'   =>  'loadCompanyStoreConfig()',
            'route'     => null
        ], 200);
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request,$id) {
            $param = $request->all();
            $decryptedID = decrypt($id);

            $param['store_store_ownership_type_id']  = decrypt($param['store_store_ownership_type_id']);
            $param['store_building_type_id']  = decrypt($param['store_building_type_id']);
            $param['store_district_id']  = decrypt($param['store_district_id']);
            $param['store_state_id']  = decrypt($param['store_state_id']);
            $param['company_id']  = decrypt($param['company_id']);

            $formattedParam = [
                'store_ownership_type_id'       =>  $param['store_store_ownership_type_id'],
                'building_type_id'              =>  $param['store_building_type_id'],
                'district_id'                   =>  $param['store_district_id'],
                'address_1'                     =>  $param['store_address_1'],
                'address_2'                     =>  $param['store_address_2'],
                'address_3'                     =>  $param['store_address_3'],
                'postcode'                      =>  $param['store_postcode'],
                'state_id'                      =>  $param['store_state_id'],
                'company_id'                    =>  $param['company_id'],
                'email'                         =>  $param['store_email'],
                'phone_number'                  =>  $param['store_phone_number'],
                'fax_number'                    =>  $param['store_faks_number'],
            ];
            
            $companyStoreDAO = CompanyStoreDataObject::updateCompanyStore($formattedParam,$decryptedID);
        });

        return response()->json([
            'message'   =>  'Maklumat stor syarikat berjaya dikemaskini',
            'success'   =>  true,
            'datatable' =>  'company-store-datatable',
            'loadDatatableConfig'   =>  'loadCompanyStoreConfig()',
            'route'     => null
        ], 200);
    }

    public function updateCompanyStore(Request $request, $id)
    {
        DB::transaction(function () use ($request,$id) {
            $param = $request->all();
            $decryptedID = decrypt($id);
            
            $rules = [
                'store_building_type_id' => ['required', 'string' , 'max:255'],
                'store_district_id' => ['required', 'string' , 'max:255'],
                'store_address_1' => ['required', 'string' , 'max:255'],
                'store_address_2' => ['nullable', 'string' , 'max:255'],
                'store_address_3' => ['nullable', 'string' , 'max:255'],
                'store_postcode' => ['required', 'string' , 'digits:5'],
                'store_state_id' => ['required', 'string' , 'max:255'],
                'store_phone_number' => ['required', 'alpha_dash' ,'max:12'],
                'store_fax_number' => ['nullable', 'string' ,'max:12'],
                'store_email' => ['required', 'email' , 'max:255'],
            ];

            $messages = [
                'store_building_type_id.required'  =>  'Jenis Bangunan stor diperlukan',
                'store_address_1.required'  =>  'Alamat 1 stor diperlukan',
                'store_postcode.required'  =>  'Poskod stor diperlukan',
                'store_state_id.required'  =>  'Negeri stor diperlukan',
                'store_district_id.required'  =>  'Daerah stor diperlukan',
                'store_phone_number.required'  =>  'Nombor Telefon stor diperlukan',
                'store_email.required'  =>  'Emel stor diperlukan',
                'store_phone_number.required' => 'Nombor Telefon  stor diperlukan',
                'store_fax_number.required' => 'Nombor Faks stor hanya antara 10 hingga ke 11 digit',
            ];
            
            $validator = \Validator::make($param, $rules,$messages);
            $validator->validate();

            $param['store_store_ownership_type_id']  = decrypt($param['store_store_ownership_type_id']);
            $param['store_building_type_id']  = decrypt($param['store_building_type_id']);
            
            $param['store_district_id']  = decrypt($param['store_district_id']);
            $param['store_state_id']  = decrypt($param['store_state_id']);

            $param['company_id']  = decrypt($param['company_id']);

            
            $formattedParam = [
                'store_ownership_type_id'       =>  $param['store_store_ownership_type_id'],
                'store_ownership_type_name'       =>  StoreOwnershipTypeDataObject::findStoreOwnershipTypeById($param['store_store_ownership_type_id'])->id,
                'building_type_id'              =>  $param['store_building_type_id'],
                'building_type_name'              =>  BuildingTypeDataObject::findBuildingTypeById($param['store_building_type_id'])->id,
                'district_id'                   =>  $param['store_district_id'],
                'district_name'                   =>  DistrictDataObject::findDistrictById($param['store_district_id'])->id,
                'address_1'                     =>  $param['store_address_1'],
                'address_2'                     =>  $param['store_address_2'],
                'address_3'                     =>  $param['store_address_3'],
                'postcode'                      =>  $param['store_postcode'],
                'state_id'                      =>  $param['store_state_id'],
                'state_name'                    =>  StateDataObject::findStateById($param['store_state_id'])->id,
                'company_id'                    =>  $param['company_id'],
                'email'                         =>  $param['store_email'],
                'phone_number'                  =>  $param['store_phone_number'],
                'fax_number'                    =>  $param['store_faks_number'],
            ];
            
            $companyStoreDAO = LicenseApplicationCompanyStoreDataObject::updateCompanyStore($formattedParam,$decryptedID);
        });

        return response()->json([
            'message'   =>  'Maklumat stor syarikat berjaya dikemaskini',
            'success'   =>  true,
            'datatable' =>  'company-store-datatable',
            'loadDatatableConfig'   =>  'loadCompanyStoreConfig()',
            'route'     => null
        ], 200);
    }
    

    public function datatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['company_id'])) {
            $param['company_id']  = decrypt($param['company_id']);
        }

        if (isset($param['id'])) {
            $param['id']  = decrypt($param['id']);
        }

        if(isset($param['store_id'])){
            $arrayedStoreId = $param['store_id'];
            $param['store_id'] = convertDecryptedArrayInputType($arrayedStoreId);
        }

        return CompanyStoreDataObject::findAllCompanyStoresUsingDatatableFormat($param);
    }

    public function licenseApplicationatatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['company_id'])) {
            $param['company_id']  = decrypt($param['company_id']);
        }

        if (isset($param['license_application_id'])) {
            $param['license_application_id']  = decrypt($param['license_application_id']);
        }

        return LicenseApplicationCompanyStoreDataObject::findAllCompanyStoresUsingDatatableFormat($param);
    }

    

    public function findCompanyStoreDetailsByStoreId($id){
        $decryptedId                            =   decrypt($id);
        $premise                                =   CompanyStoreDataObject::findCompanyStoreById($decryptedId);
        $district                               =   $premise->district;
        $buildingType                           =   $premise->buildingType;
        $storeOwnershipType                     =   $premise->storeOwnershipType;
        $state                                  =   $premise->state;

        $premise->district_code                 =   $district->code;
        $premise->state_code                    =   $state->code;
        $premise->building_type_code            =   $buildingType->code;
        $premise->store_ownership_type_code     =   $storeOwnershipType->code;
        $premise->state_id                      =   encrypt($premise->state_id);

        $outputParam                            =   $premise->toArray();

        unset($outputParam['district']);
        unset($outputParam['store_ownership_type']);
        unset($outputParam['building_type']);
        unset($outputParam['company_id']);
        unset($outputParam['state']);
        unset($outputParam['building_type_id']);
        unset($outputParam['store_ownership_type_code_id']);
        unset($outputParam['id']);

        return response()->json($outputParam, 200);
    }


    public function buyPaddydatatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['company_id'])) {
            $param['company_id']  = decrypt($param['company_id']);
        }
        if (isset($param['store_id'])) {
            $param['id']  = decrypt($param['store_id']);
        }

        return CompanyStoreDataObject::findAllCompanyStoresUsingDatatableFormat($param);
    }
}
