<?php

namespace App\Http\Controllers\API\V1\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\DataObjects\ReferenceData\CompanyPremiseDataObject;

class CompanyPremiseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['premise_business_type_id']  = decrypt($param['premise_business_type_id']);
            $param['premise_building_type_id']  = decrypt($param['premise_building_type_id']);
            $param['premise_dun_id']  = decrypt($param['premise_dun_id']);
            $param['premise_parliament_id']  = decrypt($param['premise_parliament_id']);
            $param['premise_district_id']  = decrypt($param['premise_district_id']);
            $param['premise_state_id']  = decrypt($param['premise_state_id']);
            $param['company_id']  = decrypt($param['company_id']);
            
            $formattedParam = [
                'business_type_id'       =>  $param['premise_business_type_id'],
                'building_type_id'       =>  $param['premise_building_type_id'],
                'dun_id'                =>  $param['premise_dun_id'],
                'district_id'                =>  $param['premise_district_id'],
                'parliament_id'         =>  $param['premise_parliament_id'],
                'address_1'                 =>  $param['premise_address_1'],
                'address_2'                 =>  $param['premise_address_2'],
                'address_3'                 =>  $param['premise_address_3'],
                'postcode'                  =>  $param['premise_postcode'],
                'phone_number'                 =>  $param['premise_phone_number'],
                'fax_number'                 =>  $param['premise_fax_number'],
                'email'                  =>  $param['premise_email'],
                'state_id'                  =>  $param['premise_state_id'],
                'company_id'               =>  $param['company_id']
            ];
            
            $companyPremiseDAO = CompanyPremiseDataObject::addNewCompanyPremise($formattedParam);
        });

        return response()->json([
            'message'   =>  'Maklumat premis syarikat berjaya ditambah',
            'success'   =>  true,
            'datatable' =>  'company-premise-datatable',
            'loadDatatableConfig'   =>  'loadCompanyPremiseConfig()',
            'route'     => null
        ], 200);
    }

    public function update(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $param = $request->all();
            $param['decryptedID']               = decrypt($id);
            $param['premise_business_type_id']  = decrypt($param['premise_business_type_id']);
            $param['premise_building_type_id']  = decrypt($param['premise_building_type_id']);
            $param['premise_dun_id']            = decrypt($param['premise_dun_id']);
            $param['premise_parliament_id']     = decrypt($param['premise_parliament_id']);
            $param['premise_district_id']       = decrypt($param['premise_district_id']);
            $param['premise_state_id']          = decrypt($param['premise_state_id']);
            $param['company_id']  = decrypt($param['company_id']);
            
            $formattedParam = [
                'business_type_id'       =>  $param['premise_business_type_id'],
                'building_type_id'       =>  $param['premise_building_type_id'],
                'dun_id'                =>  $param['premise_dun_id'],
                'district_id'                =>  $param['premise_district_id'],
                'parliament_id'         =>  $param['premise_parliament_id'],
                'address_1'                 =>  $param['premise_address_1'],
                'address_2'                 =>  $param['premise_address_2'],
                'address_3'                 =>  $param['premise_address_3'],
                'postcode'                  =>  $param['premise_postcode'],
                'phone_number'                 =>  $param['premise_phone_number'],
                'fax_number'                 =>  $param['premise_fax_number'],
                'email'                  =>  $param['premise_email'],
                'state_id'                  =>  $param['premise_state_id'],
                'company_id'               =>  $param['company_id']
            ];
            
            $companyPremiseDAO = CompanyPremiseDataObject::updateCompanyPremise($formattedParam,$param['decryptedID']);
        });

        return response()->json([
            'message'   =>  'Maklumat premis syarikat berjaya dikemaskini',
            'success'   =>  true,
            'datatable' =>  'company-premise-datatable',
            'loadDatatableConfig'   =>  'loadCompanyPremiseConfig()',
            'route'     => null
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['company_id'])) {
            $param['company_id']  = decrypt($param['company_id']);
        }
        if (isset($param['id'])) {
            $param['id']  = decrypt($param['id']);
        }
        if (isset($param['premise_id'])) {
            $param['premise_id']  = decrypt($param['premise_id']);
        }
        
        return CompanyPremiseDataObject::findAllCompanyPremisesUsingDatatableFormat($param);
    }

    public function buyPaddydatatable(Request $request)
    {
        $param = $request->all();
        if (isset($param['company_id'])) {
            $param['company_id']  = decrypt($param['company_id']);
        }
        if (isset($param['premise_id'])) {
            $param['id']  = decrypt($param['premise_id']);
        }
        return CompanyPremiseDataObject::findAllCompanyPremisesUsingDatatableFormat($param);
    }

    public function findCompanyPremiseDetailsByPremiseId($id){
        $decryptedId                    =   decrypt($id);
        $premise                        =   CompanyPremiseDataObject::findCompanyPremiseById($decryptedId);
        $dun                            =   $premise->dun;
        $parliament                     =   $premise->parliament;
        $district                       =   $premise->district;
        $buildingType                   =   $premise->buildingType;
        $businessType                   =   $premise->businessType;
        $state                          =   $premise->state;

        $premise->dun_code              =   $dun->code;
        $premise->parliament_code       =   $parliament->code;
        $premise->district_code         =   $district->code;
        $premise->state_code            =   $state->code;
        $premise->building_type_code    =   $buildingType->code;
        $premise->business_type_code    =   $businessType->code;
        $premise->state_id              =   encrypt($premise->state_id);
        $premise->dun_id                =   encrypt($premise->dun_id);
        $premise->parliament_id         =   encrypt($premise->parliament_id);

        $outputParam                    =   $premise->toArray();

        unset($outputParam['dun']);
        unset($outputParam['parliament']);
        unset($outputParam['district']);
        unset($outputParam['business_type']);
        unset($outputParam['building_type']);
        unset($outputParam['company_id']);
        unset($outputParam['state']);
        unset($outputParam['building_type_id']);
        unset($outputParam['business_type_id']);
        unset($outputParam['id']);

        return response()->json($outputParam, 200);
    }
}
