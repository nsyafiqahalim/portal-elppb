<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\FactoryPaddy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\Wholesale\NewApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\Wholesale\NewApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\WholesalOutputDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;

class NewApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['premise_id']                                = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']                                = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']                                  = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['user_id']                                   = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['license_application_license_generation_type_id']    = (isset($param['license_application_license_generation_type_id']))? decrypt($param['license_application_license_generation_type_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $licenseType                                        = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $param['license_application_type_id']               = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id']                           = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $param['status_id']                                 = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_BAHARU')->id;
            $param['license_type_id']                           = $licenseType->id;
            $param['license_type_code']                         = $licenseType->code;

            $outputParam = WholesalOutputDataObject::newOutputParameter($param, 'REFERENCE_DATA');
            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();

            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']  = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['user_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['license_application_license_generation_type_id']    = (isset($param['license_application_license_generation_type_id']))? decrypt($param['license_application_license_generation_type_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BORONG_BAHARU')->id;
            $param['license_application_type_id'] = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;

            $outputParam = WholesalOutputDataObject::draftOutputParameter($param, 'REFERENCE_DATA');
            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();

            $param['premise_id']                    = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']                    = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']                      = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['user_id']                       = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['license_application_license_generation_type_id']    = (isset($param['license_application_license_generation_type_id']))? decrypt($param['license_application_license_generation_type_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
          
            $licenseType                            = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $param['license_application_type_id']   = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id']               = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $param['status_id']                     = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_BAHARU')->id;
            $param['license_type_id']               = $licenseType->id;
            $param['license_type_code']             = $licenseType->code;

            $outputParam = WholesalOutputDataObject::newOutputParameter($param, 'REFERENCE_DATA');
            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            
            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']  = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['user_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BORONG_BAHARU')->id;
            $param['license_application_license_generation_type_id']    = (isset($param['license_application_license_generation_type_id']))? decrypt($param['license_application_license_generation_type_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $param['license_application_type_id'] = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;

            $outputParam = WholesalOutputDataObject::draftOutputParameter($param, 'REFERENCE_DATA');
            DraftApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
