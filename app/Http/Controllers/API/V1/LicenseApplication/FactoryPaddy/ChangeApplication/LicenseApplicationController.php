<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\FactoryPaddy\ChangeApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\FactoryPaddy\ChangeApplication\LicenseApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\FactoryPaddy\ChangeApplication\LicenseApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\ChangeApplication\FactoryPaddy\LicenseApplicationOutputDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationChangeTypeDataObject;
use App\DataObjects\LicenseApplicationDataObject;
use App\DataObjects\Admin\StatusDataObject;

class LicenseApplicationController extends Controller
{
    private const LICENSE_APPLICATION_DATA_SOURCE = 'REFERENCE_DATA';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['original_license_application_decrypted_id']         = decrypt($param['original_license_application_id']);
            $param['premise_id']                                        = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']                                        = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['include_license_id']                                        = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['material_domain']                                   = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $param['user_id']                                           = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['change_type_code']                                  = (isset($param['change_type_code']))? decrypt($param['change_type_code']) : null;
            $param['store_id']                      = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            
            //setup custom parameter and replacing original parameter with other parameter
            $param['license_application_change_type']                   = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode($param['change_type_code']);
            $originalLicenseApplication                                 = LicenseApplicationDataObject::findLicenseApplicationById($param['original_license_application_decrypted_id']);
            $licenseType                                                = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $param['license_application_type_id']                       = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::PERUBAHAN)->id;
            $param['license_type_id']                                   = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $param['status_id']                                         = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_PERUBAHAN')->id;
            $param['license_type_id']                                   = $licenseType->id;
            $param['license_type_code']                                 = $licenseType->code;
            $param['original_load']                                     = $originalLicenseApplication->apply_load;
            $param['apply_load']                                        = $param['apply_load'];
            $param['apply_duration']                                    = $originalLicenseApplication->apply_duration;

            $outputParam = LicenseApplicationOutputDataObject::newOutputParameter($param, self::LICENSE_APPLICATION_DATA_SOURCE);
            
            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();

            $param['original_license_application_decrypted_id'] = decrypt($param['original_license_application_id']);
            $param['include_license_id']  = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $param['user_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BORONG_PERUBAHAN')->id;
            $param['change_type_code']                                  = (isset($param['change_type_code']))? decrypt($param['change_type_code']) : null;
            $param['store_id']                      = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            
            //setup custom parameter and replacing original parameter with other parameter
            $param['license_application_change_type']                   = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode($param['change_type_code']);
            $param['license_application_type_id']                   = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::PERUBAHAN)->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $originalLicenseApplication             =   LicenseApplicationDataObject::findLicenseApplicationById($param['original_license_application_decrypted_id']);
            $param['original_load']                                     = $originalLicenseApplication->apply_load;
            $param['apply_load']                                        = $param['apply_load'];
            $param['apply_duration']                                    = $originalLicenseApplication->apply_duration;
        
            $outputParam = LicenseApplicationOutputDataObject::draftOutputParameter($param, self::LICENSE_APPLICATION_DATA_SOURCE);

            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();

            $param['original_license_application_decrypted_id'] = decrypt($param['original_license_application_id']);
            $param['premise_id']                    = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']                    = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $param['user_id']                       = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['change_type_code']                                  = (isset($param['change_type_code']))? decrypt($param['change_type_code']) : null;
            $param['store_id']                      = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            
            //setup custom parameter and replacing original parameter with other parameter
            $param['license_application_change_type']                   = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode($param['change_type_code']);
            $originalLicenseApplication             =   LicenseApplicationDataObject::findLicenseApplicationById($param['original_license_application_decrypted_id']);
            $licenseType                            = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG);
            $param['license_application_type_id']   = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::PERUBAHAN)->id;
            $param['license_type_id']               = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $param['include_license_id']  = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['status_id']                     = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BORONG_PERUBAHAN')->id;
            $param['license_type_id']               = $licenseType->id;
            $param['license_type_code']             = $licenseType->code;
            $param['original_load']                                     = $originalLicenseApplication->apply_load;
            $param['apply_load']                                        = $param['apply_load'];
            $param['apply_duration']                                    = $originalLicenseApplication->apply_duration;
        
            $outputParam = LicenseApplicationOutputDataObject::newOutputParameter($param, self::LICENSE_APPLICATION_DATA_SOURCE);
            
            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            
            $param['original_license_application_decrypted_id'] = decrypt($param['original_license_application_id']);
            $param['include_license_id']  = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            $param['user_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['change_type_code']                                  = (isset($param['change_type_code']))? decrypt($param['change_type_code']) : null;
            $param['store_id']                      = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            
            //setup custom parameter and replacing original parameter with other parameter
            $param['license_application_change_type']                   = LicenseApplicationChangeTypeDataObject::findLicenseApplicationChangeTypeByCode($param['change_type_code']);
            $originalLicenseApplication             =   LicenseApplicationDataObject::findLicenseApplicationById($param['original_license_application_decrypted_id']);
            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BORONG_PERUBAHAN')->id;
            $param['license_application_type_id'] = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::PERUBAHAN)->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BORONG)->id;
            $param['original_load']                                     = $originalLicenseApplication->apply_load;
            $param['apply_load']                                        = $param['apply_load'];
            $param['apply_duration']                                    = $originalLicenseApplication->apply_duration;
        
            $outputParam = LicenseApplicationOutputDataObject::draftOutputParameter($param, self::LICENSE_APPLICATION_DATA_SOURCE);

            DraftApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
