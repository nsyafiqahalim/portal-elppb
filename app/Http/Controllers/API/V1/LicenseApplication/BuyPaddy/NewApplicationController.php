<?php

namespace App\Http\Controllers\API\V1\LicenseApplication\BuyPaddy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseType;

use App\DataObjects\LicenseApplication\BuyPaddy\NewApplication\DraftApplicationDataObject;
use App\DataObjects\LicenseApplication\BuyPaddy\NewApplication\SubmittedApplicationDataObject;
use App\DataObjects\LicenseApplication\Output\Buypaddy\NewApplicationOutputDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;
use App\DataObjects\Admin\LicenseApplicationTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;

class NewApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $param['premise_id']                                = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']                                = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']                                  = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['applicant_id']                                   = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['license_application_buy_paddy_condition_id']    = (isset($param['license_application_buy_paddy_condition_id']))? decrypt($param['license_application_buy_paddy_condition_id']) : null;
            $param['include_license_id']    = (isset($param['include_license_id']))? decrypt($param['include_license_id']) : null;
            $param['approval_letter_id']    = (isset($param['approval_letter_id']))? decrypt($param['approval_letter_id']) : null;
            $licenseType                                        = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BELI_PADI);
            $param['license_application_type_id']               = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id']                           = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BELI_PADI)->id;
            $param['status_id']                                 = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BELI_PADI_BAHARU')->id;
            $param['license_type_id']                           = $licenseType->id;
            $param['license_type_code']                         = $licenseType->code;

            $outputParam = NewApplicationOutputDataObject::newOutputParameter($param, 'REFERENCE_DATA');
            SubmittedApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
        'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function draft(Request $request)
    {
        DB::transaction(function () use ($request) {
            $param = $request->all();
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']  = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['applicant_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            
            $param['approval_letter_id']    = (isset($param['approval_letter_id']))? decrypt($param['approval_letter_id']) : null;
            $param['license_application_buy_paddy_condition_id']    = (isset($param['license_application_buy_paddy_condition_id']))? decrypt($param['license_application_buy_paddy_condition_id']) : null;

            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BELI_PADI_BAHARU')->id;
            
            $param['license_application_type_id'] = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BELI_PADI)->id;
             
            $outputParam = NewApplicationOutputDataObject::draftOutputParameter($param, 'REFERENCE_DATA');
            DraftApplicationDataObject::store($outputParam);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function submitDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $param['premise_id']                    = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']                    = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']                      = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['applicant_id']                       = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['approval_letter_id']    = (isset($param['approval_letter_id']))? decrypt($param['approval_letter_id']) : null;
            $param['license_application_buy_paddy_condition_id']    = (isset($param['license_application_buy_paddy_condition_id']))? decrypt($param['license_application_buy_paddy_condition_id']) : null;

            $licenseType                            = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BELI_PADI);
            $param['license_application_type_id']   = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id']               = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BELI_PADI)->id;
            $param['status_id']                     = StatusDataObject::findStatusByCode('PERMOHONAN_BAHARU_BELI_PADI_BAHARU')->id;
            $param['license_type_id']               = $licenseType->id;
            $param['license_type_code']             = $licenseType->code;
            $outputParam = NewApplicationOutputDataObject::newOutputParameter($param, 'REFERENCE_DATA');
            SubmittedApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohonan berjaya dihantar',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function updateDraft(Request $request, $id)
    {
        DB::transaction(function () use ($request, $id) {
            $decryptedID = decrypt($id);
            $param = $request->all();
            $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
            $param['premise_id']  = (isset($param['premise_id']))? decrypt($param['premise_id']) : null;
            $param['company_id']  = (isset($param['company_id']))? decrypt($param['company_id']) : null;
            $param['store_id']  = (isset($param['store_id']))? decrypt($param['store_id']) : null;
            $param['applicant_id']  = (isset($param['user_id']))? decrypt($param['user_id']) : null;
            $param['status_id'] = StatusDataObject::findStatusByCode('DRAF_BELI_PADI_BAHARU')->id;
            $param['approval_letter_id']    = (isset($param['approval_letter_id']))? decrypt($param['approval_letter_id']) : null;

            $param['license_application_buy_paddy_condition_id']    = (isset($param['license_application_buy_paddy_condition_id']))? decrypt($param['license_application_buy_paddy_condition_id']) : null;

            $param['license_application_type_id'] = LicenseApplicationTypeDataObject::findLicenseApplicationTypeByCode(LicenseApplicationType::BAHARU)->id;
            $param['license_type_id'] = LicenseTypeDataObject::findLicenseTypeByCode(LicenseType::BELI_PADI)->id;

            $outputParam = NewApplicationOutputDataObject::draftOutputParameter($param, 'REFERENCE_DATA');
            DraftApplicationDataObject::update($outputParam, $decryptedID);
        });

        return response()->json([
            'message'   =>  'Permohon berjaya disimpan',
            'success'   =>  true,
            'route'     => route('license_application.list_of_application')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        $param['material_domain']                           = (isset($param['material_domain']))? decrypt($param['material_domain']) : null;
            
        return CompanyDataObject::findAllCompanyUsingDatatableFormat($param);
    }
}
