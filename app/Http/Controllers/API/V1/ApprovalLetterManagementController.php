<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\Admin\ApprovalLetterType;


use App\DataObjects\ApprovalLetterDataObject;
use App\DataObjects\Admin\ApprovalLetterTypeDataObject;
use App\DataObjects\Admin\StatusDataObject;

class ApprovalLetterManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return ApprovalLetterDataObject::findMyApprovalLetterUsingDatatableFormat($param);
    }

    public function isBuypaddyApprovalLetterExist(Request $request)
    {
        $param = $request->all();
        
        $param = $request->all();
        $param['company_id']                =   decrypt($param['company_id']);
        $param['status_id']                 =   StatusDataObject::findStatusByCode('SURAT_KELULUSAN_BERSYARAT_AKTIF')->id;
        $param['approval_letter_type_id']   =   ApprovalLetterTypeDataObject::findApprovalLetterTypeByCode(ApprovalLetterType::BELI_PADI)->id;
        $approvalLetter                     =   ApprovalLetterDataObject::findActiveApprovalLetterExistByCompanyIdAndapprovalLetterTypeId($param);
        dd($param['approval_letter_type_id']);

        if (isset($approvalLetter)) {
            $status = 'EXIST';
            $approvalLetterId = encrypt($approvalLetter->id);
            $approvalLetterNumber = $approvalLetter->approval_letter_number;
            $expiryDate = $approvalLetter->expiry_date->format('d/m/Y');
            $premiseId = $approvalLetter->approvalLetterApplication->companyPremise->company_premise_id;
            $storeId = $approvalLetter->approvalLetterApplication->companyStore->company_store_id;
            $licesenApplicationBuypaddyConditionId  =   $approvalLetter->approvalLetterApplication->buyPaddy->buyPaddyCondition->id;
            $licesenApplicationBuypaddyConditionName  =   $approvalLetter->approvalLetterApplication->buyPaddy->buyPaddyCondition->name;
            
        } else {
            $status = 'NOT_EXIST';
            $approvalLetterNumber = null;
            $approvalLetterId = null;
            $expiryDate = null;
            $premiseId = null;
            $storeId = null;
            $licesenApplicationBuypaddyConditionId  =   null;
            $licesenApplicationBuypaddyConditionName    =   null;
        }
        return response()->json([
            'status'   =>  $status,
            'approval_letter_id'    =>  $approvalLetterId,
            'approval_letter_number'  =>  $approvalLetterNumber,
            'expiry_date'  =>  $expiryDate,
            'premise_id'  =>  encrypt($premiseId),
            'store_id'  =>  encrypt($storeId),
            'license_application_buy_paddy_condition_id'    =>  encrypt($licesenApplicationBuypaddyConditionId),
            'license_application_buy_paddy_condition_name'  =>  $licesenApplicationBuypaddyConditionName,
        ], 200);
    }
}
