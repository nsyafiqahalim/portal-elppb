<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DataObjects\Portal\AnnouncementDataObject;
use App\DataObjects\Portal\GalleryDataObject;
use App\DataObjects\Portal\SettingDataObject;
use App\DataObjects\Portal\LicenseDataObject;
use App\DataObjects\Portal\PermitDataObject;
use App\DataObjects\Portal\FaqDataObject;

class WebsiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $announcementDAOs = AnnouncementDataObject::findAllPublishedAnnouncements();
        $galleryDAOs = GalleryDataObject::findNumberOfImage(6);

        return view('website.index', compact('announcementDAOs', 'galleryDAOs'));
    }

    public function infoChief()
    {
        $nameD = SettingDataObject::findSettingByCode('director_name');
        $infoD = SettingDataObject::findSettingByCode('director_remarks');
        $imageD = SettingDataObject::findSettingByCode('director_images');
        $medalD = SettingDataObject::findSettingByCode('director_medal');

        return view('website.info.chief', compact('nameD', 'infoD', 'imageD', 'medalD'));
    }

    public function infoHistory()
    {
        $infoHistory = SettingDataObject::findSettingByCode('info_1_description');
        $imageHistory = SettingDataObject::findSettingByCode('info_1_image');

        return view('website.info.history', compact('infoHistory', 'imageHistory'));
    }

    public function infoRole()
    {
        $info1 = SettingDataObject::findSettingByCode('SKPB_description');
        $image1 = SettingDataObject::findSettingByCode('SKPB_image');

        return view('website.info.role', compact('info1', 'image1'));
    }

    public function infoRules()
    {
        $nameRules = SettingDataObject::findSettingByCode('info_2_title');
        $infoRules = SettingDataObject::findSettingByCode('info_2_description');
        $imageRules = SettingDataObject::findSettingByCode('info_2_image');

        return view('website.info.rules', compact('nameRules', 'infoRules', 'imageRules'));
    }

    public function contact()
    {
        return view('website.contact');
    }

    public function mainContact()
    {
        return view('website.main-contact');
    }

    public function license()
    {
        $licenseDAOs = LicenseDataObject::findAllPublishedLicense();
        return view('website.license', compact('licenseDAOs'));
    }

    public function permit()
    {
        $permitDAOs = PermitDataObject::findAllPublishedPermit();

        return view('website.permit', compact('permitDAOs'));
    }

    public function faq()
    {
        $faqDAOs = FaqDataObject::findAllPublishedFaqs();

        return view('website.faq', compact('faqDAOs'));
    }

    public function gallery()
    {
        $galleryDAOs = GalleryDataObject::findImageWithPagination(12);

        return view('website.gallery', compact('galleryDAOs'));
    }

    public function manualLicense()
    {
        $galleryDAOs = GalleryDataObject::findImageWithPagination(12);

        return view('website.manual-license');
    }

    public function manualPermit()
    {
        return view('website.manual-permit');
    }

    public function userManual()
    {
        return view('website.user-manual');
    }

    public function manualManagement()
    {
        return view('website.manual-management');
    }

    public function conditionApplication()
    {
        return view('website.condition-application');
    }

    public function licenseResponsibility()
    {
        return view('website.license-responsibility');
    }

    public function licenseFeeRate()
    {
        return view('website.license-fee-rate');
    }

    public function mobileApp()
    {
        return view('website.mobile-app');
    }

    public function term()
    {
        return view('website.basic.term');
    }

    public function deny()
    {
        return view('website.basic.deny');
    }

    public function privacy()
    {
        return view('website.basic.privacy');
    }

    public function security()
    {
        return view('website.basic.security');
    }

    public function forgotID()
    {
        return view('website.forgot-id');
    }
}
