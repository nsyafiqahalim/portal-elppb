<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RiceGradeDataObject;
use App\DataObjects\Admin\TotalGradeDataObject;
use App\DataObjects\Admin\SpinTypeDataObject;
use App\DataObjects\Admin\StockTypeDataObject;
use App\DataObjects\Admin\LicenseTypeDataObject;

class CompanyStockController extends Controller
{
    public function index()
    {
        return view('company-stock.index');
    }

    public function view()
    {
        return view('company-stock.view');
    }

    public function edit()
    {
        $riceGrades         =   RiceGradeDataObject::findAllActiveRiceGrades();
        $totalGrades        =   TotalGradeDataObject::findAllActiveTotalGrades();
        $spinTypes          =   SpinTypeDataObject::findAllActiveSpinTypes();
        $stockTypes         =   StockTypeDataObject::findAllActiveStockTypes();
        
        return view('company-stock.edit');
    }

    public function add()
    {
        return  view('company-stock.add');
    }
}
