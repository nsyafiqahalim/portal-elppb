<?php

namespace App\Http\Controllers\ApprovalLetterApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApprovalLetterController extends Controller
{
    public function retail()
    {
    }

    public function wholesale()
    {
        return view('approval-letter-application.wholesale.new.add');
    }

    public function importExport()
    {
        return view('approval-letter-application.import.new.add');
    }

    public function buyPaddy()
    {
        return view('approval-letter-application.buy-paddy');
    }

    public function factory()
    {
        return view('approval-letter-application.factory');
    }

    public function listOfApprovalLetterApplication()
    {
        return view('approval-letter-application.list-of-approval-letter-application-statuses');
    }
}
