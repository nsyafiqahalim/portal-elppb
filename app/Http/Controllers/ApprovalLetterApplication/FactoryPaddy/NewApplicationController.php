<?php

namespace App\Http\Controllers\ApprovalLetterApplication\FactoryPaddy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\ApprovalLetterApplication\Input\FactoryPaddy\NewApplicationDataObject;

class NewApplicationController extends Controller
{
    public function add()
    {
        $inputParam = NewApplicationDataObject::newInputParameter();

        return view('approval-letter-application.factory-paddy.new-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = NewApplicationDataObject::draftInputParameter($decryptedId);
        return view('approval-letter-application.factory-paddy.new-application.draft', compact('inputParam'));
    }
}
