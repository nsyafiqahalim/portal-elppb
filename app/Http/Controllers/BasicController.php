<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BasicController extends Controller
{
    public function privacy()
    {
      return view('website.basic.privacy');
    }

    public function deny()
    {
      return view('website.basic.deny');
    }

    public function security()
    {
      return view('website.basic.security');
    }

    public function term()
    {
      return view('website.basic.term');
    }
}
