<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApprovalLetterManagementController extends Controller
{
    public function index()
    {
        return view('approval-letter-management.index');
    }

    public function view()
    {
        return view('approval-letter-management.view');
    }

    public function edit()
    {
        return view('approval-letter-management.edit');
    }

    public function add()
    {
        return view('approval-letter-management.add');
    }
}
