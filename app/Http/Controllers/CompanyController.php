<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        return view('company.index');
    }

    public function view()
    {
        return view('company.view');
    }

    public function edit()
    {
        return view('company.edit');
    }

    public function add()
    {
        return view('company.add');
    }
}
