<?php

namespace App\Http\Controllers\LicenseApplication\BuyPaddy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\BuyPaddy\NewApplicationDataObject;

class NewApplicationController extends Controller
{
    public function add()
    {
        $inputParam = NewApplicationDataObject::newInputParameter();

        return view('license-application.buy-paddy.new-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = NewApplicationDataObject::draftInputParameter($decryptedId);
        return view('license-application.buy-paddy.new-application.draft', compact('inputParam'));
    }
}
