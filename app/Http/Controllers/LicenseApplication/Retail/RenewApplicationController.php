<?php

namespace App\Http\Controllers\LicenseApplication\Retail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\Retail\RenewApplicationDataObject;

class RenewApplicationController extends Controller
{
    private const LICENSE_APPLICATION_DATA_SOURCE = 'REFERENCE_DATA';
    
    public function add($id)
    {
        $decryptedId = decrypt($id);

        $inputParam = RenewApplicationDataObject::newInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);

        return view('license-application.retail.renew-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = RenewApplicationDataObject::draftInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);
        return view('license-application.retail.renew-application.draft', compact('inputParam'));
    }
}
