<?php

namespace App\Http\Controllers\LicenseApplication\Retail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\Retail\NewApplicationDataObject;

class NewApplicationController extends Controller
{
    public function add()
    {
        $inputParam = NewApplicationDataObject::newInputParameter();

        return view('license-application.retail.new-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = NewApplicationDataObject::draftInputParameter($decryptedId);
        return view('license-application.retail.new-application.draft', compact('inputParam'));
    }
}
