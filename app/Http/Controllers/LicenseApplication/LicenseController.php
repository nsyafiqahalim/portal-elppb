<?php

namespace App\Http\Controllers\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    public function retail()
    {
    }

    public function wholesale()
    {
        return view('license-application.wholesale.new.add');
    }

    public function importExport()
    {
        return view('license-application.import.new.add');
    }

    public function buyPaddy()
    {
        return view('license-application.buy-paddy');
    }

    public function factory()
    {
        return view('license-application.factory');
    }

    public function listOfApplication()
    {
        return view('license-application.list-of-application-statuses');
    }

    public function listOfPermit()
    {
        return view('license-application.list-of-permit');
    }
}
