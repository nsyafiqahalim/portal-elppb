<?php

namespace App\Http\Controllers\LicenseApplication\FactoryPaddy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\FactoryPaddy\NewApplicationDataObject;

class NewApplicationController extends Controller
{
    public function add()
    {
        $inputParam = NewApplicationDataObject::newInputParameter();

        return view('license-application.export.new-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = NewApplicationDataObject::draftInputParameter($decryptedId);
        return view('license-application.export.new-application.draft', compact('inputParam'));
    }
}
