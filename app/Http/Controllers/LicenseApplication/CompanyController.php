<?php

namespace App\Http\Controllers\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        return view('license-application.company.index');
    }

    public function view()
    {
        return view('license-application.company.view');
    }

    public function edit()
    {
        return view('license-application.company.edit');
    }

    public function add()
    {
        return view('license-application.company.add');
    }
}
