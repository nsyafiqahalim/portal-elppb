<?php

namespace App\Http\Controllers\LicenseApplication\Import\ChangeApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\Import\ChangeApplication\CompanyDataObject;

class CompanyApplicationController extends Controller
{
    private const LICENSE_APPLICATION_DATA_SOURCE = 'REFERENCE_DATA';
    
    public function add($id)
    {
        $decryptedId = decrypt($id);

        $inputParam = CompanyDataObject::newInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);
        
        return view('license-application.import.change-application.company-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = CompanyDataObject::draftInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);
        return view('license-application.import.change-application.company-application.draft', compact('inputParam'));
    }
}
