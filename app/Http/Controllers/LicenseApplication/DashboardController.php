<?php

namespace App\Http\Controllers\LicenseApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public function index()
    {
        return view('license-application.index');
    }
}
