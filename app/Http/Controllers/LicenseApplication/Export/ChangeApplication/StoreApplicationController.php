<?php

namespace App\Http\Controllers\LicenseApplication\Export\ChangeApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\Export\ChangeApplication\StoreDataObject;

class StoreApplicationController extends Controller
{
    private const LICENSE_APPLICATION_DATA_SOURCE = 'REFERENCE_DATA';
    
    public function add($id)
    {
        $decryptedId = decrypt($id);

        $inputParam = StoreDataObject::newInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);

        return view('license-application.export.change-application.store-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = StoreDataObject::draftInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);
        return view('license-application.export.change-application.store-application.draft', compact('inputParam'));
    }
}
