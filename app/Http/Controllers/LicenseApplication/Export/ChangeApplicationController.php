<?php

namespace App\Http\Controllers\LicenseApplication\Export;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\LicenseApplication\Input\Export\ChangeApplicationDataObject;

class ChangeApplicationController extends Controller
{
    private const LICENSE_APPLICATION_DATA_SOURCE = 'REFERENCE_DATA';
    
    public function add($id)
    {
        $decryptedId = decrypt($id);

        $inputParam = ChangeApplicationDataObject::newInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);

        return view('license-application.export.change-application.add', compact('inputParam'));
    }

    public function draft($id)
    {
        $decryptedId = decrypt($id);
        $inputParam = ChangeApplicationDataObject::draftInputParameter($decryptedId, self::LICENSE_APPLICATION_DATA_SOURCE);
        return view('license-application.export.change-application.draft', compact('inputParam'));
    }
}
