<?php

namespace App\Providers\LicenseApplication;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Facades\Processors\LicenseApplication\GenerationType;

class GenerationTypeServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('license-application-generation-type',function(){
            return new GenerationType();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
