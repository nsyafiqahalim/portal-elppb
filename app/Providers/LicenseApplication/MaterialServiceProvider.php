<?php

namespace App\Providers\LicenseApplication;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Facades\Processors\LicenseApplication\Material;

class MaterialServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('license-application-material',function(){
            return new Material();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
