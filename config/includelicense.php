<?php

return [
    'NEW_WHOLESALE'     =>  [
        'NEW_WHOLESALE' => [
            'C'   
        ],
        'NEW_WHOLESALE_NEW_IMPORT' => [
            'C',
            'A'
        ],
        'NEW_WHOLESALE_NEW_EXPORT' => [
            'C',
            'B'
        ],
        'NEW_WHOLESALE_NEW_IMPORT_NEW_EXPORT' => [
            'C',
            'B',
            'A'
        ],
    ],
    'RENEW_WHOLESALE'   =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_WHOLESALE_NEW_IMPORT_NEW_EXPORT' => [
            'C',
            'B',
            'A'  
        ],
        'RENEW_WHOLESALE_NEW_IMPORT' => [
            'C',
            'A'  
        ],
        'RENEW_WHOLESALE_NEW_EXPORT' => [
            'C',
            'B'  
        ] 
    ],
    'RENEW_WHOLESALE_RENEW_IMPORT'  =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_WHOLESALE_RENEW_IMPORT'  =>  [
            'C',
            'A'  
        ],
        'RENEW_WHOLESALE_RENEW_IMPORT_NEW_EXPORT'  =>  [
            'C',
            'B',
            'A'  
        ],
    ],
    'RENEW_WHOLESALE_RENEW_EXPORT'  =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_WHOLESALE_RENEW_EXPORT'  =>[
            'C',
            'B'  
        ],
        'RENEW_WHOLESALE_RENEW_EXPORT_NEW_IMPORT'   =>[
            'C',
            'B',
            'A' 
        ]
    ],
    'RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT' =>  [
            'RENEW_WHOLESALE' => [
                'C'   
            ],
            'RENEW_WHOLESALE_RENEW_IMPORT'  =>  [
                'C',
                'A'
            ],
            'RENEW_WHOLESALE_RENEW_EXPORT'  =>  [
                'C',
                'B'  
            ],
            'RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT' =>  [
                'C',
                'B',
                'A' 
            ]
    ],
    'CANCEL_WHOLESALE' =>  [
            'CANCEL_WHOLESALE' => [
                'C'   
            ],
    ],
    'CANCEL_WHOLESALE_ALL' =>  [
            'CANCEL_WHOLESALE_ALL'  =>  [
                'A',
                'B',
                'C'
            ],
            'CANCEL_WHOLESALE_IMPORT_EXPORT' => [
                'A',
                'B'   
            ],
            'CANCEL_WHOLESALE_IMPORT' => [
                'A'   
            ],
            'CANCEL_WHOLESALE_EXPORT' => [
                'B'   
            ],
    ],
    'CANCEL_WHOLESALE_IMPORT' =>  [
        'CANCEL_WHOLESALE_IMPORT'    =>  [
            'A',
            'C'
        ],
         'CANCEL_IMPORT'    =>  [
            'A'
        ],
    ],
    'CANCEL_WHOLESALE_EXPORT' =>  [
        'CANCEL_WHOLESALE_EXPORT'    =>  [
            'B',
            'C'
         ],
         'CANCEL_EXPORT'    =>  [
            'B'
         ],
    ],    
    //
    'REPLACEMENT_WHOLESALE' =>  [
            'REPLACEMENT_WHOLESALE' => [
                'C'   
            ],
    ],
    'REPLACEMENT_WHOLESALE_ALL' =>  [
            'REPLACEMENT_WHOLESALE_ALL'  =>  [
                'A',
                'B',
                'C'
            ],
            'REPLACEMENT_WHOLESALE_IMPORT_EXPORT' => [
                'A',
                'B'   
            ],
            'REPLACEMENT_WHOLESALE_IMPORT' => [
                'A'   
            ],
            'REPLACEMENT_WHOLESALE_EXPORT' => [
                'B'   
            ],
    ],
    'REPLACEMENT_WHOLESALE_IMPORT' =>  [
        'REPLACEMENT_WHOLESALE_IMPORT'    =>  [
            'A',
            'C'
        ],
         'REPLACEMENT_IMPORT'    =>  [
            'A'
        ],
    ],
    'REPLACEMENT_WHOLESALE_EXPORT' =>  [
        'REPLACEMENT_WHOLESALE_EXPORT'    =>  [
            'B',
            'C'
         ],
         'REPLACEMENT_EXPORT'    =>  [
            'B'
         ],
    ] 
    ,
    //LESEN IMPORT
    'NEW_IMPORT'     =>  [
        'NEW_IMPORT' => [
            'A'   
        ],
        'NEW_IMPORT_EXPORT' => [
            'A',
            'B'
        ]
    ],
    'NEW_IMPORT_WHOLESALE'  =>  [
        'NEW_IMPORT_WHOLESALE'  =>  [
            'C',
            'A'
        ],
        'NEW_IMPORT_WHOLESALE_EXPORT' => [
            'C',
            'B',
            'A'
        ],
    ],
    'RENEW_IMPORT'   =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_IMPORT_WHOLESALE' => [
            'C',
            'A'  
        ],
        'RENEW_WHOLESALE_NEW_IMPORT' => [
            'C',
            'A'  
        ],
        'RENEW_WHOLESALE_IMPORT_EXPORT' => [
            'C',
            'B',
            'A'  
        ],
    ],
    'RENEW_IMPORT_EXPORT'   =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_IMPORT_WHOLESALE' => [
            'C',
            'A'  
        ],
        'RENEW_WHOLESALE_IMPORT_EXPORT' => [
            'C',
            'B',
            'A'  
        ],
    ],

    //LESEN EXPORT
    'NEW_EXPORT'     =>  [
        'NEW_EXPORT' => [
            'B'   
        ],
        'NEW_EXPORT_IMPORT' => [
            'B',
            'A'
        ],
    ],
    'NEW_EXPORT_WHOLESALE'  =>  [
        'NEW_EXPORT_WHOLESALE'  =>  [
            'C',
            'A'
        ],
        'NEW_EXPORT_WHOLESALE_IMPORT' => [
            'C',
            'B',
            'A'
        ],
    ],
    'RENEW_EXPORT'   =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_IMPORT_WHOLESALE' => [
            'C',
            'B'  
        ],
        'RENEW_WHOLESALE_NEW_IMPORT' => [
            'C',
            'B'  
        ],
        'RENEW_WHOLESALE_IMPORT_EXPORT' => [
            'C',
            'B',
            'A'  
        ],
    ],
    'RENEW_EXPORT'   =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_EXPORT_WHOLESALE' => [
            'C',
            'B'  
        ],
        'RENEW_WHOLESALE_EXPORT_IMPORT' => [
            'C',
            'B',
            'A'  
        ],
    ],
    'RENEW_EXPORT_IMPORT'   =>  [
        'RENEW_WHOLESALE' => [
            'C'   
        ],
        'RENEW_EXPORT_WHOLESALE' => [
            'C',
            'B'  
        ],
        'RENEW_WHOLESALE_EXPORT_IMPORT' => [
            'C',
            'B',
            'A'  
        ],
    ],
];
