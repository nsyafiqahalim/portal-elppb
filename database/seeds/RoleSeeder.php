<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create role
        $developer = Role::create(
            ['name' => 'Developer', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $superAdministrator = Role::create(
            ['name' => 'Super Administrator', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );


        $administrator = Role::create(
            ['name' => 'Administrator', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $assessor = Role::create(
            ['name' => 'Ketua Unit Cawangan', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $student = Role::create(
            ['name' => 'Ketua Unit HQ', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $lecturer = Role::create(
            ['name' => 'Pegawai Siasatan', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $lecturer = Role::create(
            ['name' => 'Penjana Lesen', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $lecturer = Role::create(
            ['name' => 'Pemohon Lesen', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );
        $lecturer = Role::create(
            ['name' => 'Pegawai Kaunter', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $lecturer = Role::create(
            ['name' => 'Ketua Pengarah', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $applicant = Role::create(
            ['name' => 'Pelesen', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        //assign permission
        //1. Assign all permission to developer and super administrator
        $allPermissions = Permission::get()->pluck('name')->toArray();

        $developer->givePermissionTo(
            $allPermissions
        );
    }
}
