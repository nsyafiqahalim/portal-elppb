<?php

//Route::group(['middleware' => 'auth'], function () {
    Route::prefix('v1')->group(function () { 
        Route::prefix('permohonan-lesen')->group(function () {
            Route::get('/', 'API\V1\LicenseApplication\DashboardController@index')->name('api.license_application.index');

            Route::prefix('kaum')->group(function () {
                Route::get('/cari-kaum-berdasarkan-jenis-kaum/{raceTypeId}', 'API\V1\Admin\RaceController@findRacesByRaceTypeId')->name('api.race.find_race_by_race_type_id');
            });

            Route::prefix('kaum')->group(function () {
                Route::get('/cari-daerah-dan-parlimen-berdasarkan-negeri/{stateId}', 'API\V1\Admin\StateController@findDistrictsAndParliamentsByStateId')->name('api.state.find_district_and_parliament_by_id');
            });

            Route::prefix('dun')->group(function () {
                Route::get('/cari-dun-berdasarkan-parlimen/{parliamentId}', 'API\V1\Admin\DunController@findDunsByParliamentId')->name('api.dun.find_dun_by_parliament_id');
            });
            
            Route::prefix('penyata-stok')->group(function () {
                Route::get('/', 'API\V1\LicenseApplication\CompanyStockStatementController@index')->name('api.license_application.company_stock_statement.index');
                Route::post('/tambah', 'API\V1\LicenseApplication\CompanyStockStatementController@store')->name('api.license_application.company_stock_statement.store');
                Route::middleware('StoreCompanyValidationMiddleware')
                ->patch('/kemaskini/{id}', 'API\V1\LicenseApplication\CompanyStockStatementController@update')->name('api.license_application.company_stock_statement.update');
                Route::get('/senarai', 'API\V1\LicenseApplication\CompanyStockStatementController@datatable')->name('api.license_application.company_stock_statement.datatable');
            });

            Route::prefix('syarikat')->group(function () {
                Route::get('/', 'API\V1\LicenseApplication\CompanyController@index')->name('api.license_application.company.index');
                Route::middleware('StoreCompanyValidationMiddleware')
                ->post('/tambah', 'API\V1\LicenseApplication\CompanyController@store')->name('api.license_application.company.store');
                Route::middleware('StoreCompanyValidationMiddleware')
                ->patch('/kemaskini/{id}', 'API\V1\LicenseApplication\CompanyController@update')->name('api.license_application.company.update');
                Route::get('/senarai', 'API\V1\LicenseApplication\CompanyController@datatable')->name('api.license_application.company.datatable');
                Route::get('/cari-syarikat-berdasarkan-id-syarikat/{id}', 'API\V1\LicenseApplication\CompanyController@findCompanyDetailsByCompanyId')->name('api.license_application.company.find_company_detail_by_company_id');
                Route::get('/cari-syarikat-berdasarkan-kuiri', 'API\V1\LicenseApplication\CompanyController@findCompanyByQuery')->name('api.license_application.company.find_company_by_query');
                Route::get('/cari-maklumat-syarikat-dan-lesen-aktif-berdasarkan-syarikat-id/{companyId}', 'API\V1\LicenseApplication\CompanyController@findCompanyDetailAndActiveLicenseByCompanyId')->name('api.license_application.company.find_company_detail_and_active_license_by_company_id');
            });

            Route::prefix('rakan-kongsi')->group(function () {
                Route::get('/', 'API\V1\LicenseApplication\CompanyPartnerController@index')->name('api.license_application.company-partner.index');
                
                Route::middleware('StoreCompanyPartnerValidationMiddleware')
                ->post('/tambah', 'API\V1\LicenseApplication\CompanyPartnerController@store')->name('api.license_application.company-partner.store');
                Route::middleware('StoreCompanyPartnerValidationMiddleware')
                ->patch('/kemaskini/{id}', 'API\V1\LicenseApplication\CompanyPartnerController@update')->name('api.license_application.company-partner.update');
                
                Route::middleware('StoreRetailCompanyPartnerValidationMiddleware')
                ->post('/runcit/tambah', 'API\V1\LicenseApplication\CompanyPartnerController@store')->name('api.license_application.company-partner-retail.store');
                Route::middleware('StoreRetailCompanyPartnerValidationMiddleware')
                ->patch('/runcit/kemaskini/{id}', 'API\V1\LicenseApplication\CompanyPartnerController@update')->name('api.license_application.company-partner-retail.update');
                
            
                Route::delete('/padam/{id}', 'API\V1\LicenseApplication\CompanyPartnerController@delete')->name('api.license_application.company-partner.delete');    
                Route::get('/senarai', 'API\V1\LicenseApplication\CompanyPartnerController@datatable')->name('api.license_application.company-partner.datatable');
                Route::get('/cari-syarikat-berdasarkan-id-syarikat/{id}', 'API\V1\LicenseApplication\CompanyPartnerController@findCompanyPartnerDetailsByCompanyId')->name('api.license_application.company.find_company_partner_detail_by_company_id');
            });

            Route::prefix('premis')->group(function () {
                Route::get('/', 'API\V1\LicenseApplication\CompanyPremiseController@index')->name('api.license_application.company-premise.index');
                Route::middleware('StoreCompanyPremiseValidationMiddleware')
                ->post('/tambah', 'API\V1\LicenseApplication\CompanyPremiseController@store')->name('api.license_application.company-premise.store');
                Route::middleware('StoreCompanyPremiseValidationMiddleware')
                ->patch('/kemaskini/{id}', 'API\V1\LicenseApplication\CompanyPremiseController@update')->name('api.license_application.company-premise.update');
                Route::get('/senarai', 'API\V1\LicenseApplication\CompanyPremiseController@datatable')->name('api.license_application.company-premise.datatable');
                Route::get('/senarai-premis-untuk-jual-padi', 'API\V1\LicenseApplication\CompanyPremiseController@buyPaddydatatable')->name('api.license_application.company-premise.buy-paddy-datatable');
                Route::get('/cari-premise-syarikat-berdasarkan-id-premis-syarikat/{id}', 'API\V1\LicenseApplication\CompanyPremiseController@findCompanyPremiseDetailsByPremiseId')->name('api.license_application.company-premise.find_company_premise_detail_by_premise_id');
            });

            Route::prefix('stor')->group(function () {
                Route::get('/', 'API\V1\LicenseApplication\CompanyStoreController@index')->name('api.license_application.company-store.index');
                Route::middleware('StoreCompanyStoreValidationMiddleware')->post('/tambah', 'API\V1\LicenseApplication\CompanyStoreController@store')->name('api.license_application.company-store.store');
                Route::get('/senarai', 'API\V1\LicenseApplication\CompanyStoreController@datatable')->name('api.license_application.company-store.datatable');
                Route::get('/senarai-stor-permohonan-lesen', 'API\V1\LicenseApplication\CompanyStoreController@licenseApplicationatatable')->name('api.license_application.license-application-company-store.datatable');
                
                Route::get('/senarai-stor-untuk-jual-padi', 'API\V1\LicenseApplication\CompanyStoreController@buyPaddydatatable')->name('api.license_application.company-store.buy-paddy-datatable');
                Route::middleware('StoreCompanyStoreValidationMiddleware')
                ->patch('/kemaskini/{id}', 'API\V1\LicenseApplication\CompanyStoreController@update')->name('api.license_application.company-store.update');
                Route::patch('/kemaskini-stor-permohonan-lesen/{id}', 'API\V1\LicenseApplication\CompanyStoreController@updateCompanyStore')->name('api.license_application.license-application-company-store.update');
                
                Route::get('/cari-stor-syarikat-berdasarkan-id-stor-syarikat/{id}', 'API\V1\LicenseApplication\CompanyStoreController@findCompanyStoreDetailsByStoreId')->name('api.license_application.company-store.find_company_store_detail_by_store_id');
            });

            Route::prefix('jenis-penjanaan')->group(function () {
                Route::get('/senarai', 'API\V1\LicenseApplication\GenerationTypeController@datatable')->name('api.license_application.generation_type.datatable');
            });

            Route::prefix('bahan-dokumen-lampiran')->group(function () {
                Route::get('/lesen', 'API\V1\LicenseApplication\MaterialController@datatable')->name('api.license_application.material.datatable');
                Route::get('/surat-kelulusan-bersyarat', 'API\V1\ApprovalLetterApplication\MaterialController@datatable')->name('api.approval_letter_application.material.datatable');
            });

            Route::prefix('runcit')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreRetailNewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\NewApplicationController@store')->name('api.license_application.retail.new.store');
                    Route::middleware('DraftRetailNewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\NewApplicationController@draft')->name('api.license_application.retail.new.draft');
                    Route::middleware('SubmitDraftRetailNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\NewApplicationController@submitDraft')->name('api.license_application.retail.draft.submit');
                    Route::middleware('UpdateDraftRetailNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\NewApplicationController@updateDraft')->name('api.license_application.retail.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\NewApplicationController@edit')->name('api.license_application.retail.new.edit');
                });

                Route::prefix('pembaharuan')->group(function () {
                    Route::middleware('StoreNewRetailRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\RenewApplicationController@store')->name('api.license_application.retail.renew.store');
                    Route::middleware('DraftNewRetailRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\RenewApplicationController@draft')->name('api.license_application.retail.renew.draft');
                    Route::middleware('SubmitDraftNewRetailRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\RenewApplicationController@submitDraft')->name('api.license_application.retail.renew.draft.submit');
                    Route::middleware('UpdateDraftNewRetailRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\RenewApplicationController@updateDraft')->name('api.license_application.retail.renew.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\RenewApplicationController@edit')->name('api.license_application.retail.renew.edit');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::middleware('StoreRetailCompanyApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@store')->name('api.license_application.retail.change_company.store');
                        Route::middleware('DraftRetailCompanyApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@draft')->name('api.license_application.retail.change_company.draft');
                        Route::middleware('SubmitDraftRetailCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@submitDraft')->name('api.license_application.retail.change_company.draft.submit');
                        Route::middleware('UpdateDraftRetailCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@updateDraft')->name('api.license_application.retail.change_company.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@edit')->name('api.license_application.retail.change_company.edit');
                    });

                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::middleware('StoreRetailLicenseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@store')->name('api.license_application.retail.change_license_application.store');
                        Route::middleware('DraftRetailLicenseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@draft')->name('api.license_application.retail.change_license_application.draft');
                        Route::middleware('SubmitDraftRetailLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@submitDraft')->name('api.license_application.retail.change_license_application.draft.submit');
                        Route::middleware('UpdateDraftRetailLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@updateDraft')->name('api.license_application.retail.change_license_application.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@edit')->name('api.license_application.retail.change_license_application.edit');
                    });

                    Route::prefix('premis')->group(function () {
                        Route::middleware('StoreRetailPremiseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@store')->name('api.license_application.retail.change_premise.store');
                        Route::middleware('DraftRetailPremiseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@draft')->name('api.license_application.retail.change_premise.draft');
                        Route::middleware('SubmitDraftRetailPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@submitDraft')->name('api.license_application.retail.change_premise.draft.submit');
                        Route::middleware('UpdateDraftRetailPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@updateDraft')->name('api.license_application.retail.change_premise.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@edit')->name('api.license_application.retail.change_premise.edit');
                    });

                    Route::prefix('pembaharuan')->group(function () {
                        Route::middleware('StoreRetailRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\ChangeApplication\RenewApplicationController@store')->name('api.license_application.retail.change_and_renew.store');
                        Route::middleware('DraftRetailRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\ChangeApplication\RenewApplicationController@draft')->name('api.license_application.retail.change_and_renew.draft');
                        Route::middleware('SubmitDraftRetailRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\RenewApplicationController@submitDraft')->name('api.license_application.retail.change_and_renew.draft.submit');
                        Route::middleware('UpdateDraftRetailRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\ChangeApplication\RenewApplicationController@updateDraft')->name('api.license_application.retail.change_and_renew.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\ChangeApplication\RenewApplicationController@edit')->name('api.license_application.retail.change_and_renew.edit');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::middleware('StoreRetailCancellationApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\CancellationApplicationController@store')->name('api.license_application.retail.cancellation.store');
                    Route::middleware('DraftRetailCancellationApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\CancellationApplicationController@draft')->name('api.license_application.retail.cancellation.draft');
                    Route::middleware('SubmitDraftRetailCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\CancellationApplicationController@submitDraft')->name('api.license_application.retail.cancellation.draft.submit');
                    Route::middleware('UpdateDraftRetailCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\CancellationApplicationController@updateDraft')->name('api.license_application.retail.cancellation.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\CancellationApplicationController@edit')->name('api.license_application.retail.cancellation.edit');
                });

                Route::prefix('penggantian')->group(function () {
                    Route::middleware('StoreRetailReplacementApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Retail\ReplacementApplicationController@store')->name('api.license_application.retail.replacement.store');
                    Route::middleware('DraftRetailReplacementApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Retail\ReplacementApplicationController@draft')->name('api.license_application.retail.replacement.draft');
                    Route::middleware('SubmitDraftRetailReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Retail\ReplacementApplicationController@submitDraft')->name('api.license_application.retail.replacement.draft.submit');
                    Route::middleware('UpdateDraftRetailReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Retail\ReplacementApplicationController@updateDraft')->name('api.license_application.retail.replacement.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Retail\ReplacementApplicationController@edit')->name('api.license_application.retail.replacement.edit');
                });
            });

            Route::prefix('borong')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreWholesaleNewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\NewApplicationController@store')->name('api.license_application.wholesale.new.store');
                    Route::middleware('DraftWholesaleNewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\NewApplicationController@draft')->name('api.license_application.wholesale.new.draft');
                    Route::middleware('SubmitDraftWholesaleNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\NewApplicationController@submitDraft')->name('api.license_application.wholesale.draft.submit');
                    Route::middleware('UpdateDraftWholesaleNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\NewApplicationController@updateDraft')->name('api.license_application.wholesale.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\NewApplicationController@edit')->name('api.license_application.wholesale.new.edit');
                });

                Route::prefix('pembaharuan')->group(function () {
                    Route::middleware('StoreNewWholesaleRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\RenewApplicationController@store')->name('api.license_application.wholesale.renew.store');
                    Route::middleware('DraftNewWholesaleRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\RenewApplicationController@draft')->name('api.license_application.wholesale.renew.draft');
                    Route::middleware('SubmitDraftNewWholesaleRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\RenewApplicationController@submitDraft')->name('api.license_application.wholesale.renew.draft.submit');
                    Route::middleware('UpdateDraftNewWholesaleRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\RenewApplicationController@updateDraft')->name('api.license_application.wholesale.renew.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\RenewApplicationController@edit')->name('api.license_application.wholesale.renew.edit');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::middleware('StoreWholesaleCompanyApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@store')->name('api.license_application.wholesale.change_company.store');
                        Route::middleware('DraftWholesaleCompanyApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@draft')->name('api.license_application.wholesale.change_company.draft');
                        Route::middleware('SubmitDraftWholesaleCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@submitDraft')->name('api.license_application.wholesale.change_company.draft.submit');
                        Route::middleware('UpdateDraftWholesaleCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@updateDraft')->name('api.license_application.wholesale.change_company.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@edit')->name('api.license_application.wholesale.change_company.edit');
                    });

                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::middleware('StoreWholesaleLicenseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@store')->name('api.license_application.wholesale.change_license_application.store');
                        Route::middleware('DraftWholesaleLicenseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@draft')->name('api.license_application.wholesale.change_license_application.draft');
                        Route::middleware('SubmitDraftWholesaleLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@submitDraft')->name('api.license_application.wholesale.change_license_application.draft.submit');
                        Route::middleware('UpdateDraftWholesaleLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@updateDraft')->name('api.license_application.wholesale.change_license_application.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@edit')->name('api.license_application.wholesale.change_license_application.edit');
                    });

                    Route::prefix('premis')->group(function () {
                        Route::middleware('StoreWholesalePremiseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@store')->name('api.license_application.wholesale.change_premise.store');
                        Route::middleware('DraftWholesalePremiseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@draft')->name('api.license_application.wholesale.change_premise.draft');
                        Route::middleware('SubmitDraftWholesalePremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@submitDraft')->name('api.license_application.wholesale.change_premise.draft.submit');
                        Route::middleware('UpdateDraftWholesalePremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@updateDraft')->name('api.license_application.wholesale.change_premise.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@edit')->name('api.license_application.wholesale.change_premise.edit');
                    });

                    Route::prefix('stor')->group(function () {
                        Route::middleware('StoreWholesaleStoreApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@store')->name('api.license_application.wholesale.change_store.store');
                        Route::middleware('DraftWholesaleStoreApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@draft')->name('api.license_application.wholesale.change_store.draft');
                        Route::middleware('SubmitDraftWholesaleStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@submitDraft')->name('api.license_application.wholesale.change_store.draft.submit');
                        Route::middleware('UpdateDraftWholesaleStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@updateDraft')->name('api.license_application.wholesale.change_store.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@edit')->name('api.license_application.wholesale.change_store.edit');
                    });

                    Route::prefix('pembaharuan')->group(function () {
                        Route::middleware('StoreWholesaleRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@store')->name('api.license_application.wholesale.change_and_renew.store');
                        Route::middleware('DraftWholesaleRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@draft')->name('api.license_application.wholesale.change_and_renew.draft');
                        Route::middleware('SubmitDraftWholesaleRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@submitDraft')->name('api.license_application.wholesale.change_and_renew.draft.submit');
                        Route::middleware('UpdateDraftWholesaleRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@updateDraft')->name('api.license_application.wholesale.change_and_renew.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@edit')->name('api.license_application.wholesale.change_and_renew.edit');
                    });
                    
                    
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::middleware('StoreWholesaleCancellationApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\CancellationApplicationController@store')->name('api.license_application.wholesale.cancellation.store');
                    Route::middleware('DraftWholesaleCancellationApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\CancellationApplicationController@draft')->name('api.license_application.wholesale.cancellation.draft');
                    Route::middleware('SubmitDraftWholesaleCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\CancellationApplicationController@submitDraft')->name('api.license_application.wholesale.cancellation.draft.submit');
                    Route::middleware('UpdateDraftWholesaleCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\CancellationApplicationController@updateDraft')->name('api.license_application.wholesale.cancellation.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\CancellationApplicationController@edit')->name('api.license_application.wholesale.cancellation.edit');
                });

                Route::prefix('penggantian')->group(function () {
                    Route::middleware('StoreWholesaleReplacementApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Wholesale\ReplacementApplicationController@store')->name('api.license_application.wholesale.replacement.store');
                    Route::middleware('DraftWholesaleReplacementApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Wholesale\ReplacementApplicationController@draft')->name('api.license_application.wholesale.replacement.draft');
                    Route::middleware('SubmitDraftWholesaleReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Wholesale\ReplacementApplicationController@submitDraft')->name('api.license_application.wholesale.replacement.draft.submit');
                    Route::middleware('UpdateDraftWholesaleReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Wholesale\ReplacementApplicationController@updateDraft')->name('api.license_application.wholesale.replacement.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Wholesale\ReplacementApplicationController@edit')->name('api.license_application.wholesale.replacement.edit');
                });
            });

            Route::prefix('import')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreImportNewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\NewApplicationController@store')->name('api.license_application.import.new.store');
                    Route::middleware('DraftImportNewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\NewApplicationController@draft')->name('api.license_application.import.new.draft');
                    Route::middleware('SubmitDraftImportNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\NewApplicationController@submitDraft')->name('api.license_application.import.draft.submit');
                    Route::middleware('UpdateDraftImportNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\NewApplicationController@updateDraft')->name('api.license_application.import.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\NewApplicationController@edit')->name('api.license_application.import.new.edit');
                });

                Route::prefix('pembaharuan')->group(function () {
                    Route::middleware('StoreNewImportRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\RenewApplicationController@store')->name('api.license_application.import.renew.store');
                    Route::middleware('DraftNewImportRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\RenewApplicationController@draft')->name('api.license_application.import.renew.draft');
                    Route::middleware('SubmitDraftNewImportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\RenewApplicationController@submitDraft')->name('api.license_application.import.renew.draft.submit');
                    Route::middleware('UpdateDraftNewImportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\RenewApplicationController@updateDraft')->name('api.license_application.import.renew.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\RenewApplicationController@edit')->name('api.license_application.import.renew.edit');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::middleware('StoreImportCompanyApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\ChangeApplication\CompanyApplicationController@store')->name('api.license_application.import.change_company.store');
                        Route::middleware('DraftImportCompanyApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\ChangeApplication\CompanyApplicationController@draft')->name('api.license_application.import.change_company.draft');
                        Route::middleware('SubmitDraftImportCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\CompanyApplicationController@submitDraft')->name('api.license_application.import.change_company.draft.submit');
                        Route::middleware('UpdateDraftImportCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\CompanyApplicationController@updateDraft')->name('api.license_application.import.change_company.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\ChangeApplication\CompanyApplicationController@edit')->name('api.license_application.import.change_company.edit');
                    });

                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::middleware('StoreImportLicenseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\ChangeApplication\LicenseApplicationController@store')->name('api.license_application.import.change_license_application.store');
                        Route::middleware('DraftImportLicenseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\ChangeApplication\LicenseApplicationController@draft')->name('api.license_application.import.change_license_application.draft');
                        Route::middleware('SubmitDraftImportLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\LicenseApplicationController@submitDraft')->name('api.license_application.import.change_license_application.draft.submit');
                        Route::middleware('UpdateDraftImportLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\LicenseApplicationController@updateDraft')->name('api.license_application.import.change_license_application.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\ChangeApplication\LicenseApplicationController@edit')->name('api.license_application.import.change_license_application.edit');
                    });

                    Route::prefix('premis')->group(function () {
                        Route::middleware('StoreImportPremiseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\ChangeApplication\PremiseApplicationController@store')->name('api.license_application.import.change_premise.store');
                        Route::middleware('DraftImportPremiseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\ChangeApplication\PremiseApplicationController@draft')->name('api.license_application.import.change_premise.draft');
                        Route::middleware('SubmitDraftImportPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\PremiseApplicationController@submitDraft')->name('api.license_application.import.change_premise.draft.submit');
                        Route::middleware('UpdateDraftImportPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\PremiseApplicationController@updateDraft')->name('api.license_application.import.change_premise.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\ChangeApplication\PremiseApplicationController@edit')->name('api.license_application.import.change_premise.edit');
                    });

                    Route::prefix('stor')->group(function () {
                        Route::middleware('StoreImportStoreApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\ChangeApplication\StoreApplicationController@store')->name('api.license_application.import.change_store.store');
                        Route::middleware('DraftImportStoreApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\ChangeApplication\StoreApplicationController@draft')->name('api.license_application.import.change_store.draft');
                        Route::middleware('SubmitDraftImportStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\StoreApplicationController@submitDraft')->name('api.license_application.import.change_store.draft.submit');
                        Route::middleware('UpdateDraftImportStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\StoreApplicationController@updateDraft')->name('api.license_application.import.change_store.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\ChangeApplication\StoreApplicationController@edit')->name('api.license_application.import.change_store.edit');
                    });

                    Route::prefix('pembaharuan')->group(function () {
                        Route::middleware('StoreImportRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\ChangeApplication\RenewApplicationController@store')->name('api.license_application.import.change_and_renew.store');
                        Route::middleware('DraftImportRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\ChangeApplication\RenewApplicationController@draft')->name('api.license_application.import.change_and_renew.draft');
                        Route::middleware('SubmitDraftImportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\RenewApplicationController@submitDraft')->name('api.license_application.import.change_and_renew.draft.submit');
                        Route::middleware('UpdateDraftImportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\ChangeApplication\RenewApplicationController@updateDraft')->name('api.license_application.import.change_and_renew.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\ChangeApplication\RenewApplicationController@edit')->name('api.license_application.import.change_and_renew.edit');
                    });
                    
                    
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::middleware('StoreImportCancellationApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\CancellationApplicationController@store')->name('api.license_application.import.cancellation.store');
                    Route::middleware('DraftImportCancellationApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\CancellationApplicationController@draft')->name('api.license_application.import.cancellation.draft');
                    Route::middleware('SubmitDraftImportCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\CancellationApplicationController@submitDraft')->name('api.license_application.import.cancellation.draft.submit');
                    Route::middleware('UpdateDraftImportCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\CancellationApplicationController@updateDraft')->name('api.license_application.import.cancellation.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\CancellationApplicationController@edit')->name('api.license_application.import.cancellation.edit');
                });

                Route::prefix('penggantian')->group(function () {
                    Route::middleware('StoreImportReplacementApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Import\ReplacementApplicationController@store')->name('api.license_application.import.replacement.store');
                    Route::middleware('DraftImportReplacementApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Import\ReplacementApplicationController@draft')->name('api.license_application.import.replacement.draft');
                    Route::middleware('SubmitDraftImportReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Import\ReplacementApplicationController@submitDraft')->name('api.license_application.import.replacement.draft.submit');
                    Route::middleware('UpdateDraftImportReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Import\ReplacementApplicationController@updateDraft')->name('api.license_application.import.replacement.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Import\ReplacementApplicationController@edit')->name('api.license_application.import.replacement.edit');
                });
            });

            Route::prefix('eksport')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreExportNewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\NewApplicationController@store')->name('api.license_application.export.new.store');
                    Route::middleware('DraftExportNewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\NewApplicationController@draft')->name('api.license_application.export.new.draft');
                    Route::middleware('SubmitDraftExportNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\NewApplicationController@submitDraft')->name('api.license_application.export.draft.submit');
                    Route::middleware('UpdateDraftExportNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\NewApplicationController@updateDraft')->name('api.license_application.export.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\NewApplicationController@edit')->name('api.license_application.export.new.edit');
                });

                Route::prefix('pembaharuan')->group(function () {
                    Route::middleware('StoreNewExportRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\RenewApplicationController@store')->name('api.license_application.export.renew.store');
                    Route::middleware('DraftNewExportRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\RenewApplicationController@draft')->name('api.license_application.export.renew.draft');
                    Route::middleware('SubmitDraftNewExportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\RenewApplicationController@submitDraft')->name('api.license_application.export.renew.draft.submit');
                    Route::middleware('UpdateDraftNewExportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\RenewApplicationController@updateDraft')->name('api.license_application.export.renew.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\RenewApplicationController@edit')->name('api.license_application.export.renew.edit');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::middleware('StoreExportCompanyApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\ChangeApplication\CompanyApplicationController@store')->name('api.license_application.export.change_company.store');
                        Route::middleware('DraftExportCompanyApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\ChangeApplication\CompanyApplicationController@draft')->name('api.license_application.export.change_company.draft');
                        Route::middleware('SubmitDraftExportCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\CompanyApplicationController@submitDraft')->name('api.license_application.export.change_company.draft.submit');
                        Route::middleware('UpdateDraftExportCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\CompanyApplicationController@updateDraft')->name('api.license_application.export.change_company.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\ChangeApplication\CompanyApplicationController@edit')->name('api.license_application.export.change_company.edit');
                    });

                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::middleware('StoreExportLicenseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\ChangeApplication\LicenseApplicationController@store')->name('api.license_application.export.change_license_application.store');
                        Route::middleware('DraftExportLicenseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\ChangeApplication\LicenseApplicationController@draft')->name('api.license_application.export.change_license_application.draft');
                        Route::middleware('SubmitDraftExportLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\LicenseApplicationController@submitDraft')->name('api.license_application.export.change_license_application.draft.submit');
                        Route::middleware('UpdateDraftExportLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\LicenseApplicationController@updateDraft')->name('api.license_application.export.change_license_application.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\ChangeApplication\LicenseApplicationController@edit')->name('api.license_application.export.change_license_application.edit');
                    });

                    Route::prefix('premis')->group(function () {
                        Route::middleware('StoreExportPremiseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\ChangeApplication\PremiseApplicationController@store')->name('api.license_application.export.change_premise.store');
                        Route::middleware('DraftExportPremiseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\ChangeApplication\PremiseApplicationController@draft')->name('api.license_application.export.change_premise.draft');
                        Route::middleware('SubmitDraftExportPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\PremiseApplicationController@submitDraft')->name('api.license_application.export.change_premise.draft.submit');
                        Route::middleware('UpdateDraftExportPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\PremiseApplicationController@updateDraft')->name('api.license_application.export.change_premise.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\ChangeApplication\PremiseApplicationController@edit')->name('api.license_application.export.change_premise.edit');
                    });

                    Route::prefix('stor')->group(function () {
                        Route::middleware('StoreExportStoreApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\ChangeApplication\StoreApplicationController@store')->name('api.license_application.export.change_store.store');
                        Route::middleware('DraftExportStoreApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\ChangeApplication\StoreApplicationController@draft')->name('api.license_application.export.change_store.draft');
                        Route::middleware('SubmitDraftExportStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\StoreApplicationController@submitDraft')->name('api.license_application.export.change_store.draft.submit');
                        Route::middleware('UpdateDraftExportStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\StoreApplicationController@updateDraft')->name('api.license_application.export.change_store.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\ChangeApplication\StoreApplicationController@edit')->name('api.license_application.export.change_store.edit');
                    });

                    Route::prefix('pembaharuan')->group(function () {
                        Route::middleware('StoreExportRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\ChangeApplication\RenewApplicationController@store')->name('api.license_application.export.change_and_renew.store');
                        Route::middleware('DraftExportRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\ChangeApplication\RenewApplicationController@draft')->name('api.license_application.export.change_and_renew.draft');
                        Route::middleware('SubmitDraftExportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\RenewApplicationController@submitDraft')->name('api.license_application.export.change_and_renew.draft.submit');
                        Route::middleware('UpdateDraftExportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\ChangeApplication\RenewApplicationController@updateDraft')->name('api.license_application.export.change_and_renew.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\ChangeApplication\RenewApplicationController@edit')->name('api.license_application.export.change_and_renew.edit');
                    });
                    
                    
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::middleware('StoreExportCancellationApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\CancellationApplicationController@store')->name('api.license_application.export.cancellation.store');
                    Route::middleware('DraftExportCancellationApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\CancellationApplicationController@draft')->name('api.license_application.export.cancellation.draft');
                    Route::middleware('SubmitDraftExportCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\CancellationApplicationController@submitDraft')->name('api.license_application.export.cancellation.draft.submit');
                    Route::middleware('UpdateDraftExportCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\CancellationApplicationController@updateDraft')->name('api.license_application.export.cancellation.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\CancellationApplicationController@edit')->name('api.license_application.export.cancellation.edit');
                });

                Route::prefix('penggantian')->group(function () {
                    Route::middleware('StoreExportReplacementApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\ReplacementApplicationController@store')->name('api.license_application.export.replacement.store');
                    Route::middleware('DraftExportReplacementApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\ReplacementApplicationController@draft')->name('api.license_application.export.replacement.draft');
                    Route::middleware('SubmitDraftExportReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\ReplacementApplicationController@submitDraft')->name('api.license_application.export.replacement.draft.submit');
                    Route::middleware('UpdateDraftExportReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\ReplacementApplicationController@updateDraft')->name('api.license_application.export.replacement.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\ReplacementApplicationController@edit')->name('api.license_application.export.replacement.edit');
                });
            });

            Route::prefix('beli-padi')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreExportNewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\NewApplicationController@store')->name('api.license_application.buy_paddy.new.store');
                    Route::middleware('DraftExportNewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\NewApplicationController@draft')->name('api.license_application.buy_paddy.new.draft');
                    Route::middleware('SubmitDraftExportNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\NewApplicationController@submitDraft')->name('api.license_application.buy_paddy.draft.submit');
                    Route::middleware('UpdateDraftExportNewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\NewApplicationController@updateDraft')->name('api.license_application.buy_paddy.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\NewApplicationController@edit')->name('api.license_application.buy_paddy.new.edit');
                });

                Route::prefix('pembaharuan')->group(function () {
                    Route::middleware('StoreNewExportRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\Export\RenewApplicationController@store')->name('api.license_application.buy_paddy.renew.store');
                    Route::middleware('DraftNewExportRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\Export\RenewApplicationController@draft')->name('api.license_application.buy_paddy.renew.draft');
                    Route::middleware('SubmitDraftNewExportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\Export\RenewApplicationController@submitDraft')->name('api.license_application.buy_paddy.renew.draft.submit');
                    Route::middleware('UpdateDraftNewExportRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\Export\RenewApplicationController@updateDraft')->name('api.license_application.buy_paddy.renew.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\Export\RenewApplicationController@edit')->name('api.license_application.buy_paddy.renew.edit');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::middleware('StoreBuyPaddyCompanyApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@store')->name('api.license_application.buy_paddy.change_company.store');
                        Route::middleware('DraftBuyPaddyCompanyApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@draft')->name('api.license_application.buy_paddy.change_company.draft');
                        Route::middleware('SubmitDraftBuyPaddyCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@submitDraft')->name('api.license_application.buy_paddy.change_company.draft.submit');
                        Route::middleware('UpdateDraftBuyPaddyCompanyApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@updateDraft')->name('api.license_application.buy_paddy.change_company.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@edit')->name('api.license_application.buy_paddy.change_company.edit');
                    });

                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::middleware('StoreBuyPaddyLicenseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@store')->name('api.license_application.buy_paddy.change_license_application.store');
                        Route::middleware('DraftBuyPaddyLicenseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@draft')->name('api.license_application.buy_paddy.change_license_application.draft');
                        Route::middleware('SubmitDraftBuyPaddyLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@submitDraft')->name('api.license_application.buy_paddy.change_license_application.draft.submit');
                        Route::middleware('UpdateDraftBuyPaddyLicenseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@updateDraft')->name('api.license_application.buy_paddy.change_license_application.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@edit')->name('api.license_application.buy_paddy.change_license_application.edit');
                    });

                    Route::prefix('premis')->group(function () {
                        Route::middleware('StoreBuyPaddyPremiseApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@store')->name('api.license_application.buy_paddy.change_premise.store');
                        Route::middleware('DraftBuyPaddyPremiseApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@draft')->name('api.license_application.buy_paddy.change_premise.draft');
                        Route::middleware('SubmitDraftBuyPaddyPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@submitDraft')->name('api.license_application.buy_paddy.change_premise.draft.submit');
                        Route::middleware('UpdateDraftBuyPaddyPremiseApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@updateDraft')->name('api.license_application.buy_paddy.change_premise.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@edit')->name('api.license_application.buy_paddy.change_premise.edit');
                    });

                    Route::prefix('stor')->group(function () {
                        Route::middleware('StoreBuyPaddyStoreApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@store')->name('api.license_application.buy_paddy.change_store.store');
                        Route::middleware('DraftBuyPaddyStoreApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@draft')->name('api.license_application.buy_paddy.change_store.draft');
                        Route::middleware('SubmitDraftBuyPaddyStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@submitDraft')->name('api.license_application.buy_paddy.change_store.draft.submit');
                        Route::middleware('UpdateDraftBuyPaddyStoreApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@updateDraft')->name('api.license_application.buy_paddy.change_store.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@edit')->name('api.license_application.buy_paddy.change_store.edit');
                    });

                    Route::prefix('pembaharuan')->group(function () {
                        Route::middleware('StoreBuyPaddyRenewApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@store')->name('api.license_application.buy_paddy.change_and_renew.store');
                        Route::middleware('DraftBuyPaddyRenewApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@draft')->name('api.license_application.buy_paddy.change_and_renew.draft');
                        Route::middleware('SubmitDraftBuyPaddyRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@submitDraft')->name('api.license_application.buy_paddy.change_and_renew.draft.submit');
                        Route::middleware('UpdateDraftBuyPaddyRenewApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@updateDraft')->name('api.license_application.buy_paddy.change_and_renew.draft.update');
                        Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@edit')->name('api.license_application.buy_paddy.change_and_renew.edit');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::middleware('StoreBuyPaddyCancellationApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\CancellationApplicationController@store')->name('api.license_application.buy_paddy.cancellation.store');
                    Route::middleware('DraftBuyPaddyCancellationApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\CancellationApplicationController@draft')->name('api.license_application.buy_paddy.cancellation.draft');
                    Route::middleware('SubmitDraftBuyPaddyCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\CancellationApplicationController@submitDraft')->name('api.license_application.buy_paddy.cancellation.draft.submit');
                    Route::middleware('UpdateDraftBuyPaddyCancellationApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\CancellationApplicationController@updateDraft')->name('api.license_application.buy_paddy.cancellation.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\CancellationApplicationController@edit')->name('api.license_application.buy_paddy.cancellation.edit');
                });

                Route::prefix('penggantian')->group(function () {
                    Route::middleware('StoreBuyPaddyReplacementApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\LicenseApplication\BuyPaddy\ReplacementApplicationController@store')->name('api.license_application.buy_paddy.replacement.store');
                    Route::middleware('DraftBuyPaddyReplacementApplicationValidationMiddleware')->post('/simpan-sebagai-draf', 'API\V1\LicenseApplication\BuyPaddy\ReplacementApplicationController@draft')->name('api.license_application.buy_paddy.replacement.draft');
                    Route::middleware('SubmitDraftBuyPaddyReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\LicenseApplication\BuyPaddy\ReplacementApplicationController@submitDraft')->name('api.license_application.buy_paddy.replacement.draft.submit');
                    Route::middleware('UpdateDraftBuyPaddyReplacementApplicationValidationMiddleware')->patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\LicenseApplication\BuyPaddy\ReplacementApplicationController@updateDraft')->name('api.license_application.buy_paddy.replacement.draft.update');
                    Route::get('/kemaskini', 'API\V1\LicenseApplication\BuyPaddy\ReplacementApplicationController@edit')->name('api.license_application.buy_paddy.replacement.edit');
                });
            });

            Route::prefix('status')->group(function () {
                Route::get('/datatable', 'API\V1\LicenseApplicationController@datatable')->name('api.license_application.status.datatable');
            });

            Route::prefix('pengurusan-lesen')->group(function () {
                Route::get('/datatable', 'API\V1\LicenseManagementController@datatable')->name('api.license_management.datatable');
                Route::get('/adakah-lesen-borong-wujud', 'API\V1\LicenseManagementController@isWholesaleLicenseExist')->name('api.license_management.is_wholesale_license_exist');
            });
        });
        
        Route::prefix('permohonan-surat-kelulusan')->group(function () {
            Route::prefix('beli-padi')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreNewBuyPaddyApprovalLetterApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\ApprovalLetterApplication\BuyPaddy\NewApplicationController@store')->name('api.approval_letter_application.buy_paddy.new.store');
                    Route::post('/simpan-sebagai-draf', 'API\V1\ApprovalLetterApplication\BuyPaddy\NewApplicationController@draft')->name('api.approval_letter_application.buy_paddy.new.draft');
                    Route::middleware('UpdateNewBuyPaddyApprovalLetterApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\ApprovalLetterApplication\BuyPaddy\NewApplicationController@submitDraft')->name('api.approval_letter_application.buy_paddy.draft.submit');
                    Route::patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\ApprovalLetterApplication\BuyPaddy\NewApplicationController@updateDraft')->name('api.approval_letter_application.buy_paddy.draft.update');
                    Route::get('/kemaskini', 'API\V1\ApprovalLetterApplication\BuyPaddy\NewApplicationController@edit')->name('api.approval_letter_application.buy_paddy.new.edit');
                });
            });

            Route::prefix('kilang-padi')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::middleware('StoreNewFactoryPaddyApprovalLetterApplicationValidationMiddleware')->post('/simpan-dan-hantar', 'API\V1\ApprovalLetterApplication\FactoryPaddy\NewApplicationController@store')->name('api.approval_letter_application.factory_paddy.new.store');
                    Route::post('/simpan-sebagai-draf', 'API\V1\ApprovalLetterApplication\FactoryPaddy\NewApplicationController@draft')->name('api.approval_letter_application.factory_paddy.new.draft');
                    Route::middleware('UpdateNewFactoryPaddyApprovalLetterApplicationValidationMiddleware')->patch('/kemaskini-simpan-dan-hantar/{id}', 'API\V1\ApprovalLetterApplication\FactoryPaddy\NewApplicationController@submitDraft')->name('api.approval_letter_application.factory_paddy.draft.submit');
                    Route::patch('/kemaskini-simpan-sebagai-draf/{id}', 'API\V1\ApprovalLetterApplication\FactoryPaddy\NewApplicationController@updateDraft')->name('api.approval_letter_application.factory_paddy.draft.update');
                    Route::get('/kemaskini', 'API\V1\ApprovalLetterApplication\FactoryPaddy\NewApplicationController@edit')->name('api.approval_letter_application.factory_paddy.new.edit');
                });
            });

            Route::prefix('status')->group(function () {
                Route::get('/datatable', 'API\V1\ApprovalLetterApplicationController@datatable')->name('api.approval_letter_application.status.datatable');
            });

            Route::prefix('pengurusan-surat-kebenaran-bersyarat')->group(function () {
                Route::get('/datatable', 'API\V1\ApprovalLetterManagementController@datatable')->name('api.approval_letter_management.datatable');
                Route::get('/adakah-surat-kelulusan-bersyarat-beli-padi-wujud', 'API\V1\ApprovalLetterManagementController@isBuypaddyApprovalLetterExist')->name('api.approval_letter_management.is_buy_paddy_approval_letter_exist');
            });
        });
    });
//});
