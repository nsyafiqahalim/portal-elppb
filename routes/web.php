<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index')->name('website.index');
Route::get('/soalan-lazim', 'WebsiteController@faq')->name('website.faq');
Route::get('/perutusan-ketua-pengarah', 'WebsiteController@infoChief')->name('website.info.chief');
Route::get('/sejarah-perutusan', 'WebsiteController@infoHistory')->name('website.info.history');
Route::get('/peranan-fungsi', 'WebsiteController@infoRole')->name('website.info.role');
Route::get('/akta-dan-peraturan', 'WebsiteController@infoRules')->name('website.info.rules');
Route::get('/lesen', 'WebsiteController@license')->name('website.license');
Route::get('/permit', 'WebsiteController@permit')->name('website.permit');
Route::get('/galeri', 'WebsiteController@gallery')->name('website.gallery');
Route::get('/manual', 'WebsiteController@manualLicense')->name('website.manual-license');
Route::get('/manual-permit', 'WebsiteController@manualPermit')->name('website.manual-permit');
Route::get('/pengurusan-pengguna', 'WebsiteController@userManual')->name('website.userManual');
Route::get('/pengurusan-maklumat-asas', 'WebsiteController@manualManagement')->name('website.manualManagement');
Route::get('/syarat-syarat-permohonan', 'WebsiteController@conditionApplication')->name('website.condition-application');
Route::get('/tanggungjawab-pelesen', 'WebsiteController@licenseResponsibility')->name('website.license-responsibility');
Route::get('/kadar-fee-lesen', 'WebsiteController@licenseFeeRate')->name('website.license-fee-rate');
Route::get('/aplikasi-mudah-alih', 'WebsiteController@mobileApp')->name('website.mobile-app');
Route::get('/alamat-cawangan', 'WebsiteController@contact')->name('website.contact');
Route::get('/alamat-ibu-pejabat', 'WebsiteController@mainContact')->name('website.main-contact');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

//basic routes
Route::get('/dasar-privasi', 'BasicController@privacy')->name('website.basic.privacy');
Route::get('/terma-syarat', 'BasicController@term')->name('website.basic.term');
Route::get('/dasar-keselamatan', 'BasicController@security')->name('website.basic.security');
Route::get('/penafian', 'BasicController@deny')->name('website.basic.deny');

// Profile
Route::get('/kemaskini-profil', 'ProfileController@updateProfile')->name('profile.info');

// reseting
Route::get('/lupa-id-pengguna', 'WebsiteController@forgotID')->name('forgot.userid');

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('syarikat')->group(function () {
        Route::get('/', 'CompanyController@index')->name('company.index');
        Route::get('/papar', 'CompanyController@view')->name('company.view');
        Route::get('/kemaskini', 'CompanyController@edit')->name('company.edit');

        Route::get('/tambah', 'CompanyController@add')->name('company.add');
    });
});
