<?php

Route::group(['middleware' => 'auth'], function () {

        Route::prefix('penyata-stok')->group(function () {
            Route::get('/', 'CompanyStockController@index')->name('company_stock.index');
            Route::get('/papar', 'CompanyStockController@view')->name('company_stock.view');
            Route::get('/kemaskini', 'CompanyStockController@edit')->name('company_stock.edit');
            Route::get('/tambah', 'CompanyStockController@add')->name('company_stock.add');
        }); 

        Route::prefix('permohonan-lesen')->group(function () {
            Route::get('/', 'LicenseApplication\DashboardController@index')->name('license_application.index');

            Route::prefix('syarikat')->group(function () {
                Route::get('/', 'LicenseApplication\CompanyController@index')->name('license_application.company.index');
                Route::get('/papar', 'LicenseApplication\CompanyController@view')->name('license_application.company.view');
                Route::get('/kemaskini', 'LicenseApplication\CompanyController@edit')->name('license_application.company.edit');
                Route::get('/tambah', 'LicenseApplication\CompanyController@add')->name('license_application.company.add');
            });

            Route::prefix('runcit')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'LicenseApplication\Retail\NewApplicationController@add')->name('license_application.retail.new.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Retail\NewApplicationController@draft')->name('license_application.retail.new.draft');
                });
            
                Route::prefix('pembaharuan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Retail\RenewApplicationController@add')->name('license_application.retail.renew.add');//license_application.renew.add
                    Route::get('/draf/{id}', 'LicenseApplication\Retail\RenewApplicationController@draft')->name('license_application.retail.renew.draft');
                });

                Route::prefix('ganti')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Retail\ReplacementApplicationController@add')->name('license_application.retail.replacement.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Retail\ReplacementApplicationController@draft')->name('license_application.retail.replacement.draft');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@add')->name('license_application.retail.change_company.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Retail\ChangeApplication\CompanyApplicationController@draft')->name('license_application.retail.change_company.draft');
                    });
                    Route::prefix('premis')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@add')->name('license_application.retail.change_premise.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Retail\ChangeApplication\PremiseApplicationController@draft')->name('license_application.retail.change_premise.draft');
                    });
                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@add')->name('license_application.retail.change_license_application.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Retail\ChangeApplication\LicenseApplicationController@draft')->name('license_application.retail.change_license_application.draft');
                    });
                    Route::prefix('pembaharuan')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Retail\ChangeApplication\RenewApplicationController@add')->name('license_application.retail.change_and_renew.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Retail\ChangeApplication\RenewApplicationController@draft')->name('license_application.retail.change_and_renew.draft');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Retail\CancellationApplicationController@add')->name('license_application.retail.cancellation.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Retail\CancellationApplicationController@draft')->name('license_application.retail.cancellation.draft');
                });
            });

            Route::prefix('borong')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'LicenseApplication\Wholesale\NewApplicationController@add')->name('license_application.wholesale.new.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Wholesale\NewApplicationController@draft')->name('license_application.wholesale.new.draft');
                });
            
                Route::prefix('pembaharuan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Wholesale\RenewApplicationController@add')->name('license_application.wholesale.renew.add');//license_application.renew.add
                    Route::get('/draf/{id}', 'LicenseApplication\Wholesale\RenewApplicationController@draft')->name('license_application.wholesale.renew.draft');
                });

                Route::prefix('ganti')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Wholesale\ReplacementApplicationController@add')->name('license_application.wholesale.replacement.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Wholesale\ReplacementApplicationController@draft')->name('license_application.wholesale.replacement.draft');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@add')->name('license_application.wholesale.change_company.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Wholesale\ChangeApplication\CompanyApplicationController@draft')->name('license_application.wholesale.change_company.draft');
                    });
                    Route::prefix('premis')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@add')->name('license_application.wholesale.change_premise.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Wholesale\ChangeApplication\PremiseApplicationController@draft')->name('license_application.wholesale.change_premise.draft');
                    });
                    Route::prefix('stor')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@add')->name('license_application.wholesale.change_store.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Wholesale\ChangeApplication\StoreApplicationController@draft')->name('license_application.wholesale.change_store.draft');
                    });
                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@add')->name('license_application.wholesale.change_license_application.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Wholesale\ChangeApplication\LicenseApplicationController@draft')->name('license_application.wholesale.change_license_application.draft');
                    });
                    Route::prefix('pembaharuan')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@add')->name('license_application.wholesale.change_and_renew.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Wholesale\ChangeApplication\RenewApplicationController@draft')->name('license_application.wholesale.change_and_renew.draft');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Wholesale\CancellationApplicationController@add')->name('license_application.wholesale.cancellation.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Wholesale\CancellationApplicationController@draft')->name('license_application.wholesale.cancellation.draft');
                });
            });

            Route::prefix('import')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'LicenseApplication\Import\NewApplicationController@add')->name('license_application.import.new.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Import\NewApplicationController@draft')->name('license_application.import.new.draft');
                });
            
                Route::prefix('pembaharuan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Import\RenewApplicationController@add')->name('license_application.import.renew.add');//license_application.renew.add
                    Route::get('/draf/{id}', 'LicenseApplication\Import\RenewApplicationController@draft')->name('license_application.import.renew.draft');
                });

                Route::prefix('ganti')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Import\ReplacementApplicationController@add')->name('license_application.import.replacement.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Import\ReplacementApplicationController@draft')->name('license_application.import.replacement.draft');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Import\ChangeApplication\CompanyApplicationController@add')->name('license_application.import.change_company.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Import\ChangeApplication\CompanyApplicationController@draft')->name('license_application.import.change_company.draft');
                    });
                    Route::prefix('premis')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Import\ChangeApplication\PremiseApplicationController@add')->name('license_application.import.change_premise.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Import\ChangeApplication\PremiseApplicationController@draft')->name('license_application.import.change_premise.draft');
                    });
                    Route::prefix('stor')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Import\ChangeApplication\StoreApplicationController@add')->name('license_application.import.change_store.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Import\ChangeApplication\StoreApplicationController@draft')->name('license_application.import.change_store.draft');
                    });
                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Import\ChangeApplication\LicenseApplicationController@add')->name('license_application.import.change_license_application.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Import\ChangeApplication\LicenseApplicationController@draft')->name('license_application.import.change_license_application.draft');
                    });
                    Route::prefix('pembaharuan')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Import\ChangeApplication\RenewApplicationController@add')->name('license_application.import.change_and_renew.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Import\ChangeApplication\RenewApplicationController@draft')->name('license_application.import.change_and_renew.draft');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Import\CancellationApplicationController@add')->name('license_application.import.cancellation.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Import\CancellationApplicationController@draft')->name('license_application.import.cancellation.draft');
                });
            });

            Route::prefix('eksport')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'LicenseApplication\Export\NewApplicationController@add')->name('license_application.export.new.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Export\NewApplicationController@draft')->name('license_application.export.new.draft');
                });
            
                Route::prefix('pembaharuan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Export\RenewApplicationController@add')->name('license_application.export.renew.add');//license_application.renew.add
                    Route::get('/draf/{id}', 'LicenseApplication\Export\RenewApplicationController@draft')->name('license_application.export.renew.draft');
                });

                Route::prefix('ganti')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Export\ReplacementApplicationController@add')->name('license_application.export.replacement.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Export\ReplacementApplicationController@draft')->name('license_application.export.replacement.draft');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Export\ChangeApplication\CompanyApplicationController@add')->name('license_application.export.change_company.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Export\ChangeApplication\CompanyApplicationController@draft')->name('license_application.export.change_company.draft');
                    });
                    Route::prefix('premis')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Export\ChangeApplication\PremiseApplicationController@add')->name('license_application.export.change_premise.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Export\ChangeApplication\PremiseApplicationController@draft')->name('license_application.export.change_premise.draft');
                    });
                    Route::prefix('stor')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Export\ChangeApplication\StoreApplicationController@add')->name('license_application.export.change_store.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Export\ChangeApplication\StoreApplicationController@draft')->name('license_application.export.change_store.draft');
                    });
                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Export\ChangeApplication\LicenseApplicationController@add')->name('license_application.export.change_license_application.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Export\ChangeApplication\LicenseApplicationController@draft')->name('license_application.export.change_license_application.draft');
                    });
                    Route::prefix('pembaharuan')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\Export\ChangeApplication\RenewApplicationController@add')->name('license_application.export.change_and_renew.add');
                        Route::get('/draf/{id}', 'LicenseApplication\Export\ChangeApplication\RenewApplicationController@draft')->name('license_application.export.change_and_renew.draft');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\Export\CancellationApplicationController@add')->name('license_application.export.cancellation.add');
                    Route::get('/draf/{id}', 'LicenseApplication\Export\CancellationApplicationController@draft')->name('license_application.export.cancellation.draft');
                });
            });

            Route::prefix('beli-padi')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'LicenseApplication\BuyPaddy\NewApplicationController@add')->name('license_application.buy_paddy.new.add');
                    Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\NewApplicationController@draft')->name('license_application.buy_paddy.new.draft');
                });
            
                Route::prefix('pembaharuan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\BuyPaddy\RenewApplicationController@add')->name('license_application.buy_paddy.renew.add');//license_application.renew.add
                    Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\RenewApplicationController@draft')->name('license_application.buy_paddy.renew.draft');
                });

                Route::prefix('ganti')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\BuyPaddy\ReplacementApplicationController@add')->name('license_application.buy_paddy.replacement.add');
                    Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\ReplacementApplicationController@draft')->name('license_application.buy_paddy.replacement.draft');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@add')->name('license_application.buy_paddy.change_company.add');
                        Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\CompanyApplicationController@draft')->name('license_application.buy_paddy.change_company.draft');
                    });
                    Route::prefix('premis')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@add')->name('license_application.buy_paddy.change_premise.add');
                        Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\PremiseApplicationController@draft')->name('license_application.buy_paddy.change_premise.draft');
                    });
                    Route::prefix('stor')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@add')->name('license_application.buy_paddy.change_store.add');
                        Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\StoreApplicationController@draft')->name('license_application.buy_paddy.change_store.draft');
                    });
                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@add')->name('license_application.buy_paddy.change_license_application.add');
                        Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\LicenseApplicationController@draft')->name('license_application.buy_paddy.change_license_application.draft');
                    });
                    Route::prefix('pembaharuan')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@add')->name('license_application.buy_paddy.change_and_renew.add');
                        Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\ChangeApplication\RenewApplicationController@draft')->name('license_application.buy_paddy.change_and_renew.draft');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\BuyPaddy\CancellationApplicationController@add')->name('license_application.buy_paddy.cancellation.add');
                    Route::get('/draf/{id}', 'LicenseApplication\BuyPaddy\CancellationApplicationController@draft')->name('license_application.buy_paddy.cancellation.draft');
                });
            });

            Route::prefix('kilang-padi-komersial')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'LicenseApplication\FactoryPaddy\NewApplicationController@add')->name('license_application.factory_paddy.new.add');
                    Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\NewApplicationController@draft')->name('license_application.factory_paddy.new.draft');
                });
            
                Route::prefix('pembaharuan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\FactoryPaddy\RenewApplicationController@add')->name('license_application.factory_paddy.renew.add');//license_application.renew.add
                    Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\RenewApplicationController@draft')->name('license_application.factory_paddy.renew.draft');
                });

                Route::prefix('ganti')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\FactoryPaddy\ReplacementApplicationController@add')->name('license_application.factory_paddy.replacement.add');
                    Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\ReplacementApplicationController@draft')->name('license_application.factory_paddy.replacement.draft');
                });

                Route::prefix('perubahan')->group(function () {
                    Route::prefix('syarikat')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\CompanyApplicationController@add')->name('license_application.factory_paddy.change_company.add');
                        Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\CompanyApplicationController@draft')->name('license_application.factory_paddy.change_company.draft');
                    });
                    Route::prefix('premis')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\PremiseApplicationController@add')->name('license_application.factory_paddy.change_premise.add');
                        Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\PremiseApplicationController@draft')->name('license_application.factory_paddy.change_premise.draft');
                    });
                    Route::prefix('stor')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\StoreApplicationController@add')->name('license_application.factory_paddy.change_store.add');
                        Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\StoreApplicationController@draft')->name('license_application.factory_paddy.change_store.draft');
                    });
                    Route::prefix('permohonan-lesen')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\LicenseApplicationController@add')->name('license_application.factory_paddy.change_license_application.add');
                        Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\LicenseApplicationController@draft')->name('license_application.factory_paddy.change_license_application.draft');
                    });
                    Route::prefix('pembaharuan')->group(function () {
                        Route::get('/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\RenewApplicationController@add')->name('license_application.factory_paddy.change_and_renew.add');
                        Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\ChangeApplication\RenewApplicationController@draft')->name('license_application.factory_paddy.change_and_renew.draft');
                    });
                });

                Route::prefix('pembatalan')->group(function () {
                    Route::get('/{id}', 'LicenseApplication\FactoryPaddy\CancellationApplicationController@add')->name('license_application.factory_paddy.cancellation.add');
                    Route::get('/draf/{id}', 'LicenseApplication\FactoryPaddy\CancellationApplicationController@draft')->name('license_application.factory_paddy.cancellation.draft');
                });
            });


            Route::get('/senarai-permohonan-lesen', 'LicenseApplication\LicenseController@listOfApplication')->name('license_application.list_of_application');
            Route::get('/senarai-permohonan-permit', 'LicenseApplication\LicenseController@listOfPermit')->name('license_application.list_of_permit');
            Route::get('/pengurusan-lesen', 'LicenseManagementController@index')->name('license_application.license_management');
        });

        Route::prefix('permohonan-surat-kelulusan')->group(function () {
            Route::get('/', 'ApprovalLetterApplication\DashboardController@index')->name('license_application.index');
            Route::prefix('beli-padi')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'ApprovalLetterApplication\BuyPaddy\NewApplicationController@add')->name('approval_letter_application.buy_paddy.new.add');
                    Route::get('/draf/{id}', 'ApprovalLetterApplication\BuyPaddy\NewApplicationController@draft')->name('approval_letter_application.buy_paddy.new.draft');
                });
            });

            Route::prefix('kilang-padi-komersial')->group(function () {
                Route::prefix('baharu')->group(function () {
                    Route::get('/', 'ApprovalLetterApplication\FactoryPaddy\NewApplicationController@add')->name('approval_letter_application.factory_paddy.new.add');
                    Route::get('/draf/{id}', 'ApprovalLetterApplication\FactoryPaddy\NewApplicationController@draft')->name('approval_letter_application.factory_paddy.new.draft');
                });
            });

            Route::get('/senarai-permohonan-surat-kelulusan', 'ApprovalLetterApplication\ApprovalLetterController@listOfApprovalLetterApplication')->name('approval_letter_application.list_of_approval_letter_application');
            Route::get('/pengurusan-surat-kebenaran', 'ApprovalLetterManagementController@index')->name('approval_letter_application.approval_letter_management');
        });
    });
